package com.afpa.cda.service;

import java.util.List;

import com.afpa.cda.dto.MetierDto;

public interface IMetierService {
	public List<MetierDto> findAll();
}
