package com.afpa.cda.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.dto.ReponseStatut;
import com.afpa.cda.service.IPersonneService;
import com.afpa.cda.tools.Utils;

@WebServlet(urlPatterns = { "/show.do" })
public class ShowServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private IPersonneService personneService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idParam = request.getParameter("id");

		if (idParam == null || idParam.length() == 0) {
			Utils.forward(
					request,
					response,
					getServletContext(),
					"/list.do",
					ReponseStatut.KO,
					"le parametre id est obligatoire");
			return;
		} else {
			int id = 0;
			try {
				id = Integer.parseInt(idParam);

				Optional<PersonneDto> res = this.personneService.findById(id);

				if(res.isPresent()) {
					PersonneDto personne = res.get();
					String d = personne.getDateNaissance()==null?"":new SimpleDateFormat("yyyy/MM/dd").format(personne.getDateNaissance());
					request.setAttribute("maPersonne", personne);
					request.setAttribute("maDate", d);
					request.setAttribute("monMetier", personne.getMetier()==null?"":personne.getMetier().getLabel());
					request.setAttribute("monAdresse", personne.getAdresse()==null?"":personne.getAdresse());
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/show.jsp");
					dispatcher.forward(request, response);
					
				
				} else {
					Utils.forward(
							request,
							response,
							getServletContext(),
							"/list.do",
							ReponseStatut.KO,
							"aucune personne n'a cet id "+id);
					return;
				}
			} catch(NumberFormatException e) {
				Utils.forward(
						request,
						response,
						getServletContext(),
						"/list.do",
						ReponseStatut.KO,
						"le parametre id doit �tre numerique "+id);
				return;
			}
		}
	}

}
