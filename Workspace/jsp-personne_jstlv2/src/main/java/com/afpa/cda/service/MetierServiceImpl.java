package com.afpa.cda.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.MetierRepository;
import com.afpa.cda.dto.MetierDto;

@Service
public class MetierServiceImpl implements IMetierService {

	@Autowired
	private MetierRepository metierRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public List<MetierDto> findAll() {
		return this.metierRepository
				.findAll()
				.stream()
				.map(e->this.modelMapper.map(e, MetierDto.class))
				.collect(Collectors.toList());
	}

}
