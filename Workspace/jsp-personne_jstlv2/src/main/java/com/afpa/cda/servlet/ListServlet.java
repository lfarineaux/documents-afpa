package com.afpa.cda.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.dto.ReponseDto;
import com.afpa.cda.service.IPersonneService;

@WebServlet(urlPatterns = {"/index.html","/list.do"})
public class ListServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private IPersonneService personneService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config); 
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		PrintWriter writer = response.getWriter();
		String pageStr= request.getParameter("page");
		int page = 0;
		if(pageStr == null || pageStr.length() == 0) {
			
		}else {
			page = Integer.parseInt(pageStr.trim());
		}
		ReponseDto reponse = (ReponseDto)request.getAttribute("reponse");
		
		String nextJSP = "/WEB-INF/list.jsp";
		
		List <PersonneDto> malist = this.personneService.chercherToutesLesPersonnes(page);
		
		request.setAttribute("listPers",malist);
		request.setAttribute("reponse",reponse);
		request.setAttribute("pageN",page);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(request, response);
		
	}
	
	
	
	
	
	
}
