package com.afpa.cda.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.MetierDto;
import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.dto.ReponseDto;
import com.afpa.cda.dto.ReponseStatut;
import com.afpa.cda.service.IMetierService;
import com.afpa.cda.service.IPersonneService;
import com.afpa.cda.tools.Utils;

@WebServlet(urlPatterns = { "/add.do" })
public class AddServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private IPersonneService personneService;

	private IMetierService metierService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		this.personneService = ctx.getBean(IPersonneService.class);
		this.metierService = ctx.getBean(IMetierService.class);;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		PrintWriter writer = response.getWriter();
		ReponseDto reponse = (ReponseDto)request.getAttribute("reponse");

		List<MetierDto> metierListe = this.metierService.findAll();
		request.setAttribute("metierListe", metierListe);
		
		
		if(reponse != null) {
			String typeMsg= reponse.getStatus()==ReponseStatut.OK?"primary":"danger";
			request.setAttribute("typeMsg", typeMsg);
			request.setAttribute("reponse", reponse.getMsg());
		}

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/add.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nomPers = request.getParameter("nomPers");
		String prenomPers = request.getParameter("prenomPers");
		String metierPers = request.getParameter("metierPers");
		String adressePers = request.getParameter("adressePers");
		String dateNaissancePers = request.getParameter("dnPers");

		String msg = "";
		ReponseStatut statut = ReponseStatut.OK;

		Integer idMetier = null;
		if(nomPers == null || nomPers.length() == 0
				|| prenomPers == null || prenomPers.length() == 0
				|| adressePers == null || adressePers.length() == 0
				|| metierPers == null || metierPers.length() == 0 
				|| dateNaissancePers == null || dateNaissancePers.length() == 0) {
			msg = "Il manque des informations obligatoires";
			statut = ReponseStatut.KO;
		} else {
			try {
				idMetier = Integer.parseInt(metierPers);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Integer id = this.personneService.ajouter(
						PersonneDto.builder()
						.nom(nomPers)
						.prenom(prenomPers)
						.metier(MetierDto.builder().id(idMetier).build())
						.adresse(adressePers)
						.dateNaissance(sdf.parse(dateNaissancePers))
						.build());
				msg = "Personne ajoutee avec l'id "+id;
			} catch(NumberFormatException e) {
				msg = "Valeur id metier non numerique "+metierPers;
				statut = ReponseStatut.KO;
			} catch (ParseException e) {
				msg = e.getMessage();
				statut = ReponseStatut.KO;
			}
		}
		Utils.forward(
				new HttpServletRequestWrapper(request) {
					@Override
					public String getMethod() {
						return "GET";
					}
				},
				response,
				getServletContext(),
				(statut==ReponseStatut.OK)?"/list.do":"/add.do",
						statut,	msg);

	}
}
