package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;

import com.afpa.cda.dto.PersonneDto;

public interface IPersonneService {
	
	public boolean deleteById(int id);

	public Optional<PersonneDto> findById(int id);

	public Integer ajouter(PersonneDto build);

	List<PersonneDto> chercherToutesLesPersonnes(int page);
}
