<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.service.PersonneServiceImpl"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.cda.dto.*"%>



<!DOCTYPE html>

<html>
<head>
<link href="css/style.css" rel="stylesheet">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>

<body style="background-color: #eaf2f8; font-family: Tw Cen MT">
	<header>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<a href="#" class="navbar-brand d-flex align-items-center"> <strong>GESTION
						DU PERSONNEL - Ajouter une personne</strong>
				</a>
			</div>
		</div>
	</header>
	<%
		List<MetierDto> metierListe = (List<MetierDto>) request.getAttribute("metierListe");
	%>

	<div
		class="alert alert-${typeMsg} alert-dismissible fade show"
		role="alert">
		<strong>
 	${!reponse ?reponse:false}

		</strong>
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col-lg-8">
				<form id="add-form"
					style="border: solid 1px Gray; background-color: #e5e7e9"
					action="add.do" method="post">
					<div class="form-group">
						<label for="nomPers" class="col col-form-label">Nom</label>
						<div class="col-sm-12">
							<input type="text" class="form-control-plaintext" name="nomPers"
								id="nomPers" value="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="prenomPers" class="col col-form-label">Prenom</label>
						<div class="col-sm-12">
							<input type="text" class="form-control-plaintext"
								name="prenomPers" id="prenomPers" required>
						</div>
					</div>
					<div class="form-group">
						<label for="dnPers" class="col col-form-label">Date de
							naissance</label>
						<div class="col-sm-12">
							<input type="date" max="2010-01-31" min="1900-01-01"
								class="form-control-plaintext" name="dnPers" id="dnPers"
								required>
						</div>
					</div>
					<div class="form-group">
						<label for="metierPers" class="col col-form-label">Metier</label>
						<div class="col-sm-12">
							<select class="form-control-plaintext" name="metierPers"
								id="metierPers">
								<%
									for (MetierDto m : metierListe) {
								%>
								<option value="<%=m.getId()%>"><%=m.getLabel()%></option>
								<%
									}
								%>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="adressePers" class="col col-form-label">Adresse</label>
						<div class="col-sm-12">
							<input type="text" class="form-control-plaintext"
								name="adressePers" id="adressePers" required>
						</div>
					</div>
					<div class="col-sm">
						<input class="btn btn-secondary mb-3" value="valider"
							type="submit" id="submit-btn" style="display: none">
					</div>
				</form>
				<br>
				<div class="container">
					<div class="row">
						<div class="col-sm">
							<a class="btn btn-secondary mb-3" href="list.do" role="button">Annuler</a>
						</div>
						<div class="col-sm"></div>
						<div class="col-sm"></div>
						<div class="col-sm"></div>
						<div class="col-sm"></div>
						<div class="col-sm"></div>
						<div class="col-sm">
							<a class="btn btn-secondary mb-3" href="#" id="click-btn"
								role="button">Valider</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="jquery/jquery-3.3.1.slim.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>
