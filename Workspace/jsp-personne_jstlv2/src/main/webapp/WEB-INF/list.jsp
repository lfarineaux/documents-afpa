<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.service.PersonneServiceImpl"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.cda.dto.*"%>



<!DOCTYPE html>

<html>
<head>
<link href="css/style.css" rel="stylesheet">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>

<body style="background-color: #eaf2f8; font-family: Tw Cen MT">
	<header>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<a href="#" class="navbar-brand d-flex align-items-center"> <strong>GESTION
						DU PERSONNEL - Liste des personnes</strong>
				</a>
			</div>
		</div>
	</header>
	<%
		List<PersonneDto> maListDto = (List<PersonneDto>) request.getAttribute("listPers");
	%>
	<%
		String idStr = "";
	%>
	<%
		String pagee = "" + request.getAttribute("pageN");
		int pageN = Integer.parseInt(pagee);
		ReponseDto marep = (ReponseDto) request.getAttribute("reponse");
	%>
<%	 
		if (request.getAttribute("reponse") != null) {
 			String typeMsg = marep.getStatus() == ReponseStatut.OK ? "success" : "danger";
%>
	
	<%-- ${!reponse? ${String typeMsg = reponse.getStatus() == ReponseStatut.OK ? "success" : "danger";} --%>
	<div class="alert alert-" +typeMsg+" alert-dismissible fade
		show" role="alert">
		<strong>${reponse}</strong>
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<%
		}
	%>
	
	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col col-lg-8 mt-1">
				<a class="btn btn-secondary" href="add.do" role="button"> <span
					class="badge badge-light"><i class="fas fa-user-plus"></i></span></a>
			</div>
			<div class="col col-lg-8 mt-1">
				<table id="data-table" class="table table-hover"
					style="background-color: #e5e7e9; border: solid 1px Gray">
					<thead class="thead-dark">
						<tr>
							<th>Id</th>
							<th>Nom</th>
							<th>Prenom</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<%
							int cont = 1;
							for (PersonneDto p : maListDto) {
						%>
						<%
							idStr = "" + p.getId();
						%>

						<tr id="tr<%=cont%>">
							<td><a href="show.do?id=<%=idStr.trim()%>" id="supId"
								value="<%=idStr.trim()%>"><%=idStr%></a></td>
							<td><%=p.getNom()%></td>
							<td><%=p.getPrenom()%></td>
							<td><button type="button" class="fas fa-trash-alt"
									data-toggle="modal" data-target="#myModal<%=cont%>"></button></td>
						</tr>
						<div id="myModal<%=cont%>" class="modal fade" role="dialog">
							<div class="modal-dialog">
								<div class="modal-content text-center">
									<div class="modal-header" style="background-color: black;color: white">
										<br>
										<h4 class="modal-title">Confirmer la Suppression</h4>
										<br>
										<button id="close<%=cont%>" type="button"
											class="close text-right" style="color: white" data-dismiss="modal">&times;</button>
									</div>
									<div class="modal-body" style="background-color: #e5e7e9">
										<input type="text" value="<%=idStr.trim()%>"
											style="display: none" id="inpSup<%=cont%>"> </input> <a
											class="btn btn-secondary rounded" href="#" id="supC<%=cont%>"
											data-dismiss="modal" role="button">Oui</a><br> <br>
										<p>
											<button type="button" class="btn btn-secondary"
												data-dismiss="modal">Non</button>
										</p>
									</div>
								</div>
							</div>
						</div>
						<%
							cont++;
							};
						%>
					
				</table>
				<center>
					<p>
						<i>Cliquer sur une ligne pour voir le detail</i>
					</p>
				</center>

				<%
					if (pageN > 0) {
				%>
				<p style="float: left; width: 100px;">
					<a href="list.do?page=<%=(pageN - 1)%>">Precedent</a>
				</p>
				<%
					}
				%>
				<%
					if (PersonneServiceImpl.fin == 5) {
				%>
				<p style="float: right; width: 100px;">
					<a href="list.do?page=<%=(pageN + 1)%>">Suivant</a>
				</p>
				<%
					}
				%>
				<br> <br>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>
</body>
</html>
