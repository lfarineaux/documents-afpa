package com.afpa.cda.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = { "/msg.html" })
public class MsgServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String msg1="coucou msg de servlet methode get";

		request.setAttribute("message1",msg1);
		RequestDispatcher dispatcher;

		dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/vue.jsp");
		dispatcher.forward(request, response);			

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String msg2="coucou msg de servlet methode post";

		request.setAttribute("message2",msg2);
		RequestDispatcher dispatcher;
		dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/vue.jsp");
		dispatcher.forward(request, response);		


	}
}