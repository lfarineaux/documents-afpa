package com.afpa.cda.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = { "/calcul.html" })
public class CalcServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher;

		dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/calcul.jsp");
		dispatcher.forward(request, response);			

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String nbreStr = request.getParameter("param1");
		String nbre2Str = request.getParameter("param2");
		String signe = request.getParameter("signe");
		String message="Calcul Ok";
		double total=0;
		
		if (nbreStr.equals("")||nbreStr.length()==0||nbre2Str.equals("")||nbre2Str.length()==0) {
			message = "Erreur - Information manquante";
			System.out.println(message);
		} else {
			if (nbreStr.matches("[-+]?\\d*\\.?\\d+") && nbre2Str.matches("[-+]?\\d*\\.?\\d+")) {

				int nbre = Integer.parseInt(nbreStr);

				int nbre2 = Integer.parseInt(nbre2Str);

			
				
				if("+".equals(signe)) {
					total = (nbre+nbre2);
				}else if("*".equals(signe)) {
					total =(nbre*nbre2);
				}else if("/".equals(signe)) {
					if(nbre2 != 0){
						total =(double)nbre/(double)nbre2;
					}else {
						message= "Erreur - Division par 0";
						System.out.println(message);
					}
				}else if(signe == null) {
					message= "Signe absent";
				}else {
					message= "Erreur - Mauvais signes";
					System.out.println(message);
				}
			}
			else {
				message= "Erreur - Saisir un chiffre";
				System.out.println(message);

			}
		}
		
		request.setAttribute("Nbre1",nbreStr);	
		request.setAttribute("Nbre2",nbre2Str);
		request.setAttribute("Signe",signe);
		request.setAttribute("Message",message);
		request.setAttribute("Total",total);
		RequestDispatcher dispatcher;
		dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/calcul.jsp");
		dispatcher.forward(request, response);		
	}
}