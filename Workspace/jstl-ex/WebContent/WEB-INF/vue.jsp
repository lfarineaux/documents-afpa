<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c" %>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSTL</title>
</head>

<body>

<h1><c:out value="${message1}" default = "message absent"/></h1>


<form method="post" action="">
<p>Formulaire</p> 
<h1><c:out value="${message2}" default = "message absent"/></h1>
<p><input type="text" name="res" value="${message2}" default = "message absent" readonly>
<input type="submit"/>
</form>


<form method="post" action="pers.html">
<p>Formulaire 2</p> 
<h1><c:out value="${message2}" default = "message absent"/></h1>
<p><input type="text" name="res" value="${message2}" default = "message absent" readonly>
<input type="submit"/>
</form>



</body>

</html>