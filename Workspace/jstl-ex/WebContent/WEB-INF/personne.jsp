<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.cda.dto.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Personnes</title>
</head>


<table>
	<tr>
		<th>Nom</th>
		<th>Prenom</th>
		<th>Adresse</th>
	</tr>

	<c:forEach items="${ liste }" var="pers" varStatus="ligne">

		<%-- <c:if test="${ligne.count % 2 == 0}">  --%>
		<!-- <tr style="color:red"> -->
		<%-- </c:if> --%>

		<%-- <c:if test="${ligne.count % 2 != 0}">  --%>
		<!-- <tr style="color:green"> -->
		<%-- </c:if> --%>

		<c:choose>
			<c:when test="${ligne.count % 2 == 0}"><tr style="color: red">
			</c:when>
			<c:otherwise>
				<tr style="color: green">
			</c:otherwise>

		</c:choose>

		<td><c:out value="${ pers['nom']  }" /></td>
		<td><c:out value="${ pers['prenom']  }" /></td>

		</tr>

	</c:forEach>

</table>

</body>
</html>