package org.eclipse.config;




import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Multiplication extends HttpServlet {
	
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException {
		System.out.println("hello");
		response.setContentType("text/html");
		// indiquer l’encodage UTF-8 pour ́eviter lesproblemes avec les accents
		
		response.setCharacterEncoding("UTF-8");
		PrintWriter out=response.getWriter();
		out.print("<h1>hello World</h1>");
		
		
	}
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException {
		String nb1 = request.getParameter("param1");
		String nb2 = request.getParameter("param2");
		Integer total=Integer.parseInt(nb1)*Integer.parseInt(nb2);
		String reponse=total.toString();
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out=response.getWriter();
		out.print("<h1>"+reponse+"</h1>");
		out.print("<br>");
		out.print("<a href=\"http://localhost:8090/test-web1/calculatrice.html\"><button type=\"button\">retour</button></a>");
		
	}

}
