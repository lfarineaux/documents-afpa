package com.afpa.service;

import com.afpa.dto.reponse.ReponseDto;

public interface IEmpService {

	ReponseDto creerEmp(String nom, String prenom);

	ReponseDto listerEmpParNom();


}
