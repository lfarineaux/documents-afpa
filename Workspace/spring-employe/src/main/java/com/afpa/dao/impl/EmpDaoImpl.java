package com.afpa.dao.impl;

import java.util.Collection;

import com.afpa.dao.IEmpDao;
import com.afpa.entity.Emp;

public class EmpDaoImpl extends AbstractDao<Emp> implements IEmpDao {
	
	@Override
	public Collection<Emp> listerEmpParNom() {
		return this.findAllByNamedQuery("listAllNameAsc");
	}
	
}
