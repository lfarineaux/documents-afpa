package com.afpa.dao;

import java.util.Collection;

import com.afpa.entity.Emp;

public interface IEmpDao extends IDao<Emp> {

	public Collection<Emp> listerEmpParNom();

}
