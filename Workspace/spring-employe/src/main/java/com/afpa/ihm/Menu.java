package com.afpa.ihm;

import java.util.Scanner;

import com.afpa.dto.reponse.ReponseDto;
import com.afpa.service.IEmpService;
import com.afpa.service.impl.EmpServiceImpl;


public class Menu {

	static IEmpService empService = new EmpServiceImpl (); 
	
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		String menu = "";
		boolean fin = true;

		/**
		 * @param menu : numéro saisi dans le menu fin : arrêt du programme
		 */

		while (fin) {
			System.out.println();
			System.out.println("      **************");
			System.out.println("      *    MENU    *");
			System.out.println("      **************");
			System.out.println();
			System.out.println("0- Arreter le programme");
			System.out.println("1- Ajouter un employe");
			System.out.println("2- Lister les employes par ordre croissant du nom");
			System.out.print("-> ");
			menu = Clavier.lireTxt(sc, "");

			switch (menu) {
			case "0":
				System.out.println("0 - Arret du programme");
				fin = false;
				break;
			case "1":
				System.out.println("1- Ajouter un employe");
				System.out.print("Ajouter un nom > ");
				String nom = Clavier.lireTxt(sc, "");
				System.out.print("Ajouter un prenom > ");
				String prenom = Clavier.lireTxt(sc, "");
				
				ReponseDto creer = empService.creerEmp(nom,prenom);
				
				
				break;
			case "2":
				System.out.println("2- Lister les employes par ordre croissant du nom");
				ReponseDto lister = empService.listerEmpParNom();
				
				
				break;
				
			default:
				System.out.println("Mauvaise saisie");
				continue;
				
			}
		}
		sc.close();
	}
}
