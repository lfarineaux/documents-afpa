package com.afpa.dao;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.afpa.entity.Person;

public class PersonneDao extends AbstractDao<Person> implements IPersonDao {

	@Override
	public Collection<Person> findAll() {
		return this.findAllByNamedQuery("findAll");
	}

	
	public Person findPersonByName(String nomPersonne) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Person> q = em.createNamedQuery("findByName", Person.class);
			q.setParameter("laurentParamName", nomPersonne);
			return q.getSingleResult();
		} finally {
			closeEntityManager(em);
		}
	}
	
}
