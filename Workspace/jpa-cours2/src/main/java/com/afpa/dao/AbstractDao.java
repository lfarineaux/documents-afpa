package com.afpa.dao;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

abstract class AbstractDao<T> implements IDao<T> {
	private EntityManagerFactory factory = null;

	private final Class<T> coucouClass;

	@SuppressWarnings("unchecked")
	public AbstractDao() {
		this.coucouClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		this.factory = Persistence.createEntityManagerFactory("myBase");
	}

	public void close() {
		if (this.factory != null) {
			this.factory.close();
		}
	}

	// Creer un EM et ouvrir une transaction
	public EntityManager newEntityManager() {
		EntityManager em = this.factory.createEntityManager();
		em.getTransaction().begin();
		return (em);
	}

	// Fermer un EM et defaire la transaction si necessaire
	public void closeEntityManager(EntityManager em) {
		if (em != null) {
			if (em.isOpen()) {
				EntityTransaction t = em.getTransaction();
				if (t.isActive()) {
					try {
						t.rollback();
					} catch (PersistenceException e) {
						e.printStackTrace();
					}
				}
				em.close();
			}
		}
	}

	public T find(Object id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			return em.find(this.coucouClass, id);
		} finally {
			closeEntityManager(em);
		}
	}

	public Collection<T> findAllByNamedQuery(String query) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<T> q = em.createNamedQuery(query, this.coucouClass);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}

	public T add(T entity) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			em.persist(entity);
			em.getTransaction().commit();
			return (entity);
		} finally {
			closeEntityManager(em);
		}
	}

	public T update(T entity) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			entity = em.merge(entity);
			em.getTransaction().commit();
		} finally {
			closeEntityManager(em);
		}
		return entity;
	}

	public void remove(Object pk) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			T entity = em.find(this.coucouClass, pk);
			if (entity != null) {
				em.remove(entity);
			}
			em.getTransaction().commit();
		} finally {
			closeEntityManager(em);
		}
	}
}
