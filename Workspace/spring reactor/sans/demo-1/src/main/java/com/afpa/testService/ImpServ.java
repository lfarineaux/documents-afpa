package com.afpa.testService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.dao.RqstDao;
import com.afpa.testEntity.Employe;


@RestController
@RequestMapping("/test")
public class ImpServ {
	
	@Autowired
	RqstDao monDao;

	@GetMapping
	public List<Employe> test() {
		monDao.findAll().stream().forEach(System.out::println);
		System.out.println(monDao.findAll());
		return monDao.findAll();
	}

}
