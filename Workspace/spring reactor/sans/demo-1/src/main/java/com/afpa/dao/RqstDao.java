package com.afpa.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.testEntity.Employe;


@Repository
public interface RqstDao extends CrudRepository<Employe, Long>{
	
	@Query("Select p from Employe p")
	List<Employe> findAll();

}
