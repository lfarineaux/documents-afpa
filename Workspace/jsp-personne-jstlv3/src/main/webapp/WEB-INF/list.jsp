<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.service.PersonneServiceImpl"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.cda.dto.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>

<html>
<head>
<link href="css/style.css" rel="stylesheet">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>

<body style="background-color: #eaf2f8; font-family: Tw Cen MT">
	<header>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<a href="#" class="navbar-brand d-flex align-items-center"> <strong>GESTION
						DU PERSONNEL - Liste des personnes</strong>
				</a>
			</div>
		</div>
	</header>

	<c:if test="${reponse !=null}">
		<c:if test="${reponse.status == ReponseStatut.OK}">
			<c:set var="typeMsg" value="success" />
		</c:if>
		<c:if test="${reponse.status == ReponseStatut.KO}">
			<c:set var="typeMsg" value="danger" />
		</c:if>


		<div class="alert alert-${typeMsg} alert-dismissible fade show"
			role="alert">
			<strong>${reponse.msg}</strong>
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	</c:if>

	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col col-lg-8 mt-1">
				<a class="btn btn-secondary" href="add.do" role="button"> <span
					class="badge badge-light"><i class="fas fa-user-plus"></i></span></a>
			</div>
			<div class="col col-lg-8 mt-1">
				<div id="tableau">
					<table id="data-table" class="table table-hover"
						style="background-color: #e5e7e9; border: solid 1px Gray;">
						<thead class="thead-dark">
							<tr>
								<th>Id</th>
								<th>Nom</th>
								<th>Prenom</th>
								<th></th>
							</tr>
						</thead>
						<tbody>

							<c:set var="cont" value="1" />
							<c:forEach items="${listPers}" var="p">
								<tr id="tr${cont}">
									<td><a href="show.do?id=${p.id}" id="supId">${p.id}</a></td>
									<td>${p.nom}</td>
									<td>${p.prenom}</td>
									<td><button type="button" class="fas fa-trash-alt"
											data-toggle="modal" data-target="#myModal${cont}"></button></td>
								</tr>
								<div id="myModal${cont}" class="modal fade" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content text-center">
											<div class="modal-header"
												style="background-color: black; color: white">
												<br>
												<h4 class="modal-title">Confirmer la Suppression</h4>
												<br>
												<button id="close${cont}" type="button"
													class="close text-right" style="color: white"
													data-dismiss="modal">&times;</button>
											</div>
											<div class="modal-body" style="background-color: #e5e7e9">
												<input type="text" value="${p.id}" style="display: none"
													id="inpSup${cont}"> <a
													class="btn btn-secondary rounded" href="#" id="supC${cont}"
													data-dismiss="modal" role="button">Oui</a> <br> <br>
												<p>
													<button type="button" class="btn btn-secondary"
														data-dismiss="modal">Non</button>
												</p>
											</div>
										</div>
									</div>
								</div>
								<c:set var="cont" value="${ cont + 1 }" />

							</c:forEach>
					</table>
					<center>
						<p>
							<i>Cliquer sur une ligne pour voir le detail</i>
						</p>
					</center>

					<c:if test="${pageN > 0}">

						<p style="float: left; width: 100px;">
							<a href="list.do?page=${pageN-1}">Precedent</a>
						</p>

					</c:if>


					<c:if test="${PersonneServiceImpl.fin==5}">

						<p style="float: right; width: 100px;">
							<a href="list.do?page=${pageN+1}">Suivant</a>
						</p>
					</c:if>
					<br> <br>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>
