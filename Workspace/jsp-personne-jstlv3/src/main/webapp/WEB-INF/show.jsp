<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.service.PersonneServiceImpl"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.cda.dto.*"%>



<!DOCTYPE html>

<html>
<head>
<link href="css/style.css" rel="stylesheet">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>

<!-- <body> -->
<!-- <html> -->
<!-- <head> -->
<!-- <link href="css/style.css" rel="stylesheet"> -->
<!-- <link href="fontawesome/css/all.min.css" rel="stylesheet"> -->
<!-- <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
<!-- </head> -->
<body style="background-color: #eaf2f8  ;font-family:Tw Cen MT">
<header>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<a href="#" class="navbar-brand d-flex align-items-center"> <strong>GESTION
						DU PERSONNEL - Liste par personne</strong>
				</a>
			</div>
		</div>
	</header>
	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col col-lg-8">

				<form style="border:solid 1px Gray;background-color:#e5e7e9; border-top-left-radius:15px;border-top-right-radius:15px;border-bottom-left-radius:15px;
    border-bottom-right-radius:15px;">
					<div class="form-group">
						<label for="idPers" class="col col-form-label"><b>Identifiant</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="idPers" value="${maPersonne.getId()}">
						</div>
					</div>
					<div class="form-group">
						<label for="nomPers" class="col col-form-label"><b>Nom</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="nomPers" value="${maPersonne.getNom()}">
						</div>
					</div>
					<div class="form-group">
						<label for="prenomPers" class="col col-form-label"><b>Prenom</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="prenomPers" value="${maPersonne.getPrenom()}">
						</div>
					</div>
					<div class="form-group">
						<label for="dnPers" class="col col-form-label"><b>Date de
							naissance</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="dnPers" value="${maDate}">
						</div>
					</div>
					<div class="form-group">
						<label for="metierPers" class="col col-form-label"><b>Métier</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="metierPers" value="${monMetier}">
						</div>
					</div>
					<div class="form-group">
						<label for="adrPers" class="col col-form-label"><b>Adresse</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="adrPers" value="${monAdresse}">
						</div>
					</div>
				</form>
			</div>
		</div>
		<br>
		<div class="row justify-content-md-center">
			<div class="col  col-lg-8">
				<center><a class="btn btn-secondary" href="list.do" role="button">Retour
					vers la liste</a></center>
	</div>
</div>
</div>

	
<script src="jquery/jquery-3.3.1.slim.min.js"></script>
<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>
