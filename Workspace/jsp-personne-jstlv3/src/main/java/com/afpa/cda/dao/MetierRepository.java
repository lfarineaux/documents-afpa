package com.afpa.cda.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Metier;

@Repository
public interface MetierRepository extends CrudRepository<Metier, Integer> {
	@Query("select m from Metier m")
	public List<Metier> findAll();
}
