package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.PersonneRepository;
import com.afpa.cda.dto.MetierDto;
import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.entity.Personne;

@Service
public class PersonneServiceImpl implements IPersonneService {

	@Autowired
	private PersonneRepository personneRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public static final int INC=5;
	public static int fin ; 
	public static int nbPage;
	@Override
	public List<PersonneDto> chercherToutesLesPersonnes(int page) {

		Pageable pageable = PageRequest.of(page, INC);
		nbPage = this.personneRepository.findAll(pageable).getTotalPages();

		List<PersonneDto> maliste = this.personneRepository.findAll(pageable)
				.stream()
				.map(e->PersonneDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.prenom(e.getPrenom())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	
	@Override
	public boolean deleteById(int id) {
		if(this.personneRepository.existsById(id)) {
			this.personneRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public Optional<PersonneDto> findById(int id) {
		Optional<Personne> pers = this.personneRepository.findById(id);
		Optional<PersonneDto> res = Optional.empty();
		if(pers.isPresent()) {
			Personne p = pers.get();
			PersonneDto persDto = this.modelMapper.map(p, PersonneDto.class);
			if(p.getMetier() != null) {
				MetierDto metDto = this.modelMapper.map(p.getMetier(),MetierDto.class);
				persDto.setMetier(metDto);
			}
			res = Optional.of(persDto);
		}
		return res;
	}

	@Override
	public Integer ajouter(PersonneDto pers) {
		Personne p = this.modelMapper.map(pers,Personne.class);
		p = this.personneRepository.save(p);
		return p.getId();
	}

}
