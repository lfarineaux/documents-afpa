<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.cda.dto.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<!DOCTYPE html>

<html>
<head>
<link href="static/css/style.css" rel="stylesheet">
<link href="static/fontawesome/css/all.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>

<body>
	<header>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<a href="#" class="navbar-brand d-flex align-items-center"> <strong>GESTION
						DES PRODUITS - Se connecter</strong>
				</a>
			</div>
		</div>
	</header>

	<div class="card text-center">
 		<div class="card-header">AUTHENTIFICATION <p>${msg}</p></div> <%-- Récupération du message avec une Expression Language. "msg étant l'identifiant dans le controller "AuthentificationController" --%>
		<div class="card-body">
			<form style="border: solid 1px Gray; background-color: #e5e7e9; border-top-left-radius: 15px; border-top-right-radius: 15px; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px;" method="post" action="connexionControle">
			<h5 class="card-title">CONNECTION</h5>
				<label for="login">Login : </label>
				<p>
					<input type="email" name="identifiant" value="">
				</p>
				<label for="pwd">Password : </label>
				<p>
					<input type="password" name="pass" value="">
				</p>
				<p>
					<input class="btn btn-secondary mb-3" value="valider" type="submit"
						id="submit-btn">
					<!-- 			<input type="submit" name="send" /> -->
				</p>
			</form>
			<p><a class="btn btn-secondary" href="index" role="button"><span
					class="badge badge-light"><i class="fas fa-book-open"></i></span>Retour au Menu</a></p>
			</p>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
<script src="static/js/script.js"></script>
</body>
</html>
