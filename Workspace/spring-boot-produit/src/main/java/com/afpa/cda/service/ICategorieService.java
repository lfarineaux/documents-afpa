package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;

import com.afpa.cda.dto.CategorieDto;

public interface ICategorieService {
	
	public Optional<CategorieDto> findById(int id);
	
	public List<CategorieDto> findAllCategories(int page);
	
	public Integer AddCategorie(CategorieDto categorie);
	
	public void updateCategorie(String label,int id);
	
	public boolean deleteById(int id);
	
	String findLabelById(int id);

}
