package com.afpa.cda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class MenuController {


	@RequestMapping(path = {"/index","/"})
	public ModelAndView initMenu(ModelAndView mv) {

		System.out.println("controller");
		mv.setViewName("menu");
		return mv;
	}


}
