package com.afpa.cda.controller;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.RoleDto;
import com.afpa.cda.service.IRoleService;
import com.afpa.cda.service.RoleServiceImpl;

@Controller
public class RoleController {

	@Autowired
	private IRoleService roleService;

	@RequestMapping(value="/addRol")
	public ModelAndView initAdd(ModelAndView mv) {

		mv.setViewName("addRole");
		return mv;
	}

	@RequestMapping(value="/addRol",
			method =RequestMethod.POST,
			params = {"typeRole"})
	public void add(
			@RequestParam(value = "typeRole") String typeR,
			HttpServletResponse resp,
			ModelAndView mv) throws IOException {
		this.roleService.ajouterRole(
				RoleDto.builder()
				.type(typeR)
				.build());

		resp.sendRedirect("listRol?page=0");

	}

	@RequestMapping(value="/listRol",
			method =RequestMethod.GET)
	public ModelAndView list
	(@RequestParam(value = "page", required = false) String pageStr,
			HttpServletResponse resp,
			HttpServletRequest req,
			ModelAndView mv) {
		if(req.getSession()!=null) {
			
			int page = 0;
			if(pageStr == null || pageStr.length() == 0) {
				
			}else {
				try {
					page = Integer.parseInt(pageStr.trim());
					
					if(page > RoleServiceImpl.nbPage) {
						page = 0;
					}
					
				}catch (NumberFormatException e) {
					String message = "Erreur pas de lettre pour le numero de page";
					
				}
			}
			List <RoleDto> listRol = this.roleService.chercherTousLesRoles(page);
			mv.addObject("pageN",page);
			mv.addObject("pageMax",RoleServiceImpl.nbPage);
			mv.addObject("listRole",listRol);
			mv.setViewName("listRole");
			return mv;
		}else {
			mv.setViewName("Menu");
			return mv;
		}

	}


	@RequestMapping(value="/showRol",
			method =RequestMethod.GET,params = {"id"})
	public ModelAndView initShow
	(@RequestParam(value = "id") String idStr,
			ModelAndView mv) {

		int id=0;
		String message=null;
		String typeR =null;
		try {
			id = Integer.parseInt(idStr.trim());
			typeR =this.roleService.findTypeById(id);

		} catch (NumberFormatException e) {
			message = "Erreur pas de lettre pour l'id";
		} catch (Exception e) {
			message = e.getMessage();
		}

		mv.addObject("typeR",typeR);
		mv.addObject("id",id);
		mv.setViewName("showRole");
		return mv;


	}


	@RequestMapping(value="/updateRol",
			method =RequestMethod.GET,params = {"id"})
	public ModelAndView initUpdate
	(@RequestParam(value = "id") String idStr,
			ModelAndView mv) {

		int id=0;
		String message=null;
		String typeR =null;
		try {
			id = Integer.parseInt(idStr.trim());
			typeR =this.roleService.findTypeById(id);
		} catch (NumberFormatException e) {
			message = "Erreur pas de lettre pour l'id";
		}catch (Exception e) {
			message = e.getMessage();
		}

		mv.addObject("typeR",typeR);
		mv.addObject("id",id);
		mv.setViewName("updateRole");
		return mv;
	}


	@RequestMapping(value="/updateRol",
			method =RequestMethod.POST)
	public void Update(
			@RequestParam(value = "id") String idStr,
			@RequestParam(value = "typeRole") String typeRol,
			HttpServletResponse resp) throws IOException {

		int id=0;
		try {
			id = Integer.parseInt(idStr.trim());

		} catch (NumberFormatException e) {
			String message = "Erreur pas de lettre pour l'id";
		}
		this.roleService.updateRole(typeRol, id);
		resp.sendRedirect("listRol?page=0");

	}


	@RequestMapping(value="/deleteRol",
			method =RequestMethod.GET,params = {"id"})
	public void delete(
			@RequestParam(value = "id") String idStr,
			HttpServletResponse resp) throws IOException {

		int id=0;
		String message=null;
		try {
			id = Integer.parseInt(idStr.trim());
			this.roleService.deleteById(id);

		} catch (NumberFormatException e) {
			message = "Erreur pas de lettre pour l'id";
		} catch (Exception e) {
			message = e.getMessage();
		}


		resp.sendRedirect("listRol");

	}

}
