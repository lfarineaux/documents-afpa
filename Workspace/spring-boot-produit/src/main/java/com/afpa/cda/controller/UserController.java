package com.afpa.cda.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.RoleDto;
import com.afpa.cda.dto.UserDto;
import com.afpa.cda.entity.Role;
import com.afpa.cda.service.IRoleService;
import com.afpa.cda.service.IUserService;
import com.afpa.cda.service.UserServiceImpl;

@Controller
public class UserController {

	@Autowired
	private IUserService userService;
	@Autowired
	IRoleService roleS;

	
	@RequestMapping(value="/addUser")
	public ModelAndView initAddUser(ModelAndView mv) {
		List<RoleDto> maListe = roleS.chercherTousLesRoles(0);
		mv.addObject("maListe",maListe);
		mv.setViewName("addUser");
		return mv;
	}
	
	@RequestMapping(value="/addUser",
			method =RequestMethod.POST,
			params = {"nom","prenom","monRole","email","pass"})
	public void ajoutUtilisateur(
			@RequestParam(value="nom") String nom,
			@RequestParam(value="prenom") String prenom,
			@RequestParam(value="monRole") String monRole,
			@RequestParam(value="email") String email,
			@RequestParam(value="pass") String pass,
			HttpServletResponse resp
			) throws IOException {
		int IdRole =0;
		try {
			IdRole = Integer.parseInt(monRole);
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		this.userService.ajouter(
				UserDto.builder()
					.login(email)
					.pwd(pass)
					.role(RoleDto.builder().id(IdRole).build())
					.nom(nom)
					.prenom(prenom)
					.build());
		resp.sendRedirect("listUser?page=0");
		
	}
	
	@RequestMapping(value="/listUser", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam(value = "page", required = false) String pageStr, ModelAndView mv) {
		int page = 0;
		if(pageStr == null || pageStr.length() == 0) {
			
		} else {
			try {
				page = Integer.parseInt(pageStr.trim());
				if (page > UserServiceImpl.nbPage) {
					page = 0;
				}
			} catch (NumberFormatException e) {
				String message = "Erreur pas de lettre pour le num�ro de page";
				
				
			}
		}
		List<UserDto> listUser = this.userService.chercherToutesLesPersonnes(page);
		mv.addObject("pageN", page);
		mv.addObject("pageMax", UserServiceImpl.nbPage);
		mv.addObject("listUser", listUser);
		mv.setViewName("listUser");
		return mv;
	}
	
	@RequestMapping(value="/showUser", method = RequestMethod.GET, params = {"id"})
			public ModelAndView initShow(@RequestParam(value = "id") String idStr, ModelAndView mv) {
		int id = 0;
		String message = null;
		UserDto userDto = null;
		
		try {
			id = Integer.parseInt(idStr.trim());
			userDto = this.userService.findById(id);
			
		} catch (NumberFormatException e) {
			message = "Erreur pas de lettre pour l'id";
		} catch (Exception e) {
			message = e.getMessage();
		}
		//userDto.get();
		//mv.addObject("nom", nom);
		mv.addObject("id", id);
		mv.addObject("nom",userDto.getNom());
		mv.addObject("prenom", userDto.getPrenom());
		mv.addObject("role", userDto.getRole());
		mv.addObject("login", userDto.getLogin());
		mv.setViewName("showUser");
		return mv;
	}
	
	@RequestMapping(value="/updateUser",
			method = RequestMethod.GET, params = {"id"})
	public ModelAndView initUpdate(@RequestParam(value = "id") String idStr, ModelAndView mv) {
		
		int id = 0;
		String message = null;
		UserDto userDto = null;
		try {
			id = Integer.parseInt(idStr.trim());
			userDto = this.userService.findById(id);
			
		} catch (NumberFormatException e) {
			message = "Erreur pas de lettre pour l'id";
		} catch (Exception e) {
			message = e.getMessage();
		}
		List<RoleDto> listRole = roleS.chercherTousLesRoles(0);
		System.out.println("ssssssssssssssss"+listRole.get(0).getId());
		mv.addObject("listRole", listRole);
		mv.addObject("id", id);
		mv.addObject("nom",userDto.getNom());
		mv.addObject("prenom", userDto.getPrenom());
		mv.addObject("login", userDto.getLogin());
		mv.addObject("pwd", userDto.getPwd());
		mv.setViewName("updateUser");
		return mv;
	}
	
	@RequestMapping(value="/updateUser", method= RequestMethod.POST)
	public void update(@RequestParam(value = "id") String pageStr, 
			@RequestParam(value = "nom") String nom, 
			@RequestParam(value = "prenom") String prenom, 
			@RequestParam(value = "login") String login, 
			@RequestParam(value = "pwd") String pwd, 
			@RequestParam(value = "idRole") String idRole, 
			HttpServletResponse resp) throws IOException {
		int id = 0;
		int idr =0;
		try {
			id = Integer.parseInt(pageStr.trim());
			idr = Integer.parseInt(idRole.trim());
		} catch (NumberFormatException e) {
			String message = "Erreur pas de lettre pour l'id";
		}
		
		
		RoleDto monRole = roleS.findById(idr).get();
		this.userService.updateUser(id, nom, prenom, login, pwd, monRole);
		resp.sendRedirect("listUser?page=0");
	}
	
	@RequestMapping(value="/deleteUser",method=RequestMethod.GET)
	public void initDelete(
			@RequestParam(value = "id") String pageStr,
			HttpServletResponse resp
			) throws IOException {
		
		int id = 0;
		try {
			id = Integer.parseInt(pageStr.trim());
		} catch (NumberFormatException e) {
			String message = "Erreur pas de lettre pour l'id";
		}
		
		this.userService.deleteById(id);
		resp.sendRedirect("listUser?page=0");
	}
	
	@RequestMapping(value="/deleteUser", method=RequestMethod.POST)
	public void delete(
			@RequestParam(value = "id") String pageStr,
			HttpServletResponse resp) throws IOException {
		int id = 0;
		try {
			id = Integer.parseInt(pageStr.trim());
		} catch (NumberFormatException e) {
			String message = "Erreur pas de lettre pour l'id";
		}
		this.userService.deleteById(id);
		resp.sendRedirect("listUser?page=0");
	}
	
}
