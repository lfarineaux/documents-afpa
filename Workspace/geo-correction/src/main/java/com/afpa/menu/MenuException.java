package com.afpa.menu;

import com.afpa.commun.GeoException;

public class MenuException extends GeoException {

	private static final long serialVersionUID = 1L;

	public MenuException(Exception e) {
		super(e);
	}

}
