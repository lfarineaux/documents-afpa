package com.afpa.menu;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;

import com.afpa.point.NomPointInvalidException;
import com.afpa.point.Point;
import com.afpa.point.PointExisteDejaException;
import com.afpa.point.PointFactory;

public class MenuApp {

	public static void main(String[] args) throws Exception {
		exec(System.in, System.out, System.out);
	}

	public static void exec(InputStream in, OutputStream outMenu, OutputStream outStatus) throws Exception {

		final MenuInOut menu = new MenuInOut(in, outMenu, outStatus);

		menu.ecrire("Bienvenu dans l'appli de geometrie ! ");

		int choix = 0;
		boolean continuer = true;

		String nomPoint = "";
		int absPoint = 0;
		int ordPoint = 0;

		while (continuer) {
			menu.ecrire("choisir une action");
			menu.ecrire("1 - ajouter un point");
			menu.ecrire("2 - ajouter un triangle");
			menu.ecrire("3 - ajouter un cercle");
			menu.ecrire("4 - lister les points");
			menu.ecrire("5 - lister les triangles");
			menu.ecrire("6 - lister les cercles");
			menu.ecrire("7 - arreter");
			menu.ecrire(" > ", false);
			choix = menu.lireEntier(true, "saisir une valeur numérique entre 1 et 7\n > ", false);
			switch (choix) {
			case 1:
				menu.ecrire("saisir les information du point :");

				menu.ecrire("nom > ", false);
				nomPoint = menu.lireString();

				menu.ecrire("abscisse > ", false);
				absPoint = menu.lireEntier();

				menu.ecrire("ordonnée > ", false);
				ordPoint = menu.lireEntier();

				Point pCree = null;
				try {
					pCree = PointFactory.creer(absPoint, ordPoint, nomPoint);
					menu.ecrire(true,"point créé avec succès : " + pCree);
				} catch (PointExisteDejaException e) {
					menu.ecrire(true,"le point existe deja : " + pCree);
				} catch (NomPointInvalidException e) {
					menu.ecrire(true,"nom saisi invalide " + nomPoint + ", le nom doit être une lettre + un chiffre ");
				}

				break;
			case 2:
				List<Point> points = PointFactory.getCopie();
				Collections.sort(points);
				menu.ecrire(true,"il y a " + points.size() + " points : ");
				for (Point p : points) {
					menu.ecrire(true,"  " + p);
				}
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				break;
			case 6:
				break;
			case 7:
				continuer = false;
				menu.ecrire("au revoir !");
				break;
			default:
				menu.ecrire("choix incorrect");
			}
			menu.ecrire("");
		}
	}

}
