package com.afpa.point;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)
class PointTest {
	
	@BeforeAll
	static void init() {
		PointFactory.vider();
	}
	
	@Test
	@Order(1)
	void testAjoutPoint() {
		int nb = PointFactory.getNb();
		try {
			PointFactory.creer(2, 4, "A2");
			assertEquals(nb+1, PointFactory.getNb());
		} catch (Exception e) {
			fail("pas d'exception normalement ",e);
		}
	}
	
	@Test
	@Order(2)
	void testAjoutPointDejaExistant() {
		Exception ex = null;
		try {
			PointFactory.creer(2, 4, "A2");
		} catch (Exception e) {
			ex = e;
		}
		assertTrue(ex instanceof PointExisteDejaException);
	}
	
	@Test
	@Order(3)
	void testAjoutPointNomInvalide() {
		Exception ex = null;
		try {
			PointFactory.creer(2, 4, null);
		} catch (Exception e) {
			ex = e;
		}
		assertTrue(ex instanceof NomPointInvalidException);
		
		try {
			PointFactory.creer(2, 4, "");
		} catch (Exception e) {
			ex = e;
		}
		assertTrue(ex instanceof NomPointInvalidException);
		
		try {
			PointFactory.creer(2, 4, "a");
		} catch (Exception e) {
			ex = e;
		}
		assertTrue(ex instanceof NomPointInvalidException);
		
		try {
			PointFactory.creer(2, 4, "aa");
		} catch (Exception e) {
			ex = e;
		}
		assertTrue(ex instanceof NomPointInvalidException);
		
		try {
			PointFactory.creer(2, 4, "aa2");
		} catch (Exception e) {
			ex = e;
		}
		assertTrue(ex instanceof NomPointInvalidException);
	}
}
