package com.afpa.point;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.menu.MenuApp;

@TestMethodOrder(OrderAnnotation.class)
class MenuPointTest {

	@BeforeAll
	static void init() {
		PointFactory.vider();
	}

	@Test
	@Order(1)
	void creation() throws Exception {
		InputStream in = MenuPointTest.class.getResourceAsStream("/point/creation_ok.txt");
		OutputStream out = Files.newOutputStream(Files.createTempFile("CDA_Menu_", ".txt"));
		OutputStream outStatus = Files.newOutputStream(Files.createTempFile("CDA_Status_", ".txt"));

		MenuApp.exec(in, out, outStatus);
	}

	  @Test
	    @Order(2)
	    void creation_deja_existant() throws Exception {
	        InputStream in = MenuPointTest.class.getResourceAsStream("/point/creation_cration_doublon.txt");
	        OutputStream out = Files.newOutputStream(Files.createTempFile("CDA_2_Menu_", ".txt"));
	        OutputStream outStatus = Files.newOutputStream(Files.createTempFile("CDA_2_Status_", ".txt"));
	        MenuApp.exec(in, out, outStatus);
	        
	    }
	
}
