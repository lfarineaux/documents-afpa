package dto;

import entity.Enclos;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AnimalDto {
	private String nomAnimal;
	private Enclos nomEnclos;
}
