package dto;

import entity.Enclos;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ResponsableDto {
	private String nomResp;
	private Enclos enclos;
}
