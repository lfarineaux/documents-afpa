package dao;

import java.util.List;

import entity.Aliment;

public interface IAlimentDao extends IDao<Aliment> {
	
	public Aliment findIdByAlim(String nom) ;

}
