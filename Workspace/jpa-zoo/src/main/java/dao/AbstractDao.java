package dao;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractDao<T> implements IDao<T> {
	private static Logger monLogger = LoggerFactory.getLogger(AbstractDao.class);
	private EntityManagerFactory factory = null;

	private final Class<T> clazz;

	@SuppressWarnings("unchecked")
	public AbstractDao() {
		this.clazz = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		this.factory = Persistence.createEntityManagerFactory("zoo");
	}

	public void close() {
		if (this.factory != null) {
			this.factory.close();
		}
	}

	// Creer un EM et ouvrir une transaction
	public EntityManager newEntityManager() {
		EntityManager em = this.factory.createEntityManager();
		em.getTransaction().begin();
		return (em);
	}

	// Fermer un EM et defaire la transaction si necessaire
	public void closeEntityManager(EntityManager em) {
		if (em != null) {
			if (em.isOpen()) {
				EntityTransaction t = em.getTransaction();
				if (t.isActive()) {
					try {
						t.rollback();
					} catch (PersistenceException e) {
						e.printStackTrace();
					}
				}
				em.close();
			}
		}
	}

	// @Override
	public T find(Object id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			monLogger.info("Try Find");
			return em.find(this.clazz, id);
		} finally {
			closeEntityManager(em);
		}
	}

	
	@Override
	public Collection<T> findNamedQuery(String query) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<T> q = em.createNamedQuery(query, this.clazz);
			monLogger.info("Try FindNamedQuery");
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}

	@Override
	public T add(T entity) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			em.persist(entity);
			em.getTransaction().commit();
			monLogger.info("Try Add");
			return (entity);
		} finally {
			closeEntityManager(em);
		}
	}

	@Override
	public T update(T entity) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			entity = em.merge(entity);
			em.getTransaction().commit();
			monLogger.info("Try Update");
		} finally {
			closeEntityManager(em);
		}
		return entity;
	}

	@Override
	public void remove(int id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			T entity = em.find(this.clazz, id);
			if (entity != null) {
				em.remove(entity);
			}
			em.getTransaction().commit();
			monLogger.info("Try Remove");
		} finally {
			closeEntityManager(em);
		}
	}
}