package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entity.Aliment;

public class AlimentDao extends AbstractDao<Aliment> implements IAlimentDao {

	// test si un animal est lié à un aliment //
	// supprimer l'aliment de la liste de l'animal avant de supprimer l'aliment //
	public void suppAliment (int id) {



	}

	public Aliment findIdByAlim(String nom) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Aliment> q = em.createNamedQuery("findIdByAlim", Aliment.class);
			q.setParameter("NameParam", nom);
			return q.getSingleResult();
		} finally {
			closeEntityManager(em);
		}
	}

}
