package ihm;

import java.util.Scanner;

import service.ServiceAliment;
import service.ServiceAnimal;
import service.ServiceEnclos;
import service.ServiceResponsable;

public class MenuEnclos {

	public static ServiceEnclos se = new ServiceEnclos();
	
	public static void menu() {
		Scanner sc = new Scanner(System.in);
		boolean	sousfin = true;

		System.out.println("3- Menu Enclos");
		sousfin = true;

		while (sousfin) {

			System.out.println();
			System.out.println("      *************************");
			System.out.println("      *    SOUS-MENU ENCLOS   *");
			System.out.println("      *************************");
			System.out.println();
			System.out.println("0- Retourner au menu principal");
			System.out.println("1- Créer un Enclos");
			System.out.println("2- Lister les Enclos");
			System.out.println("3- Mettre à jour un Enclos");
			System.out.println("4- Supprimer un Enclos");
			System.out.print("-> ");
			String sousmenu = Clavier.lireTxt(sc, "");
			System.out.println();

			switch (sousmenu) {
			case "0":
				System.out.println("0 - Retour au menu principal");
				sousfin = false;
				break;

			case "1":
				System.out.println("1- Créer un enclos");
				System.out.print("Saisir le nom de l enclos : ");
				String in = Clavier.lireTxt(sc, "");
				se.creerenclos(in);

				break;

			case "2":
				System.out.println("2- Lister les Enclos");
			
				
				break;
			case "3":	
				System.out.println("3- Mettre à jour un Enclos");
				System.out.print("Saisir l'Id de l'enclos à mettre à jour");
				int id = Clavier.lireEntier(sc, "");
				System.out.print("Saisir nouveau nom de l'enclos");
				String resnew = Clavier.lireTxt(sc, "");
				se.majEnclos(resnew, id);
				break;

			case "4":
				System.out.println("4- Supprimer un Enclos");
				System.out.print("Saisir l'Id de l'enclos à supprimer ");
				id = Clavier.lireEntier(sc, "");
				se.suppEnclos(id);
				break;

			default:
				System.out.println("Mauvaise saisie");
				continue;

			}
			break;
		}

	}

}
