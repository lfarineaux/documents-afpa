package ihm;

import java.util.Scanner;

import service.ServiceAnimal;

public class MenuAnimal {

	public static ServiceAnimal sa = new ServiceAnimal();
	
	public static void menu() {
		Scanner sc = new Scanner(System.in);
		boolean	sousfin = true;

		System.out.println("2- Menu Animal");
		sousfin = true;

		while (sousfin) {

			System.out.println();
			System.out.println("      *************************");
			System.out.println("      *    SOUS-MENU ANIMAL   *");
			System.out.println("      *************************");
			System.out.println();
			System.out.println("0- Retourner au menu principal");
			System.out.println("1- Créer un Animal");
			System.out.println("2- Lister les animaux");
			System.out.println("3- Mettre à jour un Animal");
			System.out.println("4- Supprimer un animal");
			System.out.print("-> ");
			String sousmenu = Clavier.lireTxt(sc, "");

			switch (sousmenu) {
			case "0":
				System.out.println("0 - Retour au menu principal");
				sousfin = false;
				break;

			case "1":
				System.out.println("1- Créer un animal");
				System.out.print("Saisir l animal : ");
				String in = Clavier.lireTxt(sc, "");
				System.out.print("Saisir l'Id de l'enclos ");
				int enc = Clavier.lireEntier(sc, "");
				System.out.print("Saisir l'Id de l'aliment ");
				int alim = Clavier.lireEntier(sc, "");
				sa.creerAnimal(in, enc,alim);
				break;

			case "2":
				System.out.println("2- Lister les animaux");

				break;

			case "3":
				System.out.println("3- Mettre à jour un Animal");
				System.out.print("Saisir l'Id de l'animal à mettre à jour");
				int id = Clavier.lireEntier(sc, "");
				System.out.print("Saisir nouveau nom de l'animal ");
				String resnew = Clavier.lireTxt(sc, "");
				sa.majAnimal(resnew, id);
				break;

			case "4":
				System.out.println("4- Supprimer un animal");
				System.out.print("Saisir l'Id de l'animal à supprimer ");
				id = Clavier.lireEntier(sc, "");
				sa.suppAnimal(id);
				break;

			default:
				System.out.println("Mauvaise saisie");
				continue;

			}
			break;
		}
	}

}
