package ihm;

import java.util.Scanner;

/**
 * La classe Menu contient le menu
 *
 * 
 * @authors ag/lf
 */

public class Menu {

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		String menu = "";
		boolean fin = true;

		/**
		 * @param menu : numéro saisi dans le menu fin : arrêt du programme
		 */

		while (fin) {
			System.out.println();
			System.out.println("      **************");
			System.out.println("      *    MENU    *");
			System.out.println("      **************");
			System.out.println();
			System.out.println("0- Arreter le programme");
			System.out.println("1- Menu Aliment");
			System.out.println("2- Menu Animal");
			System.out.println("3- Menu Enclos");
			System.out.println("4- Menu Responsable");
			System.out.print("-> ");
			menu = Clavier.lireTxt(sc, "");

			switch (menu) {
			case "0":
				System.out.println("0 - Arrêt du programme");
				fin = false;
				break;
			case "1":
				MenuAliment.menu();
				break;
			case "2":
				MenuAnimal.menu();
				break;
			case "3":
				MenuEnclos.menu();
				break;
			case "4":
				MenuResponsable.menu();
				break;
			}
		}
		sc.close();
	}
}
