package ihm;

import java.util.Scanner;

import service.ServiceAliment;
import service.ServiceEnclos;
import service.ServiceResponsable;

public class MenuResponsable {

	public static ServiceResponsable sr = new ServiceResponsable();
	
	public static void menu() {
		Scanner sc = new Scanner(System.in);
		boolean	sousfin = true;

		System.out.println("4- Menu Responsable");
		sousfin = true;

		while (sousfin) {

			System.out.println();
			System.out.println("      *****************************");
			System.out.println("      *    SOUS-MENU RESPONSABLE  *");
			System.out.println("      *****************************");
			System.out.println();
			System.out.println("0- Retourner au menu principal");
			System.out.println("1- Créer un Responsable");
			System.out.println("2- Lister les Responsables");
			System.out.println("3- Mettre à jour le nom d un Responsable");
			System.out.println("4- Supprimer un Responsable");
			System.out.print("-> ");
			String sousmenu = Clavier.lireTxt(sc, "");

			System.out.println();

			switch (sousmenu) {
			case "0":
				System.out.println("0 - Retour au menu principal");
				sousfin = false;
				break;

			case "1":
				System.out.println("1- Créer un Responsable");
				System.out.print("Saisir le nom du responsable : ");
				String in = Clavier.lireTxt(sc, "");
				System.out.print("Saisir l'id de l'enclos : ");
				int id = Clavier.lireEntier(sc, "");
				sr.creerResponsable(in,id);
				
				break;

			case "2":
				// fonctionne pas
				System.out.println("2- Lister Responsable");

				System.out.println(ServiceResponsable.listertout());
				break;

			case "3":
				System.out.println("3- Mettre à jour le nom d un Responsable");
				System.out.print("Saisir l'Id du responsable à mettre à jour");
				id = Clavier.lireEntier(sc, "");
				System.out.print("Saisir le nouveau nom du responsable ");
				String resnew = Clavier.lireTxt(sc, "");
				sr.majResp(resnew, id);
				break;
			case "4":
				System.out.println("4- Supprimer une Responsable");
				System.out.print("Saisir l'Id du responsable à supprimer ");
				id= Clavier.lireEntier(sc, "");
				sr.suppResp(id);
				break;
			default:
				System.out.println("Mauvaise saisie");
				continue;

			}
			break;
		}	
	}

}
