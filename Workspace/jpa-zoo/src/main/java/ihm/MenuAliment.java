package ihm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import dao.IAlimentDao;
import entity.Aliment;
import service.ServiceAliment;

public class MenuAliment {

	public static ServiceAliment sa = new ServiceAliment();
	public static  IAlimentDao ialimentDao;

	public static void menu() {
		Scanner sc = new Scanner(System.in);
		boolean	sousfin = true;

		System.out.println("1- Menu Aliment");

		while (sousfin) {

			System.out.println();
			System.out.println("      **************************");
			System.out.println("      *    SOUS-MENU ALIMENT   *");
			System.out.println("      **************************");
			System.out.println();
			System.out.println("0- Retourner au menu principal");
			System.out.println("1- Créer un Aliment");
			System.out.println("2- Lister les aliments");
			System.out.println("3- Mettre à jour un aliment");
			System.out.println("4- Supprimer un aliment");
			System.out.println("5- Lister l'Id d'un aliment");

			System.out.print("-> ");
			String sousmenu = Clavier.lireTxt(sc, "");

			switch (sousmenu) {
			case "0":
				System.out.println("0 - Retour au menu principal");
				sousfin = false;
				break;

			case "1":
				System.out.println("1- Créer un aliment");
				System.out.print("Saisir le nom  ");
				String in = Clavier.lireTxt(sc, "");
				sa.creerAliment(in);
				break;

			case "2":
				System.out.println("2- Lister les aliments");
				sa.findAll().forEach(System.out::println);

				break;

			case "3":
				System.out.println("3- Mettre à jour un aliment");
				System.out.print("Saisir l'Id de l'aliment à mettre à jour ");
				int id = Clavier.lireEntier(sc, "");
				System.out.print("Saisir nouveau nom de l'aliment ");
				String resnew = Clavier.lireTxt(sc, "");
				sa.majAliment(resnew, id);
				break;

			case "4":
				System.out.println("4- Supprimer un aliment");
				System.out.print("Saisir l'Id de l'aliment à supprimer ");
				id = Clavier.lireEntier(sc, "");
				sa.suppAliment(id);

				break;

			case "5":
				System.out.println("5- Lister l'Id d'un aliment");
				System.out.print("Saisir le nom de l'Aliment ");
				String nom = Clavier.lireTxt(sc, "");
			
				Aliment a=ialimentDao.findIdByAlim(nom);
				System.out.println("Id : "+a.getIdAliment());
				break;

			default:
				System.out.println("Mauvaise saisie");
				continue;

			}
			break;
		}
	}
}
