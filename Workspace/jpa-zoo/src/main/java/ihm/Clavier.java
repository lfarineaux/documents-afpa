package ihm;

import java.util.Scanner;

public class Clavier {

	static int lireEntier(Scanner clavier, String message) {
//		System.out.print(message);
		try {
			int res = clavier.nextInt();
			clavier.nextLine();
			return res;
		} catch (Exception e) {
			clavier.nextLine();
			System.out.println("Erreur");
			return Integer.MIN_VALUE;
			//throw new InputMismatchException();
		}
	}
	static double lireDouble(Scanner clavier, String message){
//		System.out.print(message);
		try {            
			double res = clavier.nextDouble();
			clavier.nextLine();
			return res;
		}catch(Exception e){
			clavier.nextLine();
			System.out.println("Erreur");
			return Integer.MIN_VALUE;
		}
	}
	static String lireTxt(Scanner clavier, String message) {
//		System.out.println(message);
		String res = clavier.next();
		clavier.nextLine();
		return res;
	}
	
	


}