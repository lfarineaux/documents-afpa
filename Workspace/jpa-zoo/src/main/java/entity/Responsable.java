package entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "Responsable")
@NamedQuery(name = "RFindAll", query = "SELECT p FROM Responsable p")
//@NamedQueries({ 
//	@NamedQuery(name = "findByResp", query = "SELECT p FROM Responsable p where p.nom_Responsable= :fNameParam"), })

public class Responsable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Responsable", length = 3, nullable = false)
	private int idResponsable;

	@Column(name = "nom_Resp", length = 25, nullable = false)
	private String nomResp;

	@OneToOne(optional = true)
	private Enclos enclos;

	public Responsable(String i) {
		super();
		this.nomResp = i;

	}
}