package entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "Animal")
@NamedQuery(name = "AFindAll", query = "SELECT p FROM Animal p")
public class Animal {

	public Animal(String nomAnimal) {
		super();
		this.nomAnimal = nomAnimal;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Animal", length = 3, nullable = false)
	private int idAnimal;

	@Column(name = "nom_animal", length = 25, unique = true, nullable = false)
	private String nomAnimal;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE })
	@JoinTable(name = "Animal_aliment", joinColumns = { @JoinColumn(name = "nom_Animal") },
	inverseJoinColumns = {@JoinColumn(name = "nom_Aliment") })
	private Set<Aliment> listAliment;
	
	@ManyToOne(optional = true, cascade = { CascadeType.MERGE })
	@JoinColumn(name = "nom_enclos")
	private Enclos nomEnclos;

}
