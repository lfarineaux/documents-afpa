package entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
//@Builder
@Table(name = "Enclos")
@NamedQuery(name = "EFindAll", query = "SELECT p FROM Enclos p")
//@NamedQueries({ 
//	@NamedQuery(name = "findByEnclos", query = "SELECT p FROM Enclos p where p.id_enclos= :fNameParam"), 
// })
public class Enclos {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Enclos", length = 3, nullable = false)
	private int idEnclos;

	@Column(name = "nom_enclos", length = 25, nullable = false, unique = true)
	private String nomEnclos;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "nomEnclos")
	private Set<Animal> listAnimal;
	
	public Enclos(String nom) {

		this.nomEnclos = nom;
	}

}
