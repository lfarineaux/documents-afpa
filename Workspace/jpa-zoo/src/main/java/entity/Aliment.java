package entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "Aliment")
@NamedQueries({ 
	@NamedQuery(name = "findAll", query = "SELECT p FROM Aliment p"),
		@NamedQuery(name = "findIdByAlim", query = "SELECT p.idAliment FROM Aliment p where p.nomAliment= :NameParam"),
		})
public class Aliment {

	public Aliment(String aliment) {
		super();
		this.nomAliment = aliment;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Aliment", length = 3, nullable = false)
	private int idAliment;

	@Column(name = "nom_aliment", length = 25, nullable = false, unique = true)
	private String nomAliment;

	@ManyToMany(fetch = FetchType.EAGER, 
			mappedBy = "listAliment",
			cascade = CascadeType.MERGE)
	private Set<Animal> listAnimal;

}