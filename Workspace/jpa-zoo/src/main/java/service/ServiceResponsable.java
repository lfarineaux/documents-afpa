package service;

import java.util.ArrayList;

import dao.EnclosDao;
import dao.IDao;
import dao.ResponsableDao;
import dto.EnclosDto;
import dto.ResponsableDto;
import entity.Aliment;
import entity.Enclos;
import entity.Responsable;

public class ServiceResponsable {

	
	ResponsableDao respDao = new ResponsableDao();
	ResponsableDto respDto = new ResponsableDto();
	
	EnclosDao encDao = new EnclosDao();

	public void creerResponsable(String in,int id) {
		try {
			Responsable re = new Responsable();
			Enclos enc = encDao.find(id);
			re.setNomResp(in);
			re.setEnclos(enc);
			respDao.add(re);
		} catch (Exception e) {
			System.out.println(e);
		}
	}


	public void majResp(String nom, int id) {
		Responsable a = respDao.find(id);
		a.setNomResp(nom);
		respDao.update(a);
	}

	public static ArrayList<Responsable> listertout() {
		return null;
	}

	public  void suppResp(int id) {
		try {
			respDao.remove(id);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
