package service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import dao.AlimentDao;
import dao.AnimalDao;
import dao.EnclosDao;
import dto.AnimalDto;
import entity.Aliment;
import entity.Animal;
import entity.Enclos;

public class ServiceAnimal {

	AnimalDao animalDao = new AnimalDao();
	AnimalDto animalDto = new AnimalDto();
	
	EnclosDao enclosDao = new EnclosDao();
	AlimentDao alimentDao = new AlimentDao();
	

	public void creerAnimal(String in,int enclos,int alim) {
		Animal a = new Animal();
		Enclos e = enclosDao.find(enclos);
		Aliment al = alimentDao.find(alim);
		a.setNomAnimal(in);
		a.setNomEnclos(e);
		
		List<Aliment> aliments = Arrays.asList(al);
		a.setListAliment(new HashSet<Aliment>(aliments));
		animalDao.add(a);

	}

	public void majAnimal(String nom, int id) {
	Animal a = animalDao.find(id);
	a.setNomAnimal(nom);
	animalDao.update(a);
}


	public  void suppAnimal(int id) {
		try {
			animalDao.remove(id);
		} catch (Exception e) {
			System.out.println(e);
		}
}



}
