package service;

import dao.AnimalDao;
import dao.EnclosDao;
import dao.IDao;
import dto.AnimalDto;
import dto.EnclosDto;
import entity.Aliment;
import entity.Enclos;

public class ServiceEnclos {

//	static IDao<Enclos> enclosDao = new EnclosDao();

	EnclosDao enclosDao = new EnclosDao();
	EnclosDto enclosDto = new EnclosDto();
	
	public  Enclos creerenclos(String in) {
		try {
			Enclos a = new Enclos();
			a.setNomEnclos(in);
			enclosDao.add(a);
			return a;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public void majEnclos(String nom, int id) {
		Enclos a = enclosDao.find(id);
	a.setNomEnclos(nom);
	enclosDao.update(a);
}

	public  void suppEnclos(int id) {
	try {
		enclosDao.remove(id);
	} catch (Exception e) {
		System.out.println(e);
	}
}
}
