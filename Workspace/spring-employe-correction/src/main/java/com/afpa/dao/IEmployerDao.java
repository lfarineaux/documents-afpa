package com.afpa.dao;

import java.util.List;

import com.afpa.entity.Employer;

public interface IEmployerDao extends IDao<Employer> {

	List<Employer> trouverParNom(String nom);

}
