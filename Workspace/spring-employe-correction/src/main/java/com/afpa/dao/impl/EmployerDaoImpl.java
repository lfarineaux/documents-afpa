package com.afpa.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.afpa.dao.IEmployerDao;
import com.afpa.entity.Employer;

public class EmployerDaoImpl extends AbstractDao<Employer> implements IEmployerDao {

	@Override
	public List<Employer> trouverParNom(String nom) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Employer> q = em.createNamedQuery("Employer.parNom", Employer.class);
			q.setParameter("nomParam", nom);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}

}
