package com.afpa.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.afpa.dao.IEmployerDao;
import com.afpa.dao.impl.EmployerDaoImpl;

@Configuration
@ComponentScan(basePackages = {"com.afpa"})
public class SpringConfiguration {
	
	@Bean
	public IEmployerDao initiationEmployerDaoCDA() {
		EmployerDaoImpl em = new EmployerDaoImpl();
		
		return em;
	}
	
}
