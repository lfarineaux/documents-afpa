package com.afpa.ihm;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.afpa.dto.EmployerDto;
import com.afpa.service.IEmployerService;

public class Program {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(SpringConfiguration.class);
		ctx.refresh();

		IEmployerService employerService = ctx.getBean(IEmployerService.class);

		boolean continuer = true;
		int choix = -1;
		while (continuer) {
			System.out.println("choisir une action : ");
			System.out.println("0- arreter");
			System.out.println("1- ajouter");
			System.out.println("2- lister");

			choix = sc.nextInt();
			sc.nextLine();

			switch (choix) {
			case 0:
				System.out.println("au revoir");
				continuer = false;
				break;
			case 1:
				System.out.println("saisir un nom : ");
				System.out.print("> ");
				String nom = sc.nextLine();
				EmployerDto creer = employerService.creer(nom);
				if (creer == null) {
					System.out.println("erreur creation");
				} else {
					System.out.println("l'employer " + creer.getId() + " a �t� cr��");
				}
				break;
			case 2:
				List<EmployerDto> lst = employerService.lister();
				System.out.println("la liste des employers : ");
				lst.forEach(System.out::println);
				break;

			default:
				break;
			}

			System.out.println("\n===========");
		}

		sc.close();
	}
}
