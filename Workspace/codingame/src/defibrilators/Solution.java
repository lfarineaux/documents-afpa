package defibrilators;

import java.util.*;
import java.io.*;
import java.math.*;

public class Solution {

	/**
	 * Auto-generated code below aims at helping you parse
	 * the standard input according to the problem statement.
	 **/

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		String LON = in.next();
		String LAT = in.next();
		int N = in.nextInt();
		if (in.hasNextLine()) {
			in.nextLine();
		}

		double lon = Math.toRadians(Double.parseDouble(LON.replaceAll(",", ".")));
		double lat = Math.toRadians(Double.parseDouble(LAT.replaceAll(",", ".")));
		double di = Double.MAX_VALUE;
		String nom="";

		for (int i = 0; i < N; i++) {
			String DEFIB = in.nextLine();
			DEFIB = DEFIB.replaceAll(",", "."); 
			String[] splitstr = DEFIB.split(";");

			double londe = Math.toRadians(Double.parseDouble(splitstr[4]));
			double lade = Math.toRadians(Double.parseDouble(splitstr[5]));
			double x = ((londe-lon)*Math.cos((lat+lade)/2));
			double y = (lade-lat);
			double dis = Math.sqrt(((Math.pow(x, 2))+(Math.pow(y, 2))))*6371;
			if (dis<di) {
				di=dis;
				nom=splitstr[1];
			}
		}
		// Write an action using System.out.println()
		// To debug: System.err.println("Debug messages...");
		System.out.println(nom);
	}
}



