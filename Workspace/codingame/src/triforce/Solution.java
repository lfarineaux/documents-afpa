package triforce;

import java.util.*;
import java.io.*;
import java.math.*;

public class Solution {

	/**
	 * Auto-generated code below aims at helping you parse
	 * the standard input according to the problem statement.
	 **/

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();

		String star = "*";
		for (int i = 1; i <= N; i++) {
			if (i<2) {
				System.out.print(".");
			}
			else {
				System.out.print(" ");
			}
			for (int j = N*2; j > i+1; j--) {
				System.out.print(" ");
			}
			System.out.print(star);
			star+="**";
			System.out.println();
		}

		star="*";
		for (int i = 1; i <= N; i++) {
			for (int j = 1; j <= N-i; j++) {
				System.out.print(" ");
			}
			System.out.print(star);
			for (int j = 1; j <= N*2-star.length(); j++) {
				System.out.print(" ");
			}
			System.out.print(star);
			star+="**";
			System.out.println();
		}
		// Write an action using System.out.println()
		// To debug: System.err.println("Debug messages...");
	}
}
