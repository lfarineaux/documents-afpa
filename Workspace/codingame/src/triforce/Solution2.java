package triforce;

import java.util.*;


import java.io.*;
import java.math.*;

public class Solution2 {

	/**
	 * Auto-generated code below aims at helping you parse
	 * the standard input according to the problem statement.
	 **/

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
//		int N = in.nextInt();
		int N = 5;

		String [] line= new String [((2*N-1)*2)+1];
		for (int x=0;x<((2*N-1)*2)+1;x++) {
			line [x] = " ";	
		}
			

		for (int i = 0; i <= N-1; i++) {

			int z=0;
			for (;z<(2*N)-i-2;z++) {
				line [z] = " ";
			}

			for (int k=1;k<=2*i+1;k++) {
				line [z] = "*";
				z++;
			}

			String res = "";
			for (int m=0;m<line.length-1;m++) {
				res+=line [m];
			}
			if (i<1) {
				res='.'+res;
			}
			else {
				res=" "+res;
			}
			System.out.println(res);
		}

		String [] line2= new String [((2*N-1)*2)+1];
		for (int x=0;x<((2*N-1)*2)+1;x++) {
			line2 [x] = " ";	
		}
		for (int i = 0; i <= N-1; i++) {

			int z=0;
			for (;z<N-i-1;z++) {
				line2 [z] = " ";
			}

			for (int k=1;k<=2*i+1;k++) {
				line2 [z] =  "*";
				z++;
			}

			for (; z <(N*3)-i-1; z++) {
				line2 [z] = " ";
			}

			for (int k=1;k<=2*i+1;k++) {
				line2 [z] =  "*";
				z++;
			}

			String res = "";
			for (int m=0;m<line2.length;m++) {
				res+=line2 [m];
			}
			System.out.println(res);
		}
		// Write an action using System.out.println()
		// To debug: System.err.println("Debug messages...");
	}
}
