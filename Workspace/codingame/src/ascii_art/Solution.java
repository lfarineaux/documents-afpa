package ascii_art;

import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

class Solution {

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		int L = in.nextInt();

		int H = in.nextInt();

		if (in.hasNextLine()) {
			in.nextLine();
		}
		String T = in.nextLine();
		T = T.toUpperCase();

		String [] style = new String [H];
		for (int i = 0; i < H; i++) {
			String ROW = in.nextLine();
			style[i]=ROW;
		}

		// Write an action using System.out.println()
		// To debug: System.err.println("Debug messages...");

		for (int i=0;i<H;i++) {
			for (char ch:T.toCharArray()) {
//				 int start = ('A' <= ch && ch <= 'Z') ? (ch-'A') * L : 26*L;
				if ('A' <= ch && ch <= 'Z') {
					int start = (ch-'A') * L;
					int end = start + L;
					System.out.print(style[i].substring(start, end));
				}
				else {
					int start = 26 * L;
					int end = start + L;
					System.out.print(style[i].substring(start, end));
				}
			}
			System.out.println();
		}
	}
}
