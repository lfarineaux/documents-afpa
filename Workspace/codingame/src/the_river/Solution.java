package the_river;
import java.util.*;
import java.io.*;
import java.math.*;


/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		long r1 = in.nextLong();
		long r2 = in.nextLong();

		while (r1!=r2) {
			System.out.println(r1+"/"+r2);
			if (r1>r2) {
				long swap = r2;
				r2=r1;
				r1=swap;
			}

			long s1=0;
			String str1 = String.valueOf(r1);
			char [] ch1 = str1.toCharArray();

			for (int i=0;i<str1.length();i++) {
				long a=Character.getNumericValue(ch1[i]);
				s1 = s1+a;
				System.out.println(s1+"/"+a);
			}
			r1+=s1;
		}
		System.out.println(r1);
	}
}

