package simple_awale;

import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

public class Solution3 {

	public static Integer [] string2tab (String str)	{
		ArrayList<Integer> al = new ArrayList<Integer>();
		String res="";
		str += " ";
		for (int i=0;i<=str.length()-1;i++) {
			if (str.charAt(i)!=' ') {
				res+=str.charAt(i);
			}
			else {
				int a = Integer.parseInt(res);
				al.add(a);
				res="";
			}
		}
		Integer tab[] = new Integer[al.size()]; 
		for (int j = 0; j < al.size(); j++) { 
			tab[j] = al.get(j); 
		}
		return tab;
	}

	public static void display (Integer [] tab)	{
		int k=0;
		for (;k<tab.length-1;k++) {
			System.out.print(tab[k]+" ");
		}
		System.out.print("["+tab[k]+"]");
		System.out.println();
	}

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		String opBowls = in.nextLine();
		String myBowls = in.nextLine();
		int num = in.nextInt();

		// Write an action using System.out.println()
		// To debug: System.err.println("Debug messages...");

		Integer op [] = string2tab(opBowls);
		Integer my [] = string2tab(myBowls);

		int tail = my.length+op.length-1;
		int play [] = new int [tail];
		int i=0;
		for (;i<my.length;i++) {
			play[i]=my[i];
		}
		for (;i<tail;i++) {
			play[i]=op[i-my.length];
		}

		int index = play [num];
		play [num]=0;
		boolean replay = false;

		for (int j=index;j>0;j--) {
			play [num+1]=play [num+1]+1;
			num++;
			if (num==12) {
				num=-1;
			}
		}

		if (num==6) {
			replay = true;
		}

		int j=0;		
		for (;j<my.length;j++) {
			my[j]=play[j];
		}
		for (;j<tail;j++) {
			op[j-my.length]=play[j];
		}

		display(op);
		display(my);

		if (replay) {
			System.out.println("REPLAY");
		}
	}
}

