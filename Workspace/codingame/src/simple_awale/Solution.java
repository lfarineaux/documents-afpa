package simple_awale;

import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

public class Solution {

	public static Integer [] string2tab (String str)	{
		String[] splitstr = str.split(" ");
		Integer [] tab = new Integer[splitstr.length];

		for (int i = 0; i <splitstr.length ; i++) {
			tab[i] = Integer.parseInt(splitstr[i]);
		}
		return tab;
	}

	public static void display (Integer [] tab)	{
		int k=0;
		for (;k<tab.length-1;k++) {
			System.out.print(tab[k]+" ");
		}
		System.out.print("["+tab[k]+"]");
		System.out.println();
	}

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		String opBowls = in.nextLine();
		String myBowls = in.nextLine();
		int num = in.nextInt();

		// Write an action using System.out.println()
		// To debug: System.err.println("Debug messages...");

		Integer op [] = string2tab(opBowls);
		Integer my [] = string2tab(myBowls);

		int index = my [num];
		my [num]=0;
		boolean replay = false;

		for (int j=index;j>0;j--) {
			num++;
			if (num<my.length) {
				my [num]=my [num]+1;
			}
			else {
				op [num-my.length]=op [num-my.length]+1;
			}
			if (num==2*my.length-2) {
				num=-1;
			}
		}
		if (num==my.length-1) {
			replay = true;
		}

		display(op);
		display(my);

		if (replay) {
			System.out.println("REPLAY");
		}
	}
}

