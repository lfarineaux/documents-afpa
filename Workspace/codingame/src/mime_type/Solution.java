package mime_type;

import java.util.*;
import java.io.*;
import java.math.*;

public class Solution {

	/**
	 * Auto-generated code below aims at helping you parse
	 * the standard input according to the problem statement.
	 **/

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		HashMap<String, String> tab = new HashMap<String, String>();

		int N = in.nextInt(); // Number of elements which make up the association table.
		int Q = in.nextInt(); // Number Q of file names to be analyzed.
		for (int i = 0; i < N; i++) {
			String EXT = in.next().toLowerCase(); // file extension
			String MT = in.next(); // MIME type.
			tab.put(EXT,MT);
		}
		in.nextLine();

		for (int i = 0; i < Q; i++) {
			String FNAME = in.nextLine().toLowerCase(); // One file name per line.

			if(!FNAME.contains(".") ||  FNAME.charAt(FNAME.length()-1) == '.') {
				System.out.println("UNKNOWN");
			}
			else {
//				String[] splitstr = FNAME.split("\\.");
//				String ext = splitstr[splitstr.length-1];
			    String ext = FNAME.substring(FNAME.lastIndexOf(".") + 1);

				if (tab.containsKey(ext)) {
					System.out.println(tab.get(ext));
				}
				else {
					System.out.println("UNKNOWN");
				}
			}
		}
	}
}
