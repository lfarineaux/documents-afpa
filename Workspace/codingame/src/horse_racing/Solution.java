package horse_racing;

import java.util.*;
import java.io.*;
import java.math.*;

public class Solution {

	/**
	 * Auto-generated code below aims at helping you parse
	 * the standard input according to the problem statement.
	 **/


	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		ArrayList <Integer> al = new ArrayList<Integer> ();
		
		int N = in.nextInt();
		for (int i = 0; i < N; i++) {
			int pi = in.nextInt();
			al.add(pi);
		}
		Collections.sort(al);
		int dif=Integer.MAX_VALUE;
		int cal = 0;
		for (int i=0;i<al.size()-1;i++) {
			cal = Math.abs(al.get(i+1)-al.get(i));
			if (cal<dif) {
				dif=cal;
			}
		}
		// Write an action using System.out.println()
		// To debug: System.err.println("Debug messages...");
		System.out.println(dif);
	}
}

