package chuck;

//***********************
import java.util.*;

import java.io.*;

public class Chuck1 {
	public static void main(String args[]) {

		Scanner in = new Scanner(System.in);

		String MESSAGE = in.nextLine();

		String res="";

		for (int i = 0; i < MESSAGE.length(); i++) {

			String bin = Integer.toBinaryString(MESSAGE.charAt(i));
			for (int j=bin.length(); j<7; j++) {
				bin="0"+bin;
			}

			res+=bin;
		}

		String un="";

		char ch=0;

		for (int j = 0; j < res.length(); j++) {

			char c = res.charAt(j);
			if (c!=ch) {

				ch=c;

				un+=" ";
				if (c=='0' ) {
					un+="00";
				}
				else {
					un+="0";
				}
				un+=" ";

			}
			un+="0";
		}

		un=un.substring(1) ;	
		System.out.println(un);

	}


}
