package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IAnimalDao;
import com.afpa.dao.IEnclosDao;
import com.afpa.dao.IResponsableDao;
import com.afpa.dto.EnclosDto;
import com.afpa.dto.ResponsableDto;
import com.afpa.entity.Animal;
import com.afpa.entity.Enclos;
import com.afpa.entity.Responsable;
import com.afpa.service.IEnclosService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EnclosServiceImpl implements IEnclosService {

	@Autowired
	IEnclosDao enclosDao;

	@Autowired
	IResponsableDao responsableDao;

	@Autowired
	IAnimalDao animalDao;

	@Override
	public EnclosDto creer(String nom) {
		List<Enclos> enclos = this.enclosDao.findByNom(nom);
		if(enclos != null && enclos.size() != 0) {
			log.error("des enclos ont deja ce nom : " + enclos.get(0));
			return null;
		}

		Enclos enclo = Enclos.builder().nomEnclos(nom).build();

		enclo = this.enclosDao.save(enclo);

		return EnclosDto.builder().nom(enclo.getNomEnclos()).build();
	}

	@Override
	public List<EnclosDto> lister() {
		Iterator<Enclos> iterator = this.enclosDao.findAll().iterator();
		List<EnclosDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Enclos x = iterator.next();
			lst.add(EnclosDto.builder()
					.id(x.getIdEnclos())
					.nom(x.getNomEnclos())
					.build());
		}
		return lst;
	}

	@Override
	public EnclosDto enclosParNom(String n) {
		Iterator<Enclos> iterator = this.enclosDao.findByNom(n).iterator();
		EnclosDto res = null;
		if(iterator.hasNext()) {
			Enclos x = iterator.next();
			res = EnclosDto.builder()
					.id(x.getIdEnclos())
					.nom(x.getNomEnclos())
					.build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Enclos> iterator = this.enclosDao.findByNom(oldName).iterator();
		if(iterator.hasNext()) {
			Enclos x = iterator.next();
			x.setNomEnclos(newName);
			this.enclosDao.save(x);
		}
	}

	//	@Override
	public EnclosDto supprimerParNom(String nom) {
		Enclos r = this.enclosDao.findNom(nom);
		if (r == null) {
			log.error("L'enclos "+nom+" n'existe pas.");
			return null;
		}
		// A tester
		List<Responsable> responsables = this.responsableDao.findResponsableByEnclos(nom);
		if (! (responsables == null || responsables.isEmpty())) {
			log.error("Cet enclos a un responsable : " + responsables.get(0).getNomResponsable());
			return null;
		}
		// A tester
		List<Animal> animaux = this.animalDao.findAnimalByEnclos(nom);
		if (! (animaux == null || animaux.isEmpty())) {
			log.error("Cet enclos a un animal : " + animaux.get(0).getNomAnimal());
			return null;
		}

		this.enclosDao.deleteById(r.getIdEnclos());

		return EnclosDto.builder()
				.id(r.getIdEnclos())
				.nom(r.getNomEnclos())
				.build();
	}


}
