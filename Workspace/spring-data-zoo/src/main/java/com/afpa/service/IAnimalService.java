package com.afpa.service;

import java.util.List;

import com.afpa.dto.AlimentDto;
import com.afpa.dto.AnimalDto;

public interface IAnimalService {

	AnimalDto creer(String nom, String enclos, String aliment);

	List<AnimalDto> lister();

	AnimalDto animalParNom(String n);

	void miseAjourNom(String oldName, String newName);
	
	AnimalDto supprimerParNom(String nom);

}
