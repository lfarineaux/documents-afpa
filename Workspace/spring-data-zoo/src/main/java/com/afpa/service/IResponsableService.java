package com.afpa.service;

import java.util.List;

import com.afpa.dto.ResponsableDto;

public interface IResponsableService {

	ResponsableDto creer(String nom, String nomEnclos);

	List<ResponsableDto> lister();

	ResponsableDto responsableParNom(String n);

	void miseAjourNom(String oldName, String newName);
	
	ResponsableDto supprimerParNom(String nom);

}
