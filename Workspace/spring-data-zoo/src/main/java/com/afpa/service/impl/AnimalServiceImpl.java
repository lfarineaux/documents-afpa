package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IEnclosDao;
import com.afpa.dao.IAlimentDao;
import com.afpa.dao.IAnimalDao;
import com.afpa.dto.AnimalDto;
import com.afpa.entity.Enclos;
import com.afpa.entity.Aliment;
import com.afpa.entity.Animal;
import com.afpa.service.IAnimalService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AnimalServiceImpl implements IAnimalService {

	@Autowired
	IAnimalDao animalDao;

	@Autowired
	IEnclosDao enclosDao;
	
	@Autowired
	IAlimentDao alimentDao;

	@Override
	public AnimalDto creer(String nom, String enclos, String aliment) {
		List<Animal> animals = this.animalDao.findByNom(nom);
		if(animals != null && animals.size() != 0) {
			log.error("des animaux ont deja ce nom : " + animals.get(0));
			return null;
		}
		Enclos e = this.enclosDao.findNom(enclos);
		if(e == null) {
			e = this.enclosDao.save(Enclos.builder().nomEnclos(enclos).build());
		}

		Aliment a = this.alimentDao.findNom(aliment);
		if(a == null) {
			a = this.alimentDao.save(Aliment.builder().nomAliment(aliment).build());
		}
		
//		Animal animal = animals.get(0);
//		animal.getListAliment().add(e)
		
		Set<Aliment> aliments = new HashSet<>(Arrays.asList(a));
		
		Animal animal = Animal.builder().nomAnimal(nom).nomEnclos(e).listAliment(aliments).build();

		animal = this.animalDao.save(animal);

		return AnimalDto.builder().nom(animal.getNomAnimal()).build();
	}

	@Override
	public List<AnimalDto> lister() {
		Iterator<Animal> iterator = this.animalDao.findAll().iterator();
		List<AnimalDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Animal x = iterator.next();
			lst.add(AnimalDto.builder()
					.id(x.getIdAnimal())
					.nom(x.getNomAnimal())
					.build());
		}
		return lst;
	}

	@Override
	public AnimalDto animalParNom(String n) {
		Iterator<Animal> iterator = this.animalDao.findByNom(n).iterator();
		AnimalDto res = null;
		if(iterator.hasNext()) {
			Animal x = iterator.next();
			res = AnimalDto.builder()
					.id(x.getIdAnimal())
					.nom(x.getNomAnimal())
					.build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Animal> iterator = this.animalDao.findByNom(oldName).iterator();
		if(iterator.hasNext()) {
			Animal x = iterator.next();
			x.setNomAnimal(newName);
			this.animalDao.save(x);
		}
	}

	//	@Override
	public AnimalDto supprimerParNom(String nom) {
		Animal r = this.animalDao.findNom(nom);
		if (r == null) {
			log.error("L'animal "+nom+" n'existe pas.");
			return null;
		}

		this.animalDao.deleteById(r.getIdAnimal());

		return AnimalDto.builder()
				.id(r.getIdAnimal())
				.nom(r.getNomAnimal())
				.build();
	}

	
	
	
	public boolean atitrerAlimentAAnimal(String aliment, String animal) {
		
		
		List<Aliment> findByAliment = this.alimentDao.findByNom(aliment);
		if(findByAliment == null || findByAliment.isEmpty()) {
			return false;
		}
		
		List<Animal> findByAnimal = this.animalDao.findByNom(animal);
		if(findByAnimal == null || findByAnimal.isEmpty()) {
			return false;
		}
		
		Aliment aliment2 = findByAliment.get(0);
		Animal animal2 = findByAnimal.get(0);
		
		Set<Animal> animaux = aliment2.getListAnimal();
		if(animaux == null) {
			animaux = new HashSet<>();
			aliment2.setListAnimal(animaux);
		}
		
		animaux.add(animal2);
		
		this.alimentDao.save(aliment2);
		
		return true;
	}
	

}
