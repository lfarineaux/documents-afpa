package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IAlimentDao;
import com.afpa.dao.IAnimalDao;
import com.afpa.dto.AlimentDto;
import com.afpa.entity.Aliment;
import com.afpa.entity.Animal;
import com.afpa.service.IAlimentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlimentServiceImpl implements IAlimentService {
	
	@Autowired
	IAlimentDao alimentDao;
	
	@Autowired
	IAnimalDao animalDao;
	
	@Override
	public AlimentDto creer(String nom) {
		List<Aliment> aliments = this.alimentDao.findByNom(nom);
		if(aliments != null && aliments.size() != 0) {
			log.error("des aliments ont deja ce nom : " + aliments.get(0));
			return null;
		}
		
		Aliment aliment = Aliment.builder().nomAliment(nom).build();
		
		aliment = this.alimentDao.save(aliment);
		
		return AlimentDto.builder().nom(aliment.getNomAliment()).build();
	}

	@Override
	public List<AlimentDto> lister() {
		Iterator<Aliment> iterator = this.alimentDao.findAll().iterator();
		List<AlimentDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Aliment x = iterator.next();
			lst.add(AlimentDto.builder()
					.id(x.getIdAliment())
					.nom(x.getNomAliment())
					.build());
		}
		return lst;
	}

	@Override
	public AlimentDto alimentParNom(String n) {
		Iterator<Aliment> iterator = this.alimentDao.findByNom(n).iterator();
		AlimentDto res = null;
		if(iterator.hasNext()) {
			Aliment x = iterator.next();
			res = AlimentDto.builder()
					.id(x.getIdAliment())
					.nom(x.getNomAliment())
					.build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Aliment> iterator = this.alimentDao.findByNom(oldName).iterator();
		if(iterator.hasNext()) {
			Aliment x = iterator.next();
			x.setNomAliment(newName);
			this.alimentDao.save(x);
		}
	}

//	@Override
	public AlimentDto supprimerParNom(String nom) {
		Aliment r = this.alimentDao.findNom(nom);
		if (r == null) {
			log.error("L'aliment "+nom+" n'existe pas.");
			return null;
		}
		
		List<Animal> animaux = this.animalDao.findAnimalByAliment(nom);
		if (! (animaux == null || animaux.isEmpty())) {
			log.error("Cet aliment a un animal : " + animaux.get(0).getNomAnimal());
			return null;
		}
		
		
		this.alimentDao.deleteById(r.getIdAliment());
		
		return AlimentDto.builder()
				.id(r.getIdAliment())
				.nom(r.getNomAliment())
				.build();
	}
	
	
}
