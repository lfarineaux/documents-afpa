package com.afpa.service;

import java.util.List;

import com.afpa.dto.AlimentDto;

public interface IAlimentService {

	AlimentDto creer(String nom);

	List<AlimentDto> lister();

	AlimentDto alimentParNom(String n);

	void miseAjourNom(String oldName, String newName);
	
	AlimentDto supprimerParNom(String nom);


}
