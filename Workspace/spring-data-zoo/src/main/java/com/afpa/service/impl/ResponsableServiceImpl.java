package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IEnclosDao;
import com.afpa.dao.IResponsableDao;
import com.afpa.dto.ResponsableDto;
import com.afpa.entity.Enclos;
import com.afpa.entity.Responsable;
import com.afpa.service.IResponsableService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ResponsableServiceImpl implements IResponsableService {

	@Autowired
	IResponsableDao responsableDao;

	@Autowired
	IEnclosDao enclosDao;

	@Override
	public ResponsableDto creer(String nom,String nomEnclos) {
		List<Responsable> responsables = this.responsableDao.findByNom(nom);
		if(responsables != null && responsables.size() != 0) {
			log.error("des responsables ont deja ce nom : " + responsables.get(0));
			return null;
		}
		Enclos e = this.enclosDao.findNom(nomEnclos);
		if(e == null) {
			e = this.enclosDao.save(Enclos.builder().nomEnclos(nomEnclos).build());
		}

		Responsable responsable = Responsable.builder().nomResponsable(nom).enclos(e).build();

		responsable = this.responsableDao.save(responsable);

		return ResponsableDto.builder().nom(responsable.getNomResponsable()).build();
	}

	@Override
	public List<ResponsableDto> lister() {
		Iterator<Responsable> iterator = this.responsableDao.findAll().iterator();
		List<ResponsableDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Responsable x = iterator.next();
			lst.add(ResponsableDto.builder()
					.id(x.getIdResponsable())
					.nom(x.getNomResponsable())
					.build());
		}
		return lst;
	}

	@Override
	public ResponsableDto responsableParNom(String n) {
		Iterator<Responsable> iterator = this.responsableDao.findByNom(n).iterator();
		ResponsableDto res = null;
		if(iterator.hasNext()) {
			Responsable x = iterator.next();
			res = ResponsableDto.builder()
					.id(x.getIdResponsable())
					.nom(x.getNomResponsable())
					.build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Responsable> iterator = this.responsableDao.findByNom(oldName).iterator();
		if(iterator.hasNext()) {
			Responsable x = iterator.next();
			x.setNomResponsable(newName);
			this.responsableDao.save(x);
		}
	}

	//	@Override
	public ResponsableDto supprimerParNom(String nom) {
		Responsable r = this.responsableDao.findNom(nom);
		if (r == null) {
			log.error("Le responsable "+nom+" n'existe pas.");
			return null;
		}


		this.responsableDao.deleteById(r.getIdResponsable());

		return ResponsableDto.builder()
				.id(r.getIdResponsable())
				.nom(r.getNomResponsable())
				.build();
	}


}
