package com.afpa.ihm;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.afpa.dto.EnclosDto;
import com.afpa.dto.ResponsableDto;
import com.afpa.service.IAnimalService;
import com.afpa.service.IEnclosService;
import com.afpa.service.impl.AnimalServiceImpl;
import com.afpa.service.impl.EnclosServiceImpl;

public class MenuEnclos {

	//	public static ServiceEnclos se = new ServiceEnclos();

	private static IEnclosService enclosService;

	public static void init(AnnotationConfigApplicationContext ctx) {
		enclosService = ctx.getBean(IEnclosService.class);
	}
	
	public static void menu() {

//		IEnclosService enclosService = new EnclosServiceImpl();
		
//		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//		ctx.register(SpringConfiguration.class);
//		ctx.refresh();
//
//		IEnclosService enclosService = ctx.getBean(IEnclosService.class);

		Scanner sc = new Scanner(System.in);
		boolean	sousfin = true;

		System.out.println("3- Menu Enclos");
		sousfin = true;

		while (sousfin) {

			System.out.println();
			System.out.println("      *************************");
			System.out.println("      *    SOUS-MENU ENCLOS   *");
			System.out.println("      *************************");
			System.out.println();
			System.out.println("0- Retourner au menu principal");
			System.out.println("1- Creer un Enclos");
			System.out.println("2- Lister les enclos");
			System.out.println("3- Mettre a jour un enclos");
			System.out.println("4- Supprimer un enclos");
			System.out.print("-> ");
			String sousmenu = Clavier.lireTxt(sc, "");
			System.out.println();

			switch (sousmenu) {
			case "0":
				System.out.println("0 - Retour au menu principal");
				sousfin = false;
				break;

			case "1":
				System.out.println("1- Creer un enclos");
				System.out.print("Saisir le nom du responsable : ");
				String nom = Clavier.lireTxt(sc, "");

				System.out.print("Saisir le nom de l'enclos : ");
				String nomEnclos = Clavier.lireTxt(sc, "");

				EnclosDto create = enclosService.creer(nom);
				if (create == null) {
					System.out.println("erreur creation");
				} else {
					System.out.println("l'enclos " + create.getNom() + " a �t� cr��");
				}
				break;

			case "2":
				System.out.println("2- Lister les enclos");
				List<EnclosDto> lst = enclosService.lister();
				System.out.println("Liste des enclos : ");
				lst.forEach(System.out::println);
				break;
				
			case "3":	
				System.out.println("3- Mettre a jour un enclos");
				System.out.print("Saisir le nom de l'enclos a mettre a jour ");
				String oldName = Clavier.lireTxt(sc, "");
				System.out.print("Saisir le nouveau nom de l'enclos  ");
				String newName = Clavier.lireTxt(sc, "");
				enclosService.miseAjourNom(oldName, newName);
				break;

			case "4":
				System.out.println("4- Supprimer un enclos");
				System.out.print("Saisir le nom de l'enclos a supprimer ");
				nom = Clavier.lireTxt(sc, "");
				EnclosDto delete = enclosService.supprimerParNom(nom);
				if (delete == null) {
					System.out.println("erreur suppression");
				} else {
					System.out.println("l'enclos " + nom + " a �t� supprim�");
				}
				break;

			default:
				System.out.println("Mauvaise saisie");
				continue;

			}
			break;
		}

	}

}
