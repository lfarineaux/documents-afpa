package com.afpa.ihm;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.afpa.dto.ResponsableDto;
import com.afpa.service.IAnimalService;
import com.afpa.service.IResponsableService;
import com.afpa.service.impl.ResponsableServiceImpl;

public class MenuResponsable {

	private static IResponsableService responsableService;

	public static void init(AnnotationConfigApplicationContext ctx) {
		responsableService = ctx.getBean(IResponsableService.class);
	}
	
	
	public static void menu() {

//		IResponsableService responsableService = new ResponsableServiceImpl() ;
		
//		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//		ctx.register(SpringConfiguration.class);
//		ctx.refresh();
//
//		IResponsableService responsableService = ctx.getBean(IResponsableService.class);

		Scanner sc = new Scanner(System.in);
		boolean sousfin = true;

		System.out.println("4- Menu Responsable");
		sousfin = true;

		while (sousfin) {

			System.out.println();
			System.out.println("      *****************************");
			System.out.println("      *    SOUS-MENU RESPONSABLE  *");
			System.out.println("      *****************************");
			System.out.println();
			System.out.println("0- Retourner au menu principal");
			System.out.println("1- Creer un Responsable");
			System.out.println("2- Lister les Responsables");
			System.out.println("3- Mettre a jour le nom d un Responsable");
			System.out.println("4- Supprimer un Responsable");
			System.out.print("-> ");
			String sousmenu = Clavier.lireTxt(sc, "");

			System.out.println();

			switch (sousmenu) {
			case "0":
				System.out.println("0 - Retour au menu principal");
				sousfin = false;
				break;

			case "1":
				System.out.println("1- Creer un Responsable");
				System.out.print("Saisir le nom du responsable : ");
				String nom = Clavier.lireTxt(sc, "");
				
				System.out.print("Saisir le nom de l'enclos : ");
				String nomEnclos = Clavier.lireTxt(sc, "");
				
				ResponsableDto create = responsableService.creer(nom,nomEnclos);
				if (create == null) {
					System.out.println("erreur creation");
				} else {
					System.out.println("le responsable " + create.getNom() + " a �t� cr��");
				}
				break;

			case "2":
				System.out.println("2- Lister les Responsables");
				List<ResponsableDto> lst = responsableService.lister();
				System.out.println("Liste des responsables : ");
				lst.forEach(System.out::println);
				break;

			case "3":
				System.out.println("3- Mettre a jour le nom d un Responsable");
				System.out.print("Saisir le nom du responsable a mettre a jour ");
				String oldName = Clavier.lireTxt(sc, "");
				System.out.print("Saisir le nouveau nom du responsable  ");
				String newName = Clavier.lireTxt(sc, "");
				responsableService.miseAjourNom(oldName, newName);
				break;

			case "4":
				System.out.println("4- Supprimer une Responsable");
				System.out.print("Saisir le nom du responsable a supprimer ");
				nom = Clavier.lireTxt(sc, "");
				ResponsableDto delete = responsableService.supprimerParNom(nom);
				if (delete == null) {
					System.out.println("erreur suppression");
				} else {
					System.out.println("le responsable " + nom + " a �t� supprim�");
				}

				break;

			default:
				System.out.println("Mauvaise saisie");
				continue;

			}
			break;
		}
	}

}
