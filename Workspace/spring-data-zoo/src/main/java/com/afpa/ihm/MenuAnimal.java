package com.afpa.ihm;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.afpa.dao.IAlimentDao;
import com.afpa.dto.AnimalDto;
import com.afpa.dto.ResponsableDto;
import com.afpa.entity.Aliment;
import com.afpa.service.IAnimalService;
import com.afpa.service.impl.AnimalServiceImpl;

public class MenuAnimal {

	private static IAnimalService animalService;

	public static void init(AnnotationConfigApplicationContext ctx) {
		animalService = ctx.getBean(IAnimalService.class);
	}

	public static void menu() {
		Scanner sc = new Scanner(System.in);
		boolean sousfin = true;

		System.out.println("2- Menu Animal");
		sousfin = true;

		while (sousfin) {

			System.out.println();
			System.out.println("      *************************");
			System.out.println("      *    SOUS-MENU ANIMAL   *");
			System.out.println("      *************************");
			System.out.println();
			System.out.println("0- Retourner au menu principal");
			System.out.println("1- Creer un animal");
			System.out.println("2- Lister les animaux");
			System.out.println("3- Mettre a jour un animal");
			System.out.println("4- Supprimer un animal");
			System.out.print("-> ");
			String sousmenu = Clavier.lireTxt(sc, "");

			switch (sousmenu) {
			case "0":
				System.out.println("0 - Retour au menu principal");
				sousfin = false;
				break;

			case "1":
				System.out.println("1- Creer un animal");
				System.out.print("Saisir le nom de l'animal : ");
				String nom = Clavier.lireTxt(sc, "");
				System.out.print("Saisir le nom de l'enclos : ");
				String enclos = Clavier.lireTxt(sc, "");
				System.out.print("Saisir le nom de l'aliment : ");
				String aliment = Clavier.lireTxt(sc, "");

				boolean listAliment = true;
				Set<Aliment> setAliment;
				// manque l'ajout d'une liste d'aliments � l'animal
				// while (listAliment) {
				// System.out.print("Saisir le nom de l'aliment : ");
				// String unAliment = Clavier.lireTxt(sc, "");
				// // IAlimentDao alimentDao;
				//
				// // setAliment.add(;)
				// System.out.print("Voulez-vous saisir une autre aliment ? (O/N) ");
				// String rep = Clavier.lireTxt(sc, "");
				// switch (rep) {
				// case "O" :
				// break;
				// case "N" :
				// listAliment=false;
				// break;
				//
				//
				// }
				// }
				AnimalDto create = animalService.creer(nom, enclos, aliment);
				if (create == null) {
					System.out.println("erreur creation");
				} else {
					System.out.println("l'animal " + create.getNom() + " a �t� cr��");
				}
				break;

			case "2":
				System.out.println("2- Lister les animaux");
				List<AnimalDto> lst = animalService.lister();
				System.out.println("Liste des animaux : ");
				lst.forEach(System.out::println);
				break;

			case "3":
				System.out.println("3- Mettre a jour un animal");
				System.out.print("Saisir le nom de l'animal a mettre a jour ");
				String oldName = Clavier.lireTxt(sc, "");
				System.out.print("Saisir le nouveau nom de l'animal  ");
				String newName = Clavier.lireTxt(sc, "");
				animalService.miseAjourNom(oldName, newName);
				break;

			case "4":
				System.out.println("4- Supprimer un animal");
				System.out.print("Saisir le nom du responsable a supprimer ");
				nom = Clavier.lireTxt(sc, "");
				AnimalDto delete = animalService.supprimerParNom(nom);
				if (delete == null) {
					System.out.println("erreur suppression");
				} else {
					System.out.println("le responsable " + nom + " a �t� supprim�");
				}
				break;

			default:
				System.out.println("Mauvaise saisie");
				continue;

			}
			break;
		}
	}

}
