package com.afpa.ihm;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.afpa.dto.AlimentDto;
import com.afpa.service.IAlimentService;
import com.afpa.service.IAnimalService;

public class MenuAliment {

	private static IAlimentService alimentService;

	public static void init(AnnotationConfigApplicationContext ctx) {
		alimentService = ctx.getBean(IAlimentService.class);
	}
	
	public static void menu() {
	
//		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//		ctx.register(SpringConfiguration.class);
//		ctx.refresh();

//		IAlimentService alimentService = ctx.getBean(IAlimentService.class);
		
		
		Scanner sc = new Scanner(System.in);
		boolean	sousfin = true;

		System.out.println("1- Menu Aliment");

		while (sousfin) {

			System.out.println();
			System.out.println("      **************************");
			System.out.println("      *    SOUS-MENU ALIMENT   *");
			System.out.println("      **************************");
			System.out.println();
			System.out.println("0- Retourner au menu principal");
			System.out.println("1- Creer un aliment");
			System.out.println("2- Lister les aliments");
			System.out.println("3- Mettre a jour le nom d un aliment");
			System.out.println("4- Supprimer un aliment");

			System.out.print("-> ");
			String sousmenu = Clavier.lireTxt(sc, "");

			switch (sousmenu) {
			case "0":
				System.out.println("0 - Retour au menu principal");
				sousfin = false;
				break;

			case "1":
				System.out.println("1- Creer un aliment");
				System.out.print("Saisir le nom d un aliment : ");
				String nom = Clavier.lireTxt(sc, "");
				AlimentDto create = alimentService.creer(nom);
				if (create == null) {
					System.out.println("erreur creation");
				} else {
					System.out.println("l'aliment " + create.getNom() + " a �t� cr��");
				}
				break;

			case "2":
				System.out.println("2- Lister les aliments");
				List<AlimentDto> lst = alimentService.lister();
				System.out.println("Liste des aliments : ");
				lst.forEach(System.out::println);
				break;

			case "3":
				System.out.println("3- Mettre a jour le nom d un aliment");
				System.out.print("Saisir le nom de l'aliment a mettre a jour ");
				String oldName = Clavier.lireTxt(sc, "");
				System.out.print("Saisir le nouveau nom de l'aliment ");
				String newName = Clavier.lireTxt(sc, "");
				alimentService.miseAjourNom(oldName, newName);
				break;

			case "4":
				System.out.println("4- Supprimer un aliment");
				System.out.print("Saisir le nom de l'aliment a supprimer ");
				nom = Clavier.lireTxt(sc, "");
				AlimentDto delete = alimentService.supprimerParNom(nom);
				if (delete == null) {
					System.out.println("erreur suppression");
				} else {
					System.out.println("l'aliment " + nom + " a �t� supprim�");
				}
				break;

			default:
				System.out.println("Mauvaise saisie");
				continue;

			}
			break;
		}
	}
}
