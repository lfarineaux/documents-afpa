package com.afpa.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
//@Builder
@Table(name = "Enclos")
@NamedQueries({
	@NamedQuery(name = "Enclos.findByNom",query = "select e from Enclos e where e.nomEnclos= :nomParam"),
	@NamedQuery(name = "Enclos.findNom",query = "select e from Enclos e where e.nomEnclos= :nomParam")
})
public class Enclos {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Enclos", length = 3, nullable = false)
	private int idEnclos;

	@Column(name = "nom_enclos", length = 25, nullable = false, unique = true)
	private String nomEnclos;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "nomEnclos")
	private Set<Animal> listAnimal;

}
