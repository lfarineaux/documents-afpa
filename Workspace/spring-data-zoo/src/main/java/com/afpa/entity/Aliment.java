package com.afpa.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "Aliment")
@NamedQueries({
	@NamedQuery(name = "Aliment.findByNom",query = "select e from Aliment e where e.nomAliment= :nomParam"),
	@NamedQuery(name = "Aliment.findNom",query = "select e from Aliment e where e.nomAliment= :nomParam"),
	//	@NamedQuery(name = "Aliment.findAll",query = "select e from Aliment e")
})
public class Aliment {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Aliment", length = 3, nullable = false)
	private int idAliment;

	@Column(name = "nom_aliment", length = 25, nullable = false, unique = true)
	private String nomAliment;

	@ManyToMany(fetch = FetchType.EAGER, 
			mappedBy = "listAliment",
			cascade = CascadeType.MERGE)
	private Set<Animal> listAnimal;

}