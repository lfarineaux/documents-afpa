package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Aliment;
import com.afpa.entity.Responsable;

@Repository
public interface IAlimentDao extends CrudRepository<Aliment,Integer> {

	List<Aliment> findByNom(@Param("nomParam")String p);
	
	Aliment findNom (@Param("nomParam")String p);
	
	List<Aliment> findAll();

	
	
//	List<Aliment> findByAliment(String aliment);
	
	

//	@Query("select e from Employer e order by e.nom asc")
//	List<Employer> findAllByOrderByNomDesc();

}
