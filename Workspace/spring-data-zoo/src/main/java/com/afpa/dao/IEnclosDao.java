package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Animal;
import com.afpa.entity.Enclos;
import com.afpa.entity.Responsable;

@Repository
public interface IEnclosDao extends CrudRepository<Enclos,Integer> {

	List<Enclos> findByNom(@Param("nomParam")String p);
	
	Enclos findNom (@Param("nomParam")String p);
	
	List<Enclos> findAll();
	
	

//	@Query("select e from Employer e order by e.nom asc")
//	List<Employer> findAllByOrderByNomDesc();

}
