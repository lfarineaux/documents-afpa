package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Responsable;

@Repository
public interface IResponsableDao extends CrudRepository<Responsable,Integer> {

	List<Responsable> findByNom(@Param("nomParam")String p);
	
	Responsable findNom (@Param("nomParam")String p);
	
	List<Responsable> findAll();

	List<Responsable> findResponsableByEnclos(@Param("nomParam")String nom);

	
	

//	@Query("select e from Employer e order by e.nom asc")
//	List<Employer> findAllByOrderByNomDesc();

}
