package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Animal;
import com.afpa.entity.Responsable;

@Repository
public interface IAnimalDao extends CrudRepository<Animal,Integer> {

	List<Animal> findByNom(@Param("nomParam")String p);
	
	Animal findNom (@Param("nomParam")String p);
	
	List<Animal> findAll();

	List<Animal> findAnimalByEnclos(@Param("nomParam")String nom);

	List<Animal> findAnimalByAliment(@Param("nomParam")String nom);

	

	
	
//	List<Animal> findByAnimal(String animal);

//	@Query("select e from Employer e order by e.nom asc")
//	List<Employer> findAllByOrderByNomDesc();

}
