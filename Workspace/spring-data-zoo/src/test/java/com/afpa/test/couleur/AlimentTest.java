//package com.afpa.test.aliment;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestMethodOrder;
//
//import com.afpa.dto.AlimentDto;
//import com.afpa.dto.reponse.CreationReponseDto;
//import com.afpa.dto.reponse.ElementSimpleDto;
//import com.afpa.dto.reponse.ReponseDto;
//import com.afpa.dto.reponse.Status;
//import com.afpa.service.IAlimentService;
//import com.afpa.service.impl.AlimentServiceImpl;
//
//import lombok.extern.slf4j.Slf4j;
//
//@Slf4j
//@TestMethodOrder(OrderAnnotation.class)
//public class AlimentTest {
//	
//	static IAlimentService alimentService;
//	
//	@BeforeAll
//	public static void init() {
//		alimentService = new AlimentServiceImpl();
//	}
//	
//	static int alimentCreeCode;
//	static String alimentCreeLabel;
//	
//	@Test
//	@Order(1)
//	void creation() {
//		alimentCreeLabel = "rouge"+System.currentTimeMillis();
//		AlimentDto reponse = alimentService.creer(alimentCreeLabel);
//		log.debug(reponse.toString());
//		assertNotNull(reponse);
//		assertEquals(reponse.getCode(), Status.OK);
//		assertNotNull(reponse.getContenu());
//		alimentCreeCode =((CreationReponseDto) reponse.getContenu()).getCode();
//	}
//	
//	@Test
//	@Order(2)
//	void recherche() {
//		ReponseDto reponse = alimentService.chercherAlimentParCode(alimentCreeCode);
//		log.debug(reponse.toString());
//		assertNotNull(reponse);
//		assertEquals(Status.OK,reponse.getCode());
//		assertNotNull(reponse.getContenu());
////		idAlimentCree =((CreationReponseDto) reponse.getContenu()).getId();
//	}
//	
//	@Test
//	@Order(3)
//	void modificatio() {
//		String nouveauLabel = "rouge"+System.currentTimeMillis()+"M";
//		ReponseDto reponse = alimentService.mettreAjour(alimentCreeLabel,nouveauLabel);
//		assertNotNull(reponse);
//		assertEquals(Status.OK,reponse.getCode());
//		assertNotNull(reponse.getContenu());
//		ElementSimpleDto resDto = (ElementSimpleDto)reponse.getContenu();
//		assertEquals(nouveauLabel,resDto.getLabel());
//	}
//	
//}
