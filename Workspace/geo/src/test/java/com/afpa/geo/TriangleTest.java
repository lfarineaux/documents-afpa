package com.afpa.geo;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.commons.math3.util.Precision;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TriangleTest {
	static Point ptx;
	static Point pty;
	static Point ptz;
	static Point pta;
	static Point ptb;
	static Point ptc;
	static Triangle tx;
	static Triangle ta;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ptx = new Point ("x1",0,0);
		pty = new Point ("y1",10,0);
		ptz = new Point ("z1",5,10);
		pta = new Point ("a1",0,0);
		ptb = new Point ("b1",5,0);
		ptc = new Point ("c1",2,5);
		tx = new Triangle(ptx, pty, ptz);
		ta = new Triangle(pta, ptb, ptc);

	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

//	@Test
//	void testSortTriangle() {
//		try {
//			Triangle t1 = Triangle.getListTriangles().get(0);
//			Triangle t2 = Triangle.getListTriangles().get(1);
//			Triangle.sortTriangle(Triangle.getListTriangles());
//			assertTrue(Triangle.getListTriangles().get(0).getSurf() < Triangle.getListTriangles().get(1).getSurf());
//
//		} catch (Exception e) {
//			fail("Faux");
//		}
//
//	}

	@Test
	void testSurfaceT() {
		try {
			assertTrue(Precision.equals(Triangle.surfaceT(ptx, pty, ptz), 50, 0.1));			
		}catch (Exception e) {
			fail("Faux");
		}
	}

}
