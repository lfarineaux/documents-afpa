package com.afpa.geo;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CircleTest {
	static double rayon;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		rayon=5;
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSortCircle() {
		
	}

	@Test
	void testSurfaceC() {
		try {
			assertTrue(Precision.equals(Circle.surfaceC(rayon), 78.5, 0.1));			
		}catch (Exception e) {
			fail("Faux");
		}
	}

}
