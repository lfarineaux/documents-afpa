package com.afpa.geo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class PointTest {
	static Point ptx;
	static Point pty;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ptx = new Point ("x1",6,8);
		pty = new Point ("y1",3,4);
		Point.getListPoints().add(ptx);
		Point.getListPoints().add(pty);

	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}


	@Test
	void testSortPoint() {
		try {
			Point pt1 = Point.getListPoints().get(0);
			Point pt2 = Point.getListPoints().get(1);
			Point.sortPoint(Point.getListPoints());
			System.out.println(pt1.getNom()+" "+pt2.getNom());
			assertTrue(Point.getListPoints().get(0).getNom().equals("y1") && (Point.getListPoints().get(1).getNom().equals("x1")));
		}  catch (Exception e) {
			fail("Faux");
		}
	}

	@Test
	void testNorme() {
		try {
			assertEquals(5,Point.norme(3, 4));
		} catch (Exception e) {
			fail("Faux");
		}
	}

	@Test
	void testLongueur() {
		try {
			assertEquals(5,Point.longueur(ptx, pty));
		} catch (Exception e) {
			fail("Faux");
		}
	}

	@Test
	void testDistance() {
		try {
			assertEquals(5,Point.distance(pty));
		} catch (Exception e) {
			fail("Faux");
		}
	}

}
