package com.afpa.geo;

import java.util.ArrayList;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;


/**
* La classe Programme modélise un Cercle
*
* 
* @author lf
*/

public class Circle {

	static Logger monLogger = LoggerFactory.getLogger(Circle.class);
	@Getter
	@Setter
	private Point centre;
	@Getter
	@Setter
	private double rayon;
	@Getter
	private final static double PI = 3.1415;
	@Getter
	@Setter
	private double surf;
	@Getter
	@Setter
	private static ArrayList <Circle> listCircles =new ArrayList<>();


	/**
	 * Constructeur pour la classe 
	 *
	 */
	
	public Circle (Point c, Double r) {
		this.centre=c;
		this.rayon=r;
		this.surf=surfaceC(r);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Circle other = (Circle) obj;
		if (Double.doubleToLongBits(PI) != Double.doubleToLongBits(other.PI))
			return false;
		if (centre == null) {
			if (other.centre != null)
				return false;
		} else if (!centre.equals(other.centre))
			return false;
		if (Double.doubleToLongBits(rayon) != Double.doubleToLongBits(other.rayon))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "[ Rayon = " + rayon+" , Centre = " + centre +" , Surface = "+surf+" ]";
	}


	public static void createCircle () throws CircleExistant {
		Scanner sc = new Scanner(System.in);
		ArrayList <Point> listPointsTmp=Point.getListPoints();
		Point pt = new Point ();

		System.out.print("** CERCLE **");
		System.out.println();
		System.out.print("* CREATION D'UN CERCLE *");
		System.out.println();

		boolean oldpt=false;
		while (!oldpt) {
			System.out.print("Saisir le nom du point pour le centre (une lettre et un chiffre ) : ");
			String nom = sc.next();
			sc.nextLine();

			for (Point p : listPointsTmp) {
				if (p.getNom().equals(nom)) {
					oldpt=true;
					pt=p;
					monLogger.info("Création du centre du cercle "+p);
				}
			}
			if (!oldpt) {
				monLogger.error("Le point n'existe pas");
			}

		}
		System.out.print("Saisir la valeur du rayon : ");
		double ray = sc.nextDouble();
		sc.nextLine();

		boolean oldcir=false;

		if (listCircles.size()==0) {
			Circle circ = new Circle (pt,ray);
			listCircles.add(circ);
			monLogger.info("Création du cercle "+circ);
		}
		else if (listCircles.size()>=1) {
			for (Circle c : listCircles) {
				if (c.getCentre().equals(pt) && (c.getRayon()==ray)){
					oldcir=true;
					monLogger.error("Le cercle existe déjà.");
					throw new CircleExistant();
				}
			}
			if (!oldcir) {
				Circle circ = new Circle (pt,ray);
				listCircles.add(circ);
				monLogger.info("Création du cercle "+circ);
			}
		}
	}

	public static void sortCircle(ArrayList<Circle> lc) {

		int tail=lc.size();
		boolean permut=false;
		if (tail>=2) {
			while (!permut) {
				permut=true;

				for (int i=0; i<tail-1;i++) {
					Circle c1 = lc.get(i);
					Circle c2 = lc.get(i+1);
					if (c2.surf<c1.surf) {
						Circle c3=c2;
						c2=c1;
						c1=c3;
						lc.set(i,c1);
						lc.set(i+1,c2);

						permut=false;
					}
					else if (c2.surf==c1.surf) {
						double d1=Point.distance(c1.getCentre());
						double d2=Point.distance(c2.getCentre());

						if (d2>d1) {
							Circle c3=c2;
							c2=c1;
							c1=c3;
							lc.set(i,c1);
							lc.set(i+1,c2);

							permut=false;

						}
					}
				}
			}
		}
	}



	public static void displayCircle () {
		monLogger.debug("Liste des cercles");
		for (Circle c : listCircles) {
			System.out.println(c);
		}

	}


	public static double surfaceC (double r) {
		double surf=0;
		return surf = PI*(Math.pow((r),2));

	}




}
