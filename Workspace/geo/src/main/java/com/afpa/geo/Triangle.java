package com.afpa.geo;

import java.util.ArrayList;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;


/**
* La classe Programme modélise un Triangle
*
* 
* @author lf
*/


public class Triangle {

	static Logger monLogger = LoggerFactory.getLogger(Triangle.class);
	@Getter
	@Setter
	private Point a;
	@Getter
	@Setter
	private Point b;
	@Getter
	@Setter
	private Point c;
	@Getter
	@Setter
	private double surf;
	@Getter
	@Setter
	private static ArrayList <Triangle> listTriangles =new ArrayList<>();


	/**
	 * Constructeur pour la classe 
	 *
	 */
	
	public Triangle (Point a, Point b, Point c) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.surf=surfaceT(a,b,c);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Triangle other = (Triangle) obj;
		if (a == null) {
			if (other.a != null)
				return false;
		} else if (!a.equals(other.a))
			return false;
		if (b == null) {
			if (other.b != null)
				return false;
		} else if (!b.equals(other.b))
			return false;
		if (c == null) {
			if (other.c != null)
				return false;
		} else if (!c.equals(other.c))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[ " + a + ", \n" + b + ", \n" + c + "]\n"+"Surface "+surf;
	}

	public static void createTriangle () throws TriangleExistant {
		Scanner sc = new Scanner(System.in);
		ArrayList <Point> listPointsTmp=Point.getListPoints();
		Point ptA = new Point ();
		Point ptB = new Point ();
		Point ptC = new Point ();

		System.out.print("** TRIANGLE **");
		System.out.println();
		System.out.print("* CREATION D'UN TRIANGLE *");
		System.out.println();

		boolean oldpt=false;
		while (!oldpt) {
			System.out.print("Saisir le nom du premier point (une lettre et un chiffre) : ");
			String nomA = sc.next(); 
			sc.nextLine();
  
			for (Point p : listPointsTmp) {
				if (p.getNom().equals(nomA)) {
					oldpt=true;
					ptA=p;
					monLogger.info("Création du premier point du triangle "+p);
				}
			}
			if (!oldpt) {
				monLogger.error("Le point n'existe pas");
			}
		}

		oldpt=false;
		while (!oldpt) {
			System.out.print("Saisir le nom du deuxième point (une lettre et un chiffre) : ");
			String nomB = sc.next();
			sc.nextLine();

			for (Point p : listPointsTmp) {
				if (p.getNom().equals(nomB)) {
					oldpt=true;
					ptB=p;
					monLogger.info("Création du deuxième point du triangle "+p);
				}
			}
			if (!oldpt) {
				monLogger.error("Le point n'existe pas");
			}
		}

		oldpt=false;
		while (!oldpt) {
			System.out.print("Saisir le nom du troisième point (une lettre et un chiffre ) : ");
			String nomC = sc.next();
			sc.nextLine();

			for (Point p : listPointsTmp) {
				if (p.getNom().equals(nomC)) {
					oldpt=true;
					ptC=p;
					monLogger.info("Création du troisième point du triangle "+p);
				}
			}
			if (!oldpt) {
				monLogger.error("Le point n'existe pas");
			}
		}

		boolean oldtri=false;

		if (listTriangles.size()==0) {
			Triangle triang = new Triangle (ptA,ptB,ptC);
			listTriangles.add(triang);
			monLogger.info("Création du triangle "+triang);
		}

		else if (listTriangles.size()>=1) {
			for (Triangle t : listTriangles) {
				if (((t.getA().equals(ptA)) ||(t.getA().equals(ptB)) || (t.getA().equals(ptC))) 
						&& ((t.getB().equals(ptA)) ||(t.getB().equals(ptB)) ||(t.getB().equals(ptC))) 
						&&((t.getC().equals(ptA)) ||(t.getC().equals(ptB)) || (t.getC().equals(ptC)))) {
					oldtri=true;
					monLogger.error("Le triangle existe déjà.");
					throw new TriangleExistant();
				}
			}
			if (!oldtri) {
				Triangle triang = new Triangle (ptA,ptB,ptC);
				listTriangles.add(triang);
				monLogger.info("Création du triangle "+triang);
			}
		}
	}

	
	public static void sortTriangle(ArrayList<Triangle> lt) {

		int tail=lt.size();
		boolean permut=false;
		if (tail>=2) {
			while (!permut) {
				permut=true;

				for (int i=0; i<tail-1;i++) {
					Triangle t1 = lt.get(i);
					Triangle t2 = lt.get(i+1);
					if (t2.surf<t1.surf) {
						Triangle t3=t2;
						t2=t1;
						t1=t3;
						lt.set(i,t1);
						lt.set(i+1,t2);

						permut=false;
					}
				}
			}
		}
	}


	public static void displayTriangle () {
		monLogger.debug("Liste des triangles");
		for (Triangle t : listTriangles) {
			System.out.println(t);
		}
	}
	
	public static double surfaceT (Point a, Point b, Point c) {

		double surf=0;
		double dX=Point.longueur(a,b);
		double dY=Point.longueur(b,c);
		double dZ=Point.longueur(c,a);
		double dp=((dX+dY+dZ)/2);

		surf = Math.sqrt((dp*(dp-dX)*(dp-dY)*(dp-dZ)));
		return surf;
	}

}
