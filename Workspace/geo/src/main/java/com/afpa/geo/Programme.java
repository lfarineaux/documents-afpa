package com.afpa.geo;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* La classe Programme contient la méthode main
*
* 
* @author lf
*/


public class Programme {
	
	public static void main(String[] args)  {
		Scanner sc = new Scanner(System.in);
		String menu = "";
		boolean fin = true;
  

		/** @param
		 *  menu : numéro saisi dans le menu 
		 *  fin : arrêt du programme
		 */ 

		while (fin) {
			boolean numMenu=false;
			
			while (!numMenu) {
			System.out.println();
			System.out.println("      **************");
			System.out.println("      *    MENU    *");
			System.out.println("      **************");
			System.out.println();
			System.out.println(" 1. Créer un point");
			System.out.println(" 2. Lister les points");
			System.out.println(" 3. Créer un triangle");
			System.out.println(" 4. Lister les triangles");
			System.out.println(" 5. Créer un cercle");
			System.out.println(" 6. Lister les cercles");
			System.out.println(" 0. Arrêter le programme");
			System.out.println();
			System.out.print("   ->  ");
			menu = sc.next();
			sc.nextLine();
			System.out.println();

			
			if (menu.length()==1 && Character.isDigit(menu.charAt(0))) {
				numMenu=true;
			}
			else {
				System.out.println("Saisir un chiffre entre 0 et 6.");
			}
			 
			
			}
			
			switch (menu) {
			case "1":
				System.out.println(" 1. Créer un point");
				try {
					Point.createPoint();
				} catch (PointExistant  e) {
					System.out.println("Saisir un autre point.");
				}
				break;

			case "2":
				System.out.println(" 2. Lister les points");
				Point.sortPoint(Point.getListPoints());
				Point.displayPoint();
				break;

			case "3":
				System.out.println(" 3. Créer un triangle");
				try {
					Triangle.createTriangle();
				} catch (TriangleExistant e) {
					System.out.println("Saisir un autre triangle.");
				}
				break;

			case "4":
				System.out.println(" 4. Lister les triangles");
				Triangle.sortTriangle(Triangle.getListTriangles());
				Triangle.displayTriangle();
				break;	

			case "5":
				System.out.println(" 5. Créer un cercle");
				try {
					Circle.createCircle();
				} catch (CircleExistant e) {
					System.out.println("Saisir un autre cercle.");
				}
				break;	

			case "6":
				System.out.println(" 6. Lister les cercles");
				Circle.sortCircle(Circle.getListCircles());
				Circle.displayCircle();
				break;	

			case "0" :	
				System.out.println("Arrêt du programme");
				fin=false;
				break;	
			default:
				System.out.println("Saisir un chiffre entre 0 et 6.");
				break;
			}	

		}

		sc.close();
	}
}
