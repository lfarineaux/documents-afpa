package com.afpa.geo;

import java.util.ArrayList;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;

/**
 * La classe Programme modélise un Point
 *
 * 
 * @author lf
 */

public class Point {

	static Logger monLogger = LoggerFactory.getLogger(Point.class);
	@Getter
	@Setter
	private String nom;
	@Getter
	@Setter
	private double abscisse;
	@Getter
	@Setter
	private double ordonnee;
	@Getter
	@Setter
	private double dist;
	@Getter
	@Setter
	private static ArrayList<Point> listPoints = new ArrayList<>();

	/**
	 * Constructeurs pour la classe
	 *
	 */

	public Point() {

	}

	public Point(String n, double a, double o) {
		this.nom = n;
		this.abscisse = a;
		this.ordonnee = o;
		this.dist = norme(a, o);
	}

	@Override
	public String toString() {
		return "Point " + nom + " (" + abscisse + ", " + ordonnee + ") - distance/origine = " + dist;
	}

	public static void createPoint() throws PointExistant {
		Scanner sc = new Scanner(System.in);
		String nom = "";

		System.out.print("** POINT **");
		System.out.println();
		System.out.print("* CREATION D'UN POINT *");
		System.out.println();

		boolean nompt = false;
		while (!nompt) {

			System.out.print("Saisir le nom du point (une lettre et un chiffre) : ");
			nom = sc.next();
			sc.nextLine();

			if (nom.length() == 2 && !Character.isDigit(nom.charAt(0)) && Character.isDigit(nom.charAt(1))) {
				nompt = true;
			} else {
				monLogger.error("Saisir une lettre et un chiffre.");
			}
		}

		System.out.println("Saisir les coordonnees du point " + nom);
		System.out.print("Abscisse : ");
		double ab = sc.nextDouble();

		System.out.print("Ordonnee : ");
		double or = sc.nextDouble();
		System.out.println();

		Point pt = new Point(nom, ab, or);

		boolean oldpt = false;

		if (listPoints.size() == 0) {
			listPoints.add(pt);
			monLogger.info("Création du point " + pt);
		} else if (listPoints.size() >= 1) {
			for (Point p : listPoints) {
				if (pt.getNom().equals(p.getNom())) {
					oldpt = true;
					monLogger.error("Le point existe déjà.");
					throw new PointExistant();
				}
			}
			if (!oldpt) {
				listPoints.add(pt);
				monLogger.info("Création du point " + pt);
			}
		}
	}

	public static void sortPoint(ArrayList<Point> lp) {
		int tail = lp.size();
		boolean permut = false;
		if (tail >= 2) {
			while (!permut) {
				permut = true;

				for (int i = 0; i < tail - 1; i++) {
					Point pt1 = lp.get(i);
					Point pt2 = lp.get(i + 1);
					if (pt2.dist < pt1.dist) {
						Point pt3 = pt2;
						pt2 = pt1;
						pt1 = pt3;
						lp.set(i, pt1);
						lp.set(i + 1, pt2);

						permut = false;
					} else if (pt2.dist == pt1.dist) {
						int z = (pt1.nom).compareTo(pt2.nom);
						if (z > 0) {
							Point pt3 = pt2;
							pt2 = pt1;
							pt1 = pt3;
							lp.set(i, pt1);
							lp.set(i + 1, pt2);

							permut = false;

						}
					}
				}
			}
		}
	}

	public static void displayPoint() {

		monLogger.debug("Liste des points");
		for (Point p : listPoints) {
			System.out.println(p);
		}

	}

	public static double norme(double a, double b) {
		return (double) Math.sqrt(a * a + b * b);
	}

	public static double longueur(Point x, Point y) {
		double di = Math.sqrt(Math.pow((x.abscisse - y.abscisse), 2) + (Math.pow((x.ordonnee - y.ordonnee), 2)));
		return di;
	}

	public static double distance(Point x) {
		return (double) Math.sqrt(x.abscisse * x.abscisse + x.ordonnee * x.ordonnee);
	}

}
