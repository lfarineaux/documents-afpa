<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>


<!DOCTYPE html>

<html>
<head>
<link href="static/css/style.css" rel="stylesheet">
<link href="static/fontawesome/css/all.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body style="background-color: #eaf2f8; font-family: Tw Cen MT">
	<header>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<div class="col col-lg-9 mt-1">
					<a href="#" class="navbar-brand d-flex align-items-center"> <strong>GESTION
							DES PRODUITS - Modifier un utilisateur</strong>
					</a>
				</div>

				<div class="col col-lg-3 mt-1">
									<!-- 								Methode spring-security -->
			
				<sec:authorize access="isAuthenticated()">
					<%-- 					<c:if test="${sessionScope.user.login!=null}"> --%>
						<p style="color: white">
							<b>Bienvenue <i>${sessionScope.user.nom} !</i></b>
						</p>
					<%-- 					</c:if> --%>
				</sec:authorize>
				</div>
			</div>
		</div>
	</header>

	<div class="card text-center container mt-5">
		<form id="add-form"
			style="border: solid 1px Gray; background-color: #e5e7e9; border-top-left-radius: 15px; border-top-right-radius: 15px; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px;"
			action="updateUser.html" method="post">
			<h5>
				<b>MODIFIER UN UTILISATEUR</b>
			</h5>
			<div class="form-group">
				<label for="id" class="col col-form-label"><b>Id : </b></label>
				<div class="col-sm-12">
					<input type="text" class="rounded" name="id" id="id" value="${id}"
						style="">
				</div>

				<label for="nom" class="col col-form-label"><b>Nom</b></label>

				<div class="col-sm-12">
					<input type="text" class="rounded" name="nom" id="nom"
						value="${nom}" required>
				</div>

				<label for="prenom" class="col col-form-label"><b>Prénom</b></label>
				<div class="col-sm-12">
					<input type="text" class="rounded" name="prenom" id="prenom"
						value="${prenom}" required>
				</div>
				<label for="typeRol" class="col col-form-label"><b>Ancien
						role : </b></label>
				<div class="col-sm-12">
					<input type="text" class="rounded" id="nom" value="${role.type}"
						style="">
				</div>

				<label for="typeRole" class="col col-form-label"><b>Nouveau
						role :</b></label>

				<div class="col-sm-12">
					<select name="idRole">

						<c:forEach items="${listRole}" var="role">
							<option value="${role.id}">${role.type}</option>

						</c:forEach>

					</select>


				</div>

				<label for="login" class="col col-form-label"><b>Ancien
						login : </b></label>
				<div class="col-sm-12">
					<input type="text" class="rounded" id="login" value="${login}"
						style="">
				</div>

				<label for="typeRole" class="col col-form-label"><b>Nouveau
						login :</b></label>

				<div class="col-sm-12">
					<input type="text" class="rounded" name="login" id="login" value=""
						required>
				</div>

				<label for="typeRol" class="col col-form-label"><b>Ancien
						mot de passe : </b></label>
				<div class="col-sm-12">
					<input type="password" class="rounded" id="pwd" value="${pwd}"
						style="">
				</div>

				<label for="typeRole" class="col col-form-label"><b>Nouveau
						mot de passe :</b></label>

				<div class="col-sm-12">
					<input type="password" class="rounded" name="pwd" id="pwd" value=""
						required>
				</div>

				<br>
				<div class="container my-4">
					<div class="row justify-content-md-center">
						<div class="col col-lg-1 mt-1"></div>

						<div class="col col-lg-2 mt-1">
							<a class="btn btn-secondary" href="listUser.html?page=0" role="button"><span
								class="badge badge-light"><i class="fas fa-window-close"></i></span>
								Annuler</a>
						</div>
						<div class="col col-lg-1 mt-1"></div>
						<div class="col col-lg-2 mt-1">
							<input class="btn btn-secondary mb-3" value="valider"
								type="submit" id="submit-btn" style="display: none">
							<div class="col-sm">
								<a class="btn btn-secondary mb-3" href="#" id="click-btn"
									role="button"><span class="badge badge-light"><i
										class="fas fa-check-square"></i></span> Valider</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	<script src="static/js/script.js"></script>
</body>
</html>