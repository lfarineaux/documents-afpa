<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>

<html>
<head>
<link href="static/css/style.css" rel="stylesheet">
<link href="static/fontawesome/css/all.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>

<body style="background-color: #eaf2f8; font-family: Tw Cen MT">
	<header>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<div class="col col-lg-9 mt-1">
					<a href="#" class="navbar-brand d-flex align-items-center"> <strong>GESTION
							DES PRODUITS - Liste des roles</strong></a>
				</div>
				<div class="col col-lg-3 mt-1">
					<!-- 				Methode spring-security -->
					<sec:authorize access="isAuthenticated()">
						<%-- 					<c:if test="${sessionScope.user.login!=null}"> --%>
						<p style="color: white">
							<b>Bienvenue <i>${sessionScope.userLog.nom} !</i></b>
						</p>
						<%-- 					</c:if> --%>
					</sec:authorize>
				</div>
			</div>
		</div>
	</header>

	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col col-lg-3 mt-1"></div>
			<div class="col col-lg-3 mt-1">
				<%-- 				<c:if test="${sessionScope.userLog.login!=null}"> --%>
				<!-- 				Methode spring-security -->
				<sec:authorize access="hasRole('ADMIN')">
					<a style="float: left;" class="btn btn-secondary"
						href="addRole.html" role="button"> <span
						class="badge badge-light"><i class="fas fa-plus-square"></i></span>
						Ajout
					</a>
				</sec:authorize>
				<%-- 								</c:if> --%>

			</div>

			<div class="col col-lg-3 mt-1"></div>


			<div class="col col-lg-3 mt-1">
				<!-- 				Methode spring-security -->
				<sec:authorize access="!isAuthenticated()">
					<%-- 				<c:if test="${sessionScope.userLog.login==null}"> --%>

					<a class="btn btn-secondary" href="login.html" role="button"> <span
						class="badge badge-light"><i class="fas fa-user-plus"></i></span>
						Connection
					</a>

					<br>
					<%-- 				</c:if> --%>
				</sec:authorize>

				<!-- 				Methode spring-security -->
				<sec:authorize access="isAuthenticated()">
					<%-- 				<c:if test="${sessionScope.userLog.login!=null}"> --%>
					<a class="btn btn-secondary" href="deconnexion.html" role="button"><span
						class="badge badge-light"><i class="fas fa-user-slash"></i></span>
						Déconnection</a>
					<br>
					<%-- 				</c:if> --%>
				</sec:authorize>
			</div>
			<div class="col col-lg-3 mt-1"></div>
		</div>
		<div class="container">

			<table id="data-table-Rol" class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">id</th>
						<th scope="col">Type</th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${listRole != null}">
						<c:forEach items="${listRole}" var="role">
							<tr>
								<td>${role.id}</td>
								<td>${role.type}</td>
								<th class="d-flex justify-content-end"><a
									class="btn btn-secondary" href="showRole.html?id=${role.id}"
									id="showId" role="button"><span class="badge badge-light"><i
											class="fas fa-eye"></i></span> Détail</a></th>

								<!-- 				Methode spring-security -->
								<sec:authorize access="isAuthenticated()">
									<%-- 								<c:if test="${sessionScope.userLog.login!=null}"> --%>
									<th><a class="btn btn-secondary"
										href="updateRole.html?id=${role.id}" id="roleId" role="button">
											<span class="badge badge-light"><i
												class="fas fa-pen-alt"></i></span> Modification
									</a></th>
									<th><a class="btn btn-secondary"
										href="deleteRole.html?id=${role.id}" id="deleteId"
										role="button"><span class="badge badge-light"><i
												class="fas fa-trash-alt"></i></span> Suppression </a></th>
									<%-- 				</c:if> --%>
								</sec:authorize>

								<sec:authorize access="!isAuthenticated()">
									<%-- 				<c:if test="${sessionScope.userLog.login==null}"> --%>
									<th></th>
									<th></th>
									<%-- 				</c:if> --%>
								</sec:authorize>

							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>

			<div class="container" style="float: right;">
				<div>
					<c:if test="${pageN > 0}">
						<p style="float: left">
							<a href="listRole.html?page=${pageN-1}">Precedent</a>
						</p>
					</c:if>
					<c:if test="${pageN < pageMax-1}">
						<p style="float: right;">
							<a href="listRole.html?page=${pageN+1}">Suivant</a>
						</p>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col  col-lg-4"></div>
			<div class="col  col-lg-8">
				<a class="btn btn-secondary" href="index.html" role="button"><span
					class="badge badge-light"><i class="fas fa-book-open"></i></span>
					Retour vers le menu principal</a>


			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
	<script src="static/js/script.js"></script>
</body>
</html>
