<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>
<head>
<link href="css/style.css" rel="stylesheet">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body style="background-color: #eaf2f8; font-family: Tw Cen MT">
	<header>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<div class="col col-lg-9 mt-1">

					<a href="#" class="navbar-brand d-flex align-items-center"> <strong>GESTION
							DES PRODUITS - Menu principal</strong></a>
				</div>
				<div class="col col-lg-3 mt-1">
					<!-- 				Methode spring-security -->
					<sec:authorize access="isAuthenticated()">
						<%-- 					<c:if test="${sessionScope.user.login!=null}"> --%>
						<p style="color: white">
							<b>Bienvenue <i>${userLog} !</i></b>
						</p>
						<%-- 					</c:if> --%>
					</sec:authorize>
				</div>
			</div>
		</div>
	</header>
	<div class="container my-4">
		<div class="row justify-content-md-center">

			<!-- 				Methode spring-security -->
			<sec:authorize access="!isAuthenticated()">
				<%-- 				<c:if test="${sessionScope.userLog.login==null}"> --%>
				<a class="btn btn-secondary mb-3" href="login.html" role="button">Se
					connecter</a>
				<br>
				<%-- 				</c:if> --%>
			</sec:authorize>

			<!-- 				Methode spring-security -->
			<sec:authorize access="isAuthenticated()">
				<%-- 				<c:if test="${sessionScope.userLog.login!=null}"> --%>
				<a class="btn btn-secondary mb-3" href="deconnexion.html"
					role="button">Se déconnecter</a>
				<br>
				<%-- 				</c:if> --%>
			</sec:authorize>
		</div>
	</div>
	<br>
	<br>
	<div class="container my-4 ">
		<div class="row justify-content-between">

			<div class="">
				<a style="float: left;" class="btn btn-secondary"
					href="listCategorie.html?page=0" role="button">Menu Categories</a>
			</div>
			<div class="">

				<a style="float: left;" class="btn btn-secondary"
					href="listProduit.html?page=0" role="button">Menu Produits</a>
			</div>

			<%-- 				<c:if test="${sessionScope.userLog.role.type == 'admin' }"> --%>
				<!-- 				Methode spring-security -->
				<sec:authorize access="hasRole('ADMIN')">

				<div class="">
					<a style="float: left;" class="btn btn-secondary"
						href="listUser.html?page=0" role="button">Menu Users </a>
				</div>

				<div class="">
					<a style="float: left;" class="btn btn-secondary"
					 href="listRole.html?page=0"
						role="button">Menu Roles</a>
				</div>
			</sec:authorize>
				<%-- 								</c:if> --%>
		</div>
	</div>
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	<script src="js/script.js"></script>
</body>
</html>
