
package com.afpa.cda.dto;

import com.afpa.cda.entity.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class UserDto {

	private int id;
	public String login;
	private String pwd;
	private String nom;
	private String prenom;
	private RoleDto role;
	
}
