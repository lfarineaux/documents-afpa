package com.afpa.cda.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class MenuController {

	@RequestMapping(path = {"/index.html","/"}, method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView initMenu1(ModelAndView mv) {
		mv.setViewName("redirect:/index.html");
		return mv;
	}


//	@RequestMapping(path = {"/index.html","/"}, method = RequestMethod.GET)
//	public ModelAndView initMenu2(ModelAndView mv,HttpServletRequest request) {
//        Principal principal = request.getUserPrincipal();
//		System.out.println("principal : " + principal.getName());
//		mv.setViewName("menu");
//		return mv;
//	}

	
	@RequestMapping(path = {"/index.html","/"}, method = RequestMethod.GET)
	public ModelAndView initMenu(ModelAndView mv,Principal p) {
		if (p!=null) {
			System.out.println(p.getName());
		} else {
			System.out.println("Pas d'utilisateur");
		}
		mv.addObject("userLog",p.getName());
		mv.setViewName("menu");
		return mv;
	}

	
	
	
}
