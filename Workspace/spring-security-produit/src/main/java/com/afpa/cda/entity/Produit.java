package com.afpa.cda.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Produit {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PRODUIT_SEQ")
	
	
	@Column(name="Code")
	private int code ;
	@Column(name="Label")
	private String label;
	@Column(name="Prix")
	private String prix;
	@Column(name="Quantite")
	private String quantite;
	@Column(name="Image")
	private String img;
	@ManyToOne
	private Categorie categorie;

}
