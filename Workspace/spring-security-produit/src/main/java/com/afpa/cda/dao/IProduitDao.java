package com.afpa.cda.dao;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Produit;

@Repository
public interface IProduitDao extends PagingAndSortingRepository<Produit, Integer> {
	
	
	@Query("select pr from Produit pr")
	public  Page<Produit>findAll(Pageable pageable);
	

}
