package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.IUtilisateurDao;
import com.afpa.cda.dto.RoleDto;
import com.afpa.cda.dto.UserDto;
import com.afpa.cda.entity.Role;
import com.afpa.cda.entity.Utilisateur;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUtilisateurDao userRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public static final int INC = 5;
	public static int fin;
	public static int nbPage;
	

	@Override
	public boolean deleteById(int id) {
		if (this.userRepository.existsById(id)) {
			this.userRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public UserDto findById(int id) {
		Optional<Utilisateur> user = this.userRepository.findById(id);
		Optional<UserDto> use = Optional.empty();
		UserDto userDto = null;
		if(user.isPresent()) {
			Utilisateur us = user.get();
			userDto = this.modelMapper.map(us, UserDto.class);
		}
		
		return userDto;
	}

	@Override
	public Integer ajouter(UserDto build) {
		Utilisateur ut = this.modelMapper.map(build, Utilisateur.class);
		ut = this.userRepository.save(ut);
		return ut.getId();
	}

	@Override
	public List<UserDto> chercherToutesLesPersonnes(int page) {
		Pageable pageable = PageRequest.of(page, INC);
		nbPage = this.userRepository.findAll(pageable).getTotalPages();
		
		List<UserDto> maListe = this.userRepository.findAll(pageable)
				.stream()
				.map(e->UserDto.builder()
				.id(e.getId())
				.login(e.getLogin())
				.pwd(e.getPwd())
				.nom(e.getNom())
				.prenom(e.getPrenom())
				.role(new RoleDto(e.getRole().getId(),e.getRole().getType()))
				.build())
			.collect(Collectors.toList());
		
		fin = maListe.size();
		return maListe;
	}

	@Override
	public void updateUser(int id, String nom, String prenom, String login, String pwd, RoleDto role) {
		Optional<Utilisateur> user = this.userRepository.findById(id);
		if (user.isPresent()) {
			Utilisateur u = user.get();
			u.setId(id);
			u.setNom(nom);
			u.setPrenom(prenom);
			u.setLogin(login);
			u.setPwd(pwd);
			u.setRole(new Role().builder().id(role.getId()).type(role.getType()).build());
			this.userRepository.save(u);
		
	}

	
		
	}

	

	

}
