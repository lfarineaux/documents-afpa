package com.afpa.cda;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurityConf extends WebSecurityConfigurerAdapter {

//	@Autowired
//	private UserDetailsService userDetailsService;

	//password = pwd
//		@Override
//		public void configure(AuthenticationManagerBuilder auth) throws Exception {
//			auth.inMemoryAuthentication().withUser("admin").password("{noop}pwd").roles("USER");
//		}
//	
		
	@Override
	 	public void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.inMemoryAuthentication().withUser("admin").password("{noop}pwd1").roles("ADMIN")
			.and()
			.withUser("user").password("{noop}pwd2").roles("USER");
		}

//	@Override
//	public void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.userDetailsService(this.userDetailsService).passwordEncoder(bCryptPasswordEncoder());
//	}

//	@Bean
//	public BCryptPasswordEncoder bCryptPasswordEncoder() {
//		return new BCryptPasswordEncoder();
//	}

//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		http
//		.csrf()
//		.disable()
//		.authorizeRequests()
//		.antMatchers("/admin/**")
//		.authenticated()
//		.and().headers().frameOptions().sameOrigin()
//		.and()
//		.formLogin();
//	}

			@Override
			protected void configure(HttpSecurity http) throws Exception {
				http
					.csrf()
					.disable()
					.authorizeRequests()
					.antMatchers("/addRole.html","/addUser.html","/listRole.html","/listUser.html","/updateRole.html","/updateUser.html","/deleteRole.html","/deleteUser.html","/showRole.html","/showUser.html")
					.hasRole("ADMIN")
					.antMatchers("/addCategorie.html","/addProduit.html","/updateCategorie.html","/updateProduit.html","/deleteCategorie.html","/deleteProduit.html")
					.authenticated();
				
//		            .and().headers().frameOptions().sameOrigin(); // base H2 embarquée
				
				 http.formLogin()
				 	.loginPage("/login.html")
				 	.loginProcessingUrl("/login") // methode POST de login.jsp ?
				 	.successForwardUrl("/index.html")
				 	.failureUrl("/login.html");
			}
	
}
