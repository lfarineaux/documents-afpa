package com.afpa.cda.service;

import java.util.List;

import com.afpa.cda.dto.ProduitDto;

public interface IProduitService {

	List<ProduitDto> searchAllProduct();
	
}
