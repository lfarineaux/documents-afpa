package com.afpa.cda.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Produit {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PRODUIT_SEQ")
	@Column(name="Id")
	private int id ;
	@Column(name="Label")
	private String label;

}
