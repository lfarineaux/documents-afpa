package com.afpa.cda.dao;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Produit;

@Repository
public interface IProduitDao extends CrudRepository<Produit, Integer> {


	@Query("select p from Produit p")
	public  List<Produit>findAll( );


}
