package com.afpa.cda.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.IProduitDao;
import com.afpa.cda.dto.ProduitDto;

@Service
public class ProduitServiceImpl implements IProduitService{


	@Autowired
	private IProduitDao produitDao;

	
	@Override
	public List<ProduitDto> searchAllProduct( ) {
		
		System.out.println("service");
		List<ProduitDto> liste = this.produitDao.findAll()
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.build())
				.collect(Collectors.toList());
		
		for (ProduitDto p : liste ) {
			System.out.println(p);
		}
		
		return liste;
	}
	
}
