package com.afpa.cda.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.ProduitDto;
import com.afpa.cda.service.IProduitService;

@Controller
public class ProduitController {

	@Autowired
	private IProduitService produitService;

	@RequestMapping(value = "/listProduit", method = RequestMethod.GET)
	public ModelAndView list(ModelAndView mv) {

		System.out.println("controller");
		List<ProduitDto> listProduit = this.produitService.searchAllProduct();
		mv.addObject("listProduit", listProduit);
		mv.setViewName("listProd");
		return mv;

	}

}
