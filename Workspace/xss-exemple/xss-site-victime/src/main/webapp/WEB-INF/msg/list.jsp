<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="../commun/header.jsp">
	<jsp:param name="navActive" value="msgList" />
	<jsp:param name="titre" value="messages liste" />
</jsp:include>

<table class="table w-75">
	<thead class="thead-dark">
		<tr class="head">
			<th>user</th>
			<th>message</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${messages}" var="msg" varStatus="index">
			<tr class="${index.count%2==0?'pair':'impair'}">
				<td>${msg.user.name}</td>
				<td>${msg.value}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

</body>
</html>