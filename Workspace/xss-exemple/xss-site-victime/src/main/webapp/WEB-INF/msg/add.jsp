<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../commun/header.jsp">
	<jsp:param name="navActive" value="msgAdd"/>
    <jsp:param name="titre" value="message add"/>
</jsp:include>

<form action="add.html" method="post">
	<div class="form-group">
	    <label for="msg">message</label>
	    <textarea class="form-control" id="msg" name="msg" rows="3"></textarea>
  	</div>
  	
  	
	<input class="btn btn-secondary" type="submit" value="valider">
	
</form>
<br>
<a class="btn btn-secondary" href="/index.html" role="button">
					Retour</a>


</body>
</html>