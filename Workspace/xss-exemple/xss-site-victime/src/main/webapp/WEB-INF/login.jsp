<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="commun/header.jsp">
	<jsp:param name="navActive" value="login"/>
    <jsp:param name="titre" value="login"/>
</jsp:include>

   <h1>Login AFPA</h1>
   <form action="/login" method="POST" autocomplete="off">
      <table>
         <tr>
            <td>User:</td>
            <td><input type='text' name='username' value=''></td>
         </tr>
         <tr>
            <td>Password:</td>
            <td><input type='password' name='password' /></td>
         </tr>
         <tr>
            <td><input name="submit" type="submit" value="submit" /></td>
         </tr>
      </table>
  </form>
  <br>
<a class="btn btn-secondary" href="/index.html" role="button">
					Retour</a>
  
</body>
</html>