<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="../commun/header.jsp">
	<jsp:param name="navActive" value="userList" />
	<jsp:param name="titre" value="users liste" />
</jsp:include>

<table class="table w-75">
	<thead class="thead-dark">
		<tr class="head">
			<th>Id</th>
			<th>user</th>
			<th>date creation</th>
			<th>date derni�re connexion</th>
			<th>Statut</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${users}" var="user" varStatus="index">
			<tr class="${index.count%2==0?'pair':'impair'}">
				<td>${user.id}</td>
				<td>${user.name}</td>
				<td>${user.date}</td>
				<td>${user.dateLog}</td>
				<td>${user.active}</td>
			</tr>
		</c:forEach>
</table>
<br>
<a class="btn btn-secondary" href="/index.html" role="button">
					Retour</a>
</body>
</html>