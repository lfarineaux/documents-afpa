<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="../commun/header.jsp">
	<jsp:param name="navActive" value="userAdd" />
	<jsp:param name="titre" value="user add" />
</jsp:include>

<form method="post" action="add.html">

	<label for="username" class="col col-form-label">Nom</label>
	<p>
		<input type="text" name="username" required>
	</p>

	<label for="password" class="col col-form-label">Password</label>
	<p>
		<input type="password" name="password" required>
	</p>

	<select name="roleName">
		<c:forEach items="${roles}" var="role">
			<option value="${role.name}">${role.name}</option>
		</c:forEach>

	</select> 
	<br>
	</br> 
	<select name="statutUser">
		<option value="vrai">Actif</option>
		<option value="faux">Inactif</option>
	</select> 
	<br>
	<br>
	<input class="btn btn-secondary " value="valider" type="submit"
		id="submit-btn">
</form>
<br>
<a class="btn btn-secondary" href="/index.html" role="button">
					Retour</a>
</body>
</html>