<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="../commun/header.jsp">
	<jsp:param name="navActive" value="userEdit" />
	<jsp:param name="titre" value="user edit" />
</jsp:include>


<c:forEach items="${users}" var="user">
	<form action="edit.html" method="post">

		<label for="username" class="col col-form-label">Nom</label> <input
			type="text" value="${user.name}" name="username" readonly> <label
			for="newusername" class="col col-form-label">Nouveau nom</label> <input
			type="text" value="${user.name}" name="newusername" required>

	<label for="roleName" class="col col-form-label">Role</label>
		<select name="roleName">
			<c:forEach items="${roles}" var="role">
				<option value="${role.name}">${role.name}</option>
			</c:forEach>
		</select>
		
		<label for="roleName" class="col col-form-label">Actif</label>
		 <select name="statutUser">
			<c:if test="${user.active}">
				<option value="vrai" selected>true</option>
				<option value="faux">false</option>
			</c:if>
			<c:if test="${!user.active}">
				<option value="vrai">true</option>
				<option value="faux" selected>false</option>
			</c:if>
			<br>
		</select> <br>	<br><input type="submit">
<hr>

	</form>
</c:forEach>
<br>
<a class="btn btn-secondary" href="/index.html" role="button">
					Retour</a>
</body>
</html>