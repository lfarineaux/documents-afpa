<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>${param.titre}</title>
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light w-75">
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav nav-tabs nav-fill w-100">
				<sec:authorize access="!isAuthenticated()">
					<li class="nav-item"><a
						class="nav-link ${param.navActive eq 'login'?'active':'' }"
						href="/login.html">login</a></li>
				</sec:authorize>
				<sec:authorize access="isAuthenticated()">
					<li class="nav-item"><a class="nav-link " href="/logout">logout</a>
					</li>
				</sec:authorize>
				<li class="nav-item"><a
					class="nav-link ${param.navActive eq 'msgList'?'active':'' }"
					href="/msg/list.html">msg list</a></li>
				<sec:authorize access="hasAnyRole('USER','ADMIN')">
					<li class="nav-item"><a
						class="nav-link ${param.navActive eq 'msgAdd'?'active':'' }"
						href="/msg/add.html">msg add</a></li>
				</sec:authorize>
				<sec:authorize access="hasRole('ADMIN')">
					<li class="nav-item"><a
						class="nav-link ${param.navActive eq 'userList'?'active':'' }"
						href="/user/list.html">user list</a></li>
					<li class="nav-item"><a
						class="nav-link ${param.navActive eq 'userAdd'?'active':'' }"
						href="/user/add.html">user add</a></li>
					<li class="nav-item"><a
						class="nav-link ${param.navActive eq 'userEdit'?'active':'' }"
						href="/user/edit.html">edit add</a></li>
				</sec:authorize>

				<c:if test="${userLog!=null}">
					<li class="nav-item">
						<p class="nav-link">
							<b>User en cours : <i>${userLog}</i></b>
						</p>
					</li>
				</c:if>
			</ul>
		</div>
	</nav>