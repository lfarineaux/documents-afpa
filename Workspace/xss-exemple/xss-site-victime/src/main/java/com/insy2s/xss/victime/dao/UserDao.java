package com.insy2s.xss.victime.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.insy2s.xss.victime.entity.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
	
	@Query("select u from User u where u.name != 'admin'")
	public List<User> findAll();
	
	public Optional<User> findUserByName(String username);
}
