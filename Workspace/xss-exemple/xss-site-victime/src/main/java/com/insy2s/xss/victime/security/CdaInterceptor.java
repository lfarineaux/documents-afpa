package com.insy2s.xss.victime.security;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

public class CdaInterceptor implements HandlerInterceptor  {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		Principal userPrincipal = request.getUserPrincipal();
		if(userPrincipal != null) {
			request.setAttribute("userLog", userPrincipal.getName());
		}
		return true;
	}
	
}
