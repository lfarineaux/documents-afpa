package com.insy2s.xss.victime.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccueilController {
	
	@GetMapping(path = {"/","index.html"})
	public ModelAndView accueil(ModelAndView mv) {
		mv.setViewName("redirect:msg/list.html");
		return mv;
	}
	
	@RequestMapping(path = {"/","index.html"}, method = {RequestMethod.POST})
	public ModelAndView accueilRedirect(ModelAndView mv) {
		mv.setViewName("redirect:msg/list.html");
		return mv;
	}
	
	@GetMapping(path = {"login.html"})
	public ModelAndView login(ModelAndView mv) {
		mv.setViewName("login");
		return mv;
	}
}
