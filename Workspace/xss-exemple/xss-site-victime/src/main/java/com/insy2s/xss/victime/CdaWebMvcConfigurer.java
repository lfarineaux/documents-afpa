package com.insy2s.xss.victime;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.insy2s.xss.victime.security.CdaInterceptor;

@Configuration
//@EnableWebMvc
public class CdaWebMvcConfigurer implements WebMvcConfigurer  {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	    registry.addInterceptor(new CdaInterceptor());
	}
	
}
