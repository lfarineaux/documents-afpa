package com.insy2s.xss.victime.controller;

import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.insy2s.xss.victime.dao.RoleDao;
import com.insy2s.xss.victime.dao.UserDao;
import com.insy2s.xss.victime.entity.User;

@Controller
@RequestMapping("user")
public class UserAdminController {
	@Autowired
	private UserDao userDao;

	@Autowired
	private RoleDao roleDao;

	@GetMapping(path = { "list.html","/"})
	public ModelAndView list(ModelAndView mv) {
		mv.addObject("users", this.userDao.findAll());
		mv.setViewName("user/list");
		return mv;
	}

	@GetMapping("add.html")
	public ModelAndView init(ModelAndView mv) {
		mv.addObject("roles",roleDao.findAll());
		mv.setViewName("user/add");
		return mv;
	}

	@PostMapping("add.html")
	public ModelAndView add(
			@RequestParam String username,
			@RequestParam String password,
			@RequestParam String roleName,
			@RequestParam String statutUser,
			BCryptPasswordEncoder bcryp,
			ModelAndView mv) {


		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		String dat = df.format(date);

		boolean statut;

		if (statutUser.equals("vrai")) {
			statut=true;

		} else {
			statut = false;
		}

		BCryptPasswordEncoder en = new BCryptPasswordEncoder();
		password = en.encode(password);

		userDao.save(
				User.builder()
				.name(username)
				.password(password)
				.date(dat)
				.active(statut)
				.role(roleDao.findByName(roleName).get())
				.build());

		mv.setViewName("redirect:/");
		return mv;
	}

	@GetMapping("edit.html")
	public ModelAndView edit(ModelAndView mv,Principal p) {
		mv.addObject("users", this.userDao.findAll());
		mv.addObject("roles", this.roleDao.findAll());

		mv.setViewName("user/edit");
		return mv;
	}

	@PostMapping("edit.html")
	public ModelAndView edit(
			@RequestParam String username,
			@RequestParam String newusername,
			@RequestParam String roleName,
			@RequestParam String statutUser,
			BCryptPasswordEncoder bcryp,
			ModelAndView mv) {
		Optional<User> user = userDao.findUserByName(username);


		boolean statut;

		if (statutUser.equals("vrai")) {
			statut=true;
		} else {
			statut = false;
		}

		if(user.isPresent()) {
			User u = user.get();
			u.setName(newusername);
			u.setRole(roleDao.findByName(roleName).get());
			u.setActive(statut);
			userDao.save(u);
		}

		mv.setViewName("redirect:/user/list.html");
		return mv;
	}


}
