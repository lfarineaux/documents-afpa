package com.insy2s.xss.victime.security;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.insy2s.xss.victime.dao.UserDao;
import com.insy2s.xss.victime.entity.User;

@Component
public class CdaAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	private UserDao userDao;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		System.out.println("1 utilisateur s'est connecté : "+authentication.getName());
		User user = userDao.findUserByName(authentication.getName()).get();
		
		user.setDateLog(new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		
		this.userDao.save(user);
	
		response.sendRedirect("/");
	}

}
