package com.insy2s.xss.victime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.insy2s.xss.victime.dao.MessageDao;
import com.insy2s.xss.victime.dao.RoleDao;
import com.insy2s.xss.victime.dao.UserDao;
import com.insy2s.xss.victime.entity.Message;
import com.insy2s.xss.victime.entity.Role;
import com.insy2s.xss.victime.entity.User;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class ConfApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ConfApplication.class, args);
	}
	
	@Bean
	public ModelMapper createModelMapper() {
		return new ModelMapper();
	}

	@Bean
    public CommandLineRunner init (RoleDao roleDao,UserDao userDao, MessageDao messageDao, BCryptPasswordEncoder bcryp){
        return args -> {
        	
        	if(! roleDao.findByName("USER").isPresent()) {
        		roleDao.save(Role.builder().name("USER").build());
        	}
        	
        	Role adminRole = null;
        	if((adminRole = roleDao.findByName("ADMIN").orElse(null)) == null) {
        		adminRole = roleDao.save(Role.builder().name("ADMIN").build());
        	}
        	
        	User userAdmin = null;
            if((userAdmin = userDao.findUserByName("admin").orElse(null)) == null) {
            	String adminPassword = RandomStringUtils.randomAlphabetic(6);
            	StringBuilder sb = new StringBuilder();
            	sb.append("\n\n*****\n\n")
            		.append("insertion compte admin ...")
            		.append("\n")
            		.append("mot de passe : "+adminPassword)
            		.append("\n")
            		.append("\n\n*****\n");
            	log.error(sb.toString());
            	
            	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        		Date date = new Date();
        		String dat = df.format(date);
            	
            	
        		
            	userAdmin = userDao.save(User.builder()
            			.name("admin")
            			.password(bcryp.encode(adminPassword))
            			.role(adminRole)
            			.date(dat)
            			.active(true)
            			.build());
            }
            
            if(messageDao.count() == 0) {
            	messageDao.save(
            			Message.builder()
            			.value("1 er msg de l'admin")
            			.user(userAdmin)
            			.build());
            }
        };
    }
}
