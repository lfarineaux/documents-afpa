package com.insy2s.xss.attaquant.dao;

import org.springframework.data.repository.CrudRepository;

import com.insy2s.xss.attaquant.entity.Victime;


public interface VictimeDao extends CrudRepository<Victime, Integer> {

}
