package fr.catteau.examen;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

//Test unitaire
class TestSauvegardePoint {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		Throwable thrw = null; 
		
		try {
			SauvegardePoint.ajoutPoint("p1", 1.8, 7.5);
		} catch (Exception e) {
			thrw = e;
		}
		assertNull(thrw);
		
		try {
			SauvegardePoint.ajoutPoint("p2", 4, 8);
		} catch (InvalideParametreException e) {
			thrw = e;
		}
		assertFalse(thrw instanceof InvalideParametreException);
		thrw = null;		
		
		try {
			SauvegardePoint.ajoutPoint("p1", 1.8, 7.5);
		} catch (InvalideParametreException e) {
			thrw = e;
		}
		assertTrue(thrw instanceof InvalideParametreException);
		thrw = null;	
		
		try {
			SauvegardePoint.ajoutPoint("p1", 7, 1.5);
		} catch (InvalideParametreException e) {
			thrw = e;
		}
		assertTrue(thrw instanceof InvalideParametreException);
		thrw = null;
		
		try {
			SauvegardePoint.ajoutPoint("p3", 1.8, 7.5);
		} catch (InvalideParametreException e) {
			thrw = e;
		}
		assertTrue(thrw instanceof InvalideParametreException);
		thrw = null;
		
		try {
			SauvegardePoint.ajoutPoint("p4", 111, 7.5);
		} catch (InvalideParametreException e) {
			thrw = e;
		}
		assertNull(thrw);
		
		SauvegardePoint.listerPoint();
		
		SauvegardePoint.getListPoint();
	}

}
