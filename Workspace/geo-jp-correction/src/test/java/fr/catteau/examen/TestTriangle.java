package fr.catteau.examen;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

//Test unitaire
class TestTriangle {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		Point p1 = new Point("p1", 1.8, 7.5);
		System.out.println(p1.getNom());
		System.out.println(p1.getAbscisse());
		System.out.println(p1.getOrdonnee());
		Point p2 = new Point("p2", 4, 8);
		System.out.println(p2.getNom());
		System.out.println(p2.getAbscisse());
		System.out.println(p2.getOrdonnee());
		Point p3 = new Point("p3", 2, 3);
		System.out.println(p3.getNom());
		System.out.println(p3.getAbscisse());
		System.out.println(p3.getOrdonnee());

		Triangle t1 = new Triangle("t1", p1, p2, p3);
		System.out.println(t1.getNom());
		System.out.println(t1.getP1());
		System.out.println(t1.getP2());
		System.out.println(t1.getP3());
		
		System.out.println(t1.calculSurface());
	}

}
