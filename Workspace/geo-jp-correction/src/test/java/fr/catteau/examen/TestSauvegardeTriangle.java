package fr.catteau.examen;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

//Test unitaire
class TestSauvegardeTriangle {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		Point p1 = new Point("p1", 1.8, 7.5);
		System.out.println(p1.getNom());
		System.out.println(p1.getAbscisse());
		System.out.println(p1.getOrdonnee());
		Point p2 = new Point("p2", 4, 8);
		System.out.println(p2.getNom());
		System.out.println(p2.getAbscisse());
		System.out.println(p2.getOrdonnee());
		Point p3 = new Point("p3", 2, 3);
		System.out.println(p3.getNom());
		System.out.println(p3.getAbscisse());
		System.out.println(p3.getOrdonnee());
		Point p4 = new Point("p1", 1.8, 7.5);
		System.out.println(p1.getNom());
		System.out.println(p1.getAbscisse());
		System.out.println(p1.getOrdonnee());
		Point p5 = new Point("p2", 4, 8);
		System.out.println(p2.getNom());
		System.out.println(p2.getAbscisse());
		System.out.println(p2.getOrdonnee());
		Point p6 = new Point("p3", 2, 3);
		System.out.println(p3.getNom());
		System.out.println(p3.getAbscisse());
		System.out.println(p3.getOrdonnee());
		Point p7 = new Point("p3", 2, 3);
		System.out.println(p3.getNom());
		System.out.println(p3.getAbscisse());
		System.out.println(p3.getOrdonnee());
		Point p8 = new Point("p3", 2, 3);
		System.out.println(p3.getNom());
		System.out.println(p3.getAbscisse());
		System.out.println(p3.getOrdonnee());
		Point p9 = new Point("p3", 2, 3);
		System.out.println(p3.getNom());
		System.out.println(p3.getAbscisse());
		System.out.println(p3.getOrdonnee());

		Throwable thrw = null; 

		try {
			SauvegardeTriangle.ajoutTriangle("t1", p1, p2, p3);
		} catch (InvalideParametreException e) {
			thrw = e;
		}
		assertFalse(thrw instanceof InvalideParametreException);
		thrw = null;

		try {
			SauvegardeTriangle.ajoutTriangle("t1", p1, p2, p3);
		} catch (InvalideParametreException e) {
			thrw = e;
		}
		assertTrue(thrw instanceof InvalideParametreException);
		thrw = null;

		try {
			SauvegardeTriangle.ajoutTriangle("t2", p1, p4, p5);
		} catch (InvalideParametreException e) {
			thrw = e;
		}
		assertTrue(thrw instanceof InvalideParametreException);
		thrw = null;

		try {
			SauvegardeTriangle.ajoutTriangle("t1", p1, p2, p6);
		} catch (InvalideParametreException e) {
			thrw = e;
		}
		assertTrue(thrw instanceof InvalideParametreException);
		thrw = null;

		try {
			SauvegardeTriangle.ajoutTriangle("t3", p7, p8, p9);
		} catch (InvalideParametreException e) {
			thrw = e;
		}
		assertFalse(thrw instanceof InvalideParametreException);
		thrw = null;
		
		SauvegardeTriangle.listTriangle();
		
		SauvegardeTriangle.getListTriangle();
	}

}
