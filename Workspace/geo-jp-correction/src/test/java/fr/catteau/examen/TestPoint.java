package fr.catteau.examen;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

//Test unitaire
class TestPoint {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		Point p1 = new Point("p1", 1.8, 7.4);
		System.out.println(p1.getNom());
		System.out.println(p1.getAbscisse());
		System.out.println(p1.getOrdonnee());
		
		System.out.println(p1.distanceAlOrigine());
	}

}
