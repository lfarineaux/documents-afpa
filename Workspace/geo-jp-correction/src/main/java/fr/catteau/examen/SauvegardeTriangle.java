package fr.catteau.examen;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;

public abstract class SauvegardeTriangle {
	private static Logger monLogger = LoggerFactory.getLogger(SauvegardeTriangle.class);
	@Getter
	private static List<Triangle> listTriangle = new ArrayList<Triangle>();
	
	/**
	 * 
	 * @param Un triangle
	 * @throws InvalideParametreException
	 */
	public static void ajoutTriangle(String n, Point p1, Point p2, Point p3) throws InvalideParametreException {
		Triangle t = new Triangle(n, p1, p2, p3);
		monLogger.info("Triangle créer.");
		boolean nom = false;
		boolean position = false;

		for (Triangle triangle : listTriangle) {
			if (triangle.getNom() == t.getNom() && triangle.getP1() == t.getP1() && triangle.getP2() == t.getP2() && triangle.getP3() == t.getP3()) {
				position = true;
				nom = true;
				break;
			} else if (triangle.getNom() == t.getNom()) {
				nom = true;
				break;
			} else if (triangle.getP1() == t.getP1() || triangle.getP2() == t.getP2() || triangle.getP3() == t.getP3()) {
				position = true;
				break;
			}
		}

		if (nom && position) {
			System.out.println("Erreur : Impossible le nom, et les points sont déjà utilisée.");
			throw new InvalideParametreException();
		} else if (nom) {
			System.out.println("Erreur : Impossible le nom du point existe déjà.");
			throw new InvalideParametreException();
		} else if (position) {
			System.out.println("Erreur : Impossible les points sont déjà utilisée");
			throw new InvalideParametreException();
		} else {
			listTriangle.add(t);
			monLogger.debug("Triangle ajouter a la list.");
			System.out.println("Le triangle est désormais crée");
		}
	}
	
	public static void listTriangle() {
		for (Triangle triangle : listTriangle) {
			System.out.println(triangle);
		}
	}
}
