package fr.catteau.examen;

import lombok.Getter;
import lombok.ToString;

@ToString
public class Cercle {
	@Getter
	private String nom;
	@Getter
	private Point p1;

	/**
	 * 
	 * @param nom
	 * @param p1
	 */
	public Cercle(String nom, Point p1) {
		super();
		this.nom = nom;
		this.p1 = p1;
	}
}
