package fr.catteau.examen;

import lombok.Getter;
import lombok.ToString;

@ToString
public class Point {
	@Getter
	private String nom;
	@Getter
	private double abscisse;
	@Getter
	private double ordonnee;
	
	/**
	 * 
	 * @param nom
	 * @param abscisse
	 * @param ordonnee
	 */
	public Point(String nom, double abscisse, double ordonnee) {
		super();
		this.nom = nom;
		this.abscisse = abscisse;
		this.ordonnee = ordonnee;
	}
	
	/**
	 * 
	 * @return La distance par rapport à l'origine, d'un point
	 */
	public double distanceAlOrigine() {
		return Math.sqrt(Math.pow(this.abscisse, 2)+Math.pow(this.ordonnee, 2));
	}
	
	/**
	 * 
	 * @param Un point
	 * @return La distance entre les deux po
	 * ints
	 */
	public double distanceEntreDeuxPoints(Point p) {
		double save;
		if(p.getAbscisse() < this.getAbscisse()) {
			save = -this.getAbscisse();
			save = Math.sqrt(Math.pow(p.getAbscisse(), 2)-Math.pow(this.getAbscisse(), 2));
		} else {
			save = Math.sqrt(Math.pow(p.getAbscisse(), 2)-Math.pow(this.getAbscisse(), 2));
		}
		return save;
	}
}
