package fr.catteau.examen;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;

public abstract class SauvegardeCercle {
	@Getter
	private static List<Cercle> listCercle = new ArrayList<Cercle>();
	private static Logger monLogger = LoggerFactory.getLogger(SauvegardeCercle.class);

	/**
	 * 
	 * @param Un Cercle
	 * @throws InvalideParametreException
	 */
	public static void ajoutCercle(String n, Point p) throws InvalideParametreException {
		Cercle c = new Cercle(n, p);
		monLogger.info("Cercle créer.");
		boolean nom = false;
		boolean position = false;

		for (Cercle cercle : listCercle) {
			if (cercle.getNom() == c.getNom() && cercle.getP1() == c.getP1()) {
				position = true;
				nom = true;
				break;
			} else if (cercle.getNom() == c.getNom()) {
				nom = true;
				break;
			} else if (cercle.getP1() == c.getP1()) {
				position = true;
				break;
			}
		}

		if (nom && position) {
			System.out.println("Erreur : Impossible le nom, et le point sont déjà utilisée.");
			throw new InvalideParametreException();
		} else if (nom) {
			System.out.println("Erreur : Impossible le nom du cercle existe déjà.");
			throw new InvalideParametreException();
		} else if (position) {
			System.out.println("Erreur : Impossible le point sont déjà utilisée");
			throw new InvalideParametreException();
		} else {
			listCercle.add(c);
			monLogger.debug("Cercle ajouter à la liste");
			System.out.println("Le cercle est désormais crée");
		}
	}

	public static void listerCercle() {
		for (Cercle cercle : listCercle) {
			System.out.println(cercle);
		}
	}
}
