package com.afpa.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.afpa.dao.IModeleDao;
import com.afpa.entity.Energie;
import com.afpa.entity.Modele;

public class ModeleDaoImpl extends AbstractDao<Modele> implements IModeleDao {

	@Override
	public Modele getModeleByLabel(String modele) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Modele> q = em.createNamedQuery("getModeleByLabel", Modele.class);
			q.setParameter("labelParam", modele);
			return q.getSingleResult();
		}catch(NoResultException e) {
			return null;
		} finally {
			closeEntityManager(em);
		}
	}

	@Override
	public List<Modele> findModelesByEnergieLabel(String modele) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Modele> q = em.createNamedQuery("Voiture.findAll.byEnergieLabel", Modele.class);
			q.setParameter("labelParam", modele);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}
	
}
