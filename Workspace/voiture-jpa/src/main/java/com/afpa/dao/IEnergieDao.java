package com.afpa.dao;

import java.util.List;

import com.afpa.entity.Energie;
import com.afpa.entity.Voiture;

public interface IEnergieDao extends IDao<Energie> {

	Energie getEnergieByLabel(String energie);
	


}
