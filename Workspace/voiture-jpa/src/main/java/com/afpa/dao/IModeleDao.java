package com.afpa.dao;

import java.util.List;

import com.afpa.entity.Energie;
import com.afpa.entity.Modele;

public interface IModeleDao extends IDao<Modele> {

	Modele getModeleByLabel(String modele);

	List<Modele> findModelesByEnergieLabel (String modeles);
	
}
