package com.afpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="t_voiture",
	uniqueConstraints = { 
		@UniqueConstraint(columnNames = { "matricule" }) 
	})
@NamedQueries({
	@NamedQuery(name = "getVoitureByMatricule",query = "select v from Voiture v where UPPER(v.matricule)=UPPER( :labelParam)"),
	@NamedQuery(name = "Voiture.findAll",query = "select v from Voiture v"),
	@NamedQuery(name = "Voiture.findAll.byColorLabel",query = "select v from Voiture v where UPPER(v.couleur.label)=UPPER( :labelParam)")
})
public class Voiture {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(nullable = false)
	private String matricule;
	
	@ManyToOne
	@JoinColumn(name = "modele")
	private Modele modele;
	
	@ManyToOne
	@JoinColumn(name = "couleur")
	private Couleur couleur;
}
