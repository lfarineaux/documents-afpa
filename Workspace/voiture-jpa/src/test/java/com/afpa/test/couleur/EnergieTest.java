package com.afpa.test.couleur;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ElementSimpleDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.service.IEnergieService;
import com.afpa.service.impl.EnergieServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@TestMethodOrder(OrderAnnotation.class)
public class EnergieTest {
	
	static IEnergieService energieService;
	
	@BeforeAll
	public static void init() {
		energieService = new EnergieServiceImpl();
	}
	
	static int energieCreeCode;
	static String energieCreeLabel;
	
	@Test
	@Order(1)
	void creation() {
		energieCreeLabel = "diesel"+System.currentTimeMillis();
		ReponseDto reponse = energieService.creerEnergie(energieCreeLabel);
		log.debug(reponse.toString());
		assertNotNull(reponse);
		assertEquals(reponse.getCode(), Status.OK);
		assertNotNull(reponse.getContenu());
		energieCreeCode =((CreationReponseDto) reponse.getContenu()).getCode();
	}

	@Test
	@Order(2)
	void recherche() {
		ReponseDto reponse = energieService.chercherEnergieParCode(energieCreeCode);
		log.debug(reponse.toString());
		assertNotNull(reponse);
		assertEquals(Status.OK,reponse.getCode());
		assertNotNull(reponse.getContenu());
//		idCouleurCree =((CreationReponseDto) reponse.getContenu()).getId();
	}
	
	@Test
	@Order(3)
	void modificatio() {
		String nouveauLabel = "diesel"+System.currentTimeMillis()+"M";
		ReponseDto reponse = energieService.mettreAjour(energieCreeLabel,nouveauLabel);
		assertNotNull(reponse);
		assertEquals(Status.OK,reponse.getCode());
		assertNotNull(reponse.getContenu());
		ElementSimpleDto resDto = (ElementSimpleDto)reponse.getContenu();
		assertEquals(nouveauLabel,resDto.getLabel());
	}
	
	
}
