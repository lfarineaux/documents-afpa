package org.eclipse.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.Personne;

@Controller
public class MessageController {

	@RequestMapping(value="/hello", method = RequestMethod.GET)
	public void sayHello() {
		System.out.println("Hello World!");
	}


	@RequestMapping(value="/message", method = RequestMethod.GET,params= {"msg"})
	public ModelAndView message(
			@RequestParam(value="msg") String msg,
			ModelAndView mv) {

		mv.addObject("msg",msg);
		mv.setViewName("vue");

		return mv;

	}

	@RequestMapping(value="/liste")
	public ModelAndView listPers(ModelAndView mv) {

		Random r= new Random();
		int x=r.nextInt(5);

		List <Personne> listpers = new ArrayList<Personne>();

		for (int i=0;i<x;i++) {
			Personne p = new Personne("nom "+i, "prenom "+i, "adresse "+i);
			listpers.add (p);
		}

		mv.addObject("list", listpers);
		mv.setViewName("vue");

		return mv;

	}


	@RequestMapping(value="/calcul")
	public ModelAndView initCalcul(ModelAndView mv) {

		mv.setViewName("calcul");

		return mv;

	}
	
	@RequestMapping(value="/calcul", method = RequestMethod.POST)
	public ModelAndView execCalcul(
		@RequestParam(value="param1", required = false) String nbreStr,
		@RequestParam(value="signe", required = false) String signe,
		@RequestParam(value="param2", required = false) String nbre2Str,
		ModelAndView mv) {
	
		String message="Calcul Ok";
		double total=0;
		
		if (nbreStr.equals("")||nbreStr.length()==0||nbre2Str.equals("")||nbre2Str.length()==0) {
			message = "Erreur - Information manquante";
			System.out.println(message);
		} else {
			if (nbreStr.matches("[-+]?\\d*\\.?\\d+") && nbre2Str.matches("[-+]?\\d*\\.?\\d+")) {

				int nbre = Integer.parseInt(nbreStr);

				int nbre2 = Integer.parseInt(nbre2Str);
			
				
				if("+".equals(signe)) {
					total = (nbre+nbre2);
				}else if("*".equals(signe)) {
					total =(nbre*nbre2);
				}else if("/".equals(signe)) {
					if(nbre2 != 0){
						total =(double)nbre/(double)nbre2;
					}else {
						message= "Erreur - Division par 0";
						System.out.println(message);
					}
				}else if(signe == null) {
					message= "Signe absent";
				}else {
					message= "Erreur - Mauvais signes";
					System.out.println(message);
				}
			}
			else {
				message= "Erreur - Saisir un chiffre";
				System.out.println(message);

			}
		}
		
		mv.addObject("Nbre1",nbreStr);
		
		mv.addObject("Nbre1",nbreStr);	
		mv.addObject("Nbre2",nbre2Str);
		mv.addObject("Signe",signe);
		mv.addObject("Message",message);
		mv.addObject("Total",total);

		mv.setViewName("calcul");

		return mv;

	}

}
