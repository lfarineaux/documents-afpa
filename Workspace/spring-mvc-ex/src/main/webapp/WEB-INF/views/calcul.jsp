<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CALCULATRICE</title>
</head>

<body>
<h1>CALCUL</h1>
<form method="post" action="">
<p>Chiffre 1 <input type="text" name="param1" value="${Nbre1}" /></p>

<p>Signe</p>
<input type="radio" id="plus" name="signe" value="+" ${(Signe == '+')?'checked':''}>+ <br>
<input type="radio" id="divi" name="signe" value="/" ${(Signe eq '/')?'checked':''}>/ <br>
<input type="radio" id="multi" name="signe" value="*" ${(Signe eq '*')?'checked':''}>* <br>

<br>
<p>Chiffre 2 <input type="text" name="param2" value="${Nbre2}" /></p>

<p><input type="submit" name="send"/></p>
</form>
<br>
<h1>RESULTAT</h1>
<p>Message<input type="text" name="res" value="${Message}" readonly></p>
<form method="post" action="">
<p><input type="text" name="res" value="${Nbre1}" readonly>
<c:out value="${Signe}"/>
<input type="text" name="res" value="${Nbre2}" readonly></p>

<p> = <input type="text" name="res" value="${Total}" readonly>
<fmt:formatNumber value = "${ Total }"
type="currency"/></p>

<br>
</form>


</body>
</html>