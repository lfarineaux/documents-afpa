<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<h1>Message :${msg}</h1>



	<table>
		<tr>
			<th>Nom</th>
			<th>Prenom</th>
			<th>Adresse</th>
		</tr>

		<c:forEach items="${list}" var="pers" varStatus="ligne">

			<%-- <c:if test="${ligne.count % 2 == 0}">  --%>
			<!-- <tr style="color:red"> -->
			<%-- </c:if> --%>

			<%-- <c:if test="${ligne.count % 2 != 0}">  --%>
			<!-- <tr style="color:green"> -->
			<%-- </c:if> --%>

			<c:choose>
				<c:when test="${ligne.count % 2 == 0}">
					<tr style="color: red">
				</c:when>
				<c:otherwise>
					<tr style="color: green">
				</c:otherwise>

			</c:choose>

			<td><c:out value="${ pers['nom']  }" /></td>
			<td><c:out value="${ pers['prenom']  }" /></td>

			</tr>

		</c:forEach>

	</table>


</body>
</html>