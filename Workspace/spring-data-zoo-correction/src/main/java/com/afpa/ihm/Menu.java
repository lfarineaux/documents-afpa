package com.afpa.ihm;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.afpa.service.IAlimentService;
import com.afpa.service.IAnimalService;
import com.afpa.service.IEnclosService;
import com.afpa.service.IResponsableService;

/**
 * La classe Menu contient le menu
 *
 * 
 * @authors ag/lf
 */

public class Menu {

	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(SpringConfiguration.class);
		ctx.refresh();
//		IEmployerService employerService = ctx.getBean(IEmployerService.class);
		IAlimentService alimentService = ctx.getBean(IAlimentService.class);
		IAnimalService animalService = ctx.getBean(IAnimalService.class);
		IEnclosService enclosService = ctx.getBean(IEnclosService.class);
		IResponsableService responsableService = ctx.getBean(IResponsableService.class);

		String menu = "";
		String sousmenu = "";
		boolean fin = true;
		boolean sousfin = true;

		/**
		 * @param menu : numéro saisi dans le menu fin : arrêt du programme
		 */

		while (fin) {

			System.out.println();
			System.out.println("      **************");
			System.out.println("      *    MENU    *");
			System.out.println("      **************");
			System.out.println();
			System.out.println("0- Arreter le programme");
			System.out.println("1- Menu Aliment");
			System.out.println("2- Menu Animal");
			System.out.println("3- Menu Enclos");
			System.out.println("4- Menu Responsable");

			System.out.print("-> ");
			menu = Clavier.lireTxt(sc, "");

			switch (menu) {
			case "0":
				System.out.println("0 - Arrêt du programme");
				fin = false;
				break;

			case "1":
				System.out.println("1- Menu Aliment");
				sousfin = true;

				while (sousfin) {

					System.out.println();
					System.out.println("      **************************");
					System.out.println("      *    SOUS-MENU ALIMENT   *");
					System.out.println("      **************************");
					System.out.println();
					System.out.println("0- Retourner au menu principal");
					System.out.println("1- Creer un Aliment");
					System.out.println("2- Lister les aliment");
					System.out.println("3- Mettre à jour un aliment");
					System.out.println("4- Mettre aliment avec animal");
					System.out.println("5- Supprimer un aliment");

					System.out.print("-> ");
					sousmenu = Clavier.lireTxt(sc, "");

					switch (sousmenu) {
					case "0":
						System.out.println("0 - Retour au menu principal");
						sousfin = false;
						break;

					case "1":
						System.out.println("1- Créer un aliment");
						System.out.print("Saisir le nom : ");
						String in = Clavier.lireTxt(sc, "");
						alimentService.creer(in);

						break;

					case "2":
						System.out.println("2- Lister les aliment");
						List list = alimentService.lister();
						list.forEach(System.out::println);
//						System.out.println(alimentService.lister());

						break;

					case "3":
						System.out.println("3- Mettre à jour un aliment");
						System.out.println("Saisir nom Aliment ");
						String res3 = Clavier.lireTxt(sc, "");
						System.out.println("Saisir nouveau nom Aliment ");
						String resnew = Clavier.lireTxt(sc, "");
						alimentService.miseAjourNom(res3, resnew);
//						ServiceAliment.majAliment(res3, resnew);
						break;

					case "4":
						System.out.println("4- Mettre aliment avec animal");
						System.out.println("Saisir nom Aliment ");
						String aliment = Clavier.lireTxt(sc, "");
						System.out.println("Saisir nouveau nom Aliment ");
						String animal = Clavier.lireTxt(sc, "");
						if (alimentService.atitrerAlimentAAnimal(aliment, animal)) {
							System.out.println("�a maaaaarch");
						} else {
							System.out.println("kooooooooooo");
						}
						break;
					case "5":
						// fonctionne pas
						System.out.println("2- Lister un Aliment");
						System.out.println("Saisir nom d aliment");
						String name = Clavier.lireTxt(sc, "");
						System.out.println(alimentService.alimentParNom(name));
//						System.out.println(ServiceResponsable.listertout());
						break;
					case "6":
						System.out.println("5- Supprimer un aliment");
						System.out.println("Saisir nom Aliment a sup");
						String asup = Clavier.lireTxt(sc, "");
						alimentService.supp(asup);
						break;

					default:
						System.out.println("Mauvaise saisie");
						continue;

					}
					break;
				}

				break;

			case "2":
				System.out.println("2- Menu Animal");
				sousfin = true;

				while (sousfin) {

					System.out.println();
					System.out.println("      *************************");
					System.out.println("      *    SOUS-MENU ANIMAL   *");
					System.out.println("      *************************");
					System.out.println();
					System.out.println("0- Retourner au menu principal");
					System.out.println("1- Créer un Animal");
					System.out.println("2- Lister animal");
					System.out.println("3- Mettre à jour un Animal");
					System.out.println("4- Mettre animal avec enclos");
					System.out.println("5- lister un animal");
					System.out.println("6- Supprimer un animal");
					System.out.print("-> ");
					sousmenu = Clavier.lireTxt(sc, "");

					switch (sousmenu) {
					case "0":
						System.out.println("0 - Retour au menu principal");
						sousfin = false;
						break;

					case "1":
						System.out.println("1- Creer un animal");
						System.out.print("Saisir l animal : ");
						String in = Clavier.lireTxt(sc, "");
						animalService.creer(in);
						break;

					case "2":
						System.out.println("2- Lister animal");

						List list = animalService.lister();
						list.forEach(System.out::println);

						break;

					case "3":
						System.out.println("3- Mettre à jour un Animal");
						System.out.println("Saisir Le nom de l animal");
						String res3 = Clavier.lireTxt(sc, "");
						System.out.println("Saisir Le nouveau nom de l animal");
						String res3a = Clavier.lireTxt(sc, "");
						animalService.miseAjourNom(res3, res3a);
//						ServiceAnimal.majAnimal(res3, res3a);
						break;

					case "4":
						System.out.println("4- Mettre animal avec enclos");
						System.out.println("Saisir l animal");
						String res4 = Clavier.lireTxt(sc, "");
						System.out.println("Saisir l enclos");
						String enclos = Clavier.lireTxt(sc, "");
						animalService.miseAjourEnclos(res4, enclos);
//					
						break;
					case "5":

						System.out.println("2- Lister un animal");
						System.out.println("Saisir nom de l animal");
						String name = Clavier.lireTxt(sc, "");
						System.out.println(animalService.animalParNom(name));
//					
						break;
					case "6":
						System.out.println("5- Supprimer un animal");
						System.out.println("Saisir l animal");
						String res5 = Clavier.lireTxt(sc, "");
						animalService.supp(res5);
						break;

					default:
						System.out.println("Mauvaise saisie");
						continue;

					}
					break;
				}

				break;

			case "3":
				System.out.println("3- Menu Enclos");
				sousfin = true;

				while (sousfin) {

					System.out.println();
					System.out.println("      *************************");
					System.out.println("      *    SOUS-MENU ENCLOS   *");
					System.out.println("      *************************");
					System.out.println();
					System.out.println("0- Retourner au menu principal");
					System.out.println("1- Creer un Enclos");
					System.out.println("2- Mettre à jour un Enclos");
					System.out.println("3- Supprimer un Enclos");
					System.out.println("4- Lister enclos");
					System.out.print("-> ");
					sousmenu = Clavier.lireTxt(sc, "");
					System.out.println();

					switch (sousmenu) {
					case "0":
						System.out.println("0 - Retour au menu principal");
						sousfin = false;
						break;

					case "1":
						System.out.println("1- Creer un enclos");
						System.out.print("Saisir le nom de l enclos : ");
						String in = Clavier.lireTxt(sc, "");
//						ServiceEnclos.creerenclos(in);
						enclosService.creer(in);
						break;

					case "2":

						System.out.println("2- Mettre à jour un Enclos");
						System.out.println("Saisir le nom de l enclos");
						String res2 = Clavier.lireTxt(sc, "");
						System.out.println("Saisir le nouveau nom de l enclos");
						String res2a = Clavier.lireTxt(sc, "");
//						ServiceEnclos.majEnclos(res2, res2a);
						enclosService.miseAjourNom(res2, res2a);
						break;

					case "3":
						// ne fonctionne pas
						System.out.println("3- Supprimer un Enclos");
						System.out.println("Saisir nom enclos");
						String res3 = Clavier.lireTxt(sc, "");
//						ServiceEnclos.supp(res3);
						enclosService.supp(res3);
						break;

					case "4":
						// fonctionne pas
						System.out.println("4- Lister enclos");
						List list = enclosService.lister();
						list.forEach(System.out::println);
						break;
					default:
						System.out.println("Mauvaise saisie");
						continue;
					}
					break;
				}
				break;

			case "4":
				System.out.println("4- Menu Responsable");
				sousfin = true;

				while (sousfin) {

					System.out.println();
					System.out.println("      *************************");
					System.out.println("      *    SOUS-MENU Responsable  *");
					System.out.println("      *************************");
					System.out.println();
					System.out.println("0- Retourner au menu principal");
					System.out.println("1- Créer un Responsable");
					System.out.println("2- Lister Responsable");
					System.out.println("3- Mettre à jour le nom d un Responsable");
					System.out.println("4- Mettre Enclos avec Responsable");
					System.out.println("5- Lister un responsable");
					System.out.println("6- Supprimer un Responsable");
					System.out.print("-> ");
					sousmenu = Clavier.lireTxt(sc, "");

					System.out.println();

					switch (sousmenu) {
					case "0":
						System.out.println("0 - Retour au menu principal");
						sousfin = false;
						break;

					case "1":
						System.out.println("1- Creer un Responsable");

						System.out.print("Saisir le nom et prenom du responsable (nom_prenom) : ");
						String in = Clavier.lireTxt(sc, "");
//						ServiceResponsable.creerResponsable(in);
						responsableService.creer(in);
						break;

					case "2":
						// fonctionne pas
						System.out.println("2- Lister Responsable");
						List list = responsableService.lister();
						list.forEach(System.out::println);

//						System.out.println(ServiceResponsable.listertout());
						break;

					case "3":

						System.out.println("3- Mettre à jour le nom d un Responsable");
						System.out.println("Saisir le nom de l employe");
						String resa = Clavier.lireTxt(sc, "");
						System.out.println("Saisir le nouveau nom");
						String new_name = Clavier.lireTxt(sc, "");
//						ServiceResponsable.majnom(resa, new_name);
						responsableService.miseAjourNom(resa, new_name);
						break;
					case "4":
						System.out.println("4- Mettre Enclos avec Responsable");
						System.out.println("Saisir le nom de l employe");
						String res = Clavier.lireTxt(sc, "");
						System.out.println("Saisir le nom de l enclos");
						String enclos = Clavier.lireTxt(sc, "");
//						ServiceResponsable.mettreEnclos(res, enclos);
						responsableService.miseAjourEnclos(res, enclos);
						break;
					case "5":
						// fonctionne pas
						System.out.println("2- Lister un responsable");
						System.out.println("Saisir nom du responsable");
						String name = Clavier.lireTxt(sc, "");
						System.out.println(responsableService.responsableParNom(name));
//						System.out.println(ServiceResponsable.listertout());
						break;
					case "6":
						// ne fonctionne pas
						System.out.println("5- Supprimer une Responsable");
						System.out.println("Saisir le responsable a DELETE");
						String res5 = Clavier.lireTxt(sc, "");
						responsableService.supp(res5);
						break;
					default:
						System.out.println("Mauvaise saisie");
						continue;

					}
					break;
				}

				break;
			}

		}

		sc.close();
	}
}
