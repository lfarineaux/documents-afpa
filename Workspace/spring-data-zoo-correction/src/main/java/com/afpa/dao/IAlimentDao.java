package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.afpa.entity.Aliment;

public interface IAlimentDao extends CrudRepository<Aliment, Integer> {

	public List<Aliment> findByAliment(@Param("nomParam") String p);

	public List<Aliment> findAll();
}
