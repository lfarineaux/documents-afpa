package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.afpa.entity.Animal;

public interface IAnimalDao extends CrudRepository<Animal, Integer> {

	public List<Animal> findByAnimal(@Param("nomParam") String p);

	public List<Animal> findAll();
}