package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.afpa.entity.Responsable;

public interface IResponsableDao extends CrudRepository<Responsable, Integer> {

	public List<Responsable> findByResp(@Param("nomParam") String p);

	public List<Responsable> findAll();
}