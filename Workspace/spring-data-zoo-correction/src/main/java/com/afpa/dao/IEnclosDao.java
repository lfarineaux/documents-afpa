package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.afpa.entity.Enclos;

public interface IEnclosDao extends CrudRepository<Enclos, Integer> {

	public List<Enclos> findByEnclos(@Param("nomParam") String p);

	public List<Enclos> findAll();

}