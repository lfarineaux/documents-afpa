package com.afpa.service;

import java.util.List;

import com.afpa.dto.EnclosDto;

public interface IEnclosService {

	EnclosDto creer(String nom);

	List<EnclosDto> lister();

	EnclosDto enclosParNom(String n);

	void miseAjourNom(String oldName, String newName);

	void supp(String d);

}
