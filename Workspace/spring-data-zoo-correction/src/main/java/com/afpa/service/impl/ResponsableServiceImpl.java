package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IEnclosDao;
import com.afpa.dao.IResponsableDao;
import com.afpa.dto.ResponsableDto;
import com.afpa.entity.Enclos;
import com.afpa.entity.Responsable;
import com.afpa.service.IResponsableService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ResponsableServiceImpl implements IResponsableService {

	@Autowired
	IResponsableDao responsableDao;

	@Override
	public ResponsableDto creer(String nom) {
		List<Responsable> responsables = responsableDao.findByResp(nom);
		if (responsables != null && responsables.size() != 0) {
			log.error("des employers ont deja ce nom : " + responsables);
			return null;
		}
		Responsable responsable = Responsable.builder().nom_Resp(nom).build();

		responsable = this.responsableDao.save(responsable);

		return ResponsableDto.builder().id(responsable.getId_Responsable()).build();
	}

	@Override
	public List<ResponsableDto> lister() {
		Iterator<Responsable> iterator = this.responsableDao.findAll().iterator();
		List<ResponsableDto> lst = new ArrayList<>();
		while (iterator.hasNext()) {
			Responsable x = iterator.next();
			lst.add(ResponsableDto.builder().id(x.getId_Responsable()).nom(x.getNom_Resp()).enclos(x.getEnclos())
					.build());
		}
		return lst;
	}

	@Override
	public ResponsableDto responsableParNom(String n) {
		Iterator<Responsable> iterator = this.responsableDao.findByResp(n).iterator();
		ResponsableDto res = null;
		if (iterator.hasNext()) {
			Responsable x = iterator.next();
			res = ResponsableDto.builder().id(x.getId_Responsable()).nom(x.getNom_Resp()).enclos(x.getEnclos()).build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Responsable> iterator = this.responsableDao.findByResp(oldName).iterator();
		ResponsableDto res = null;
		if (iterator.hasNext()) {
			Responsable x = iterator.next();
			x.setNom_Resp(newName);
			this.responsableDao.save(x);
		}
	}

	@Override
	public void supp(String d) {

		Iterator<Responsable> iterator = this.responsableDao.findByResp(d).iterator();
		if (iterator.hasNext()) {
			Responsable x = iterator.next();

			this.responsableDao.delete(x);
		}

	}
	@Autowired
	IEnclosDao enclosDao;
	@Override
	public void miseAjourEnclos(String responsable, String enclos) {
		Enclos enclosOk = new Enclos();
		Responsable responsableOk = new Responsable();
	
		Iterator<Responsable> iterator2 = responsableDao.findByResp(responsable).iterator();
		if (iterator2.hasNext()) {
			Responsable y = iterator2.next();
			responsableOk = y;
		}
		Iterator<Enclos> iterator = enclosDao.findByEnclos(enclos).iterator();
		if (iterator.hasNext()) {
			Enclos x = iterator.next();
			enclosOk = x;
			responsableOk.setEnclos(enclosOk);
			responsableDao.save(responsableOk);
		}
	}

}
