package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IEnclosDao;
import com.afpa.dto.EnclosDto;
import com.afpa.entity.Enclos;
import com.afpa.service.IEnclosService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EnclosServiceImpl implements IEnclosService {

	@Autowired
	IEnclosDao enclosDao;

	@Override
	public EnclosDto creer(String nom) {
		List<Enclos> encloss = (List<Enclos>) this.enclosDao.findByEnclos(nom);
		if (encloss != null && encloss.size() != 0) {
			log.error("des employers ont deja ce nom : " + encloss);
			return null;
		}
		Enclos enclos = Enclos.builder().nom_enclos(nom).build();

		enclos = this.enclosDao.save(enclos);

		return EnclosDto.builder().nom(enclos.getNom_enclos()).build();
	}

	@Override
	public List<EnclosDto> lister() {
		Iterator<Enclos> iterator = enclosDao.findAll().iterator();
		List<EnclosDto> lst = new ArrayList<>();
		while (iterator.hasNext()) {
			Enclos x = iterator.next();
			lst.add(EnclosDto.builder().id(x.getId_Enclos()).nom(x.getNom_enclos()).build());
		}
		return lst;
	}

	@Override
	public EnclosDto enclosParNom(String n) {
		Iterator<Enclos> iterator = ((List<Enclos>) this.enclosDao.findByEnclos(n)).iterator();
		EnclosDto res = null;
		if (iterator.hasNext()) {
			Enclos x = iterator.next();
			res = EnclosDto.builder().id(x.getId_Enclos()).nom(x.getNom_enclos()).build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Enclos> iterator = ((List<Enclos>) this.enclosDao.findByEnclos(oldName)).iterator();
		EnclosDto res = null;
		if (iterator.hasNext()) {
			Enclos x = iterator.next();
			x.setNom_enclos(newName);
			this.enclosDao.save(x);
		}
	}

	@Override
	public void supp(String d) {

		Iterator<Enclos> iterator = ((List<Enclos>) this.enclosDao.findByEnclos(d)).iterator();
		if (iterator.hasNext()) {
			Enclos x = iterator.next();

			this.enclosDao.delete(x);
		}

	}

}
