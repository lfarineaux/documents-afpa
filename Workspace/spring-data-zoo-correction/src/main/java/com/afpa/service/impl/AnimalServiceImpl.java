package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IAnimalDao;
import com.afpa.dao.IEnclosDao;
import com.afpa.dto.AnimalDto;
import com.afpa.entity.Animal;
import com.afpa.entity.Enclos;
import com.afpa.service.IAnimalService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AnimalServiceImpl implements IAnimalService {

	@Autowired
	IAnimalDao animalDao;

	@Override
	public AnimalDto creer(String nom) {
		List<Animal> animals = this.animalDao.findByAnimal(nom);
		if (animals != null && animals.size() != 0) {
			log.error("des employers ont deja ce nom : " + animals);
			return null;
		}
		Animal animal = Animal.builder().nom_animal(nom).build();

		animal = this.animalDao.save(animal);

		return AnimalDto.builder().id(animal.getId_Animal()).build();
	}

	@Override
	public List<AnimalDto> lister() {
		Iterator<Animal> iterator = this.animalDao.findAll().iterator();
		List<AnimalDto> lst = new ArrayList<>();
		while (iterator.hasNext()) {
			Animal x = iterator.next();
			lst.add(AnimalDto.builder().id(x.getId_Animal()).nom(x.getNom_animal()).enclos(x.getEnclos()).build());
		}
		return lst;
	}

	@Override
	public AnimalDto animalParNom(String n) {
		Iterator<Animal> iterator = this.animalDao.findByAnimal(n).iterator();
		AnimalDto res = null;
		if (iterator.hasNext()) {
			Animal x = iterator.next();
			res = AnimalDto.builder().id(x.getId_Animal()).nom(x.getNom_animal()).enclos(x.getEnclos()).build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Animal> iterator = this.animalDao.findByAnimal(oldName).iterator();
		AnimalDto res = null;
		if (iterator.hasNext()) {
			Animal x = iterator.next();
			x.setNom_animal(newName);
			this.animalDao.save(x);
		}
	}

	@Override
	public void supp(String d) {

		Iterator<Animal> iterator = this.animalDao.findByAnimal(d).iterator();
		if (iterator.hasNext()) {
			Animal x = iterator.next();
			this.animalDao.delete(x);
		}

	}
	@Autowired
	IEnclosDao enclosDao;
	@Override
	public Boolean miseAjourEnclos(String animal, String enclos) {
		
		List<Animal> findByAnimal = this.animalDao.findByAnimal(animal);
		if (findByAnimal == null || findByAnimal.isEmpty()) {
			return false;
		}
		List<Enclos> findByEnclos = enclosDao.findByEnclos(enclos);
		if (findByEnclos == null || findByEnclos.isEmpty()) {
			return false;
		}

		Enclos enclos2 = findByEnclos.get(0);
		Animal animal2 = findByAnimal.get(0);

		
		animal2.setEnclos(enclos2);
		
		this.animalDao.save(animal2);

		

		return true;
	}
}
