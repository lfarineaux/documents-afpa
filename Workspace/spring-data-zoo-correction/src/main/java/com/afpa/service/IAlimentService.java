package com.afpa.service;

import java.util.List;

import com.afpa.dto.AlimentDto;

public interface IAlimentService {

	AlimentDto creer(String nom);

	List<AlimentDto> lister();

	AlimentDto alimentParNom(String n);

	void miseAjourNom(String oldName, String newName);

	void supp(String d);

	public boolean atitrerAlimentAAnimal(String aliment, String animal);

}
