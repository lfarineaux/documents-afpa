package com.afpa.service;

import java.util.List;

import com.afpa.dto.AnimalDto;

public interface IAnimalService {

	AnimalDto creer(String nom);

	List<AnimalDto> lister();

	AnimalDto animalParNom(String n);

	void miseAjourNom(String oldName, String newName);

	void supp(String d);

	public Boolean miseAjourEnclos(String animal, String enclos);

}
