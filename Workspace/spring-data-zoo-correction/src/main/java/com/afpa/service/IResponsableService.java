package com.afpa.service;

import java.util.List;

import com.afpa.dto.ResponsableDto;

public interface IResponsableService {

	ResponsableDto creer(String nom);

	List<ResponsableDto> lister();

	ResponsableDto responsableParNom(String n);

	void miseAjourNom(String oldName, String newName);

	void supp(String d);

	public void miseAjourEnclos(String responsable, String enclos);
}
