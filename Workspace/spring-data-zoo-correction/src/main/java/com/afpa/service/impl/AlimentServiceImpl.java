package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IAlimentDao;
import com.afpa.dao.IAnimalDao;
import com.afpa.dto.AlimentDto;
import com.afpa.entity.Aliment;
import com.afpa.entity.Animal;
import com.afpa.service.IAlimentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlimentServiceImpl implements IAlimentService {

	@Autowired
	IAlimentDao alimentDao;

	@Override
	public AlimentDto creer(String nom) {
		List<Aliment> aliments = (List<Aliment>) this.alimentDao.findByAliment(nom);
		if (aliments != null && aliments.size() != 0) {
			log.error("des employers ont deja ce nom : " + aliments);
			return null;
		}
		Aliment aliment = Aliment.builder().nom_aliment(nom).build();

		aliment = this.alimentDao.save(aliment);

		return AlimentDto.builder().id(aliment.getId_Aliment()).build();
	}

	@Override
	public List<AlimentDto> lister() {
		Iterator<Aliment> iterator = this.alimentDao.findAll().iterator();
		List<AlimentDto> lst = new ArrayList<>();
		while (iterator.hasNext()) {
			Aliment x = iterator.next();
			lst.add(AlimentDto.builder().id(x.getId_Aliment()).nom(x.getNom_aliment()).animal(x.getAnimal()).build());
		}
		return lst;
	}

	@Override
	public AlimentDto alimentParNom(String n) {
		Iterator<Aliment> iterator = alimentDao.findAll().iterator();
		AlimentDto res = null;
		if (iterator.hasNext()) {
			Aliment x = iterator.next();
			res = AlimentDto.builder().id(x.getId_Aliment()).nom(x.getNom_aliment()).animal(x.getAnimal()).build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Aliment> iterator = alimentDao.findByAliment(oldName).iterator();
		AlimentDto res = null;
		if (iterator.hasNext()) {
			Aliment x = iterator.next();
			x.setNom_aliment(newName);
			this.alimentDao.save(x);
		}
	}

	@Override
	public void supp(String d) {

		Iterator<Aliment> iterator = this.alimentDao.findByAliment(d).iterator();
		if (iterator.hasNext()) {
			Aliment x = iterator.next();

			this.alimentDao.delete(x);
		}

	}

	@Autowired
	IAnimalDao animalDao;

	@Override
	public boolean atitrerAlimentAAnimal(String aliment, String animal) {
		List<Aliment> findByAliment = this.alimentDao.findByAliment(aliment);
		if (findByAliment == null || findByAliment.isEmpty()) {
			return false;
		}

		List<Animal> findByAnimal = this.animalDao.findByAnimal(animal);
		if (findByAnimal == null || findByAnimal.isEmpty()) {
			return false;
		}

		Aliment aliment2 = findByAliment.get(0);
		Animal animal2 = findByAnimal.get(0);

		Set<Animal> animaux = aliment2.getAnimal();
		if (animaux == null) {
			animaux = new HashSet<>();
			aliment2.setAnimal(animaux);
		}

		animaux.add(animal2);

		this.alimentDao.save(aliment2);

		return true;
	}

}
