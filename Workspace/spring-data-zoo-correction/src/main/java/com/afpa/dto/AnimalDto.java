package com.afpa.dto;

import com.afpa.entity.Enclos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@Getter
@Setter
public class AnimalDto {
	private int id;
	private String nom;
	private Enclos enclos;
}
