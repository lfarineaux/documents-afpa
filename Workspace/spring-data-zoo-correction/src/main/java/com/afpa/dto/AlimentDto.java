package com.afpa.dto;

import java.util.Set;

import com.afpa.entity.Animal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@Getter
@Setter
public class AlimentDto {
	private int id;
	private String nom;
	private Set<Animal> animal;
}
