package com.afpa.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "Animal")
@NamedQueries({ @NamedQuery(name = "Animal.FindAll", query = "SELECT p FROM Animal p"),
		@NamedQuery(name = "Animal.findByAnimal", query = "SELECT p FROM Animal p where p.nom_animal= :nomParam"), })
public class Animal {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Animal", length = 3, nullable = false)
	private int id_Animal;

	@Column(name = "nom_animal", length = 25, unique = true, nullable = false)
	private String nom_animal;

//	@ManyToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "puissance")
//	private Set<Modele> modeles;

	@ManyToOne(optional = true, cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "nom_enclos")
	private Enclos enclos;

}
