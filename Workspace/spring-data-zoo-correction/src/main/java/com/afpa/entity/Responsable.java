package com.afpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "Responsable")
@NamedQueries({ @NamedQuery(name = "Responsable.FindAll", query = "SELECT p FROM Responsable p"),
		@NamedQuery(name = "Responsable.findByResp", query = "SELECT p FROM Responsable p where p.nom_Resp= :nomParam"), })
public class Responsable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Responsable", length = 3, nullable = false)
	private int id_Responsable;

	@Column(name = "nom_Resp", length = 25, nullable = false)
	private String nom_Resp;

	@OneToOne(optional = true)
	private Enclos enclos;

}