package com.afpa.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "Aliment")
@NamedQueries({ @NamedQuery(name = "Aliment.FindAll", query = "SELECT p FROM Aliment p"),
		@NamedQuery(name = "Aliment.findByAliment", query = "SELECT p FROM Aliment p where p.nom_aliment= :nomParam"), })
public class Aliment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Aliment", length = 3, nullable = false)
	private int id_Aliment;

	@Column(name = "nom_aliment", length = 25, nullable = false, unique = true)
	private String nom_aliment;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinTable(name = "Aliment_Animal", joinColumns = { @JoinColumn(name = "nom_Animal") }, inverseJoinColumns = {
			@JoinColumn(name = "nom_aliment") })
	private Set<Animal> animal;

}