package com.afpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Table(name = "Enclos")
@NamedQueries({ 
	@NamedQuery(name = "Enclos.FindAll", query = "SELECT p FROM Enclos p"),
	@NamedQuery(name = "Enclos.findByEnclos", query = "SELECT p FROM Enclos p where p.nom_enclos= :nomParam"), 
 })
public class Enclos {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Enclos", length = 3, nullable = false)
	private int id_Enclos;

	@Column(name = "nom_enclos", length = 25, nullable = false, unique = true)
	private String nom_enclos;



}
