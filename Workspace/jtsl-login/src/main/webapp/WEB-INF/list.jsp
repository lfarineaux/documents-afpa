<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.dto.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LIST</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body style="background-color: #eaf2f8; font-family: Tw Cen MT">
	<div class="row justify-content-md-center">
		<h2><a href="list.do" >Liste Produits</a></h2>
	</div>
	<div class="row justify-content-md-center">
		<div class="col col-lg-2 mt-1">
			<p>Login : ${sessionScope.user.login}</p>
		</div>
	</div>
	<div class="row justify-content-md-center">
		<c:if test="${sessionScope.user.login==null}">
			<a class="btn btn-secondary mb-3" href="login.do" role="button">Se
				connecter</a>
			<br>
		</c:if>
		<c:if test="${sessionScope.user.login!=null}">
			<a class="btn btn-secondary mb-3" href="deconnexion.do" role="button">Se
				déconnecter</a>
			<br>
			<br>
		</c:if>
		<c:if test="${sessionScope.user.login!=null}">
			<a class="btn btn-secondary mb-3" href="ajout.do" role="button">Ajout
				produit</a>
			<br>
		</c:if>

	</div>
	<div class="row justify-content-md-center">
		<p>
			Message :
			<c:out value="${msg}" />
		</p>
	</div>
	<div class="row justify-content-md-center">
		<table id="data-table"
			style="background-color: white; border: solid 1px Gray;">
			<tr style="border: solid 1px Gray;">
				<th>Label</th>
				<th>Prix</th>
			</tr>

			<c:forEach items="${listProd}" var="p">
				<tr style="border: solid 1px Gray;">
					<td><c:out value="${p.label}" /></td>
					<td><c:out value="${p.prix}" /></td>
				</tr>
			</c:forEach>

		</table>
	</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>