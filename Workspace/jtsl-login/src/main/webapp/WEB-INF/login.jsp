<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.dto.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LOGIN</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body style="background-color: #eaf2f8; font-family: Tw Cen MT">
	<div class="row justify-content-md-center">
		<div class="col col-lg-8 mt-1">
	<h2>Connexion</h2>
	</div>
	</div>
<div class="row justify-content-md-center">
		<div class="col col-lg-8 mt-1">
	<p>Login : ${sessionScope.user.login}</p>

	<p>
		Message :
		<c:out value="${msg}" />
	</p>

	<form method="post" action="login.do">

		<label for="login">Login : </label>

		<p>
			<input type="text" name="login" value="">
		</p>

		<label for="pwd">Password : </label>

		<p>
			<input type="password" name="pwd" value="">
		</p>

		<p>
			<input type="submit" name="send" />
		</p>

	</form>
	</div>
	</div>
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>