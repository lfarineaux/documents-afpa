package com.afpa.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.afpa.dto.ProduitDto;
import com.afpa.dto.UserDto;

/**
 * Servlet implementation class ConnectionServlet
 */
@WebServlet(urlPatterns = {"/deconnexion.do"})
public class DeconnexionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserDto userEnCours = null;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher;
		HttpSession session = request.getSession();
		userEnCours = (UserDto)session.getAttribute("user");
		
		if (userEnCours!=null) {
			session.invalidate();
//			request.setAttribute("msg", "Fin de connexion");
//			
		} else {
//			request.setAttribute("msg", "Pas d'user connecte");
		}
//		dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/list.jsp");
//		dispatcher.forward(request, response);
		response.sendRedirect("list.do");
		
	}


}
