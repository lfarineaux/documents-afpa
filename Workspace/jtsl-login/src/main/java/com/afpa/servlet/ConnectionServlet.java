package com.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.afpa.dto.UserDto;

/**
 * Servlet implementation class ConnectionServlet
 */
@WebServlet(urlPatterns = {"/login.do"})
public class ConnectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher;

		dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/login.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher;
		String login = request.getParameter("login");
		String pwd = request.getParameter("pwd");

		if (login!=null && pwd !=null && login.length()!=0 && pwd.length()!=0) {
			UserDto userEnCours;
			userEnCours = new UserDto(login,pwd);
			
			HttpSession session = request.getSession();
			session.setAttribute("user",userEnCours);
			
//			request.setAttribute("msg", "Connexion Ok");
//			dispatcher = getServletContext().getRequestDispatcher( "/list.do");
//			dispatcher.forward(request, response);
			response.sendRedirect("list.do");

		} else {
			request.setAttribute("msg", "Merci de saisir un login ET un mot de passe");
			dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/login.jsp");
			dispatcher.forward(request, response);
		}
	}
}
