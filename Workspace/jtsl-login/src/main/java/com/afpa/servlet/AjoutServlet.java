package com.afpa.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.afpa.dto.ProduitDto;
import com.afpa.dto.UserDto;

/**
 * Servlet implementation class ConnectionServlet
 */
@WebServlet(urlPatterns = {"/ajout.do"})
public class AjoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static List <ProduitDto> produits = new ArrayList<ProduitDto>();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher;
		UserDto userEnCours = null;
		HttpSession session = request.getSession();
		userEnCours = (UserDto)session.getAttribute("user");
		
		if (userEnCours!=null) {
			dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/ajout.jsp");
			dispatcher.forward(request, response);		
		} else {
//			request.setAttribute("msg", "Merci de saisir un login ET un mot de passe");
//			dispatcher = getServletContext().getRequestDispatcher( "/WEB-INF/login.jsp");
//			dispatcher.forward(request, response);
			response.sendRedirect("login.do");
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String label = request.getParameter("label");
		String prix = request.getParameter("prix");

		produits.add(new ProduitDto(label, prix));

		request.setAttribute("msg","Produit "+label+" ajoute");
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher( "/list.do");
		dispatcher.forward(request, response);
		
//		response.sendRedirect("list.do?msg=Produit "+label+" ajoute");

	}


}
