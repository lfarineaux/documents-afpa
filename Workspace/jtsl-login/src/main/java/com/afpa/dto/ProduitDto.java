package com.afpa.dto;

public class ProduitDto {

	public String label;
	public String prix;
	
	
	public ProduitDto(String label, String prix) {
		this.label = label;
		this.prix = prix;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getPrix() {
		return prix;
	}
	
	public void setPrix(String prix) {
		this.prix = prix;
	}
	
	@Override
	public String toString() {
		return "ProduitDto [label=" + label + ", prix=" + prix + "]";
	}
		
	
	
}
