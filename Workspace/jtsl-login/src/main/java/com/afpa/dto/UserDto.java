package com.afpa.dto;

public class UserDto {

	public String login;
	public String pwd;

	public UserDto(String login, String pwd) {
		this.login = login;
		this.pwd = pwd;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Override
	public String toString() {
		return "UserDto [login=" + login + ", pwd=" + pwd + "]";
	}
	
		
	
	
	
}
