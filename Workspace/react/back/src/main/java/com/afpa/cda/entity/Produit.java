package com.afpa.cda.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Produit {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PRODUIT_SEQ")
	@Column(name="id")
	private int id ;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="Prix")
	private String prix;
	
	

	

}
