package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.IProduitDao;
import com.afpa.cda.dto.CategorieDto;
import com.afpa.cda.dto.ProduitDto;
import com.afpa.cda.entity.Categorie;
import com.afpa.cda.entity.Produit;

@Service
public class ProduitServiceImpl implements IProduitService{



	@Autowired
	private IProduitDao produitDao;

	@Autowired
	private ModelMapper modelMapper;


	@Override
	public boolean deleteById(int id) {
		if(this.produitDao.existsById(id)) {
			this.produitDao.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public ProduitDto findById(int id) {
		Optional<Produit> prod = this.produitDao.findById(id);
		ProduitDto prodDto = null;
		if(prod.isPresent()) {
			Produit p = prod.get();
			System.out.println("++++++++++++++++++++++++++++"+  p);
			prodDto = ProduitDto.builder()
					.id(p.getId())
					.nom(p.getNom())
					.prix(p.getPrix())
					.build();
		}

		System.out.println("badrane=================== "+prodDto);
		return prodDto;
	}

	@Override
	public Integer ajouter(ProduitDto build) {
		Produit p = this.modelMapper.map(build,Produit.class);
		p = this.produitDao.save(p);
		return p.getId();

	}


	@Override
	public List<ProduitDto> searchAllProduct() {


		List<ProduitDto> maliste = this.produitDao.findAll()
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.prix(e.getPrix())
						.build())
				.collect(Collectors.toList());

		return maliste;
	}


	@Override
	public void updateProduit(int id, String nom, String prix)  {
		Optional<Produit> prod = this.produitDao.findById(id);
		if (prod.isPresent()) {
			Produit p = prod.get();
			p.setNom(nom);
			p.setPrix(prix);

			this.produitDao.save(p);

		}
	}




}
