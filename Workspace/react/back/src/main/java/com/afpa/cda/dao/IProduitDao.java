package com.afpa.cda.dao;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Produit;

@Repository
public interface IProduitDao extends CrudRepository<Produit, Integer> {
	
	
	@Query("select pr from Produit pr")
	public  List<Produit>findAll();
	

}
