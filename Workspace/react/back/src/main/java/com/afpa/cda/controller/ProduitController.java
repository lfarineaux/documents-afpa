package com.afpa.cda.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.ProduitDto;
import com.afpa.cda.service.IProduitService;
import com.afpa.cda.service.ProduitServiceImpl;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class ProduitController {

	@Autowired
	private IProduitService produitService;

	


	@RequestMapping(value="/liste")
	public ResponseEntity<?> initAddProd() {
		List<ProduitDto> list=produitService.searchAllProduct();
		return new ResponseEntity(list, HttpStatus.OK);
		
		
	}

//	@RequestMapping(value="/addProduit",method =RequestMethod.POST)
//	public void addProd(
//			
//			) {
//		
//
//		this.produitService.ajouter(
//				ProduitDto.builder()
//				.nom(label)
//				.prix(prix)
//				.build());
//		
//
//	}
//
//	
//@RequestMapping(value="/listProduit", method = RequestMethod.GET)
//	public ModelAndView list
//	(@RequestParam(value = "page", required = false) String pageStr, ModelAndView mv) {
//		int page = 0;
//		if(pageStr == null || pageStr.length() == 0) {
//
//		} else {
//			try {
//				page = Integer.parseInt(pageStr.trim());
//				if (page > ProduitServiceImpl.nbPage) {
//					page = 0;
//				}
//			} catch (NumberFormatException e) {
//				String message = "Erreur pas de lettre pour le numero de page";
//
//			}
//		}
//		List<ProduitDto> listProduit = this.produitService.searchAllProduct(page);
//		mv.addObject("pageN", page);
//		mv.addObject("pageMax", UserServiceImpl.nbPage);
//		mv.addObject("listProduit", listProduit);
//		mv.setViewName("listProd");
//		return mv;
//	}
//	
//	
//	@RequestMapping(value="/showProduit", method = RequestMethod.GET, params = {"id"})
//	public ModelAndView initShow(@RequestParam(value = "id") String idStr, ModelAndView mv) {
//		int id = 0;
//		String message = null;
//		ProduitDto produitDto = null;
//		try {
//			id = Integer.parseInt(idStr.trim());
//			produitDto = this.produitService.findById(Integer.parseInt(idStr.trim()));
//			System.out.println(produitDto);
//
//		} catch (NumberFormatException e) {
//			message = "Erreur pas de lettre pour l'id";
//		} catch (Exception e) {
//			message = e.getMessage();
//		}
//		mv.addObject("id", id);
//		mv.addObject("labelProd",produitDto.getNom());
//		mv.addObject("prixProd", produitDto.getPrix());
//		mv.addObject("quantiteProd", produitDto.getQuantite());
//		mv.addObject("categorieProd", produitDto.getCategorie().getNom());
//		mv.setViewName("showProd");
//		return mv;
//	}
//
//	@RequestMapping(value="/updateProduit",
//			method =RequestMethod.GET,params = {"id"})
//	public ModelAndView initUpdate
//	(@RequestParam(value = "id") String idStr,
//			ModelAndView mv) {
//		int id=0;
//		String message=null;
//		ProduitDto produitDto = null;
//		try {
//			id = Integer.parseInt(idStr.trim());
//			produitDto =this.produitService.findById(id);
//		} catch (NumberFormatException e) {
//			message = "Erreur pas de lettre pour l'id";
//		}catch (Exception e) {
//			message = e.getMessage();
//		}
//
//		mv.addObject("id", id);
//		mv.addObject("oldNomProd",produitDto.getNom());
//		mv.addObject("oldPrixProd", produitDto.getPrix());
//		
//		
//		mv.setViewName("updateProd");
//		return mv;
//	}
//
//	@RequestMapping(value="/updateProduit", method= RequestMethod.POST)
//	public void update(@RequestParam(value = "id") String idStr, 
//			@RequestParam(value="labelProd") String label,
//			@RequestParam(value="prixProd") String prix,
//			@RequestParam(value="quantiteProd") String quantite,
//			@RequestParam(value="categorieProd") Categorie categorie,
//			HttpServletResponse resp) throws IOException {
//		int id = 0;
//		try {
//			id = Integer.parseInt(idStr.trim());
//
//		} catch (NumberFormatException e) {
//			String message = "Erreur pas de lettre pour l'id";
//		}
//		this.produitService.updateProduit(id, label, prix, quantite, categorie);
//		resp.sendRedirect("listProduit?page=0");
//	}
//
//
//	@RequestMapping(value="/deleteProduit",
//			method=RequestMethod.GET,params = {"id"})
//	public void delete(
//			@RequestParam(value = "id") String idStr,
//			HttpServletResponse resp) throws IOException {
//		int id = 0;
//		try {
//			id = Integer.parseInt(idStr.trim());
//		} catch (NumberFormatException e) {
//			String message = "Erreur pas de lettre pour l'id";
//		}
//		this.produitService.deleteById(id);
//		resp.sendRedirect("listProduit?page=0");
//	}

}
