package com.afpa.cda.service;

import java.util.List;

import com.afpa.cda.dto.ProduitDto;

public interface IProduitService {

	
	public boolean deleteById(int id);
	
	public ProduitDto findById(int id);

	public Integer ajouter(ProduitDto build);

	List<ProduitDto> searchAllProduct();
	



	void updateProduit(int id, String nom, String prix);

	
}
