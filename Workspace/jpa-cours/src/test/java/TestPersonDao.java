import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import myapp.dao.CarDaoImpl;
import myapp.dao.IDao;
import myapp.dao.PersonDaoImpl;
import myapp.model.Car;
import myapp.model.Person;

@TestMethodOrder(value = OrderAnnotation.class)
public class TestPersonDao {
	static PersonDaoImpl dao;
	static Person p;
	static Car c;

	@BeforeAll
	public static void beforeAll() {
		dao = new PersonDaoImpl();
		dao.init();
	}


	@AfterAll
	public static void afterAll() {
		dao.close();
	}


	@Test
	@Order(1)
	public void testAddOne() {
		Person p = new Person ("Eliott",new Date());
		assertNotNull(p);
		Person add=dao.addOne(p);
		assertNotNull(add);
	}

	@Test
	@Order(2)
	public void testFindOne() {
		Person find=dao.findOne(p.getId());
		assertNotNull(find);
		assertEquals(find.getFirstName(), p.getFirstName());

	}


	@Test
	@Order(3)
	public void testUpdateOne() {
		String firstName="UpDateOne";
		p.setFirstName(firstName);
		dao.updateOne(p);
		Person upDateOne = dao.findOne(p.getId());
		assertNotNull(upDateOne);		
		assertEquals(firstName, p.getFirstName());
		
	}


	@Test
	@Order(4)
	public void testremoveOne() {
		dao.removeOne(p.getId());
		Person removeone = dao.findOne(p.getId());
		assertNull(removeone);
		
	}

	@Test
	@Order(5)
	public void testfindAll() {
		List <Person> listPerson=dao.findAll();
		assertNotEquals(0, listPerson.size());
	}

	@Test
	@Order(6)
	public void findAllnq() {
		List <Person> listPerson=dao.findAll();
		assertNotEquals(0, listPerson.size());
	}
	
	@Test
	@Order(7)
	public void findPersonsByFirstName() {
		String firstName="Ketsia";
		List <Person> findPerson = dao.findPersonsByFirstName(firstName);
		System.out.println(findPerson);
	}



	@Test
	@Order(8)	
	public void findPersonsByCarModel(String model) {
		p = new Person ("Lolo",new Date());
		c = new Car ("1a59","911");
		Set <Car> listCar = null;
		listCar.add(c);
		p.setCars(listCar);

//		Car tmp=dao.addOne(p);
//		assertNotNull(tmp);
	}


}