import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import myapp.dao.CarDaoImpl;
import myapp.dao.IDao;
import myapp.model.Car;
import myapp.model.Person;
public class TestCarDao {
	static CarDaoImpl dao;
	static Person p;
	static Car c;

	@BeforeAll
	public static void beforeAll() {
		dao = new CarDaoImpl();
		dao.init();
	}


	@AfterAll
	public static void afterAll() {
		dao.close();
	}
	
	@Test
	public void testAddCar() {
		c = new Car ("1a59","911");
		Car tmp=dao.addOne(c);
		assertNotNull(tmp);
	}
	
	@Test
	public void testFindOne() {
		c = new Car ("22bb59","find");
		Car tmp=dao.findOne(c.getImmatriculation());
		assertNotNull(tmp);

	}

	@Test
	public void testUpdateOne() {
		c = new Car ("9z59","UpDateOne");
		assertNotNull(c);
		c.setModel("UpDated");
		dao.updateOne(c);
		assertTrue(c.getModel().equals("UpDated"));
	}



	@Test
	public void testRemoveOne() {
		c = new Car ("8c59","RemoveOne");
		assertNotNull(c);
		dao.removeOne(c.getImmatriculation());
		assertNull(c);
	}

	@Test
	public void testfindAll() {
		List <Car> listCar=dao.findAll();
		assertNotEquals(0, listCar.size());
	}
	
	
	
}