package myapp.dao;
import java.util.List;

import myapp.model.Person;

public interface IPersonDao extends IDao<Person,Long> {

	
	public List<Person> findPersonsByFirstName(String str);
	
	public List<Person> findAllnq();
	
}
