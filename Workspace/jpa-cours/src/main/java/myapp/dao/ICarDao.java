package myapp.dao;
import java.util.List;

import myapp.model.Car;
import myapp.model.Person;

public interface ICarDao extends IDao <Car,String> {

	public List<Person> findCarByModel(String str);

	List<Person> findPersonsByCarModel(String model);
	
}
