package myapp.dao;

import java.util.List;

import myapp.model.Car;

public interface IDao <T,U> {

	public T addOne(T obj) ;
	public T findOne(U id) ;
	public void updateOne(T obj) ;
	public void removeOne(U id) ;
}

