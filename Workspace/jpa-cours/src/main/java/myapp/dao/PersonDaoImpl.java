package myapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import myapp.model.Car;
import myapp.model.Person;

public class PersonDaoImpl implements IPersonDao {

	private EntityManagerFactory factory = null;

	public void init() {
		factory = Persistence.createEntityManagerFactory("myBase");
	}

	public void close() {
		if (factory != null) {
			factory.close();
		}
	}

	// Creer un EM et ouvrir une transaction
	private EntityManager newEntityManager() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		return (em);
	}
	// Fermer un EM et defaire la transaction si necessaire
	private void closeEntityManager(EntityManager em) {
		if (em != null) {
			if (em.isOpen()) {
				EntityTransaction t = em.getTransaction();
				if (t.isActive()) {
					try {
						t.rollback();
					} catch (PersistenceException e) {
					}
				}
				em.close();
			}
		}
	}



	//		public Person addPerson(Person p) {
	//			EntityManager em = null;
	//			try {
	//				em = factory.createEntityManager();
	//				em.getTransaction().begin();
	//				// utilisation de l'EntityManager
	//				em.persist(p);
	//				em.getTransaction().commit();
	//				System.err.println("addPerson witdh id=" + p.getId());
	//				return (p);
	//			} finally {
	//				if (em != null) {
	//					em.close();
	//				}
	//			}
	//		}

	//	 Nouvelle version simplifiee

	@Override
	public Person addOne(Person p) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			// utilisation de l'EntityManager
			em.persist(p);
			em.getTransaction().commit();
			System.err.println("addPerson witdh id=" + p.getId());
			return (p);
		} finally {
			closeEntityManager(em);
		}
	}

	@Override
	public Person findOne(Long id) {
		EntityManager em = null;
		try {
			em = factory.createEntityManager();
			em.getTransaction().begin();
			// utilisation de l'EntityManager
			Person p = em.find(Person.class, id);
			System.err.println("FindPerson witdh id=" + p.getId());
			return p;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void updateOne(Person p) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			Person find = em.find(Person.class, p.getId());
			find.setBirthDay(p.getBirthDay());
			find.setFirstName(p.getFirstName());
			em.merge(find);
			System.err.println("UpDatePerson witdh id=" + p.getId());
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void removeOne(Long id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			em.remove(em.find(Person.class, id));
			em.getTransaction().commit();
			System.err.println("RemovePerson witdh id=" + id);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public List<Person> findAll() {
		EntityManager em = null;
		try {
			em = newEntityManager();
			String query = "SELECT p FROM Person p";
			TypedQuery<Person> q = em.createQuery(query, Person.class);
			System.err.println("FindAllPerson");
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}

	@Override
	public List<Person> findAllnq() {

		EntityManager em = null;
		try {
			em = newEntityManager();
			Query q = em.createNamedQuery("findAll");

			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}

	@Override
	public List<Person> findPersonsByFirstName(String pattern) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			String query = "SELECT p FROM Person p where p.firstname=?1";
			TypedQuery<Person> q = em.createQuery(query, Person.class);
			q.setParameter(1, pattern);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}


	}

	

}


