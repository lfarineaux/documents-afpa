package myapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import myapp.model.Car;
import myapp.model.Person;

public class CarDaoImpl implements ICarDao {

	private EntityManagerFactory factory = null;

	public void init() {
		factory = Persistence.createEntityManagerFactory("myBase");
	}

	public void close() {
		if (factory != null) {
			factory.close();
		}
	}

	// Creer un EM et ouvrir une transaction
	private EntityManager newEntityManager() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		return (em);
	}
	// Fermer un EM et defaire la transaction si necessaire
	private void closeEntityManager(EntityManager em) {
		if (em != null) {
			if (em.isOpen()) {
				EntityTransaction t = em.getTransaction();
				if (t.isActive()) {
					try {
						t.rollback();
					} catch (PersistenceException e) {
					}
				}
				em.close();
			}
		}
	}


	@Override
	public Car addOne(Car c) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			// utilisation de l'EntityManager
			em.persist(c);
			em.getTransaction().commit();
			System.err.println("addCar witdh id=" + c.getModel());
			return (c);
		} finally {
			closeEntityManager(em);
		}
	}


	@Override
	public Car findOne(String pattern) {
		EntityManager em = null;
		try {
			em = factory.createEntityManager();
			em.getTransaction().begin();
			// utilisation de l'EntityManager
			Car c = em.find(Car.class, pattern);
			System.err.println("FindCar witdh id=" + c.getModel());
			return c;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void updateOne(Car c) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			Car find = em.find(Car.class, c.getImmatriculation());
			find.setModel(c.getModel());
			find.setImmatriculation(c.getImmatriculation());
			em.merge(find);
			System.err.println("UpDateCar witdh id=" + c.getImmatriculation());
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void removeOne(String id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			em.remove(em.find(Car.class, id));
			System.err.println("RemoveCar witdh id=" + id);
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	
	public List<Car> findAll() {
		EntityManager em = null;
		try {
			em = newEntityManager();
			String query = "SELECT p FROM Car p";
			TypedQuery<Car> q = em.createQuery(query, Car.class);
			System.err.println("FindAllCar");
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}

	@Override
	public List<Person> findCarByModel(String pattern) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			String query = "SELECT p FROM Person p where p.firstname=?1";
			TypedQuery<Person> q = em.createQuery(query, Person.class);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}

	@Override
	public List<Person> findPersonsByCarModel(String model) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			Query q = em.createNamedQuery("findOwnersByModel");

			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}
}