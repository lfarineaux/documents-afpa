package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.ModeleDao;
import com.afpa.entity.Couleur;
import com.afpa.entity.Energie;
import com.afpa.entity.Modele;
import com.afpa.entity.Voiture;

@TestMethodOrder(value = OrderAnnotation.class)
public class ModeleDaoTest {
	static ModeleDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new ModeleDao();

	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Modele modele;

	@Test
	@Order(1)
	public void ajoutModele() {
		Modele m = Modele.builder().label("206").puissance(4).build();
		m = dao.add(m);
		assertNotNull(m);
		assertNotEquals(0, m.getCode());
		modele = m;
	}

	@Test
	@Order(2)
	public void miseAjourModele() {
		modele.setLabel("306");
		dao.update(modele);
		Modele m = dao.find(modele.getCode());
		assertNotNull(m);
		assertEquals("306", modele.getLabel());
	}

	@Test
	@Order(3)
	public void supprimerModele() {
		dao.remove(modele.getCode());
		Modele m = dao.find(modele.getCode());
		assertNull(m);
	}

	@Test
	@Order(4)
	public void ajoutVoitureAvecModele() {
		Modele m = Modele.builder().label("c3").puissance(12).build();

		Voiture v = new Voiture();
		v.setModele(m);

		Voiture v2 = new Voiture();
		v.setModele(m);

		List<Voiture> voitures = Arrays.asList(v, v2);
		m.setVoitures(new HashSet<Voiture>(voitures));

		modele = dao.add(m);
		assertNotNull(modele);
		assertNotEquals(0, modele.getCode());
	}

	@Test
	@Order(5)
	public void listerModele() {
		Collection<Modele> modele = dao.findAllNamedQuery("listeModele");
		assertNotEquals(0, modele.size());
		modele.stream().forEach(System.out::println);
	}

	@Test
	@Order(6)
	public void trouverModele() {
		Modele m = dao.find(modele.getCode());
		assertNotNull(m);
		assertEquals(modele.getCode(), m.getCode());
	}
}
