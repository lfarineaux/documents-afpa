package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.CouleurDao;
import com.afpa.entity.Couleur;

import com.afpa.entity.Voiture;

@TestMethodOrder(value = OrderAnnotation.class)
public class CouleurDaoTest {

	static CouleurDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new CouleurDao();

	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Couleur couleur;

	@Test
	@Order(1)
	public void ajoutCouleur() {
		Couleur c = Couleur.builder().label("rouge").build();
		c = dao.add(c);
		assertNotNull(c);
		assertNotEquals(0, c.getCode());
		couleur = c;
	}

	@Test
	@Order(2)
	public void miseAjourCouleur() {
		couleur.setLabel("vert");
		dao.update(couleur);
		Couleur c = dao.find(couleur.getCode());
		assertNotNull(c);
		assertEquals("vert", couleur.getLabel());
	}

	@Test
	@Order(3)
	public void supprimerCouleur() {
		dao.remove(couleur.getCode());
		Couleur c = dao.find(couleur.getCode());
		assertNull(c);
	}

	@Test
	@Order(4)
	public void ajoutVoitureAvecCouleur() {
		Couleur c = Couleur.builder().label("Noir").build();

		Voiture v = new Voiture();
		v.setCouleur(c);

		Voiture v2 = new Voiture();
		v.setCouleur(c);

		List<Voiture> voitures = Arrays.asList(v, v2);
		c.setVoitures(new HashSet<Voiture>(voitures));

		couleur = dao.add(c);
		assertNotNull(couleur);
		assertNotEquals(0, couleur.getCode());
	}

	@Test
	@Order(5)
	public void listerCouleur() {
		Collection<Couleur> couleurs = dao.findAllNamedQuery("listCouleur");
		assertNotEquals(0, couleurs.size());
		couleurs.stream().forEach(System.out::println);

	}

	@Test
	@Order(6)
	public void trouverCouleur() {
		Couleur c = dao.find(couleur.getCode());
		assertNotNull(c);
		assertEquals(couleur.getCode(), c.getCode());
	}

}
