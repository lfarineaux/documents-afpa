package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.VoitureDao;
import com.afpa.entity.Couleur;
import com.afpa.entity.Voiture;

@TestMethodOrder(value = OrderAnnotation.class)
public class VoitureDaoTest {

	static VoitureDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new VoitureDao();
	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Voiture voiture;

	@Test
	@Order(1)
	public void ajoutVoiture() {
		Voiture v = Voiture.builder().matricule("165gf7f64").build();
		v = dao.add(v);
		assertNotNull(v);
		assertNotEquals(0, v.getId());
		voiture = v;
	}

	@Test
	@Order(2)
	public void miseAjourVoiture() {
		voiture.setMatricule("354zdfz351");
		dao.update(voiture);
		Voiture v = dao.find(voiture.getId());
		assertNotNull(v);
		assertEquals("354zdfz351", voiture.getMatricule());
	}

	@Test
	@Order(3)
	public void listerVoiture() {
		Collection<Voiture> voitures = dao.findAllNamedQuery("listeVoiture");
		voitures.stream().forEach(System.out::println);
	}

//	@Test
//	@Order(4)
//	public void listerVoitureAvecCouleur() {
//		Collection<Voiture> voitures = dao.findAllNamedQuery("listVoitureByCouleur");
//		voitures.stream().forEach(System.out::println);
//
//	}

	@Test
	@Order(5)
	public void trouverVoiture() {
		Voiture v = dao.find(voiture.getId());
		assertNotNull(v);
		assertEquals(voiture.getId(), v.getId());
	}

	@Test
	@Order(6)
	public void supprimerVoiture() {
		dao.remove(voiture.getId());
		Voiture v = dao.find(voiture.getId());
		assertNull(v);
	}

}
