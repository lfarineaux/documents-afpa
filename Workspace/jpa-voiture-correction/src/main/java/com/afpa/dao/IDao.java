package com.afpa.dao;

import java.util.Collection;

public interface IDao<T> {

	public T add(T entity);
	public T find(int id);
	public T update(T entity);
	public Collection<T> findAllNamedQuery(String query);
	public void remove(int id);
}
