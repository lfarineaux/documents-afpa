package com.afpa.dao;

import java.util.List;

import com.afpa.entity.Couleur;
import com.afpa.entity.Voiture;

public interface IVoiture {

	public List<Voiture> listerVoitureByCouleur(Couleur couleur);
	public List<Voiture> listerVoiturePuissanteByMarque(String marque);
}
