package com.afpa.dao;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.afpa.entity.Couleur;
import com.afpa.entity.Voiture;

public class VoitureDao extends AbstractDao<Voiture> implements IVoiture {
	private static Logger monLogger = LoggerFactory.getLogger(VoitureDao.class);

	@Override
	public List<Voiture> listerVoitureByCouleur(Couleur couleur) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Voiture> res = em.createNamedQuery("listVoitureByCouleur", Voiture.class);
			return res.getResultList();
		} finally {
//			monLogger.info("liste des voitures selon leur couleur");
			closeEntityManager(em);
		}
	}

	@Override
	public List<Voiture> listerVoiturePuissanteByMarque(String marque) {
		// TODO Auto-generated method stub
		return null;
	}
	
//	public Collection<Voiture> findAllWithCouleurs() {
//		EntityManager em = null;
//		try {
//			em = newEntityManager();
//			TypedQuery<Voiture> q = em.createNamedQuery("listeVoiture", Voiture.class);
//			List<Voiture> resultList = q.getResultList();
//			
//			resultList.forEach(v->v.get);
//			
//			return resultList;
//		} finally {
////			monLogger.info("Entité listée");
//			closeEntityManager(em);
//		}
//	}

}
