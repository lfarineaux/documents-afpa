package com.afpa.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(of= {"code","label","puissance"})
@Builder
@Table(name = "t_modele")
@NamedQueries({ @NamedQuery(name = "listeModele", query = "SELECT m FROM Modele m") })
public class Modele {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int code;
	private String label;
	private int puissance;

	@ManyToOne
	@JoinColumn(name = "energie")
	private Energie energie;

	@ManyToOne
	@JoinColumn(name = "marque")
	private Marque marque;

	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "modele")
	private Set<Voiture> voitures;
}
