package com.afpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "t_voiture")
@NamedQueries({
//	@NamedQuery(name = "listVoitureByCouleur", query = "SELECT v FROM Voiture v WHERE color :couleur"),
		@NamedQuery(name = "listeVoiture", query = "SELECT v FROM Voiture v") })

public class Voiture {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String matricule;

	@ManyToOne
	@JoinColumn(name = "modele")
	private Modele modele;

	@ManyToOne
	@JoinColumn(name = "couleur")
	private Couleur couleur;

}