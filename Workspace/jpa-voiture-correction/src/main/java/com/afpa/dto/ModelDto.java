package com.afpa.dto;

import com.afpa.entity.Energie;
import com.afpa.entity.Marque;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModelDto {
	
	private String label;
	private int puissance;
	private Energie energie;
	private Marque marque;

}
