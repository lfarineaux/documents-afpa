package com.afpa.ihm;

import java.util.Scanner;
import com.afpa.service.ModelService;

public class MenuModele {

	
	public static ModelService ms = new ModelService();
	
	
	public static void menu() {

		boolean vrai = true;

		while(vrai) {
			System.out.println("");
			System.out.println("------------ Menu modele------------");
			System.out.println("");
			System.out.println("1: ajouter un modele");
			System.out.println("2: afficher tout les modeles");
			System.out.println("3: afficher un modele par son ID");
			System.out.println("4: metre a jour un modele");
			System.out.println("5: supprimer un modele");
			System.out.println("6: fermer menu modele");
			
			System.out.println("entrer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();

			switch (choix) {
			case "1":
				System.out.println("ajouter un modele");
				System.out.println("entré un label pour les modeles");
				String label = sc.next();
				System.out.println("entrer la puisssance");
				int puissance = sc.nextInt();
				System.out.println("entre l'energie");
				int energie = sc.nextInt();
				System.out.println("entre l'id de la marque Marque");
				int idd = sc.nextInt();
				 ms.ajouter(label, puissance, energie,idd);
				
				break;

			case "2":
				System.out.println("afficher tout les modeles");
				ms.afficherAll().stream().forEach(System.out::println);;
				break;

			case "3":
				System.out.println("afficher un modele par son ID");
				System.out.println("entré l'id a afficher");
				int id = sc.nextInt();
				System.out.println(ms.findId(id));
				break;

			case "4":
				System.out.println("metre a jour un modele");
				System.out.println("entrer l'id du model a mettre a jour");
				int id1 = sc.nextInt();
				System.out.println("entrer un nouveau labele");
				String label1 = sc.next();
				System.out.println("entre la nouvelle puissance pour le model selectionner");
				int puissance1 = sc.nextInt();
				System.out.println("entre l'energie en string pour le model selectionner");
				int energie1 = sc.nextInt();
				System.out.println("Entrer l'id de la marque");
				int iddd = sc.nextInt();
				ms.update(label1, puissance1, energie1, id1,iddd);
				break;
				
			case "5":
				System.out.println("Supprimer un model");
				System.out.println("entrer l'ID du model a supprimer");
				int id2 = sc.nextInt();
				ms.remove(id2);
				break;
				
			case "6":
				System.out.println("fin du programme");
				vrai = false;
				break;

			default:
				System.out.println("le choix entrer n'existe pas");
				break;
			}
		}
	}

}
