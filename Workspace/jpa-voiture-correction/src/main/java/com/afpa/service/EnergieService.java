package com.afpa.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.afpa.dao.EnergieDao;
import com.afpa.dto.EnergieDto;
import com.afpa.entity.Energie;

public class EnergieService {

	EnergieDao md = new EnergieDao();
	EnergieDto energieDto = new EnergieDto();

	public void ajouter(String label) {
		Energie m = new Energie();
		m.setLabel(label);
		md.add(m);
	}

	public EnergieDto findId(int id) {
		energieDto.setLabel(md.find(id).getLabel());
		return energieDto;
	}

	public Collection<EnergieDto> afficherAll() {
		List<EnergieDto> listenergiedto = new ArrayList<>();
				for (Energie energie : md.findAllNamedQuery("listEnergie")) {
					energieDto.setLabel(energie.getLabel());
					listenergiedto.add(energieDto);
				}
		return listenergiedto;
	}

	public void update(String label, int id) {
		Energie m = md.find(id);
		m.setLabel(label);
		md.update(m);
	}

	public void remove(int id) {
		md.remove(id);
	}

}
