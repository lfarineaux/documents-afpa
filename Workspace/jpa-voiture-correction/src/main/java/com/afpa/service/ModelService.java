package com.afpa.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.afpa.dao.EnergieDao;
import com.afpa.dao.MarqueDao;
import com.afpa.dao.ModeleDao;
import com.afpa.dto.ModelDto;
import com.afpa.entity.Energie;
import com.afpa.entity.Marque;
import com.afpa.entity.Modele;

public class ModelService {

	ModeleDao md = new ModeleDao();
	MarqueDao mdd = new MarqueDao();
	EnergieDao ed = new EnergieDao();
	ModelDto modelDto = new ModelDto();

	public void ajouter(String label, int puissance, int id1, int id2) {
		Modele m = new Modele();
		Energie e = ed.find(id1);
		Marque ma = mdd.find(id2);
		m.setLabel(label);
		m.setPuissance(puissance);
		m.setEnergie(e);
		m.setMarque(ma);
		md.add(m);
	}

	public ModelDto findId(int id) {
		md.find(id);
		modelDto.setEnergie(md.find(id).getEnergie());
		modelDto.setLabel(md.find(id).getLabel());
		modelDto.setMarque(md.find(id).getMarque());
		modelDto.setPuissance(md.find(id).getPuissance());
		return modelDto;
	}

	public Collection<ModelDto> afficherAll() {
		List<ModelDto> listmodeldto = new ArrayList<>();
		for (Modele modele : md.findAllNamedQuery("listeModele")) {
			modelDto.setEnergie(modele.getEnergie());
			modelDto.setLabel(modele.getLabel());
			modelDto.setMarque(modele.getMarque());
			modelDto.setPuissance(modele.getPuissance());
		}
		
		return listmodeldto;
	}

	public void update(String label, int puissance, int id1, int id, int id2) {
		Modele m = md.find(id);
		Energie e = ed.find(id1);
		Marque ma = mdd.find(id2);
		m.setLabel(label);
		m.setPuissance(puissance);
		m.setEnergie(e);
		m.setMarque(ma);
		md.update(m).getLabel();
	}

	public void remove(int id) {
		md.remove(id);
	}

}
