package com.afpa.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.afpa.dao.CouleurDao;
import com.afpa.dao.EnergieDao;
import com.afpa.dao.ModeleDao;
import com.afpa.dao.VoitureDao;
import com.afpa.dto.VoitureDto;
import com.afpa.entity.Couleur;
import com.afpa.entity.Modele;
import com.afpa.entity.Voiture;

public class VoitureService {

	VoitureDao vd = new VoitureDao();
	CouleurDao cd = new CouleurDao();
	EnergieDao ed = new EnergieDao();
	ModeleDao md = new ModeleDao();
	VoitureDto voitureDto = new VoitureDto();

	public String getCouleur(int id) {
		return cd.find(id).getLabel();
	}

	public String getModele(int id) {
		return md.find(id).getLabel();
	}

	public void ajouter(String matricule, int couleur, int modele) {
		Voiture v = new Voiture();
		Couleur c = cd.find(couleur);
		Modele m = md.find(modele);
		v.setMatricule(matricule);
		v.setCouleur(c);
		v.setModele(m);
		vd.add(v);
	}

	public VoitureDto findId(int id) {
		vd.find(id);
		voitureDto.setMatricule(vd.find(id).getMatricule());
		voitureDto.setCouleur(vd.find(id).getCouleur());
		voitureDto.setModele(vd.find(id).getModele());
		return voitureDto;
	}

	public Collection<VoitureDto> afficherAll() {
		List<VoitureDto> listvoituredto = new ArrayList<>();
		for (Voiture voiture : vd.findAllNamedQuery("listeVoiture")) {
			voitureDto.setCouleur(voiture.getCouleur());
			voitureDto.setMatricule(voiture.getMatricule());
			voitureDto.setModele(voiture.getModele());
		}
		return listvoituredto;
	}

	public void update(int id1, String matricule, int couleur, int marque) {
		Voiture v = vd.find(id1);
		Couleur c = cd.find(couleur);
		Modele m = md.find(marque);
		v.setMatricule(matricule);
		v.setCouleur(c);
		v.setModele(m);
		vd.update(v);
	}

	public void remove(int id) {
		vd.remove(id);
	}

}
