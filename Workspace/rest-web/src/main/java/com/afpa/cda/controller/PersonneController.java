package com.afpa.cda.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.cda.dao.PersonneRepository;
import com.afpa.cda.entity.Personne;

@RestController
public class PersonneController {

	@Autowired
	private PersonneRepository personneRepository;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping(path = "/users")
	public List<Personne> list(){
		return this.personneRepository.findAll();
	}

	@PostMapping(path = "/users")
	public Personne add(@RequestBody Personne p, HttpServletResponse response) {
		if ((p.getNom()!=null && p.getPrenom()!=null) &&
				(p!=this.personneRepository.findPersonneByNom(p.getNom())) && 
				(p!=this.personneRepository.findPersonneByPrenom(p.getPrenom())))
		{
			return this.personneRepository.save(p);

		} else {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return null;
	}

	@DeleteMapping(path = "/users/{idP}")
	public void delete(@PathVariable int idP, HttpServletResponse response) {
		if(this.personneRepository.findById(idP).isPresent()) {
			this.personneRepository.deleteById(idP);
		} else {
			response.setStatus(HttpStatus.NOT_FOUND.value());
		}
	}

	@PutMapping(path = "/users")
	public Personne update(@RequestBody Personne p, HttpServletResponse response) {
		if (p.getNom()==null || p.getPrenom()==null) {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
		} else {
			return this.personneRepository.save(p);
		}
		return null;
	}

}
