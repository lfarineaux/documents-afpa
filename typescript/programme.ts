import {Livre} from "./livre.module";
import {Utilisateur, TypePerson} from "./utilisateurs.module";

let util1=new Utilisateur (1,TypePerson.normal,"laurent");
let util2=new Utilisateur (2,TypePerson.habitue,"mohamed");

let livre1=new Livre (1,"dictionnaire","encyclopedie");
let livre2=new Livre (2,"guitare","magazine");
let livre3=new Livre (3,"scapin","livre");
let livre4=new Livre (4,"miserables","livre");
let livre5=new Livre (5,"running","magazine");
let livre6=new Livre (6,"atlas","encyclopedie");
let livre7=new Livre (7,"rock","magazine");
let livre8=new Livre (8,"surf","magazine");
let livre9=new Livre (8,"vtt","magazine");

let emprunt : boolean=false;
let restitution : boolean=false;

console.log("");
console.log("******* Gestion livres *******");

// Utilsateur normal emprunte 1 livre
emprunt = util1.emprunter(livre1);
console.log("Emprunt = "+emprunt);
// Utilsateur normal emprunte un 2ème livre
emprunt = util1.emprunter(livre2);
console.log("Emprunt = "+emprunt);
// Utilsateur normal rend 1 livre
restitution = util1.rendre(livre1);
console.log("Restitution = "+emprunt);

// Utilsateur habitué emprunte 6 livres
emprunt = util2.emprunter(livre3);
console.log("Emprunt = "+emprunt);
emprunt = util2.emprunter(livre4);
console.log("Emprunt = "+emprunt);
emprunt = util2.emprunter(livre5);
console.log("Emprunt = "+emprunt);
// Utilsateur habitué emprunte 1 livre indisponible
emprunt = util2.emprunter(livre1);
console.log("Emprunt = "+emprunt);
emprunt = util2.emprunter(livre6);
console.log("Emprunt = "+emprunt);
emprunt = util2.emprunter(livre7);
console.log("Emprunt = "+emprunt);
// Utilsateur habitué rend 1 livre
restitution = util2.rendre(livre6);
console.log("Restitution = "+emprunt);
emprunt = util2.emprunter(livre6);
console.log("Emprunt = "+emprunt);
// Utilsateur habitué emprunte un 6ème livre
emprunt = util2.emprunter(livre8);
console.log("Emprunt = "+emprunt);

