import { Utilisateur } from "./utilisateurs.module"

export class Livre {
    id: number;
    titre: string;
    type: string;
     emprunte: boolean;

    constructor(id: number, titre: string, type: string) {
        this.id = id;
        this.titre = titre;
        this.type = type;
        this.emprunte = false;

    }

  
}
