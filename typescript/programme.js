"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var livre_module_1 = require("./livre.module");
var utilisateurs_module_1 = require("./utilisateurs.module");
var util1 = new utilisateurs_module_1.Utilisateur(1, utilisateurs_module_1.TypePerson.normal, "laurent");
var util2 = new utilisateurs_module_1.Utilisateur(2, utilisateurs_module_1.TypePerson.habitue, "mohamed");
var livre1 = new livre_module_1.Livre(1, "dictionnaire", "encyclopedie");
var livre2 = new livre_module_1.Livre(2, "guitare", "magazine");
var livre3 = new livre_module_1.Livre(3, "scapin", "livre");
var livre4 = new livre_module_1.Livre(4, "miserables", "livre");
var livre5 = new livre_module_1.Livre(5, "running", "magazine");
var livre6 = new livre_module_1.Livre(6, "atlas", "encyclopedie");
var livre7 = new livre_module_1.Livre(7, "rock", "magazine");
var livre8 = new livre_module_1.Livre(8, "surf", "magazine");
var livre9 = new livre_module_1.Livre(8, "vtt", "magazine");
var emprunt = false;
var restitution = false;
console.log("");
console.log("******* Gestion livres *******");
// Utilsateur normal emprunte 1 livre
emprunt = util1.emprunter(livre1);
console.log("Emprunt = " + emprunt);
// Utilsateur normal emprunte un 2ème livre
emprunt = util1.emprunter(livre2);
console.log("Emprunt = " + emprunt);
// Utilsateur normal rend 1 livre
restitution = util1.rendre(livre1);
console.log("Restitution = " + emprunt);
// Utilsateur habitué emprunte 6 livres
emprunt = util2.emprunter(livre3);
console.log("Emprunt = " + emprunt);
emprunt = util2.emprunter(livre4);
console.log("Emprunt = " + emprunt);
emprunt = util2.emprunter(livre5);
console.log("Emprunt = " + emprunt);
// Utilsateur habitué emprunte 1 livre indisponible
emprunt = util2.emprunter(livre1);
console.log("Emprunt = " + emprunt);
emprunt = util2.emprunter(livre6);
console.log("Emprunt = " + emprunt);
emprunt = util2.emprunter(livre7);
console.log("Emprunt = " + emprunt);
// Utilsateur habitué rend 1 livre
restitution = util2.rendre(livre6);
console.log("Restitution = " + emprunt);
emprunt = util2.emprunter(livre6);
console.log("Emprunt = " + emprunt);
// Utilsateur habitué emprunte un 6ème livre
emprunt = util2.emprunter(livre8);
console.log("Emprunt = " + emprunt);
//# sourceMappingURL=programme.js.map