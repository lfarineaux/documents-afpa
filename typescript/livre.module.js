"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Livre = /** @class */ (function () {
    function Livre(id, titre, type) {
        this.id = id;
        this.titre = titre;
        this.type = type;
        this.emprunte = false;
    }
    return Livre;
}());
exports.Livre = Livre;
//# sourceMappingURL=livre.module.js.map