"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TypePerson;
(function (TypePerson) {
    TypePerson[TypePerson["normal"] = 1] = "normal";
    TypePerson[TypePerson["habitue"] = 2] = "habitue";
})(TypePerson = exports.TypePerson || (exports.TypePerson = {}));
;
var Utilisateur = /** @class */ (function () {
    function Utilisateur(id, type, nom) {
        this.id = id;
        this.type = type;
        this.nom = nom;
        this.nbreLivres = 0;
    }
    Utilisateur.prototype.emprunter = function (livre) {
        if (livre.emprunte == true) {
            console.log("NOK - Document indisponible.");
            return false;
        }
        else {
            if (this.type == TypePerson.normal && this.nbreLivres >= 1) {
                console.log("NOK - Quota maximum d'emprunt pour un utilisateur normal atteint (max 1 livre).");
                return false;
            }
            else {
                if (this.type == TypePerson.normal && this.nbreLivres == 0) {
                    livre.emprunte = true;
                    this.nbreLivres++;
                    console.log("OK - Livre : " + livre.titre + "  emprunt\u00E9 par " + this.nom + ". Nombre de livre emprunt\u00E9 = " + this.nbreLivres);
                    return true;
                }
                else {
                    if (this.type == TypePerson.habitue && this.nbreLivres >= 5) {
                        console.log("NOK - Quota maximum d'emprunt pour un utilisateur habitué atteint (max 5 livres).");
                        return false;
                    }
                    else {
                        if (this.type == TypePerson.habitue && this.nbreLivres < 5) {
                            livre.emprunte = true;
                            this.nbreLivres++;
                            console.log("OK - Livre : " + livre.titre + " rendu par " + this.nom + ". Nombre de livre emprunt\u00E9 = " + this.nbreLivres);
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    };
    Utilisateur.prototype.rendre = function (livre) {
        if (livre.emprunte == false) {
            console.log("NOK - Document déjà restitué.");
            return false;
        }
        else {
            this.nbreLivres--;
            livre.emprunte = false;
            console.log("OK - Livre : " + livre.titre + " rendu par " + this.nom + ". Nombre de livre(s) emprunté(s) = " + this.nbreLivres);
            return true;
        }
    };
    return Utilisateur;
}());
exports.Utilisateur = Utilisateur;
//# sourceMappingURL=utilisateurs.module.js.map