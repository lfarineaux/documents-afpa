import { Livre } from "./livre.module"
export enum TypePerson {normal=1,habitue=2};

export class Utilisateur {
    id: number;
    type: TypePerson;
    nom: string;
    nbreLivres: number;

    constructor(id: number, type: TypePerson, nom: string) {
        this.id = id;
        this.type = type;
        this.nom = nom;
        this.nbreLivres = 0;
    }

    emprunter(livre: Livre): boolean {
        if (livre.emprunte == true) {
            console.log("NOK - Document indisponible.");
            return false;
        } else {
            if (this.type == TypePerson.normal && this.nbreLivres >= 1) {
                console.log("NOK - Quota maximum d'emprunt pour un utilisateur normal atteint (max 1 livre).");
                return false;
            } else {
                if (this.type == TypePerson.normal  && this.nbreLivres == 0) {
                    livre.emprunte = true;
                    this.nbreLivres++;
                    console.log(`OK - Livre : ${livre.titre}  emprunté par ${this.nom}. Nombre de livre emprunté = ${this.nbreLivres}`);
                    return true;
                } else {
                    if (this.type == TypePerson.habitue  && this.nbreLivres >= 5) {
                        console.log("NOK - Quota maximum d'emprunt pour un utilisateur habitué atteint (max 5 livres).");
                        return false;
                    } else {
                        if (this.type == TypePerson.habitue && this.nbreLivres < 5) {
                            livre.emprunte = true;
                            this.nbreLivres++;
                            console.log(`OK - Livre : ${livre.titre} rendu par ${this.nom}. Nombre de livre emprunté = ${this.nbreLivres}`);
                            return true;
                        }
                    }
                }
            }

        }
        return false;
    }

    rendre(livre: Livre): boolean {
        if (livre.emprunte == false) {
            console.log("NOK - Document déjà restitué.");
            return false;
        } else {
            this.nbreLivres--;
            livre.emprunte = false;
            console.log("OK - Livre : " + livre.titre + " rendu par " + this.nom + ". Nombre de livre(s) emprunté(s) = " + this.nbreLivres);
            return true;
        }

    }

}