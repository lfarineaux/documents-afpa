
create table emp
(
noemp int not null,
nom varchar(20),
prenom varchar(20),
emploi varchar(20),
sup int,
embauche date,
sal float,
comm float,
noserv int
);

alter table emp
add constraint PK_EMP primary key (noemp);


create table serv
(
noserv int not null,
service varchar(20),
ville varchar(20)
);

alter table serv
add constraint PK_SERV primary key(noserv);

alter table emp
add constraint FK_EMP_SERV foreign key(noserv) references serv(noserv); 

insert into emp values (1000,'LEROY','PAUL','PRESIDENT',null,'1987-10-25',55005.5,null,1);
insert into emp values (1100,'DELPIERRE','DOROTHEE','SECRETAIRE',1000,'1987-10-25',12351.24,null,1);
insert into emp values (1101,'DUMONT','LOUIS','VENDEUR',1300,'1987-10-25',9047.9,0,1);
insert into emp values (1102,'MINET','MARC','VENDEUR',1300,'1987-10-25',8085.81,17230,1);
insert into emp values (1104,'NYS','ETIENNE','TECHNICIEN',1200,'1987-10-25',12342.23,null,1);
insert into emp values (1105,'DENIMAL','JEROME','COMPTABLE',1600,'1987-10-25',15746.57,null,1);
insert into emp values (1200,'LEMAIRE','GUY','DIRECTEUR',1000,'1987-03-11',36303.63,null,2);
insert into emp values (1201,'MARTIN','JEAN','TECHNICIEN',1200,'1987-06-25',11235.12,null,2);
insert into emp values (1202,'DUPONT','JACQUES','TECHNICIEN',1200,'1988-10-30',10313.03,null,2);
insert into emp values (1300,'LENOIR','GERARD','DIRECTEUR',1000,'1987-04-02',31353.14,13071,3);
insert into emp values (1301,'GERARD','ROBERT','VENDEUR',1300,'1999-04-16',7694.77,12430,3);
insert into emp values (1303,'MASURE','EMILE','TECHNICIEN',1200,'1988-06-17',10451.05,null,3);
insert into emp values (1500,'DUPONT','JEAN','DIRECTEUR',1000,'1987-10-23',28434.84,null,5);
insert into emp values (1501,'DUPIRE','PIERRE','ANALYSTE',1500,'1984-10-24',23102.31,null,5);
insert into emp values (1502,'DURAND','BERNARD','PROGRAMMEUR',1500,'1987-07-30',13201.32,null,5);
insert into emp values (1503,'DELNATTE','LUC','PUPITREUR',1500,'1999-01-15',8801.01,null,5);
insert into emp values (1600,'LAVARE','PAUL','DIRECTEUR',1000,'1991-12-13',31238.12,null,6);
insert into emp values (1601,'CARON','ALAIN','COMPTABLE',1600,'1985-09-16',33003.3,null,6);
insert into emp values (1602,'DUBOIS','JULES','VENDEUR',1300,'1990-12-20',9520.95,35535,6);
insert into emp values (1603,'MOREL','ROBERT','COMPTABLE',1600,'1985-07-18',33003.3,null,6);
insert into emp values (1604,'HAVET','ALAIN','VENDEUR',1300,'1991-01-01',9388.94,33415,6);
insert into emp values (1605,'RICHARD','JULES','COMPTABLE',1600,'1985-10-22',33503.35,null,5);
insert into emp values (1615,'DUPREZ','JEAN','BALAYEUR',1000,'1998-10-22',6000.6,null,5);

commit;


insert into serv values (1,'DIRECTION','PARIS');
insert into serv values (2,'LOGISTIQUE','SECLIN');
insert into serv values (3,'VENTES','ROUBAIX');
insert into serv values (4,'FORMATION','VILLENEUVE D''ASCQ');
insert into serv values (5,'INFORMATIQUE','LILLE');
insert into serv values (6,'COMPTABILITE','LILLE');
insert into serv values (7,'TECHNIQUE','ROUBAIX');


commit;

begin;
update emp set nom='CONNEXIONA' where noemp = 1100;

commit;

select * from emp where noemp = 1100;





/* R?ponse 1 */
select * 
from serv;

/* R?ponse 2 */
select noserv,service,ville 
from serv;

select serv.* 
from serv;

/* R?ponse 3 */
select service,noserv
from serv;

/* R?ponse 6 */
select distinct emploi 
from emp;

/* R?ponse 7 */
select * from emp
where noserv ='3';

/* R?ponse 8 */
select nom,prenom,noemp,noserv from emp
where emploi='TECHNICIEN';

/* R?ponse 9 */
select service,noserv from serv
where noserv>'2';

/* R?ponse 10 */
select service,noserv from serv
where noserv<='2';

/* R?ponse 11 */
select * from emp
where comm<sal;

/* R?ponse 12 */
select * from emp
where comm='0' or null;

/* R?ponse 13 */
select * from emp
where comm<>0 or null
order by comm A;

/* R?ponse 14 */
select * from emp
where noemp<='1000';

select e.*
from emp as e
where e.sup is null;

/* R?ponse 15 */
select * from emp
where noemp>='1000';

select e.*
from emp as e
where e.sup is not null

/* R?ponse 16 */
select nom,emploi,sal,noserv from emp
where noserv=5 and coalesce(comm,0)+sal>20000;

select emp.nom, emp.prenom,emp.sal,emp.noserv from emp  
where noserv='5' and case when comm is null 
then sal>= '20000' else comm+sal  > '20000' end;

/* R?ponse 17 */
select * from emp
where emploi='VENDEUR' and noserv='6' and sal>=9500;

select emp.nom, emp.prenom,emp.sal,emp.noserv from emp
where noserv='6' and emp.emploi='VENDEUR' and case
when comm is null then sal>= '9500' else comm+sal  >= '9500' end;

/* R?ponse 18 */
select * from emp
where emploi='PRESIDENT' or emploi='DIRECTEUR';

select * from emp
where emploi in ('PRESIDENT', 'DIRECTEUR');

/* R?ponse 19 */
select * from emp
where emploi in('DIRECTEUR') and noserv!=3;

/* R?ponse 20 */
select * from emp
where emploi in ('DIRECTEUR')
or  (emploi in ('TECHNICIEN') and noserv=1);

/* R?ponse 21 */
select * from emp
where emploi in ('TECHNICIEN', 'DIRECTEUR') and noserv=1;

/* R?ponse 22 */
select * from emp
where (emploi ='TECHNICIEN' or emploi= 'DIRECTEUR') and noserv=1;

/* R?ponse 23 */
select * from emp
where (emploi !='TECHNICIEN' and emploi !='DIRECTEUR') and noserv=1;

select * from emp
where noserv = 1 and emploi not in ('TECHNICIEN', 'DIRECTEUR');

/* R?ponse 24 */
select * from emp
where emploi in ('TECHNICIEN', 'COMPTABLE' , 'VENDEUR');

/* R?ponse 25 */
select * from emp
where emploi not in ('TECHNICIEN', 'COMPTABLE' , 'VENDEUR');

/* R?ponse 26 */
select * from emp
where emploi in ('DIRECTEUR') and noserv in (2,4,5);

/* R?ponse 27 */
select * from emp
where sup is not null and noserv not in (1,3,5);

/* R?ponse 28 */
select * from emp
where sal between 20000 and 40000;

/* R?ponse 29 */
select * from emp
where sal <20000 or sal >40000;

/* R?ponse 30 */
select * from emp
where emploi not in ('DIRECTEUR') and embauche between '1988-01-01' and '1988-12-31';

select * from emp as e
where not e.emploi like 'DIRECTEUR' and e.embauche::text like '__88%';

/* R?ponse 31 */
select * from emp
where emploi in ('DIRECTEUR') and noserv between 2 and 5; 

/* R?ponse 32 */
select * from emp
where nom like 'M%';

/* R?ponse 33 */
select * from emp
where nom like '%T';

/* R?ponse 34 */
select * from emp
where nom like '%E%E%';

/* R?ponse 35 */
select * from emp
where nom like '%E%' and nom not like '%E%E%';

/* R?ponse 36 */
select * from emp
where nom like '%N%' and nom like '%O%';

/* R?ponse 37 */
select * from emp
where nom like '_____N';

/* R?ponse 38 */
select * from emp
where nom like '__R%';

/* R?ponse 39 */
select * from emp
where nom not like '_____';

/* R?ponse 40 Trier les employ?s (nom, pr?nom, nn?de service, salaire) du service 3 par ordre de salaire croissant */
select nom,prenom,noserv,sal from emp as e
where e.noserv=3
order by sal;

/* R?ponse 41 Trier les employ?s (nom, pr?nom, nn?de service , salaire) du service 3 par ordre de salaire d?croissant. */
select nom,prenom,noserv,sal from emp as e
where e.noserv=3
order by sal desc;

/* R?ponse 42 Idem en indiquant le num?ro de colonne ? la place du nom colonne */
select nom,prenom,noserv,sal from emp as e
where e.noserv=3
order by 4 desc;

/* R?ponse 43 Trier les employ?s (nom, pr?nom, nn?de service, salaire, emploi) par emploi, et pour
  chaque emploi par ordre d?croissant de salaire.
*/
select nom,prenom,noserv,sal,emploi from emp as e
order by emploi asc, sal desc;

/* R?ponse 44 Idem en indiquant les num?ros de colonnes */
select nom,prenom,noserv,sal,emploi from emp as e
order by 5 asc, 4 desc;

/* R?ponse 45 Trier les employ?s (nom, pr?nom, nn?de service, commission) du service 3 par ordre
  croissant de commission */
select nom,prenom,noserv,comm from emp as e
order by coalesce(comm,0) asc;

/* R?ponse 46 Trier les employ?s (nom, pr?nom, nn?de service, commission) du service 3 par ordre 
  d?croissant de commission, en consid?rant que celui dont la commission est nulle ne touche
pas de commission */
select nom,prenom,noserv,comm from emp as e
where noserv=3 and coalesce(comm,0)!=0
order by coalesce(comm,0) desc;

/* R?ponse 47 S?lectionner le nom, le pr?nom, l'emploi, le nom du service de l'employ? pour tous les
employ?s */
select nom, prenom, emploi, service
from emp
left join serv on emp.noserv = serv.noserv;

/* R?ponse 48 S?lectionner le nom, l'emploi, le num?ro de service, le nom du service pour tous les
employ?s */
select nom, emploi, emp.noserv, service
from emp
left join serv on emp.noserv = serv.noserv;

/* Exemples union - except - temporaire - requ?te imbriqu?e */
select prenom,nom from emp where emp.nom like 'D%'
union
select service,'coucou' from serv where service like '%A%';

select nom from emp where nom like 'D%'
except
select nom from emp where nom like '%O%'
except
select nom from emp where nom like '%A%';

select * from emp
where noemp in
(select noemp from emp where nom like 'D%O%');

select * from (select noemp from emp where nom like 'D%O%') as nomTmp,
(select noemp from emp where nom like 'D%') as nomTmp2;

with tmp as (select noemp from emp where nom like 'D%O%')
select * from emp
inner join tmp on emp.noemp = tmp.noemp;

/* R?ponse 49 Idem en utilisant des alias pour les noms de tables */
select nom, emploi, e.noserv, service
from emp as e
left join serv as s on e.noserv = s.noserv;

/* R?ponse 50 S?lectionner le nom, l'emploi, suivis de toutes les colonnes de la table SERV pour tous
les employ?s */
select emp.nom, emp.emploi, serv.* from emp
left join serv on emp.noserv = serv.noserv;

/* R?ponse 51 S?lectionner les nom et date d'embauche des employ?s suivi des nom et date
d'embauche de leur sup?rieur pour les employ?s plus ancien que leur sup?rieur, dans l'ordre
nom employ?s, noms sup?rieurs */
select superieur.nom,superieur.embauche,employe.nom, employe.embauche from emp as employe
inner join emp as superieur on employe.sup = superieur.noemp
where employe.embauche<superieur.embauche order by employe.nom, superieur.nom;

/* R?ponse 52 S?lectionner sans doublon les pr?noms des directeurs et ? les pr?noms des techniciens du service 1 ? avec un UNION */
select  prenom from emp
where emploi = 'DIRECTEUR'
union
select prenom from emp
where emploi= 'TECHNICIEN' and noserv=1;

/* R?ponse 53 S?lectionner les num?ros de services n?ayant pas d?employ?s sans une jointure externe */
select serv.noserv
from serv
where serv.noserv not in (select distinct emp.noserv from emp where emp.noserv is not null);

/* R?ponse 54 S?lectionner les services ayant au moins un employ? */
select serv.service
from serv
where serv.noserv in (select distinct emp.noserv from emp);

select distinct s.service
from emp as e
inner join serv as s on e.noserv=s.noserv
where e.nom is not null and e.noserv is not null;

/* R?ponse 55 S?lectionner les employ?s qui travaillent ? LILLE */
select nom,prenom,serv.ville from emp
left join serv on emp.noserv = serv.noserv
where ville ='LILLE';

select  e.nom, e.prenom, s.ville
from   serv as s
inner join emp as e on s.noserv = e.noserv
where s.ville='LILLE';

/* R?ponse 56 S?lectionner les employ?s qui ont le m?me chef que DUBOIS, DUBOIS exclu */
select nom,prenom from emp
where sup in(select sup from emp where nom='DUBOIS') and nom!='DUBOIS'
order by nom;

select   collegue.nom, collegue.prenom
from  emp as employer
inner join emp as collegue on (collegue.sup = employer.sup and employer.nom='DUBOIS' and 
collegue.nom!='DUBOIS')
order by collegue.nom;

/* R?ponse 57 S?lectionner les employ?s embauch?s le m?me jour que DUMONT */
select nom,prenom from emp
where embauche in(select embauche from emp where nom='DUMONT')
order by nom;

select     collegue.nom, collegue.prenom
from    emp as employer
inner join emp as collegue on (collegue.embauche = employer.embauche and employer.nom='DUMONT')
order by     employer.nom,   collegue.nom;

/* R?ponse 58 S?lectionner les nom et date d'embauche des employ?s plus anciens que MINET,
dans l?ordre des embauches */
select nom,embauche from emp
where embauche < (select embauche from emp where nom='MINET')
order by embauche;

select     collegue.nom,  collegue.embauche
from   emp as employer
inner join emp as collegue on (collegue.embauche < employer.embauche and employer.nom='MINET')
order by  collegue.embauche;

/* R?ponse 59 S?lectionner le nom, le pr?nom, la date d?embauche des employ?s plus anciens
que tous les employ?s du service NN?6 */
select nom,prenom,embauche from emp
where embauche < (select min(embauche) from emp where noserv=6);

select employe.nom, employe.prenom, employe.embauche
from emp as employe
inner join (select min(embauche) as de from emp where noserv=6) as tde on employe.embauche<tde.de;


/* R?ponse 60 S?lectionner le nom, le pr?nom, le revenu mensuel des employ?s qui gagnent plus
qu'au moins un employ? du service NN?3, trier le r?sultat dans l'ordre croissant des
revenus mensuels */
select nom,prenom,sal from emp
where sal < (select min(sal) from emp where noserv=3)
order by sal;

/* R?ponse 61 S?lectionner les noms, le num?ro de service, l?emplois et le salaires des
personnes travaillant dans la m?me ville que HAVET */
select nom,noserv,emploi, sal from emp
where noserv in (select serv.noserv from serv where serv.ville in
(select ville from serv where serv.noserv in
(select emp.noserv from emp where emp.nom='HAVET')));

select e.nom,e.noserv,e.emploi,e.sal
from emp as e
inner join serv on e.noserv = serv.noserv
where serv.ville = (select serv.ville from emp as e
inner join serv on e.noserv = serv.noserv where e.nom='HAVET' );

/* select collegue.nom, collegue.noserv, collegue.emploi, collegue.sal
from emp as employe
inner join emp as collegue on () */


/* R?ponse 62 S?lectionner les employ?s du service 1, ayant le m?me emploi qu'un employ? du service NN?3 */
select nom,prenom,emploi from emp
where noserv=1 and emploi in (select emploi from emp where noserv=3);

/* R?ponse 63 S?lectionner les employ?s du service 1 dont l'emploi n'existe pas dans le service 3 */
select nom,prenom,emploi from emp
where noserv=1 and emploi not in (select emploi from emp where noserv=3);

/* R?ponse 71 S?lectionner le nom, l?emploi, le revenu mensuel (nomm? Revenu) avec deux d?cimales
pour tous les employ?s, dans l?ordre des revenus d?croissants */
select nom, emploi, cast (sal as decimal(7,2)) from emp
order by sal desc;

/* R?ponse 72 S?lectionner le nom, le salaire, commission des employ?s dont la commission repr?sente
plus que le double du salaire. */
select nom,sal,comm from emp
where (comm>= 2*sal);

/* R?ponse 73 S?lectionner nom, pr?nom, emploi, le pourcentage de commission (deux d?cimales) par
rapport au revenu mensuel ( renomm? "% Commissions") , pour tous les vendeurs dans l'ordre
d?croissant de ce pourcentage */
select nom, prenom, emploi, sal, comm, cast((comm/sal)*100 as decimal (5,2)) as "% Commissions" from emp
where emploi='VENDEUR' and comm!=0
order by "% Commissions" desc;

/* R?ponse 74 S?lectionner le nom, l?emploi, le service et le revenu annuel ( ? l?euro pr?s) de tous les
vendeurs */
select nom, emploi, serv.service, round(sal*12)
from emp as e
left join serv on e.noserv = serv.noserv;

/* R?ponse 75 S?lectionner nom, pr?nom, emploi, salaire , commissions , revenu mensuel pour les employ?s
des services 3,5,6 */
select nom, prenom, emploi, sal, comm, round(sal+coalesce(comm,0)*12)
from emp
where noserv in(3,5,6);

/* R?ponse 76 Idem pour les employ?s des services 3,5,6 en rempla?ant les noms des colonnes : SAL par
SALAIRE, COMM par COMMISSIONS, SAL+IFNULL(COMM,0 ) par GAIN_MENSUEL */
select  nom, prenom, emploi,  sal as "SALAIRE" , comm as "COMMISSIONS", round(sal+coalesce(comm,0))*12 as "GAIN_MENSUEL"
from emp
where noserv in(3,5,6);

/* R?ponse 77 Idem pour les employ?s des services 3,5,6 en rempla?ant GAIN_ MENSUEL par GAIN
MENSUEL */
select  nom, prenom, emploi,  sal as "SALAIRE" , comm as "COMMISSIONS", round(sal+coalesce(comm,0))*12 as "GAIN MENSUEL"
from emp
where noserv in(3,5,6);

/* R?ponse 78  Afficher le nom, l'emploi, les salaires journalier et horaire pour les employ?s des services
3,5,6 (22 jours/mois et 8 heures/jour), sans arrondi, arrondi au centime pr?s.*/
select nom, emploi, cast(sal/22 as decimal(6,2)) as "salaire journalier", cast(sal/22/8 as decimal(6,2)) as "salaire horaire"
from emp;

/* R?ponse 79 Idem sans arrondir mais en tronquant */
select nom, emploi, trunc(sal/22) as "salaire journalier", trunc(sal/22/8) as "salaire horaire"
from emp;



