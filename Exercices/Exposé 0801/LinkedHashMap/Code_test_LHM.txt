package expose0801;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;


public class LHM {

	public static void main(String[] args) {

		System.out.println("** Comparaison Classes HashMap et LinkedHashMap **");
		System.out.println("*LinkedHashMap utilise une liste doublement cha�n�e contenant toutes les entr�es"
				+ " de la table hach�e, dans l'ordre o� les cl�s");
		System.out.println(" ont �t� ins�r�es dans la table : ceci permet "
				+ "d'obtenir un non chaotique sur les cl�s.");
		System.out.println();

		System.out.println("1/ <Integer, String>");
		HashMap <Integer, String> hm = new HashMap<Integer, String>();
		hm.put(10, "Dix");
		hm.put(30, "Trente");
		hm.put(50, "Cinquante");
		hm.put(20, "Vingt");
		hm.put(40, "Quarante");

		System.out.println("Ordre de saisie :");
		System.out.println("10, Dix");
		System.out.println("30, Trente ");
		System.out.println("50, Cinquante");
		System.out.println("20, Vingt");
		System.out.println("40, Quarante");
		System.out.println();
		Iterator<Integer> iterat1 = hm.keySet().iterator();
		while(iterat1.hasNext()){ 
			int key = iterat1.next();
			System.out.println("HashMap hm "+key+", "+(hm.get(key)));
		}
		System.out.println("-> Tri des clefs al�atoire");
		System.out.println();

		LinkedHashMap <Integer, String> lhm = new LinkedHashMap<Integer, String>();
		lhm.put(10, "Dix");
		lhm.put(30, "Trente");
		lhm.put(50, "Cinquante");
		lhm.put(20, "Vingt");
		lhm.put(40, "Quarante");

		Iterator<Integer> iterat2 = lhm.keySet().iterator();
		while(iterat2.hasNext()){ 
			int key = iterat2.next();
			System.out.println("HashMap lhm "+key+", "+(lhm.get(key)));
		}
		System.out.println("-> Tri des clefs dans l'ordre de saisie");
		System.out.println();

		String[] tab1 = {"titre 01, titre 02, titre 03"};		
		String[] tab2 = {"titre 11, titre 12, titre 13"};
		String[] tab3 = {"titre 21, titre 22, titre 23"};

		System.out.println("2/ <Integer, String[]>");
		HashMap <Integer, String []> hmarray = new HashMap<Integer, String []>();
		hmarray.put(10, tab2);
		hmarray.put(00, tab1);
		hmarray.put(20, tab3);

		System.out.println("Ordre de saisie :");
		System.out.println("10=titre 11 titre 12 titre 13");
		System.out.println("00=titre 01 titre 02 titre 03");
		System.out.println("20=titre 21 titre 22 titre 23");
		
		System.out.println();

		Iterator<Integer> iterat3 = hmarray.keySet().iterator();
		while(iterat3.hasNext()){ 
			int key = iterat3.next();
			System.out.println("HashMap hmarray "+key+" "+Arrays.toString(hmarray.get(key)));
		}
		System.out.println(" -> Tri des clefs al�atoire");
		System.out.println();

		LinkedHashMap <Integer, String []> lhmarray = new LinkedHashMap<Integer, String []>();
		lhmarray.put(10, tab2);
		lhmarray.put(00, tab1);
		lhmarray.put(20, tab3);

		Iterator<Integer> literat4 = lhmarray.keySet().iterator();
		while(literat4.hasNext()){ 
			int key = literat4.next();
			System.out.println("LinkedHashMap lhmarray "+key+" "+Arrays.toString(lhmarray.get(key)));
		}
		System.out.println("-> Tri des clefs dans l'ordre de saisie");
		System.out.println();

		System.out.println("3/ <Integer,ArrayList<String>>");
		HashMap <Integer, ArrayList<String>> hmal = new HashMap <Integer, ArrayList<String>>();
		hmal.put(3150, new ArrayList<String>());
		hmal.put(5170, new ArrayList<String>());
		hmal.put(7190, new ArrayList<String>());

		hmal.get(3150).add("titre 43");
		hmal.get(7190).add("titre 87");
		hmal.get(5170).add("titre 54");
		hmal.get(3150).add("titre 32");
		hmal.get(5170).add("titre 65");
		hmal.get(7190).add("titre 76");

		System.out.println("Ordre de saisie : 3150,[titre 43]");
		System.out.println("Ordre de saisie : 7190,[titre 87]");
		System.out.println("Ordre de saisie : 5170,[titre 54]");
		System.out.println("Ordre de saisie : 3150,[titre 32]");
		System.out.println("Ordre de saisie : 5170,[titre 65]");
		System.out.println("Ordre de saisie : 7190,[titre 76]");
		System.out.println();
		Iterator<Integer> iterat5 = hmal.keySet().iterator();
		while(iterat5.hasNext()){ 
			int key = iterat5.next();
			System.out.println("HashMap hmal "+key+", "+(hmal.get(key)));
		}
		System.out.println(" -> Tri des clefs al�atoire");

		LinkedHashMap <Integer, ArrayList<String>> lhmal = new LinkedHashMap <Integer, ArrayList<String>>();
		lhmal.put(3150, new ArrayList<String>());
		lhmal.put(5170, new ArrayList<String>());
		lhmal.put(7190, new ArrayList<String>());

		lhmal.get(3150).add("titre 43");
		lhmal.get(3150).add("titre 32");
		lhmal.get(7190).add("titre 87");
		lhmal.get(7190).add("titre 76");
		lhmal.get(5170).add("titre 54");
		lhmal.get(5170).add("titre 65");
		

		Iterator<Integer> iterat6 = lhmal.keySet().iterator();
		while(iterat6.hasNext()){ 
			int key = iterat6.next();
			System.out.println("LinkedHashMap lhmal "+key+", "+(lhmal.get(key)));
		}
		System.out.println(" -> Tri des clefs dans l'ordre croissant");

	}

}
