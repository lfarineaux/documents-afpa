package hs;

import java.util.Scanner;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class HS {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		HashSet<Integer> hs=new HashSet<Integer>();
		System.out.println("** Classe HashSet **");
		System.out.println("** D�finitions :");
		System.out.println("- Set : Interface qui mod�lise un ensemble d'objets sans doublons");
		System.out.println(" Stockage simple uniquement de valeurs");
		System.out.println(" Clef et valeur uniques");
		System.out.println(" - Hash : structure de donn�es qui associe des cl�s � des valeurs, pour ");
		System.out.println(" faciliter et acc�l�rer les recherches de donn�es");
		System.out.println(" 3 ensembles : les cl�s, les valeurs et les entr�es (couple cl�/valeur)");
		System.out.println(" L'ordre de stockage n'est pas d�fini, et peut changer dans le temps");
		System.out.println(" Cl�s g�n�r�es automatiquement = codes de hachage(stockage)");
		System.out.println(" M�thodes equals() et hashcode() imp�ratives pour ne pas confondre adresse et valeur");
		System.out.println();

		System.out.println("Appuyer sur Entr�e pour continuer");
		sc.nextLine();

		System.out.println("1/ <V> = HashSet <Integer>");
		System.out.println("Ordre de saisie :");
		System.out.println("10");
		System.out.println("30");
		System.out.println("50");
		System.out.println("20");
		System.out.println("40");
		System.out.println("0");
		System.out.println("null");
		System.out.println("30 Doublon");
		System.out.println();

		hs.add(10);
		hs.add(30);
		hs.add(50);
		hs.add(20);
		hs.add(40);
		hs.add(0);
		hs.add(null);
		hs.add(30);
		
		System.out.println();
		System.out.println("HashSet hs ");
		Iterator <Integer>val = hs.iterator(); 
		while (val.hasNext()) { 
			System.out.println(val.next()); 
		} 
		System.out.println("-> Tri des clefs al�atoire, valeur unique (30)");

		System.out.println("Appuyer sur Entr�e pour continuer");
		sc.nextLine();

		System.out.println("1/ <K,V> = HashMap <Integer,String>");
		System.out.println("Ordre de saisie :");
		System.out.println("10, Dix");
		System.out.println("30, Trente");
		System.out.println("50, Cinquante");
		System.out.println("00, Zero");
		System.out.println("20, Vingt");
		System.out.println("40, Quarante");
		System.out.println("50, CINQUANTE : Doublon clef 50");
		System.out.println("60, Trente : Doublon valeur Trente");
		System.out.println();

		HashMap <Integer, String> hm = new HashMap<Integer, String>();
		hm.put(10, "Dix");
		hm.put(30, "Trente");
		hm.put(50, "Cinquante");
		hm.put(20, "Vingt");
		hm.put(40, "Quarante");
		hm.put(50, "CINQUANTE");
		hm.put(0, "Zero");
		hm.put(60, "Trente");
		
		System.out.println("HashMap hm ");
		
		Iterator<Integer> iterat1 = hm.keySet().iterator();
		while(iterat1.hasNext()){ 
			int key = iterat1.next();
			
			System.out.println(+key+", "+(hm.get(key)));
		}
		
		System.out.println("-> Tri des clefs al�atoire, clef unique (50), valeur en double (Trente)");

		sc.close();
	}

}
