package lhm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class LHM {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println( Classe LinkedHashMap );
		System.out.println();
		System.out.println( D�finitions );
		System.out.println( - Map  Interface qui mod�lise un ensemble de donn�es avec un syst�me cl�valeur);
		System.out.println( Cl�s uniques mais valeurs multiples (pr�sentes dans plusieurs cl�s));
		System.out.println( - Hash  structure de donn�es qui associe des cl�s � des valeurs, pour );
		System.out.println( faciliter et acc�l�rer les recherches de donn�es);
		System.out.println( 3 ensembles  les cl�s, les valeurs et les entr�es (couple cl�valeur));
		System.out.println( L'ordre de stockage n'est pas d�fini, et peut changer dans le temps);
		System.out.println( - Linked  extension de HashMap, contient une liste suppl�mentaire avec les codes de hachage (stockage), donc l'ordre de stockage);

		System.out.println();

		System.out.println(Appuyer sur Entr�e pour continuer);
		sc.nextLine();

		System.out.println(1 HashMap K,V = Integer, String);
		System.out.println(Ordre de saisie );
		System.out.println(0, zero);
		System.out.println(10, Dix);
		System.out.println(30, Trente);
		System.out.println(50, Cinquante);
		System.out.println(20, Vingt);
		System.out.println(30, TRENTE  Doublon clef 30);
		System.out.println(60, Dix  Doublon valeur Dix);
		System.out.println(40, Quarante);

		HashMap Integer, String hm = new HashMapInteger, String();
		hm.put(0, Zero);
		hm.put(10, Dix);
		hm.put(30, Trente);
		hm.put(50, Cinquante);
		hm.put(20, Vingt);
		hm.put(30, TRENTE);
		hm.put(60, Dix);
		hm.put(40, Quarante);

		System.out.println();
		System.out.println(HashMap hm );
		IteratorInteger iterat1 = hm.keySet().iterator();
		while(iterat1.hasNext()){ 
			int key = iterat1.next();
			
			System.out.println(+key+, +(hm.get(key)));
		}
		System.out.println(- Tri des clefs al�atoire, cl� unique (30), valeur en double (Dix), cl� == 0 accept�e);
		System.out.println();

		LinkedHashMap Integer, String lhm = new LinkedHashMapInteger, String();
		lhm.put(0, Zero);
		lhm.put(10, Dix);
		lhm.put(30, Trente);
		lhm.put(50, Cinquante);
		lhm.put(20, Vingt);
		lhm.put(30, TRENTE);
		lhm.put(60, Dix);
		lhm.put(40, Quarante);

		System.out.println(LinkedHashMap lhm );
		IteratorInteger iterat2 = lhm.keySet().iterator();
		while(iterat2.hasNext()){ 
			int key = iterat2.next();
			System.out.println(
					key+, +(lhm.get(key)));
		}
		System.out.println(- Tri des clefs dans l'ordre de saisie, cl� unique (30), valeur en double (Dix), cl� == 0 accept�e);
		System.out.println();

		System.out.println(Appuyer sur Entr�e pour continuer);
		sc.nextLine();

		String[] tab1 = {titre 01, titre 02, titre 03};		
		String[] tab2 = {titre 11, titre 12, titre 13};
		String[] tab3 = {titre 21, titre 22, titre 23};

		System.out.println(2 HashMap K,V = Integer, String[]);
		System.out.println(Ordre de saisie );
		System.out.println(10=titre 11 titre 12 titre 13);
		System.out.println(00=titre 01 titre 02 titre 03);
		System.out.println(20=titre 21 titre 22 titre 23);
		System.out.println(10=titre 01 titre 02 titre 03  Doublon clef 10);
		System.out.println(30=titre 21 titre 22 titre 23  Doublon valeur titre 21);

		HashMap Integer, String [] hmarray = new HashMapInteger, String []();
		hmarray.put(10, tab2);
		hmarray.put(00, tab1);
		hmarray.put(20, tab3);
		hmarray.put(10, tab1);
		hmarray.put(30, tab3);

		System.out.println();
		System.out.println(HashMap hmarray );
		IteratorInteger iterat3 = hmarray.keySet().iterator();
		while(iterat3.hasNext()){ 
			int key = iterat3.next();
			System.out.println(key+ +Arrays.toString(hmarray.get(key)));
		}
		System.out.println( - Tri des clefs al�atoire, cl� unique (10), valeur en double (titre 21));
		System.out.println();

		LinkedHashMap Integer, String [] lhmarray = new LinkedHashMapInteger, String []();
		lhmarray.put(10, tab2);
		lhmarray.put(00, tab1);
		lhmarray.put(20, tab3);
		lhmarray.put(10, tab1);
		lhmarray.put(30, tab3);

		System.out.println(LinkedHashMap lhmarray );
		IteratorInteger literat4 = lhmarray.keySet().iterator();
		while(literat4.hasNext()){ 
			int key = literat4.next();
			System.out.println(key+ +Arrays.toString(lhmarray.get(key)));
		}
		System.out.println(- Tri des clefs dans l'ordre de saisie, cl� unique (10), valeur en double (titre 21));

		System.out.println();
		System.out.println(Appuyer sur Entr�e pour continuer);
		sc.nextLine();

		System.out.println(3 HashMap K,V = Integer,ArrayListString);
		System.out.println(Ordre de saisie  3150,[titre 43]);
		System.out.println(Ordre de saisie  7190,[titre 87]);
		System.out.println(Ordre de saisie  5170,[titre 54]);
		System.out.println(Ordre de saisie  3150,[titre 32]);
		System.out.println(Ordre de saisie  5170,[titre 65]);
		System.out.println(Ordre de saisie  3150,[titre 43  Doublon clef 3150]);
		System.out.println(Ordre de saisie  7190,[titre 76]);
		System.out.println(Ordre de saisie  7190,[titre 76  Doublon Valeur titre 76]);
		System.out.println();

		HashMap Integer, ArrayListString hmal = new HashMap Integer, ArrayListString();
		hmal.put(3150, new ArrayListString());
		hmal.put(5170, new ArrayListString());
		hmal.put(7190, new ArrayListString());

		hmal.get(3150).add(titre 43);
		hmal.get(7190).add(titre 87);
		hmal.get(5170).add(titre 54);
		hmal.get(3150).add(titre 32);
		hmal.get(5170).add(titre 65);
		hmal.get(3150).add(titre 43);
		hmal.get(7190).add(titre 76);
		hmal.get(3150).add(titre 76);

		System.out.println(HashMap hmal );
		IteratorInteger iterat5 = hmal.keySet().iterator();
		while(iterat5.hasNext()){ 
			int key = iterat5.next();
			System.out.println(key+, +(hmal.get(key)));
		}
		System.out.println( - Tri des clefs al�atoire);
		System.out.println();

		LinkedHashMap Integer, ArrayListString lhmal = new LinkedHashMap Integer, ArrayListString();
		lhmal.put(3150, new ArrayListString());
		lhmal.put(5170, new ArrayListString());
		lhmal.put(7190, new ArrayListString());

		lhmal.get(3150).add(titre 43);
		lhmal.get(7190).add(titre 87);
		lhmal.get(5170).add(titre 54);
		lhmal.get(3150).add(titre 32);
		lhmal.get(5170).add(titre 65);
		lhmal.get(3150).add(titre 43);
		lhmal.get(7190).add(titre 76);
		lhmal.get(3150).add(titre 76);

		System.out.println(LinkedHashMap lhmal );
		IteratorInteger iterat6 = lhmal.keySet().iterator();
		while(iterat6.hasNext()){ 
			int key = iterat6.next();
			System.out.println(key+, +(lhmal.get(key)));
		}
		System.out.println( - Tri des clefs dans l'ordre croissant ...);

		sc.close();
		
	}

}
