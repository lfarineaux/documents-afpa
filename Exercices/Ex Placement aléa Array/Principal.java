package randomStage;
import java.util.ArrayList;
import java.util.Collections;


public class Principal {

	public static void main(String[] args) {

		//initialisation des objets

		ArrayList<Stagiaire> listStragiaire = new ArrayList<Stagiaire>();
		ArrayList<Stagiaire> listTete = new ArrayList<Stagiaire>();

		ArrayList<Ilot> mesIlots = new ArrayList<Ilot>();

		mesIlots.add(new Ilot());
		mesIlots.add(new Ilot());
		mesIlots.add(new Ilot());
		mesIlots.add(new Ilot());
		mesIlots.add(new Ilot());

		listStragiaire.add(new Stagiaire("Amaia", false));
		listStragiaire.add(new Stagiaire("Mustapha", false));
		listStragiaire.add(new Stagiaire("Yassine", false));
		listStragiaire.add(new Stagiaire("Linda", false));
		listStragiaire.add(new Stagiaire("Richard", false));
		listStragiaire.add(new Stagiaire("Clement", false));
		listStragiaire.add(new Stagiaire("Laurent", false));
		listStragiaire.add(new Stagiaire("Ketsia", false));
		listStragiaire.add(new Stagiaire("Soulaiman", false));
		listStragiaire.add(new Stagiaire("Mohamed", false));

		listTete.add(new Stagiaire("Anthony", true));
		listTete.add(new Stagiaire("Redouane", true));
		listTete.add(new Stagiaire("Maxime", true));
		listTete.add(new Stagiaire("JPhilippe", true));
		listTete.add(new Stagiaire("Badrane", true));

		// m�lange des listes

		Collections.shuffle(listTete);
		
		Collections.shuffle(listStragiaire);

		//ajout des stagiaires dans les ilots

		int a=0;

		for(Ilot p : mesIlots) {
			p.getListeStagaire().add(listTete.get(a)); // get = liste de la classe
			p.setTeteIlot(listTete.get(a).getPrenom());
			a++;
		}

		a = 0;

		for(Ilot p : mesIlots) {
			for(int j=0; j<2;j++) {
				p.getListeStagaire().add(listStragiaire.get(a));
				a++;
			}
		}

		for(Ilot p : mesIlots) {
			Collections.shuffle(p.getListeStagaire());
		}
		
		for(Ilot p : mesIlots) {
			for(int j=0; j<3;j++) {
				p.getListeStagaire().get(j).setPlace(j+1);
			}
		}
		
		for(Ilot p : mesIlots) {
			System.out.println(p);
		}
		
	}
}
