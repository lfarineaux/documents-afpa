﻿package randomStage;

public class Stagiaire {
	
	private final String prenom;
	private int place;
	private final boolean teteDeListe;


	//Construteurs
	
	public Stagiaire(String prenom, boolean teteDeListe) {
		this.prenom = prenom;
		this.place = place;
		this.teteDeListe = teteDeListe;
	}
	

	//Méthodes
	public String toString() {
		return "Stagiaire : " +prenom+ ", place : " + place ;	
		}
	
	//Getters et Setters
	public String getPrenom() {
		return prenom;
	}

	public int getPlace() {
		return place;
	}

	public boolean getTeteDeListe() {
		return teteDeListe;
	}
	
	public void setPlace(int l) {
		 this.place = l;
	}
	
}
