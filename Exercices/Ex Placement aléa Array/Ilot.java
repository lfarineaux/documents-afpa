package randomStage;

import java.util.ArrayList;

public class Ilot {

	//Attributs
	
	
	private static int idClasse;
	final int idObjet;
	private String teteIlot;
	private ArrayList <Stagiaire> listeStagiaire;

	//Constructeurs
	
	public Ilot() {
		idClasse++;
		idObjet = idClasse;
		listeStagiaire = new ArrayList<Stagiaire>();
	}
	
	//M�thodes
	
	
	public String toString() {
		
		StringBuilder res = new StringBuilder();
		
		for (Stagiaire p : listeStagiaire) {
			res.append("Ilot : ").append(idObjet).append(", t�te de liste : ").append(teteIlot).append(" ").append(p).append("\n");
		}
		return res.toString();	
	}

	
	//Getters et Setters
	
	public String getTeteIlot() {
		return this.teteIlot;
	}

	public int getIdClasse() {
		return this.idClasse;
	}

	public ArrayList<Stagiaire> getListeStagaire() {
		return this.listeStagiaire;
	}

	public void setTeteIlot(String teteIlot) {
		this.teteIlot = teteIlot;
	}
	
}
