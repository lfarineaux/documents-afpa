<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(\Doctrine\Persistence\ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('ADMIN');
        $user->setPassword($this->encoder->encodePassword($user, 'password'));
        $manager->persist($user);
        $manager->flush();
    }
}
