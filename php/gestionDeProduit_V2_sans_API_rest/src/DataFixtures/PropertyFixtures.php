<?php

namespace App\DataFixtures;

use App\Entity\Property;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Factory;

class PropertyFixtures extends Fixture
{
    public function load(\Doctrine\Persistence\ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for($i = 0; $i<100; $i++) {
            $property = new Property();
            $property
                ->setTitle($faker->words(3,true ))
                ->setDescription($faker->sentences(3,true))
                ->setSurface($faker->numberBetween(20,350))
                ->setRooms($faker->numberBetween(2,10))
                ->setBedrooms($faker->numberBetween(1,9))
                ->setFloor($faker->numberBetween(0,15))
                ->setPrice($faker->numberBetween(150000,1500000))
                ->setHeat($faker->numberBetween(0,count(Property::HEAT)-1))
                ->setCity($faker->city)
                ->setAddress($faker->address)
                ->setPostalCode($faker->postcode)
                ->setSold(false);
            /*$property
                ->setTitle('Symfonyhome' . $i)
                ->setDescription('description' . $i)
                ->setSurface(2 * $i)
                ->setRooms($i)
                ->setBedrooms($i)
                ->setFloor($i)
                ->setPrice(123456 * $i)
                ->setHeat(1)
                ->setCity('City' . $i)
                ->setAddress('Adress adress ' . $i)
                ->setPostalCode(10000 + $i);*/
            $manager->persist($property);
        }
        $manager->flush();
    }
}
