<?php
namespace App\Controller;

use App\Entity\Property;
use App\Repository\PropertyRepository;
use Doctrine\Common\Annotations\AnnotationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class HomeController extends AbstractController {
    /**
     * @Route("/", name="home", methods="GET", )
     * @param PropertyRepository $repository
     * @return Response
     * @throws AnnotationException
     * @throws ExceptionInterface
     */
    public function index (PropertyRepository $repository) : Response {
        $properties = $repository->findLatest();
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([$normalizer]);

        $data = $serializer->normalize($properties, null, ['groups' => [
            'group1',
            'group2',
            'group3',
            'group4',
            'group5',
            'group6',
            'group7',
            'group8',
            'group9',
            'group10',
            'group11',
            'group12',
            'group13',
            'group14',
            'group15',
            'group16',
            'group17'
        ]]);

        $json = file_get_contents('https://www.prevision-meteo.ch/services/json/roubaix');
        $json = json_decode($json);

        return $this->render('pages/home.html.twig', [
            'properties' => $data,
            'json' => $json
        ]);
    }
}