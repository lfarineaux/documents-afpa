<?php
namespace App\Controller;

use App\Entity\Property;
use App\Repository\PropertyRepository;
use Doctrine\Common\Annotations\AnnotationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class HomeController extends AbstractController {
    /**
     * @Route("/", name="home", methods="GET", )
     * @param PropertyRepository $repository
     * @return Response
     * @throws AnnotationException
     * @throws ExceptionInterface
     */
    public function index (PropertyRepository $repository) : Response {
        $properties = $repository->findLatest();


        $json = file_get_contents('https://www.prevision-meteo.ch/services/json/roubaix');
        $json = json_decode($json);

        return $this->render('pages/home.html.twig', [
            'properties' => $properties,
            'json' => $json
        ]);
    }
}