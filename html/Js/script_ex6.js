var btnStart = document.getElementById("start");
var btnPause = document.getElementById("pause");
var btnStop = document.getElementById("stop");
var sp = document.getElementsByTagName("span");
var timer;
var ms = 0, s = 0, mn = 0, h = 0;
function Start() {
    timer = setInterval(update_chrono, 100);
    btnStart.disabled = true;
}
function update_chrono() {
    ms += 1;
    /*si ms=10 <==> ms*cadence = 1000ms <==> 1s alors on incrémente le nombre de secondes*/
    if (ms == 10) {
        ms = 1;
        s += 1;
    }
    /*on teste si s=60 pour incrémenter le nombre de minute*/
    if (s == 60) {
        s = 0;
        mn += 1;
    }
    if (mn == 60) {
        mn = 0;
        h += 1;
    }
    /*afficher les nouvelle valeurs*/
    sp[0].innerHTML = h + " h";
    sp[1].innerHTML = mn + " min";
    sp[2].innerHTML = s + " s";
    sp[3].innerHTML = ms + " ms";
}
function Pause() {
    clearInterval(timer);
    btnStart.disabled = false;
}
function Stop() {
    clearInterval(timer);
    btnStart.disabled = false;
    ms = 0, s = 0, mn = 0, h = 0;
    /*on accède aux différents span par leurs indice*/
    sp[0].innerHTML = h + " h";
    sp[1].innerHTML = mn + " min";
    sp[2].innerHTML = s + " s";
    sp[3].innerHTML = ms + " ms";
}
document.addEventListener("DOMContentLoaded", function () {
    btnStart.addEventListener("click", Start);
    btnStop.addEventListener("click", Stop);
    btnPause.addEventListener("click", Pause);
});