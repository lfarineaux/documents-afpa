/*function surface(event) {
    var vButton = document.getElementById("ipt");
    var vParam = vButton.value;
    vButton = document.getElementById("ipt2");
    var vParam2 = vButton.value;
    vButton = document.getElementById("ipt3");
    var vParam3 = vButton.value;
    vButton = document.getElementById("ipt4");
    var vParam4 = vButton.value;

    if(vParam4) {

    } else if (vParam3) {
        alert("La surface du rectangle est de "+ vParam*vParam2);
    } else if (vParam2) {
        alert("La surface du rectangle est de "+ vParam*vParam2);
    } else {
        alert("La surface du carré est de " + vParam*vParam);
    }


}

document.getElementById("calc").addEventListener("click",surface);
*/



function toutCacher(classPram){
    var list = document.getElementsByClassName(classPram);
    for(var i=0; i<list.length; i++){
        list[i].style.display= 'none';
    }
    document.getElementById("result").innerHTML='';
    document.getElementById("phrase-result").innerHTML='';
}

function toutAfficher(classPram){
    var list = document.getElementsByClassName(classPram);
    for(var i=0; i<list.length; i++){
        list[i].style.display= 'block';
    }
}

function afficheCarre(event) {
   toutCacher('saisie-speciale');
   toutAfficher('saisie-carre');
};

function afficheRectangle(event) {
    toutCacher('saisie-speciale');
    toutAfficher('saisie-rectangle');
};

function afficheTriangle(event) {
    toutCacher('saisie-speciale');
    toutAfficher('saisie-triangle');
};

function surfaceCarre(event) {
    var vButton = document.getElementById("ipt-c");
    var vParam = vButton.value;
    var vResult = vParam*2;
    var vButtonResult = document.getElementById("phrase-result");
    vButtonResult.innerHTML="La surface du carré est de : ";
    document.getElementById("result").innerHTML=vResult;
}

function surfaceRectangle(event) {
    var vButton = document.getElementById("ipt-1-r");
    var vParam = vButton.value;
    var vButton2 = document.getElementById("ipt-2-r");
    var vParam2 = vButton2.value;
    if(!vParam2){
        vParam2 = 0;
    }
    if(!vParam){
        vParam = 0;
    }
    var vResult = vParam*vParam2;
    var vButtonResult = document.getElementById("phrase-result");
    vButtonResult.innerHTML="La surface du rectangle est de : ";
    document.getElementById("result").innerHTML=vResult;
}

function perimetreTriangle(event) {
    var vButton = document.getElementById("ipt-1-t");
    var vParam = parseInt(vButton.value,10);
    if(!vParam){
        vParam = 0;
    }
    var vButton2 = document.getElementById("ipt-2-t");
    var vParam2 = parseInt(vButton2.value, 10);
    if(!vParam2){
        vParam2 = 0;
    }
    var vButton3 = document.getElementById("ipt-3-t");
    var vParam3 = parseInt(vButton3.value,10);
    if(!vParam3){
        vParam3 = 0;
    }
    var vResult = vParam+vParam2+vParam3;
    var vButtonResult = document.getElementById("phrase-result");
    vButtonResult.innerHTML="Le perimetre du triangle est de : ";
    document.getElementById("result").innerHTML=vResult;
}

window.addEventListener("DOMContentLoaded", (event) => {
    console.log("DOM entièrement chargé et analysé");
    document.getElementById("Carree").addEventListener("click", afficheCarre);
    document.getElementById("Rectangle").addEventListener("click", afficheRectangle);
    document.getElementById("Triangle").addEventListener("click", afficheTriangle);
    toutCacher('saisie-speciale');
    document.getElementById("ipt-c").addEventListener("keyup", surfaceCarre);

    document.getElementById("ipt-1-r").addEventListener("keyup", surfaceRectangle);
    document.getElementById("ipt-2-r").addEventListener("keyup", surfaceRectangle);

    document.getElementById("ipt-1-t").addEventListener("keyup", perimetreTriangle);
    document.getElementById("ipt-2-t").addEventListener("keyup", perimetreTriangle);
    document.getElementById("ipt-3-t").addEventListener("keyup", perimetreTriangle);
});

