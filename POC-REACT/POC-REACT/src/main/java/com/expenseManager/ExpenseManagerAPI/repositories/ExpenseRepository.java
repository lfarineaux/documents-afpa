package com.expenseManager.ExpenseManagerAPI.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.expenseManager.ExpenseManagerAPI.domain.Expense;



public interface ExpenseRepository extends CrudRepository<Expense,Integer> {
	public List<Expense>findAll();

	public List<Expense> findByMonthAndYear(String month, int year);

	public	List<Expense> findByYear(int year);

}
