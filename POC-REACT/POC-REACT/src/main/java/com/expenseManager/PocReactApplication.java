package com.expenseManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocReactApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocReactApplication.class, args);
	}

}
