function toutCacher(classPram) {
  var list = document.getElementsByClassName(classPram);
  for (var i = 0; i < list.length; i++) {
    list[i].style.visibility = "hidden";
  }
}

function toutAfficher(classPram) {
  var list = document.getElementsByClassName(classPram);
  for (var i = 0; i < list.length; i++) {
    list[i].style.visibility = "visible";
  }
}

function VisibleHistorique() {
  toutCacher("hidden");
  toutAfficher("visible-text-historique");
  //  document.getElementById('descriptif_historique').innerHTML = "Notre historique";
}

function VisiblePartenaires() {
  toutCacher("hidden");
  toutAfficher("visible-text-partenaires");
  // document.getElementById('descriptif_partenaires').innerHTML = "Nos partenaires";
}

function VisibleActions() {
  toutCacher("hidden");
  toutAfficher("visible-text-actions");
  //  document.getElementById('descriptif_actions').innerHTML = "Nos actions";
}

function VisibleSalles() {
  toutCacher("hidden");
  toutAfficher("visible-text-salles");
  //  document.getElementById('descriptif_salles').innerHTML = "Nos salles";
}

function VisibleConnexion() {
  toutCacher("hidden");
  toutAfficher("col-form");
  toutAfficher("visible-text-sinscrire");
  toutAfficher("visible-text-seconnecter");
  //  document.getElementById('descriptif_salles').innerHTML = "Nos salles";
}


document.addEventListener("DOMContentLoaded", function () {
  console.log("DOM entièrement chargé et analysé");
  document.getElementById("lien_historique").addEventListener("click", VisibleHistorique);
  document.getElementById("lien_partenaires").addEventListener("click", VisiblePartenaires);
  document.getElementById("lien_actions").addEventListener("click", VisibleActions);
  document.getElementById("lien_salles").addEventListener("click", VisibleSalles);
  document.getElementById("lien_connexion").addEventListener("click", VisibleConnexion);
  });