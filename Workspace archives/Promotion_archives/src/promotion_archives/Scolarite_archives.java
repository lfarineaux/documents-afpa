package promotion_archives;

import java.util.HashSet;
import java.util.Iterator;

/** @author Laurent Fx
 *  @version 2.0
 */

public class Scolarite_archives {

	private HashSet<Promotion_archives> prolist ;

	public Scolarite_archives() {
		this.prolist =new HashSet<Promotion_archives>();
	}

	public void affiche_pro () {
		//		 ArrayList<Promotion> list = new ArrayList<Promotion>(this.prolist);
		//		 Collections.sort(list); 
		//		 System.out.println(list);

		Iterator <Promotion_archives>val = prolist.iterator(); 
		if (!val.hasNext()) {
			System.out.println("Liste de promotion vide");
		}
		while (val.hasNext()) { 
			System.out.println(val.next()); 
		} 
	}

	public boolean testnometu (String nometudiant) {
		boolean etudiant=false;
		for(Promotion_archives pr : this.prolist) {
			HashSet<Etudiant_archives> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant_archives et : etudiantlistTmp) {
				if (et.getNomEt().equals(nometudiant)) {
					etudiant=true;
					break;
				}
			}
		}
		if (!etudiant) {
			System.out.println("ERREUR : Le nom "+nometudiant+" n'existe pas.");
		}
		return etudiant;
	}

	public boolean testnompromo (String nompromo) {
		boolean promo=false;
		for (Promotion_archives pr : this.prolist ) {
			if (pr.getNomPro().equals(nompromo)) {
				promo=true;
				break;
			} 
		}
		if (!promo) {
			System.out.println("ERREUR : La promotion "+nompromo+" n'existe pas.");
		}
		return promo;
	}

	public boolean testidetu (int i) {
		boolean id=false;
		for(Promotion_archives pr : this.prolist) {
			HashSet<Etudiant_archives> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant_archives et : etudiantlistTmp) {
				if (et.getId()==i) {
					id=true;
					break;
				}
			}
		}
		if (!id) {
			System.out.println("ERREUR : L'Id "+i+" n'existe pas.");
		}
		return id;
	}

	public void ajout_promo (String nompromo) {
		Promotion_archives p = new Promotion_archives (nompromo);
		boolean newpromo = true;
		for (Promotion_archives pr:prolist) {
			if (pr.getNomPro().equals(nompromo)) {
				System.out.println("ERREUR : La promotion "+nompromo+" existe d�j�.");
				newpromo=false;
				break;
			}
		}
		if (newpromo) {
			prolist.add(p);
			System.out.println("Ajout de La promotion "+nompromo+".");
		}
	}

	public void retrait_promo (String nompromo) {
		Promotion_archives p = new Promotion_archives (nompromo);
		prolist.remove(p);
		System.out.println("Suppression de la promo "+nompromo+".");
	}

	//	public void ajout_etu (String nompromo, String nometudiant, String prenometudiant) {
	//		for (Promotion pr:this.prolist) {
	//			if (pr.getNomPro().equals(nompromo)) {
	//				HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
	//				for (Etudiant etu : etudiantlistTmp) {
	//					if ((etu.getNomEt().equals(nometudiant))&&(etu.getPrenomEt().equals(prenometudiant))) {
	//						System.out.println("ERREUR : Le nom "+nometudiant+" existe d�j�.");
	//						break;
	//					}
	//					else {
	//						Etudiant e = new Etudiant (nometudiant,prenometudiant);
	//						pr.getEtudiantlist().add(e);
	//					}
	//				}
	//			}
	//		}
	//	}
	//
	//	
	//	public void retrait_etu (String nompromo, String nometudiant, String prenometudiant) {
	//	
	//		for (Promotion pr:this.prolist) {
	//			if (pr.getNomPro().equals(nompromo)) {
	//				HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
	//				for (Etudiant etu : etudiantlistTmp) {
	//					if ((etu.getNomEt().equals(nometudiant))&&(etu.getPrenomEt().equals(prenometudiant))) {
	//						Etudiant e = new Etudiant (nometudiant,prenometudiant);
	//						pr.getEtudiantlist().remove(e);
	//						break;
	//					}
	//				}
	//			}
	//		}
	//	}


	public Promotion_archives getPromoParNom (String nompromo) {
		Promotion_archives p=null;
		for (Promotion_archives pr : this.prolist ) {
			if (pr.getNomPro().equals(nompromo)) {
				p=pr;
				break;
			} 
		}
		return p;
	}

	public void getEtuParNom (String nometudiant) {
		for(Promotion_archives pr : this.prolist) {
			HashSet<Etudiant_archives> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant_archives etu : etudiantlistTmp) {
				if (etu.getNomEt().equals(nometudiant)) {
					System.out.println(etu);
					break;
				}
			}
		}
	}

	public void getEtuParId (int id) {
		for(Promotion_archives pr : this.prolist) {
			HashSet<Etudiant_archives> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant_archives etu : etudiantlistTmp) {
				if (etu.getId()==(id)) {
					System.out.println(etu);
					break;
				}
			}
		}
	}

	public void getEtuParPromo (String nometudiant) {
		for(Promotion_archives pr : this.prolist) {
			HashSet<Etudiant_archives> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant_archives etu : etudiantlistTmp) {
				if (etu.getNomEt().equals(nometudiant)) {
					System.out.println(etu);
					System.out.println(pr);
					break;
				}
			}
		}
	}

	public void getPromoParEtu (String nompromo) {
		for(Promotion_archives pr : this.prolist) {
			if (pr.getNomPro().equals(nompromo)) {
				pr.affiche_etu();	
				break;
			}
		}
	}

	public void ajoutnotes (String nometudiant, Integer not) {
		for(Promotion_archives pr : this.prolist) {
			HashSet<Etudiant_archives> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant_archives etu : etudiantlistTmp) {
				if (etu.getNomEt().equals(nometudiant)) {
					etu.getNotes().add(not);
					break;
				}
			}
		}
	}

	public int moyEtu (String nometudiant) {
		int cpt=0;
		int moy=0;
		for(Promotion_archives pr : this.prolist) {
			HashSet<Etudiant_archives> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant_archives etu : etudiantlistTmp) {
				if (etu.getNomEt().equals(nometudiant)) {
					for (int notes : etu.getNotes()) {
						moy+=notes;
						cpt++;
					}
				}
			}
		}
		if (moy==0) {
			System.out.println("Aucune note enregistr�e pour l'�tudiant "+nometudiant+".");
			cpt=1;
		}
		return (moy/cpt);
	}

	public int moyPromo (String nompromo) {
		int cpt=0;
		int moy=0;
		for(Promotion_archives pr : this.prolist) {
			HashSet<Etudiant_archives> etudiantlistTmp = pr.getEtudiantlist();
			if (pr.getNomPro().equals(nompromo)) {
				for (Etudiant_archives etu : etudiantlistTmp) {
					for (int notes : etu.getNotes()) {
						moy+=notes;
						cpt++;
					}
				}
			}
		}
		if (moy==0) {
			System.out.println("Aucune note enregistr�e pour la promotion "+nompromo+".");
			cpt=1;
		}
		return (moy/cpt);
	}


}



