package promotion_archives;

import java.util.HashSet;
import java.util.Iterator;

/** @author Laurent Fx
 *  @version 2.0
 */

public class Promotion_archives {

	private final String nom;
	private HashSet<Etudiant_archives> etudiantlist;

	public Promotion_archives (String p) {
		this.nom=p;
		this.etudiantlist=new HashSet<Etudiant_archives>();
	}

	public String getNomPro() {
		return this.nom;
	}

/*	public void tri(){
	Iterator <Etudiant>val = etudiantlist.iterator(); 
		while (val.hasNext()) { 
			System.out.println(val.next()); 
		} 
 }*/

	public void affiche_etu () {
//		for (Etudiant pr : this.etudiantlist ) {
//			System.out.println(etudiantlist);
//			}
//		This.etudiantlist.trie();
		
		Iterator <Etudiant_archives>val = etudiantlist.iterator(); 
		if (!val.hasNext()) {
			System.out.println("Liste d'�tudiants vide");
		}
		while (val.hasNext()) { 
			System.out.println(val.next()); 
		} 
	}

	public void ajoutetu (String nometudiant, String prenometudiant) {
		boolean newetu = true;
		Etudiant_archives e = new Etudiant_archives (nometudiant,prenometudiant);
		for (Etudiant_archives et:etudiantlist) {
			if (et.equals(e)) {
				System.out.println("L'�tudiant "+nometudiant+" "+prenometudiant+" existe d�j�.");
				newetu=false;
			}
		}
		if (newetu) {
			etudiantlist.add(e);
			System.out.println("Ajout de l'�tudiant "+nometudiant+" "+prenometudiant+".");
		}
	}
	
	public void retraitetu (String nometudiant, String prenometudiant) {
		Etudiant_archives e = new Etudiant_archives (nometudiant,prenometudiant);
		etudiantlist.remove(e);
			System.out.println("Suppression de l'�tudiant "+nometudiant+" "+prenometudiant+".");
	}

	public String toString() {
		return "Promotion ["+ nom +"]";
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Promotion_archives other = (Promotion_archives) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}
	
	public HashSet<Etudiant_archives> getEtudiantlist() {
		return this.etudiantlist;
	}
}
