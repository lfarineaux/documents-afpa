package promotion_archives;

import java.util.Scanner;

/** @author Laurent Fx
 *  @version 2.0
 */

public class Prog_promotion_archives {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int menu, sousmenu = 0;
		boolean fin=true;
		boolean sousfin = true;
		Scolarite_archives school = new Scolarite_archives();
		/** @param
		 *  menu : num�ro saisi dans le menu 
		 *  sousmenu : num�ro saisi dans les sous-menus
		 *	sousfin : sortie des sous-menus
		 *  fin : arr�t du programme
		 */ 

		while (fin) {
			System.out.println();
			System.out.println("      **************");
			System.out.println("      *    MENU    *");
			System.out.println("      **************");
			System.out.println();
			System.out.println(" 1. Ajout/Retrait/Liste de promotion");
			System.out.println(" 2. Ajout/Retrait/Liste d'�tudiant");
			System.out.println(" 3. Recherche �tudiant/promotion");
			System.out.println(" 4. Ajout/Affichage notes");
			System.out.println();
			System.out.println(" 0. Arr�t du programme");
			System.out.println();
			System.out.println("Entrer un chiffre de 0 � 4");
			System.out.print("   ->  ");
			menu = sc.nextInt();
			sc.nextLine();
			System.out.println();

			String promo,nom,prenom = "";
			Integer not;
			int id,moy = 0;
			boolean testpromo,testnom,testid;
			/** @param
			 *  promo : nom de la promo 
			 *  nom : nom de l'�tudiant
			 *  prenom : pr�nom de l'�tudiant
			 *  id : N� Id de l'�tudiant
			 *  not : notes de l'�tudiant
			 *  moy : moyenne des notes
			 *  testpromo : test si promo existante
			 *  testnom : test si �tudiant existant
			 *  testid : test si id existant
			 */ 

			switch (menu) {
			case 1:
				while (sousfin) {

					System.out.println(" 1. Ajout de promotion");
					System.out.println(" 2. Retrait de promotion");
					System.out.println(" 3. Liste de promotions");
					System.out.println();
					System.out.println(" 0. Retour au menu");
					System.out.println();
					System.out.println("Entrer un chiffre de 0 � 3");
					System.out.print("   ->  ");
					sousmenu = sc.nextInt();
					System.out.println();
					sc.nextLine();

					if (sousmenu==1) {
						System.out.println(" 1. Ajout de promotion");
						System.out.println("Saisir la promotion � ajouter : ");
						System.out.print("   ->  ");
						promo = sc.next();
						school.ajout_promo(promo);
						System.out.println();
						break;
					}
					if (sousmenu==2) {
						System.out.println(" 2. Retrait de promotion");
						System.out.println("Saisir la promotion � retirer : ");
						school.affiche_pro();
						System.out.print("   ->  ");
						promo = sc.next();
						testpromo = school.testnompromo(promo);
						if (testpromo) {
							school.retrait_promo(promo);
							System.out.println();
						}
						break;
					}
					if (sousmenu==3) {
						System.out.println(" 3. Liste de promotions");
						school.affiche_pro();
						System.out.println();
						break;
					}

					if (sousmenu==0) {	
						System.out.println("Retour au menu");
						sousfin=false;
						break;	
					}
					if (sousmenu>4) {
						System.out.println("Num�ro incorrect");
						break;
					}

				}
break;
			case 2:
				while (sousfin) {
					System.out.println(" 1. Ajout d'un �tudiant");
					System.out.println(" 2. Retrait d'un �tudiant");
					System.out.println(" 3. Liste des �tudiants");
					System.out.println();
					System.out.println(" 0. Retour au menu");
					System.out.println();
					System.out.println("Entrer un chiffre de 0 � 3");
					System.out.print("   ->  ");
					sousmenu = sc.nextInt();
					System.out.println();
					sc.nextLine();

					switch (sousmenu) {
					case 1:
						System.out.println(" 1. Ajout d'un �tudiant");
						System.out.println("Saisir l'�tudiant � ajouter : ");
						System.out.println("Choisir une promotion existante : ");
						school.affiche_pro();
						System.out.print("   ->  ");
						promo = sc.next();
						testpromo = school.testnompromo(promo);
						if (testpromo) {
							System.out.println("Saisir : ");
							System.out.print("Nom : ");
							nom = sc.next();
							System.out.print("Pr�nom : ");
							prenom = sc.next();
							Promotion_archives pro = school.getPromoParNom(promo);
							pro.ajoutetu(nom,prenom);
							System.out.println();
						}
						break;
					case 2:
						System.out.println(" 2. Retrait d'un �tudiant");
						System.out.println("Saisir l'�tudiant � retirer : ");
						System.out.println("Choisir une promotion existante : ");
						school.affiche_pro();
						System.out.print("   ->  ");
						promo = sc.next();
						testpromo = school.testnompromo(promo);
						if (testpromo) {
							System.out.print("Saisir : ");
							System.out.print("Nom : ");
							nom = sc.next();
							testnom = school.testnometu(nom);
							if (testnom) {
								System.out.print("Pr�nom : ");
								prenom = sc.next();
								Promotion_archives pro = school.getPromoParNom(promo);
								pro.retraitetu(nom,prenom);
								System.out.println();
							}
						}
						break;
					case 3:
						System.out.println(" 3. Liste des �tudiants");

						break;

					case 0:	
						System.out.println("Retour au menu");
						System.out.println();
						sousfin=false;
						break;	

					default:
						System.out.println("Num�ro incorrect");
						break;
					}
				}
				break;
			case 3:
				while (sousfin) {
					System.out.println(" 1. Recherche d'un �tudiant par nom");
					System.out.println(" 2. Recherche d'un �tudiant par id");
					System.out.println(" 3. Recherche d'une promotion d'un �tudiant");
					System.out.println(" 4. Liste des �tudiants par promotion");
					System.out.println();
					System.out.println(" 0. Retour au menu");
					System.out.println();
					System.out.println("Entrer un chiffre de 0 � 4");
					System.out.print("   ->  ");
					sousmenu = sc.nextInt();
					System.out.println();
					sc.nextLine();

					switch (sousmenu) {
					case 1:
						System.out.println(" 1. Recherche d'un �tudiant par nom");
						System.out.println("Saisir le nom de l'�tudiant : ");
						System.out.print("   ->  ");
						nom= sc.next();
						testnom = school.testnometu(nom);
						if (testnom) {
							school.getEtuParNom(nom);
							System.out.println();
						}
						break;
					case 2:
						System.out.println(" 2. Recherche d'un �tudiant par id");
						System.out.print("Saisir l'id de l'�tudiant : ");
						System.out.print("   ->  ");
						id = sc.nextInt();
						testid = school.testidetu(id);
						if (testid) {
							school.getEtuParId(id);
							System.out.println();
						}
						break;
					case 3:
						System.out.println(" 3. Recherche d'une promotion d'un �tudiant");
						System.out.println("Saisir le nom de l'�tudiant : ");
						System.out.print("   ->  ");
						nom = sc.next();
						System.out.println();
						testnom = school.testnometu(nom);
						if (testnom) {
							school.getEtuParPromo(nom);
							System.out.println();
						}
						break;
					case 4:
						System.out.println(" 4. Liste des �tudiants par promotion");
						System.out.println("Choisir une promotion existante : ");
						school.affiche_pro();
						System.out.print("   ->  ");
						promo = sc.next();
						testpromo = school.testnompromo(promo);
						if (testpromo) {
							school.getPromoParEtu(promo);
							System.out.println();
						}
						break;
					case 0:	
						System.out.println("Retour au menu");
						System.out.println();
						sousfin=false;
						break;	
					default:
						System.out.println("Num�ro incorrect");
						break;
					}
				}	

			case 4: 
				while (sousfin) {
					System.out.println(" 1. Ajouter des notes � un �tudiant");
					System.out.println(" 2. Afficher la moyenne des notes d'un �tudiant");		
					System.out.println(" 3. Afficher la moyenne des notes d'une promotion");	
					System.out.println();
					System.out.println(" 0. Retour au menu");
					System.out.println();
					System.out.println("Entrer un chiffre de 0 � 3");
					System.out.print("   ->  ");
					sousmenu = sc.nextInt();
					System.out.println();
					sc.nextLine();

					switch (sousmenu) {
					case 1:
						System.out.println(" 1. Ajouter des notes � un �tudiant");
						System.out.println("Saisir le nom de l'�tudiant : ");
						nom = sc.next();
						testnom = school.testnometu(nom);
						if (testnom) {
							System.out.println("Saisir une note : ");
							System.out.print("   ->  ");
							not = sc.nextInt();
							System.out.println();
							school.ajoutnotes(nom,not);
							System.out.println();
						}
						break;
					case 2:
						System.out.println(" 2. Afficher la moyenne des notes d'un �tudiant");		
						System.out.println("Saisir le nom de l'�tudiant : ");
						System.out.print("   ->  ");
						nom = sc.next();
						testnom = school.testnometu(nom);
						if (testnom) {
							moy = school.moyEtu(nom);
							if (moy!=0) {
								System.out.println("Moyenne de l'�tudiant : "+moy);
								System.out.println();
							}
						}
						break;	
					case 3:
						System.out.println(" 3. Afficher la moyenne des notes d'une promotion");	
						System.out.println("Choisir une promotion existante : ");
						System.out.println();
						school.affiche_pro();
						System.out.print("   ->  ");
						promo= sc.next();
						testpromo = school.testnompromo(promo);
						if (testpromo) {
							moy = school.moyPromo(promo);
							if (moy!=0) {
								System.out.println("Moyenne de la promo : "+moy);
								System.out.println();
							}
						}
						break;	
					case 0:	
						System.out.println("Retour au menu");
						System.out.println();
						sousfin=false;
						break;	
					default:
						System.out.println("Num�ro incorrect");
						break;
					}
				}	
			case 0:	
				System.out.println("Arr�t du programme");
				fin=false;
				break;	
			default:
				System.out.println("Num�ro incorrect");
				break;
			}

		}

		sc.close();
	}
}