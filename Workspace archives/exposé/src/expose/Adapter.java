package expose;

public class Adapter {

	public interface Standard {
		public void operation(int nb1,int nb2);
	}


	public class ImplStandard implements Standard {
		public void operation(int nb1, int nb2) {
			System.out.println("Standard, R�sultat est :"+nb1*nb2);
		}
	}


	public class ImplAdaptee {
		public int operation2(int nb1,int nb2){
			return nb1*nb2;
		}
		public void operation3(int nb){
			System.out.println("Adapt�e, R�sultat="+nb);
		}
	}


	public class AdaptateurHeritage extends ImplAdaptee implements Standard {
		public void operation(int nb1, int nb2) {
			int nb=operation2(nb1, nb2);
			operation3(nb);
		}
	}


	public class AdaptateurComposition implements Standard {
		private ImplAdaptee adaptee=new ImplAdaptee();
		public void operation(int nb1, int nb2) {
			int nb=adaptee.operation2(nb1, nb2);
			adaptee.operation3(nb);
		}
	}


















}
