package com.afpa.cda.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.afpa.cda.dto.Personne;

@WebServlet(urlPatterns = { "/index.html" })
public class Servlet extends HttpServlet {

	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Random r= new Random();
		int x=r.nextInt(5);

//		// Exercice 1
//		Personne personne = new Personne("nom"+x, "prenom"+x, "adresse"+x);
//
//		request.setAttribute("attributs", personne);
//
//		RequestDispatcher dispatcher2;
//
//		dispatcher2 = getServletContext().getRequestDispatcher( "/WEB-INF/vue.jsp");
//		dispatcher2.forward(request, response);

		// Exercice 2
		List <Personne> listpers = new ArrayList<Personne>();

		for (int i=0;i<x;i++) {
			Personne p = new Personne("nom "+i, "prenom "+i, "adresse "+i);
			listpers.add (p);
		}

		request.setAttribute("liste",listpers);

		RequestDispatcher dispatcher1;

		dispatcher1 = getServletContext().getRequestDispatcher( "/WEB-INF/vue.jsp");
		dispatcher1.forward(request, response);			










	}



}