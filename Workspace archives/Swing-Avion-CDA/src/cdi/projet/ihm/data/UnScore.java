package cdi.projet.ihm.data;

import java.util.Date;

public class UnScore {
	private String joueur;
	private int score;
	private Date date;
	
	public UnScore(String joueur, int score, Date date) {
		super();
		this.joueur = joueur;
		this.score = score;
		this.date = date;
	}
	
	public String getJoueur() {
		return joueur;
	}
	public int getScore() {
		return score;
	}
	public Date getDate() {
		return date;
	}
}
