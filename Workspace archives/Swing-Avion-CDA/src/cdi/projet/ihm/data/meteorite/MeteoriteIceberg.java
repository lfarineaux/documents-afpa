package cdi.projet.ihm.data.meteorite;

import javax.swing.JPanel;

import cdi.projet.ihm.data.avion.Avion;

final class MeteoriteIceberg extends MeteoriteDeGlace {

	private static final int DEGAT_VAL = 4;
	private static final int WIDTH = 46;
	private static final int BONUS = 5;
	
	public MeteoriteIceberg(JPanel p, Meteorite m, Avion pl) {
		super(p, m, pl);
	}
	
	@Override
	public int getDegat() {
		return DEGAT_VAL;
	}
	
	@Override
	public int getWidth() {
		return WIDTH;
	}

	@Override
	public int getBonus() {
		return BONUS;
	}
}
