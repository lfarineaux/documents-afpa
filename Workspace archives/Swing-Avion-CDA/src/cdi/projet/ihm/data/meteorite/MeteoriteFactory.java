package cdi.projet.ihm.data.meteorite;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cdi.projet.ihm.component.GamePanel;
import cdi.projet.ihm.data.avion.Avion;

public class MeteoriteFactory {

	private static List<Meteorite> listMeteorites = new ArrayList<>();

	private static GamePanel gamePanel;

	private static Avion avion;

	public static void init(GamePanel gp, Avion av) {
		MeteoriteAbstract.init();

		listMeteorites.clear();

		gamePanel = gp;
		avion = av;

		Meteorite meteorite1 = produireUnMeteorite();
		Meteorite meteorite2 = produireUnMeteorite();
		Meteorite meteorite3 = produireUnMeteorite();
		Meteorite meteorite4 = produireUnMeteorite();

		meteorite1.setPrecedent(meteorite3);
		meteorite2.setPrecedent(meteorite1);
		meteorite3.setPrecedent(meteorite2);
		meteorite4.setPrecedent(meteorite3);

		listMeteorites.add(meteorite1);
		listMeteorites.add(meteorite2);
		listMeteorites.add(meteorite3);
		listMeteorites.add(meteorite4);
	}

	public static Meteorite produireUnMeteorite() {
		Meteorite result = null;
		int r = new Random(System.currentTimeMillis()).nextInt(5);
		switch (r) {
		case 0:
			result = new MeteoriteSimple(gamePanel,null,avion);
			break;
		case 1:
			result = new MeteoriteFeu(gamePanel,null,avion);
			break;
		case 2:
			result = new MeteoriteDeGlace(gamePanel,null,avion);
			break;
		case 3:
			result = new MeteoriteZigzag(gamePanel,null,avion);
			break;
		case 4:
			result = new MeteoriteIceberg(gamePanel,null,avion);
			break;
		default:
			break;
		}
		return result;
	}

	public static List<Meteorite> getMeteorites(){
		return listMeteorites;
	}

	public static void paint(Graphics g, boolean gameOver) {
		List<Meteorite> listToAdd = new ArrayList<>();

		List<Meteorite> listToRemove = new ArrayList<>();

		listMeteorites.forEach(m->{
			if(m.isRemplacable()) {
				if(!m.isDetruit()) {
					avion.gagnerPoint(m.getBonus());
				}
				Meteorite mTmp = produireUnMeteorite();
				mTmp.setPrecedent(m.getPrecedent());
				listToAdd.add(mTmp);

				listToRemove.add(m);
			}
		});

		listMeteorites.removeAll(listToRemove);
		listMeteorites.addAll(listToAdd);

		listMeteorites.forEach(m->{
			m.dessiner(g, gameOver);
		});

	}

}
