package cdi.projet.ihm.data.avion;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.Date;

import cdi.projet.ihm.component.GamePanel;
import cdi.projet.ihm.tools.Util;

public class Avion {

	private static final int MOVE_STEP = 5;

	private static Image imgMiddle		= Util.getImg("/img/avion-jeu/avion_middle_position.png");
	private static Image imgMoveRight 	= Util.getImg("/img/avion-jeu/avion_move_right.png");
	private static Image imgMoveLeft 	= Util.getImg("/img/avion-jeu/avion_move_left.png");
	private static Image imgMoveUp = Util.getImg("/img/avion-jeu/avion_move_up.png");
	private static Image imgDestruction = Util.getImg("/img/avion-jeu/avion_destruction.png");

	private Image img;
	private int posX = Integer.MIN_VALUE;
	private int posY;
	private GamePanel gamePanel;
	private int width;
	private int height;
	private int nbVies;
	private int score;
	private String nomJoueur;
	private Date dateDebutPartie;

	public Avion(GamePanel p) {
		this.gamePanel = p;
		this.width = 60;
		this.height = 60;
		
		this.init();
	}

	public void paint(Graphics g) {
		if (this.posX == Integer.MIN_VALUE) {
			this.posX = (gamePanel.getWidth() / 2) - (this.width / 2);
			this.posY = gamePanel.getHeight() - this.height;
		}

		Graphics2D g2d = (Graphics2D) g.create();

		g2d.drawImage(img, this.posX, this.posY, this.width, this.height, gamePanel);
	}

	public void moveToTheRight() {
		if (this.nbVies <= 0) {
			return;
		}
		if (this.gamePanel.getWidth() > this.posX + this.width) {
			this.posX += MOVE_STEP;
		}
		this.img = imgMoveRight;
	}

	public void moveToTheLeft() {
		if (this.nbVies <= 0) {
			return;
		}
		if (0 < this.posX) {
			this.posX -= MOVE_STEP;
		}
		this.img = imgMoveLeft;
	}

	public void moveToTheUp() {
		if (this.nbVies <= 0) {
			return;
		}
		if (0 < this.posY) {
			this.posY -= MOVE_STEP;
		}
		this.img = imgMoveUp;
	}

	public void moveToTheDown() {
		if (this.nbVies <= 0) {
			return;
		}
		if (this.gamePanel.getHeight() > this.posY + this.height) {
			this.posY += MOVE_STEP;
		}
	}

	public void impacte(int degat) {
		this.nbVies -= degat;
		if (this.nbVies <= 0) {
			this.img = imgDestruction;
			this.gamePanel.gameOver();
		}
	}
	
	public void gagnerPoint(int p) {
		this.score+=p;
	}

	public void plane() {
		if (this.nbVies <= 0) {
			return;
		}
		this.img = imgMiddle;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getNbVies() {
		return nbVies;
	}

	public void setNbVies(int nbVies) {
		this.nbVies = nbVies;
	}

	public void init() {
		this.img = imgMiddle;
		this.nbVies = 5;
		this.posX = Integer.MIN_VALUE;
		this.score = 0;
		this.dateDebutPartie = new Date();
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getNomJoueur() {
		return nomJoueur;
	}

	public void setNomJoueur(String nomJoueur) {
		this.nomJoueur = nomJoueur;
	}

	public Date getDateDebutPartie() {
		return dateDebutPartie;
	}

}
