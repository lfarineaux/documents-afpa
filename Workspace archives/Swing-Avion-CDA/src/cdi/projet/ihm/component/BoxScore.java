package cdi.projet.ihm.component;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import cdi.projet.ihm.component.listener.BoxScoreMouseListener;
import cdi.projet.ihm.data.UnScore;
import cdi.projet.ihm.tools.Util;

public class BoxScore extends JDialog  {

	private static final long serialVersionUID = 1L;

	public static final int AUCUNE_ACTION = 0;
	public static final int RETOUR_ACTION = 1;

	private int reponse;
	private JButton buttonRetour;
	private JPanel panelScores;
	
	public BoxScore(Frame parent) {
		super(parent,true);
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.setPreferredSize(new Dimension(450, 450));
		this.setResizable(false);
		this.setLayout(new FlowLayout());

		this.panelScores = new JPanel(new GridLayout(21,3));
		
		JScrollPane scrollPane = new JScrollPane(this.panelScores);
		scrollPane.setPreferredSize(new Dimension(400, 350));
		getContentPane().add(scrollPane);

		BoxScoreMouseListener boxScoreMouseListener = new BoxScoreMouseListener(this);

		JPanel panRelancer = new JPanel(new FlowLayout());
		panRelancer.setPreferredSize(new Dimension(300, 40));
		this.getContentPane().add(panRelancer);
		panRelancer.add(new JLabel("Relancer une nouvelle partie"));
		buttonRetour = new JButton("GO");
		buttonRetour.setPreferredSize(new Dimension(60, 30));
		buttonRetour.addMouseListener(boxScoreMouseListener);
		panRelancer.add(buttonRetour);

		this.setLocation(parent.getX()+30, parent.getY()+40);

		this.pack();
	}

	public int getReponse() {
		return reponse;
	}

	public void setReponse(int reponse) {
		this.reponse = reponse;
	}

	public JButton getButtonRetour() {
		return buttonRetour;
	}

	public void afficher() {
		List<UnScore> listScore = new ArrayList<>();
		try(BufferedReader br = new BufferedReader(new FileReader(System.getProperty("java.io.tmpdir")
				+File.separator+FenetreJeu.FICHIER_SAUVEGARDE_PARTIE))) {
			for(String line; (line = br.readLine()) != null; ) {
				String[] split = line.split(";");

				UnScore unScore = new UnScore(split[0], Integer.parseInt(split[1]) , Util.parserDate(split[2]));
				listScore.add(unScore);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		listScore.sort(new Comparator<UnScore>() {
			@Override
			public int compare(UnScore o1, UnScore o2) {
				return Integer.compare(o2.getScore(),o1.getScore());
			}
		});
		
		if(listScore.size()>20) {
			listScore = listScore.subList(0, Math.min(listScore.size(), 20));
			sauvegarderLaPartie(listScore);
		}
		this.panelScores.removeAll();
		this.panelScores.add(new JLabel("nom"));		
		this.panelScores.add(new JLabel("score"));
		this.panelScores.add(new JLabel("date"));
		for(int i = 0; i<listScore.size(); i++) {
			this.panelScores.add(new JLabel(listScore.get(i).getJoueur()));
			this.panelScores.add(new JLabel(Integer.toString(listScore.get(i).getScore())));
			this.panelScores.add(new JLabel(Util.formaterDate(listScore.get(i).getDate())));
		}
		for(int i=listScore.size(); i<20; i++) {
			this.panelScores.add(new JLabel(""));
			this.panelScores.add(new JLabel(""));
			this.panelScores.add(new JLabel(""));
		}
		
		this.setVisible(true);
	}
	
	private void sauvegarderLaPartie(List<UnScore> listScore) {
		FileWriter fw = null;
		try {
			fw = new FileWriter(System.getProperty("java.io.tmpdir")
					+File.separator+FenetreJeu.FICHIER_SAUVEGARDE_PARTIE);
			for(UnScore unScore : listScore) {
				fw.write(unScore.getJoueur()
						+";"
						+unScore.getScore()
						+";"
						+Util.formaterDate(unScore.getDate())
						+"\n");
			}
			fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
