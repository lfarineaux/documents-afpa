package cdi.projet.ihm.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import cdi.projet.ihm.tools.Util;

public class FenetreJeu extends JFrame{

	private static final long serialVersionUID = 1L;
	public static final String FICHIER_SAUVEGARDE_PARTIE = "0A_scores_ihm.cdi";
	private GamePanel gamePanel;
	private GameInformationPanel gameInformationPanel;
	private JPanel containerPartie = new JPanel();
	private BoxQuestion boxQuestion;
	private BoxScore boxScore;

	public FenetreJeu(){
		this.setTitle("CDI swing Ihm");
		this.setSize(500, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);

		containerPartie.setBackground(Color.white);
		containerPartie.setLayout(new BorderLayout());
		
		gamePanel = new GamePanel();
		containerPartie.add(gamePanel, BorderLayout.CENTER);
		
		gameInformationPanel = new GameInformationPanel(gamePanel);
		containerPartie.add(gameInformationPanel, BorderLayout.NORTH);

		boxQuestion = new BoxQuestion(this);
		
		boxScore = new BoxScore(this);
		
		this.setContentPane(containerPartie);
		this.setVisible(true);
		go();
	}

	private void go(){
		String playerName = JOptionPane.showInputDialog(this, "Saisissez le nom du joueur ");
		while (playerName == null 
				|| playerName.length() < 3
				|| playerName.length() > 6
				|| playerName.contains(";")) {
			String mssErr= "";
			if(playerName == null || playerName.length() < 3 || playerName.length() > 6) {
				mssErr = "(la longueur doit �tre entre 3 et 6 caract�res)";
			} else if(playerName.contains(";")) {
				mssErr = "(le caract�re ; n'est pas permis)";
			} else {
			}
			playerName = JOptionPane.showInputDialog(this, "Saisissez le nom du joueur "+mssErr);
		}
		this.gamePanel.getPlane().setNomJoueur(playerName);
		
		while(! this.gamePanel.isGameOver()){
			gameInformationPanel.repaint();
			gamePanel.repaint();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} 
		gameInformationPanel.repaint();
		gamePanel.repaint();
		
		sauvegarderLaPartie();
		
		this.boxQuestion.setVisible(true);

		while (boxQuestion.getReponse() == BoxQuestion.SCORES_ACTION) {
			this.boxScore.afficher();
			this.boxQuestion.setVisible(true);
		}
		if(boxQuestion.getReponse() == BoxQuestion.EXIT_ACTION) {
			System.exit(0);
			
		} else if (boxQuestion.getReponse() == BoxQuestion.RELANCER_ACTION) {
			this.boxQuestion.setReponse(BoxQuestion.AUCUNE_ACTION);
			this.gamePanel.init();
			go();
			
		}
		
	}
	
	private void sauvegarderLaPartie() {
		FileWriter fw = null;
		try {
			fw = new FileWriter(System.getProperty("java.io.tmpdir")
					+File.separator+FICHIER_SAUVEGARDE_PARTIE
					, true);
			fw.write(gamePanel.getPlane().getNomJoueur()
					+";"
					+gamePanel.getPlane().getScore()
					+";"
					+Util.formaterDate(gamePanel.getPlane().getDateDebutPartie())
					+"\n");
			fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
