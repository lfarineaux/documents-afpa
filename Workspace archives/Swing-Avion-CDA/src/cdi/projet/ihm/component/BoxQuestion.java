package cdi.projet.ihm.component;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import cdi.projet.ihm.component.listener.BoxQuestionMouseListener;

public class BoxQuestion extends JDialog  {

	private static final long serialVersionUID = 1L;
	
	public static final int AUCUNE_ACTION = 0;
	public static final int RELANCER_ACTION = 1;
	public static final int SCORES_ACTION = 2;
	public static final int EXIT_ACTION = 3;
	
	private int reponse;

	private JButton goQuestion1;
	private JButton goQuestion2;
	private JButton goQuestion3;
	
	public BoxQuestion(Frame parent) {
        super(parent,true);
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.setPreferredSize(new Dimension(250, 150));
        this.setResizable(false);
        
        this.setLayout(new GridLayout(3, 1));
        
        BoxQuestionMouseListener boxQuestionMouseListener = new BoxQuestionMouseListener(this);

        JPanel panRelancer = new JPanel(new FlowLayout());
        this.getContentPane().add(panRelancer);
        panRelancer.add(new JLabel("Relancer une nouvelle partie"));
        goQuestion1 = new JButton("GO");
        goQuestion1.setPreferredSize(new Dimension(60, 30));
        goQuestion1.addMouseListener(boxQuestionMouseListener);
        panRelancer.add(goQuestion1);

        JPanel panHisto = new JPanel(new FlowLayout());
        this.getContentPane().add(panHisto);
        panHisto.add(new JLabel("Afficher les scores"));
        goQuestion2 = new JButton("GO");
        goQuestion2.setPreferredSize(new Dimension(60, 30));
        goQuestion2.addMouseListener(boxQuestionMouseListener);
        panHisto.add(goQuestion2);
        
        JPanel panExit = new JPanel(new FlowLayout());
        this.getContentPane().add(panExit);
        panExit.add(new JLabel("Sortir du jeu"));
        goQuestion3 = new JButton("GO");
        goQuestion3.setPreferredSize(new Dimension(60, 30));
        goQuestion3.addMouseListener(boxQuestionMouseListener);
        panExit.add(goQuestion3);
        
        this.setLocation(parent.getX()+100, parent.getY()+200);
        
        this.pack();
	}

	public int getReponse() {
		return reponse;
	}

	public void setReponse(int reponse) {
		this.reponse = reponse;
	}

	public JButton getGoQuestion1() {
		return goQuestion1;
	}

	public JButton getGoQuestion2() {
		return goQuestion2;
	}

	public JButton getGoQuestion3() {
		return goQuestion3;
	}
}
