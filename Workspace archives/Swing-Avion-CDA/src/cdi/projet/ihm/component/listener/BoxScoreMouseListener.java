package cdi.projet.ihm.component.listener;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import cdi.projet.ihm.component.BoxQuestion;
import cdi.projet.ihm.component.BoxScore;

public class BoxScoreMouseListener implements MouseListener {

	private BoxScore boxScore;

	public BoxScoreMouseListener(BoxScore b) {
		this.boxScore = b;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Component componentClick = e.getComponent();
		boolean actionTrouvee = false;
		if(boxScore.getButtonRetour().equals(componentClick)) {
			boxScore.setReponse(BoxQuestion.RELANCER_ACTION);
			actionTrouvee = true;
		}
		if(actionTrouvee) {
			this.boxScore.setVisible(false);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

}
