package cdi.projet.ihm.component.listener;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import cdi.projet.ihm.component.BoxQuestion;

public class BoxQuestionMouseListener implements MouseListener {

	private BoxQuestion boxQuestion;

	public BoxQuestionMouseListener(BoxQuestion b) {
		this.boxQuestion = b;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Component componentClick = e.getComponent();
		boolean actionTrouvee = false;
		if(boxQuestion.getGoQuestion1().equals(componentClick)) {
			boxQuestion.setReponse(BoxQuestion.RELANCER_ACTION);
			actionTrouvee = true;
		} else if(boxQuestion.getGoQuestion2().equals(componentClick)) {
			boxQuestion.setReponse(BoxQuestion.SCORES_ACTION);
			actionTrouvee = true;
		} else if(boxQuestion.getGoQuestion3().equals(componentClick)) {
			boxQuestion.setReponse(BoxQuestion.EXIT_ACTION);
			actionTrouvee = true;
		}
		if(actionTrouvee) {
			this.boxQuestion.setVisible(false);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

}
