package cdi.projet.ihm.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import cdi.projet.ihm.data.avion.Avion;
import cdi.projet.ihm.data.avion.listener.ClavierListener;
import cdi.projet.ihm.data.meteorite.MeteoriteFactory;
import cdi.projet.ihm.tools.Util;

public class GamePanel extends JPanel {
	private static final long serialVersionUID = 1L;

	private Image bg =  Util.getImg("/img/bg_galaxy.png");

	private Avion plane;
	private boolean gameOver;

	public GamePanel() {
		super();
		this.setBorder(BorderFactory.createEmptyBorder(1, 20, 1, 1));
		this.setFocusable(true);
		this.requestFocusInWindow();

		this.plane = new Avion(this);
		ClavierListener cl = new ClavierListener(this.plane);
		this.addKeyListener(cl);

		MeteoriteFactory.init(this, plane);
	}

	public void paintComponent(Graphics g){
		g.setColor(Color.white);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

		g.drawImage(bg, 0, 0, this.getWidth(), this.getHeight(), this);

		this.plane.paint(g);
		MeteoriteFactory.paint(g,gameOver);
		
	}

	public Avion getPlane() {
		return plane;
	}

	public void setPlane(Avion plane) {
		this.plane = plane;
	}

	public void gameOver() {
		this.gameOver = true;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void init() {
		this.plane.init();
		MeteoriteFactory.init(this, plane);
		this.gameOver = false;
	}
}