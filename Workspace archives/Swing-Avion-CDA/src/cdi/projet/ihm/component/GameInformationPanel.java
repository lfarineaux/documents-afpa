package cdi.projet.ihm.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cdi.projet.ihm.tools.Util;

public class GameInformationPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private Image imgNbVie = Util.getImg("/img/game-information/avion_middle_position.png");
	private Image imgScore = Util.getImg("/img/game-information/score_bitcoin.png");

	private JLabel nbViesLabel;
	private JLabel scoreLabel;
	private JLabel nomJoueurLabel;
	
	private GamePanel gamePanel;

	public GameInformationPanel(GamePanel pan) {
		this.gamePanel = pan;
		
		this.nbViesLabel = new JLabel(Integer.toString(this.gamePanel.getPlane().getNbVies()));
		this.scoreLabel = new JLabel(formatScore(this.gamePanel.getPlane().getScore()));
		this.scoreLabel.setPreferredSize(new Dimension(210, 26));

		this.nomJoueurLabel = new JLabel(this.gamePanel.getPlane().getNomJoueur());
		
		this.setBackground(Color.CYAN);
		this.setPreferredSize(new Dimension(500, 26));
		
		BorderLayout borderLayout = new BorderLayout();
		this.setBorder(BorderFactory.createEmptyBorder(5, 33, 5, 7));
		this.setLayout(borderLayout);

		this.add(this.nbViesLabel,BorderLayout.EAST);
		this.add(this.nomJoueurLabel,BorderLayout.CENTER);
		this.add(this.scoreLabel,BorderLayout.WEST);
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
		this.nbViesLabel.setText(Integer.toString(this.gamePanel.getPlane().getNbVies()));
		this.nbViesLabel.repaint();

		this.scoreLabel.setText(formatScore(this.gamePanel.getPlane().getScore()));
		this.scoreLabel.repaint();
		
		this.nomJoueurLabel.setText(this.gamePanel.getPlane().getNomJoueur());
		this.nomJoueurLabel.repaint();

		Graphics2D g2d = (Graphics2D) g.create();
		g2d.drawImage(imgNbVie, 452, 3, 20, 20, this);
		
		g2d.drawImage(imgScore, 10, 3, 19, 20, this);
	}

	private String formatScore(int score) {
		String result = Integer.toString(score);
		switch(result.length()) {
		case 1:
			result="00"+result;
			break;
		case 2:
			result="0"+result;
			break;
		}
		return result;
	}

}
