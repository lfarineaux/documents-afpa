package cdi.projet.ihm.tools;

import java.awt.Image;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

public final class Util {

	private Util() {
	}
	
	public static Image getImg(String s)  {
		Image res = null;
		try {
			res = ImageIO.read(Util.class.getResourceAsStream(s));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public static String formaterDate(Date d)  {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return sdf.format(d);
	}

	public static Date parserDate(String dateStr)  {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			return sdf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
