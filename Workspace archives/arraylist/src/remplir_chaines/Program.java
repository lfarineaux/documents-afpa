package remplir_chaines;

import java.util.*;

public class Program {


	public static void main(String [] args){

		Scanner scan = new Scanner(System.in);

		System.out.print("Combien de chaines de caract�res � saisir : ");

		int nbre = scan.nextInt();

		ArrayList<String> a = new ArrayList<String>();

		for (int i=0;i<nbre;i++) {
			System.out.print("Saisir la chaine n�"+i+1+" : ");
			String saisie = scan.next();
			a.add(saisie);
		}

		System.out.print("Affichage dans l'ordre de saisie : ");
		System.out.println(a);

		ArrayList<String> b =new ArrayList<String>();
		b.addAll(a);

		triPerso(b);
		System.out.println("affichage b triperso : "+b);
		
		ArrayList<String> c =new ArrayList<String>();
		c.addAll(a);
		Collections.sort(c);
		System.out.println("affichage c tri Collections : "+c);

		for(int i = 0; i<a.size(); i++) {
			if(! b.get(i).equals(c.get(i))) {
				System.out.println("Prooooooooooooooobl�me ! ");
				break;
			}
		}
		scan.close();
	}
	
	public static void triPerso(ArrayList<String> bparam) {
		int tail=bparam.size();
		int permut=0;
		while (permut ==0) {
			permut=1;

			for (int i=0; i<tail-1;i++) {
				String str1=bparam.get(i);
				String str2=bparam.get(i+1);
				int z=str1.compareTo(str2);
				if (z>0) {
					String str3=str2;
					str2=str1;
					str1=str3;
					bparam.set(i,str1);
					bparam.set(i+1,str2);

					permut=0;
				}
			}
		}
	}
}
