package cda.exec;

import cda.util.Terminal;

public class ExChaine2 {
	public static void main (String [] arguments){
		String s1 = "bonjour";
		String s2;
		Terminal.ecrireString("Entrez une chaine: ");
		s2 = Terminal.lireString();
		
		Terminal.ecrireString(s1+".charAt(0) : ");
		Terminal.ecrireCharln(s1.charAt(0));
		
		Terminal.ecrireString(s1+".toUpperCase() : ");
		Terminal.ecrireStringln(s1.toUpperCase());
		
		Terminal.ecrireString(s1+".compareTo("+s2+") : ");
		Terminal.ecrireIntln(s1.compareTo(s2));
		
		Terminal.ecrireString(s1+".equals("+s2+") : ");
		Terminal.ecrireBooleanln(s1.equals(s2));
	}
}
