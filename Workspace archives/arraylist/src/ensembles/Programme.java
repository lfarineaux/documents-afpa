package ensembles;

public class Programme {

	public static void main(String [] args){
	    Ensemble a = new Ensemble();
	    Ensemble b = new Ensemble();
	    Ensemble c = new Ensemble();
	    
	    a.ajoute("1");
	    a.ajoute("2");
	    a.ajoute("3");
	    a.ajoute("3");
	    
	    b.ajoute("3");
	    b.ajoute("5");
	    b.ajoute("7");
	    
	    System.out.println(a);
	    System.out.println(b);
	    System.out.println(c);
	    
//	    System.out.println("taille de a = "+a.taille());	    
//	    System.out.println("taille de b = "+b.taille());
//	    System.out.println("taille de c = "+c.taille());
//	    
	    c = a.union(b);
	    System.out.println("union de a et b = "+c);
	    Ensemble d = new Ensemble();
	    d = a.intersection(b);
	    System.out.println("intersection de a et b = "+d);
	}
	
	
}
