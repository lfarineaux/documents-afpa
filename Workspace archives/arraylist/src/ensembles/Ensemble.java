package ensembles;

import java.util.ArrayList;

public class Ensemble {

	private ArrayList<String> elements;

	public Ensemble () {
		this.elements = new ArrayList<String>();
	}

	public void ajoute(String a) {
		if (!this.elements.contains(a)) {
		this.elements.add(a);
		}
	}


	public int taille() {
		return this.elements.size();
	}

	public String getElement(int i) {
		if (i>this.elements.size()) {
			return null;
		}
		return this.elements.get(i);
	}

	public String toString() {
		if (this.elements.size()==0) {
			return "ensemble vide";
		}

		return this.elements+"";
		
//		String s = "";
//        if (this.elements.isEmpty()) {
//            System.out.println("Ensemble vide");
//        } else {
//            // s += this.elements;
//            StringBuilder sb = new StringBuilder();
//            sb.append('[');
//            for(String e : this.elements) {
//                sb.append(e);
//                sb.append(',');
//            }
//            sb.deleteCharAt(sb.length()-1);
//            sb.append(']');
//            s = sb.toString();
//        }
//        return s;
//		
	}

	public Ensemble union (Ensemble x) {
		Ensemble uni=new Ensemble();

		for(int i =0; i < this.elements.size(); i++) {
			uni.ajoute(this.elements.get(i));
		}
		for (int i =0; i < x.taille(); i++)
			if (this.elements.contains(x.getElement(i))) {
				
			}
			else {
				uni.ajoute(x.getElement(i));		
			}
		
		return uni;
	}
	public Ensemble intersection (Ensemble x) {
		Ensemble inter=new Ensemble();
		
		for(int i =0; i < x.taille(); i++)
			if (this.elements.contains(x.getElement(i)))
				inter.ajoute(x.getElement(i));
		
		
		return inter;
	}



}
