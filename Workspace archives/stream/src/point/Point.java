package point;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Point {

	private int x, y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void affiche() {
		System.out.print("[" + x + ", " + y + "] ");
	}

	@Override
	public String toString() {
		return "" + x + ", " + y;
	}

	public static void main(String[] args) {

		// Exercice 1
		Point pta = new Point(2, 5);
		Point ptb = new Point(2, 3);
		Point ptc = new Point(6, -3);
		Point ptd = new Point(-3, 4);

		Point [] tabp =  {pta,ptb,ptc,ptd}; 

		for (Point pt : tabp) {
			pt.affiche();
		}
		System.out.println();

		List<Point> lpp = Arrays.stream(tabp)
				.filter(x->x.getX()>0)
				.collect(Collectors.toList());

		lpp.stream()
		.forEach(Point::affiche);

		System.out.println();

		// Exercice 2

		int[] tabint = {1,4,7};
//		Integer [] tabInt =  {1,4,7};

		List<Point> pointsList = Arrays.stream(tabint)
			.mapToObj(k->new Point(k,2*k))
			.collect(Collectors.toList());

		pointsList.stream()
		.forEach(Point::affiche);
		
		System.out.println();
		
		
		Map<Integer, List<Point>> mapPt = 
		Arrays.stream(tabint)
		.mapToObj(k->new Point(k,2*k))
		.collect(Collectors.groupingBy(Point::getX));
		mapPt.forEach((x, listPoint)-> System.out.println(""+x+" "+ listPoint));
		
		// Exercice 3

		String [] tabstr = {"aaa","bbbb","ccccc","dddddd"};
		
		String str=Arrays.stream(tabstr)
				.filter(x->x.length()>4)
//				.map(StringBuilder::new)
				//	.forEach(x->System.out.print(x+"|"))
				//	.map(s->s.substring(0,s.length()-1))
				//				.map( n -> n.toString())
				.collect( Collectors.joining( "|" ) );
//		.collect(StringBuilder::new,(a,n)->{a.append('|').append(n);},StringBuilder::append);

		System.out.println("---------->"+str);

		  Map<String, List<String>> motsMap = Arrays.stream(tabstr)
	                .collect(Collectors.groupingBy(p -> p.substring(0, 1)));
		
		  System.out.println(motsMap);
		
		
		//		HashMap<String, String []> hmstr = new HashMap<>();


		//		HashMap<String, String []> hmstr = Arrays.stream(tabstr)
		//		.filter(x->x.startsWith(x))
		//		.collect(Collectors.toMap(x->x.startsWith(x), y->y));
	}


}
