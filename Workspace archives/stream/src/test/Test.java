package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List <String> maList = new ArrayList<>();
		maList.add("lo");

		//		Stream <String > st1 = maList.stream();
		//		Stream <String> st2 = st1.map(String::toUpperCase);

		//		st2.forEach(x->System.out.println(x));
		// 			OU
		//		st2.forEach(System.out::println);

		maList.stream()
		.map(String::toUpperCase)
		.forEach(System.out::println);

		String [] str = { "lau","eli","loi"};
		List<String> ml = Arrays.stream(str)
				.map(String::toUpperCase)
				.collect(Collectors.toList());




				ml.forEach(System.out::println);




	}

}
