package com.afpa.cda.service;

import java.util.List;

import com.afpa.cda.dto.PersonneDto;
 
public interface IPersonneService {
	public List<PersonneDto> chercherToutesLesPersonnes();
	void ajouterPersonne(int id,String nom);
	public void supprimer(int id);
}
