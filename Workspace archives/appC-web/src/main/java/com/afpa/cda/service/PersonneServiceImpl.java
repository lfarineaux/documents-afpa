package com.afpa.cda.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.PersonneRepository;
import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.entity.Personne;
 
@Service
public class PersonneServiceImpl implements IPersonneService {

	@Autowired
	private PersonneRepository personneRepository;
	
	@Override
	public List<PersonneDto> chercherToutesLesPersonnes() {
		return this.personneRepository
			.findAll()
			.stream()
			.map(e->new PersonneDto(e.getId(), e.getName()))
			.collect(Collectors.toList());
	}

	@Override
	public void ajouterPersonne (int id,String nom) {
		PersonneDto personDto = new PersonneDto(id, nom);
		Personne person = new Personne ();
		person.setId(personDto.getId());
		person.setName(personDto.getNom());
		this.personneRepository.save(person);
	}

	@Override
	public void supprimer(int id) {
		PersonneDto personDto = new PersonneDto(id);
		Personne person = new Personne ();
		person.setId(personDto.getId());
		this.personneRepository.deleteById(person.getId());
		
	}


}
