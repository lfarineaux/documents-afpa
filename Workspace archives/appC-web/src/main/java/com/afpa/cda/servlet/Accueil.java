package com.afpa.cda.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.service.IPersonneService;

/**
 * Servlet implementation class Accueil
 */
@WebServlet(urlPatterns = {"/index.html"})
public class Accueil extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private IPersonneService personneService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config); 
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		writer.append("<html>");
		writer.append("<head>");
		writer.append("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">");
		writer.append("</head>");
		writer.append("<body>");
		writer.append("<table class=\"table\">");
		writer.append("<thead class=\"thead-dark\">");
		writer.append("<tr>");
		writer.append("<td>id</td>");
		writer.append("<td>name</td>");
		writer.append("</tr>");
		writer.append("</thead>");

		writer.append("<tbody>");
		this.personneService.chercherToutesLesPersonnes()
		.forEach(p->{
			writer.append("<tr>");
			writer.append("<td>").append(Integer.toString(p.getId())).append("</td>");
			writer.append("<td>").append(p.getNom()).append("</td>");
			writer.append("<td> <form method=\"post\" action=\"index.html\">");
			writer.append("<input type=\"hidden\" name=\"typeAction\" value=\"suppression\">");
			writer.append("<input type=\"hidden\" name=\"idsup\" value=\""+ p.getId()+"\">");
			writer.append( "<input type=\"submit\" value=\"supprimer\">");
			writer.append( "</form>");
			writer.append( "</td>");
			writer.append("</tr>");
		});
		writer.append("</table>");

		writer.append("<p>Ajouter une personne</p>");
		writer.append("<form method=\"post\" action=\"index.html\">");
		writer.append("<input type=\"hidden\" name=\"typeAction\" value=\"ajout\">");
		writer.append("<p>Id<input type=\"text\" name=\"id\"></p>");
		writer.append("<p>Nom<input type=\"text\" name=\"name\"></p>");
		writer.append("<input type=\"submit\">");

		//		writer.append("<p>Supprimer une personne</p>");
		//		writer.append("<form method=\"post\" action=\"index.html\">");
		//		writer.append("<p>Id<input type=\"text\" name=\"idsup\"></p>");
		//		writer.append("<input type=\"submit\">");

		writer.append("</body>");
		writer.append("</html>");

		


		String typeAction=request.getParameter("typeAction");

//		if(typeAction == null || typeAction.length() == 0 ) {
//			//request.getRequestDispatcher("/index.html").forward(request, response);
//			response.sendRedirect(request.getContextPath() + "/index.html");
//			return ;
//		}

		// Ajouter
		if(typeAction.equals("ajout")) {
			String str1=request.getParameter("id");
			String str2=request.getParameter("name");
			
//			if(str1 == null || str1.length() == 0 
//					|| str2 == null || str2.length() == 0) {
//				//request.getRequestDispatcher("/index.html").forward(request, response);
//				response.sendRedirect(request.getContextPath() + "/index.html");
//				return ;
//			}
			
			Integer i = Integer.parseInt(str1);
			this.personneService.ajouterPersonne(i, str2);
		
		} else if(typeAction.equals("suppression")) {
			
		} 
		




		// Supprimer
//		String str3 = "0";
//		if(request.getParameter("idsup").length() != 0 || request.getParameter("idsup") !=  null) {
//			str3=request.getParameter("idsup");
//		}
//		 
//		
//
//		if(str3 == null || str3.length() == 0) {
//			//request.getRequestDispatcher("/calcule.html").forward(req, resp);
//			response.sendRedirect(request.getContextPath() + "/index.html");
//			return ;
//		}
//		
//		Integer j = Integer.parseInt(str3);
//		this.personneService.supprimer(j);
//		
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
