package com.afpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.afpa.tools.Tools;

public class App {

	static Logger monLogger = LoggerFactory.getLogger(App.class);
	
	public static void main(String[] args) {
		monLogger.debug("dans App avant exemple");
		Tools.exemple();
		monLogger.debug("dans App apr�s exemple");
	}
	
}
