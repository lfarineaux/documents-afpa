package com.afpa.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Tools {
	
	static Logger monLogger = LoggerFactory.getLogger(Tools.class);
	
	public static void exemple() {
		monLogger.trace("debut appel methode exemple");
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<10; i++) {
			monLogger.debug("i : "+i);
			sb.append(i);
		}
		
		monLogger.trace("fin appel methode exemple");
		
		monLogger.info(" sb : "+sb);
	}
}
