package calc;

import java.util.Random;  

public class Calculatrice {

	public static int addition (int a, int b)
	{
		System.out.println("* Laurent *");
		int calcul = 0;
		calcul = a+b;
		return calcul;
	}

	public static int soustraction (int a, int b)
	{
		int calcul = 0;
		calcul = a-b;
		return calcul;
	}

	public static int multiplication (int a, int b)
	{
		int calcul = 0;
		calcul = a*b;
		return calcul;
	}

	public static int max (int a, int b)
	{
		if (a>b) {
			return a;
		}
		else {
			return b;
		}
	}

	public static char signe (int a)
	{
		if (a >= 0) {

			return '+';
		} else {
			return '-';
		}

	}

	public static int factorielle (int a)	{

		int i;
		int f=1;

		for (i=a;i>1;i--) {
			System.out.println("i = "+i);
			f = f*i;
		}

		return f;

	}

	//	public static int factorielle (int a)	{
	//
	//		int f=1;
	//		int i=a;
	//	
	//			while (i>1) {
	//				System.out.println("i = "+i);
	//				f = f*i;
	//				i--;
	//			}
	//
	//		
	//		return f;
	//	}

	public static boolean conjonction (boolean a, boolean b)	{
		return a && b;
	}

	public static boolean disjonction (boolean a, boolean b)	{
		return a || b;
	}

	public static boolean negation (boolean a)	{
		return !a;
	}

	public static int aleatoireInferieur (int a)	{
			
	 Random r = new Random();  
		int b = r.nextInt(a);
		return b;
			}
		
}
