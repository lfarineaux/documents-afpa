package calc;

import java.util.Arrays;

public class MotsUtils {

	//	public static void main(String[] args) {

	//		float f = 12.4f;
	//
	//		String monFEnStr = Float.toString(f);
	//
	//		String s =f+"";
	//
	//		char[] charArray = monFEnStr.toCharArray();
	//
	//		System.out.println(Arrays.toString(charArray));
	//
	//		System.out.println(monFEnStr);

	//	}

	public static String inverser (String str) {

		char []tab=new char[str.length()];


		for (int i=0;i<str.length();i++) {
			tab[i]=str.charAt(str.length()-1-i);
		}

		String inv=new String(tab);
		return inv;

	}

	public static String caracteresCommuns(String str,String str2) {

		String res="";

		for (int i=0;i<str.length();i++) {


			for (int j=0;j<str2.length();j++) {

				if (str.charAt(i)==str2.charAt(j)) {
					int indexcarencours = res.indexOf(str.charAt(i));

					if(indexcarencours==-1) {
						res=res+str.charAt(i);
					}
					System.out.println("str.charAt(i) = "+str.charAt(i));
					System.out.println("str2.charAt(j) = "+str2.charAt(j));
				}
			}

		}
		return res;		

	}

	public static boolean estUnPalindrome (String str) {

		//		String str="";
		//		String str2=inverser(str);
		//		boolean res=true;
		//		
		//		if (str == str2) {
		//			return res;
		//			else
		//			{
		//				return res=false
		//			}
		//		}

		int z=str.length()-1;
		boolean res=true;

		for (int a=0;a<z&&res;a++) {
			if (str.charAt(a) != str.charAt(z)) {
				res = false;
			}
			z--;
		}
		return res;
	}


	//	public static long sommeChiffresDansMot (String str) {
	//
	//        long som = 0;
	//        int a=0;
	//        for (int i=0;i<str.length();i++) {
	//            char monchar = str.charAt(i);
	//            if (Character.isDigit(monchar)) {
	//                a=Character.getNumericValue(monchar);
	//                som=som+a;
	//            }
	//        }
	//        return som;    
	//    }

	public static long sommeChiffresDansMot (String str) {

		long som = 0;

		char[] charArray = str.toCharArray();

		for(char c : charArray) {
			if (Character.isDigit(c)) {
				som=som+Character.getNumericValue(c);
			}
		}
		return som;	
	}

	public static String devinerAlgo(int param) {

		int a = param*2;
		int b = param-1;
		String res="";
		res = res+a+b;
		return res;

	}

	public static void afficherNombreOccurence(String str) {

		char ch;
		int somme=0;

		for (int i=0; i<str.length();i++) {
			if(str.charAt(i)!=' ') {

				ch=str.charAt(i);
				somme =0;

				for (int j=0;j<str.length();j++) {
					if (ch==str.charAt(j)) {
						somme++;
					}
				}
				System.out.println(ch+" : "+somme+"  ");
				str = str.replaceAll(Character.toString(str.charAt(i)), " ");

			}
		}
	}

	public static String exercice29 (int num,int nbre) {

		String res="";
		if (num%2==0) {
			num=num+1;
		}
		else {
			num=num+2;
		}
		res=res+num;

		for (int i=1;i<nbre;i++) {
			num=num+2;
			res=res+"*"+num;
		}
		return res;
	}


	public static long sommeUnicodes(String str) {
		long b = 0;
		for(int i=0;i<str.length();i++) {
			long a = str.charAt(i);

			b = b +a;
		}

		return b;

	}

	public static double binaire (String str) {

		double res = 0;

		char[] c = str.toCharArray();

		for (int i=str.length()-1; i>=0 ; i--) {
			int ch=Character.getNumericValue(c[i]);
			int puis = (str.length()-1-i);
			res=res+ch*(Math.pow(2,puis));
		}
		return res;
	}

	public static String exercice30 (String str) {

		String res="";	
		int E = 0;
		int D = 0;

		char[] c = str.toCharArray();
		int i=0;
		//		Somme des chiffres de la partie enti�re
		for (;  i<str.length() && c[i]!='.';i++) {
			int ch=Character.getNumericValue(c[i]);
			E += ch;
		}

		//		on d�place i apr�s le point.
		i++; 

		//		Somme des chiffres de la partie d�cimale
		for (; i<str.length();i++) {
			int ch=Character.getNumericValue(c[i]);
			D += ch;
		}

		//		T=E+D
		res="T"+(E+D)+"E"+E+"D"+D;
		return res;


		//	� Optimized by Isame	
	}

	public static String exercice31 (String str) {

		String res="";	
		int a = 0;
		str=str.toLowerCase();
		System.out.println("str : "+str);

		for(int i=0;i<str.length();i++) {
			a = (int) str.charAt(i)%48;

			res += a;
		}

		return res;
	}


	public static String exercice32 (String str) {

		String res="";	
		String nbre="";
		str+=" ";  // Ajout espace sinon erreur avec 1 chiffre en dernier
		int chif=1;

		for (int i=0;i<str.length();i++) {

			if (Character.isDigit(str.charAt(i))) { 

				if (Character.isDigit(str.charAt(i+1))) {  
					nbre += str.charAt(i);
				}
				else {
					nbre += str.charAt(i); 
					chif=Integer.parseInt(nbre);
					i++;
				}
			}
			for (int j=0;j<chif&&!Character.isDigit(str.charAt(i));j++) {
				res += str.charAt(i);
				nbre="";
			}
			chif=1;
		}
		return res;
	}

	public static int exercice33 (String str) { // Laurent
  //ne fonctionne pas
		
		int ope = 0;
		int num = 0;
		String nbre = "";
		char signe = '+';
		int i=0;
//				str=str.replaceAll("{\\+}2","\\+");

		if (Character.isDigit(str.charAt(0))) {
			nbre += str.charAt(0);
			ope=Integer.parseInt(nbre);
			nbre="";
			System.out.println("ope1 : "+ope);
			i=1;
		}
		else {
			signe+=str.charAt(0);
			System.out.println("signe1 : "+signe);
			i=1;
		}


		for (;i<str.length();i++) {

			if (Character.isDigit(str.charAt(i))) {
				nbre += str.charAt(i);
				num=Integer.parseInt(nbre);
				nbre="";
				System.out.println("nbre2 : "+num);
			}

			else {
				signe+=str.charAt(i);
				System.out.println("signe2 : "+signe);
			}

			//				if (signe =="") {
			//					ope=num;		
			//					System.out.println("signe vide");
			//				}

			if (signe =='+') {
				ope+=num;		
				System.out.println('+');
			}
			if (signe =='-') {
				ope-=num;
				System.out.println('-');
			}
			System.out.println("ope : "+ope);

		}
		return ope;
	}

//	public static int exercice33(String str) { // Redouane
//
//        int res = 0;
//        String nbrStr = "";
//        char operator = '+';
//        int i=0;
//
//        // si la chaine commence par un operator alors : j'enregistre le signe et passe a i=1;
//
//        if(str.charAt(0)=='-') {
//            operator = '-';
//            i = 1;
//        } else if(str.charAt(0)=='+') {
//            i = 1;
//        }
//
//
//        //boucle principale, on commence a lire la chaine de carat�rea partir de 0 ou 1;
//
//        for(; i<str.length(); i++) {
//
//
//            //*****************************************************************
//
//            // si ie charactere est un chiffre alors :  buffer du ie chiffre.
//
//            if( (47 < str.charAt(i) && str.charAt(i) < 58)) {
//
//                nbrStr = nbrStr + str.charAt(i);
//
//            }
//
//
//            //*****************************************************************
//
//            // si ie charactere est pas un chiffre ou que on est a la fin de la chaine de charactere alors : reponse = reponse + buffer; on reinitialise le buffer et l'operator;
//
//            if(operator == '+' && (!(47 < str.charAt(i) && str.charAt(i) < 58) || i+1==str.length())) {
//                res = res + Integer.parseInt(nbrStr);
//                nbrStr = "";
//                operator='+';
//
//            }else if(operator == '-' && (!(47 < str.charAt(i) && str.charAt(i) < 58)|| i+1==str.length())) {
//                res = res - Integer.parseInt(nbrStr);
//                nbrStr = "";
//                operator='+';
//            }
//
//
//
//            //*****************************************************************
//
//            //tant que le ke charactere est un signe alors : on regarde le signe de l'operator et on le change en fonction des signes; on reinitialise i = k;
//
//            for(int k = i; k<str.length() && !(47 < str.charAt(k) && str.charAt(k) < 58); k++) {
//
//                if (operator == '+' && str.charAt(i) == '-') {
//                    operator = '-';
//                }
//                else if(operator == '-' && str.charAt(i) == '-') {
//                    operator = '+';
//                }
//
//                i=k;
//            }
//            
//            //*****************************************************************
//            
//            
//        }    
//        
//        // fin de la boucle principale qui lit les charateres de la chaine .
//
//        return res;
//
//    }
//	
	
//	public static int resultatSigne(String str) { // Maxime
//
//		int res = 0;
//		int chiffreTempoEnInt = 0;
//		char signeMath = '+';
//		String chiffreTempo = "";
//		char[] tab = str.toCharArray();
//		int i = 0;
//
//		while (i < tab.length - 1) {
//
//			// *************************Gestion des
//			// signes*************************************************
//
//			while (!Character.isDigit(tab[i]) && i < tab.length) {
//
//				// break pour ne pas se prendre l'exeption
//				if (i == tab.length - 1) {
//
//					if (signeMath == '+' && tab[i] == '+') {
//						signeMath = '+';
//
//					} else if (signeMath == '+' && tab[i] == '-') {
//						signeMath = '-';
//
//					} else if (signeMath == '-' && tab[i] == '+') {
//						signeMath = '-';
//
//					} else if (signeMath == '-' && tab[i] == '-') {
//						signeMath = '+';
//					}
//					break;
//				}
//
//				if (signeMath == '+' && tab[i] == '+') {
//					signeMath = '+';
//
//				} else if (signeMath == '+' && tab[i] == '-') {
//					signeMath = '-';
//
//				} else if (signeMath == '-' && tab[i] == '+') {
//					signeMath = '-';
//
//				} else if (signeMath == '-' && tab[i] == '-') {
//					signeMath = '+';
//				}
//				i++;
//
//			}
//
//			// ***********************R�cup�ration des
//			// chiffres**********************************************
//
//			while (Character.isDigit(tab[i]) && i < tab.length) {
//
//				// rebreak pour ne pas se prendre l'exeption
//				if (i == tab.length - 1) {
//					chiffreTempo = chiffreTempo + tab[i];
//					break;
//				}
//
//				chiffreTempo = chiffreTempo + tab[i];
//				i++;
//			}
//
//			// ***************************** Le
//			// calcul*******************************************************
//
//			chiffreTempoEnInt = Integer.parseInt(chiffreTempo);
//
//			if (signeMath == '+') {
//				res = res + chiffreTempoEnInt;
//
//			} else if (signeMath == '-') {
//				res = res - chiffreTempoEnInt;
//			}
//
//			chiffreTempo = "0";
//			signeMath = '+';
//		}
//
//		return res;
//	}
//	
//	public static int calculString(String operation) {  // JP
//		boolean operateurs = true;						// Operateur : + ou -
//		String sauvegardeChiffre = "0";
//		int resultat = 0;
//		char parcoursCaractere;
//
//		for (int i = 0; i < operation.length(); i++) {
//			parcoursCaractere = operation.charAt(i);
//
//			if (!Character.isDigit(parcoursCaractere)) {
//				if (parcoursCaractere == '-') { 		// le caractere recup�r� est -
//					if (operateurs) { 					// le boolean est +
//						operateurs = false; 			// operateurs devient -
//					} else { 							// le boolean est -
//						operateurs = true; 				// operateurs devient +
//					}
//				} else { 								// le caractere recup�r� est +
//					if (operateurs) { 					// le boolean est +
//						operateurs = true; 				// operateurs devient +
//					} else { 							// le boolean est -
//						operateurs = false; 			// operateurs devient -
//					}
//				}
//			} else {
//				sauvegardeChiffre = sauvegardeChiffre + parcoursCaractere;
//
//				if (i != operation.length() - 1)
//					if (!Character.isDigit(operation.charAt(i + 1))) {
//						lanceLeCalcul = true;
//					}
//			}
//			if (lanceLeCalcul || i == operation.length() - 1) {
//				resultat = resultat + Integer.parseInt((operateurs?'+':'-')+sauvegardeChiffre);		 
////				Equivalent																			 
////				if (operateurs) {																	 
////					resultat = resultat + Integer.parseInt(sauvegardeChiffre);						 
////				} else {																			 
////					resultat = resultat - Integer.parseInt(sauvegardeChiffre);						 
////				}																					 
//
//				sauvegardeChiffre = "0";
//				operateurs = true;
//				lanceLeCalcul = false;
//			}
//			if (i != operation.length() - 1) {
//				continue;
//			}
//		}
//		return resultat;
//	}

	//exo 33

//	public static int exercice33 (String str) { // Badrane
//		String tempo = "0";
//		String tempo2 = "+";
//		String tempo3 = "";
//		int res = 0;
//		int compte = 0;
//		int compte2 = 0;
//		//ajoute un signe + qui ala fin d string pour eviter le out of renge
//		str = str + "+";
//
//		boolean nbr1 = true;
//		boolean nbr2 = true;
//
//		// boucle pour parcourir mon string
//
//		for (int i = 0; i < str.length(); i++) {
//
//			//mettre tout les chiffres avant le signe dans le premier tempo
//			if (nbr1 == true && Character.isDigit(str.charAt(i))) {
//				while (nbr1 == true && compte == 0) {
//					tempo = tempo + str.charAt(i);
//					compte ++;
//					System.out.println(tempo);
//					compte++;
//				}
//
//				// mettre le signe dans tempo2 qui gere les signe et ferme tempo pour utiliser tempo 3
//			} else if (!Character.isDigit(str.charAt(i))){
//
//
//				if ( tempo2 == "" && str.charAt(i) == '+' || tempo2.charAt(0) == '+' && str.charAt(i)== '+') {
//					tempo2 = "+";
//					//compte2++;
//					System.out.println(tempo2);
//				}  else if (tempo2.charAt(0)== '-' && str.charAt(i)== '-') {
//					tempo2 = "+";
//					compte2++;
//				}else if (tempo2 == ""&& str.charAt(i) == '-' || str.charAt(i) == '-') {
//					tempo2 = "-";
//					compte2++;
//				}
//
//				nbr1 = false;
//
//
//
//				// ici recupere les suite de chiffre avant le prohain signe dans tempo3
//			} else if(nbr1 == false && Character.isDigit(str.charAt(i))) {
//
//				while (nbr2 && compte2 == 0) {
//					tempo3 = tempo3 + str.charAt(i);
//					System.out.println(tempo3);
//					compte2++;
//				}
//
//				if (tempo2 != "" && tempo3 != "" && !Character.isDigit(str.charAt(i+1))) {
//					nbr2 = false;
//				}
//				System.out.println(nbr2);
//
//			} 
//
//
//			if (nbr2 == false) {
//
//
//				if (tempo2.charAt(0) == '+') {
//					System.out.println("moi tempo 2:" + tempo2);
//
//					res = res + (Integer.parseInt(tempo) + Integer.parseInt(tempo3));
//				} else {
//					res = res + (Integer.parseInt(tempo) - Integer.parseInt(tempo3));
//				}
//
//
//				tempo = "0";
//				// mettre une autre valeur dans tmpo2 parce que ajouter un signe (+ ou -) 
//				// dans un string vide (null) renvoie une erreur out of renge.
//				// ne pas metre d espace parce que il gardera l ancien valur qui sera + si +ou - si -
//				tempo2 = "+";
//				tempo3 ="";
//
//				nbr1 = true;
//				nbr2 = true;
//
//				System.out.println(res);
//
//			}
//
//			compte = 0;
//			compte2 = 0;
//		}
//
//		return res;
//
//
//	}
	
	public static void triangle (int nbre) {

		for (int c=nbre;c>0;c--) {
			for (int i=0;i<c;i++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

	public static void debug () {

		String str="132456";
		for (int i=1;i<10;i++) {
			str=str+i+'*';
			i+=2;
			System.out.println(str.charAt(i-5));
		}

	}
}














