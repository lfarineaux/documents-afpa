package calc;

import java.util.Arrays;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Random;
import java.util.stream.Stream;

public class CalculatriceTableauStream {

	public static int sommeElements (int tab[]) {
		return Arrays.stream(tab).sum();
	}

	//	{
	//		int res=0;
	//		for (int i=0;i<tab.length;i++)	
	//
	//			res=res+tab[i];
	//		return res;
	//
	//	}

	public static OptionalInt plusPetitElement (int tab[]) {
		return Arrays.stream(tab).min();
	}

	//	{
	//		int res=0;
	//		res = tab[0];
	//		for (int i=1;i<tab.length;i++)	
	//			if (res >tab[i]) 
	//				res = tab[i];
	//
	//		return res;
	//
	//	}

	public static OptionalInt plusGrandElement (int tab[]) {
		return Arrays.stream(tab).max();
	}

	//	{
	//		int res=0;
	//		res = tab[0];
	//		for (int i=1;i<tab.length;i++)	
	//			if (res <tab[i]) 
	//				res = tab[i];
	//
	//		return res;
	//
	//	}

	public static int sommeElementsDeuxTableaux (int tab1[],int tab2[]) {

		return (Arrays.stream(tab1).sum()+Arrays.stream(tab2).sum());
	}

	//	{
	//		int res=0;
	//		for (int i=0;i<tab1.length;i++)	
	//
	//			res=res+tab1[i];
	//
	//		for (int i=0;i<tab2.length;i++)	
	//
	//			res=res+tab2[i];
	//
	//		return res;
	//
	//	}

	public static int[] triAscendant (int tab[]) {
		return Arrays.stream(tab)
				.sorted()
				.toArray();

	}

	//		int swap=0;
	//		int permut = 1;
	//
	//		while (permut == 1) {
	//			permut = 0;
	//
	//			for (int j=0;j<tab.length-1;j++) {
	//				if (tab[j] > tab [j+1])
	//				{
	//					swap = tab [j];
	//					tab [j] = tab [j+1];
	//					tab [j+1] = swap;
	//					permut = 1;
	//					System.out.println("tab [j] = "+tab [j]);
	//					System.out.println("tab [j+1] = "+tab [j+1]);
	//					System.out.println("swap = "+swap);
	//					System.out.println("j = "+j);
	//					System.out.println("permut = "+permut);
	//				}
	//			}
	//
	//		}
	//		return tab;
	//	}



	//			public static int[] triAscendant(int[] tabInt) {
	//			        //        java.util.Arrays.sort(tabInt);
	//			        int b=0;
	//			        int tab1[]=new int[tabInt.length];
	//			        tab1=tabInt.clone();
	//			        for(int i=0;i<tab1.length-1;i++)
	//			        {
	//			            for(int j=i+1;j<tab1.length;j++)
	//			            {
	//			                if(tab1[i]>tab1[j])
	//			                {
	//			                    b=tab1[i];
	//			                    tab1[i]=tab1[j];
	//			                    tab1[j]=b;
	//			                }
	//			            }
	//			        }
	//
	//			return tab1;
	//			    }
	//	

	//		public static Optional<Integer> jeVeuxRecupUnEntierQuePositif(Integer[] tab){
	//		int res = Integer.MIN_VALUE;
	//        for(int v : tab) {
	//            if(v>res) {
	//                res = v;
	//            }
	//        }
	//        if(res < 0) {
	//            return Optional.empty();
	//        }
	//        return Optional.of(res);
	//	}
	//	
	//	public static void main(String[] args) {
	////		Random r = new Random();
	////      Integer[] intTab = Stream.generate(r::nextInt).limit(10).toArray(Integer[]::new);
	//      Integer[] intTab = {-1,-345,-3};
	//      System.out.println(" ---- "+Arrays.toString(intTab));
	//      Optional<Integer> resultat = jeVeuxRecupUnEntierQuePositif(intTab);
	////      if(resultat.isPresent()) {
	//          System.out.println(resultat.get());
	////      } else {
	////          System.out.println("coucou tableau vide ou que des negatif !!");
	////      }
	//	}

	public static Optional<Boolean> conjonction (boolean tab[])	{
		Boolean[] test = new Boolean[tab.length];
		for (int i = 0; i < test.length; i++) {
			test[i] = tab[i];
		}
		boolean a = Arrays.stream(test).anyMatch(x->x==false);
		if (a) {
			return Optional.of(a);
		} else {
			return Optional.empty();
		}

		//		long a = Arrays.asList(tab).stream().filter(x->false).count();
		//		if(a == 0) {
		//			return true;
		//		} else {
		//			return false;
		//		}

		//		boolean f=true;
		//
		//		for (int i=0;f && i<tab.length;i++) {
		//
		//			f = f && tab [i];
		//			System.out.println("f = "+f);
		//		} 
		//
		//		return f;
	}


	public static int nombreDElementsPair (int tab[]){
		return (int) Arrays.stream(tab).filter(x->x%2==0).count();
		//		int p=0;
		//
		//		for (int i=0;i<tab.length;i++) {		
		//			if (tab[i]%2==0) {		
		//				p ++;
		//			} else {}
		//		}
		//		return p;
	}

	public static boolean chercheSiUnElementExiste (int param,int tab[]){

		return Arrays.stream(tab).anyMatch(x->x==param);
	}
	//		{
	//		boolean res=false;
	//
	//		for (int i=0;i<tab.length;i++) {	
	//			if (tab[i]==param) {
	//				res = true;
	//				break;
	//			}
	//			System.out.println("tab[i] = "+tab[i]);
	//		}
	//		return res;
	//	}

	public static int [] mettreZeroDansLesCasesAIndicesImpair (int tab[]){
		Stream.iterate(1, i->i+1).limit(tab.length).filter(x->x%2==1).forEach(x->tab[x]=0);
		return tab;
	}


	public static int [] triAscendantDeuxTableaux (int tab1[],int tab2[]){
	
		//		int [] tab = Arrays.stream(tab1)+Arrays.stream(tab2)
		//	.toArray();

		
		
		
		return tab1;
	}


	//		int som = tab2.length+tab1.length;
	//		int res [] = new int [som];
	//		int i=0;
	//
	//		for (;i<tab1.length;i++) {
	//			res[i]=tab1[i];
	//		}
	//
	//		for (;i<som;i++) {
	//			res[i]=tab2[i-tab1.length];
	//		}
	//
	//
	//		return CalculatriceTableauStream.triAscendant(res);
	//	}

	public static int [] decalerLesElementsTableauDUneCase (int tab[]){

		int swap = tab[tab.length-1];

		for (int i=tab.length-1;i>0;i--) {
			tab[i]=tab[i-1];
			System.out.println("tab[i] = "+tab[i]);
			System.out.println("tab[i-1] = "+tab[i-1]);
		}
		tab[0]=swap;
		return tab;
	}
}
