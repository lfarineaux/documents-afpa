package ex_suite;

public enum Rank {

	R1, R2, R3, R4, R5, R6, R7, R8, R9;

    public static void main(String[] args) {

        // Affiche le nombre de valeurs du type

        System.out.println(Rank.values().length);

        // Affiche les valeurs de rang impair

        for (Rank r : Rank.values()) {

        	if((r.ordinal()%2) == 1) {

        		System.out.println(r);        		

        	}

        	// Affiche la derni�re valeur du type

        	if((r.ordinal() == Rank.values().length-1)) {

        		System.out.println(r);

        	}

        }

    }

}

