package ex_couleur;


public class Prog {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Couleurs c1 = (Couleurs.BLEU);

		Couleurs c2 = (Couleurs.ROUGE);

		Couleurs c3;

		System.out.println("c1 : "+c1);
		System.out.println("c2 : "+c2);
		System.out.println();
		
		System.out.println(c1.equals(c2));
		System.out.println(c1 == c2);
		System.out.println();

		if (c1!=c2)  {
			c3=c2;
			c2=c1;
			c1=c3;

			System.out.println("c1 : "+c1);
			System.out.println("c2 : "+c2);
		}

		else {
			System.out.println("c1 et c2 sont identiques");
		}
	}

}
