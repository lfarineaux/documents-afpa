package eleve;

import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String el="";

		System.out.println("Saisir 3 �l�ves :");

		for (int e=1; e<=3;e++) {

			System.out.println("Saisir Nom �l�ve "+e+" :");
			int note=0;
			el = sc.next();

			Eleve e1 = new Eleve (el);
			Eleve.listEleves.add(e1);

			System.out.println("Saisir 3 notes :");
			for (int n=1; n<=3;n++) {
				System.out.println("Saisir note "+n+" :");
				note = sc.nextInt();
				e1.ajouterNote(note);
			}
			e1.calculerMoyenne();
		}

		for (Eleve e : Eleve.listEleves) {
			System.out.println(e);
		}

		sc.close();
	}

}