package eleve;

public enum Admis {
	
	N ("Non admis"), P ("Passable"), AB ("Assez bien"), B ("Bien"), TB ("Tr�s bien");

	private final String val;

	private Admis(String v) {
		this.val = v;
	}

	public String getVal() {
		return val;
	}
}
