package eleve;

import java.util.ArrayList;
import java.util.List;


public class Eleve {

	private final String nom;
	private Admis adm;
	private List <Integer> listNotes;
	private double moy;

	public static List <Eleve> listEleves=new ArrayList<>();
	
	public Eleve (String n) {
		this.nom=n;
		this.listNotes = new ArrayList<>();
	}
	
	public void calculerMoyenne() {
		for(Integer n : this.listNotes) {
			this.moy+=n;
		}
		this.moy = this.moy/this.listNotes.size();
		
		if (this.moy<10) {
			this.adm = Admis.N;
			
		} else if (this.moy>=10 && this.moy<12) {
			this.adm = Admis.P;
		
		} else if (this.moy>=12 && this.moy<14) {
			this.adm = Admis.AB;
			
		} else if (this.moy>=14 && this.moy<16) {
			this.adm = Admis.B;
			
		} else if (this.moy>=16) {
			this.adm = Admis.TB;
		}
	}
	
	public void ajouterNote(Integer n) {
		this.listNotes.add(n);
	}
	
	@Override
	public String toString() {
		return "Eleve - nom : " + nom + ", moy : " +moy+", adm : " + adm.getVal();
	}
}
