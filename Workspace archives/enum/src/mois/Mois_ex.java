package mois;

public enum Mois_ex {
	janvier(31, "jan", "january"), fevrier (28, "fev", "february") ;

	private final int jour;
	private final String abrev;
	private final String name;


	private Mois_ex (int i,String a,String n) {
		this.jour=i;
		this.abrev=a;
		this.name=n;

	}


@Override
public String toString() {
	// TODO Auto-generated method stub
	return this.name()+" "+this.jour+" "+this.abrev+" "+this.name;
}

	public static void main(String[] args) {
		for (Mois_ex s :Mois_ex.values()) {
			System.out.println(s);
		}

	}


}