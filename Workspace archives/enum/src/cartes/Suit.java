package cartes;

public final class Suit {

	private final String name;
		
	public final static Suit SPADES = new Suit ("SPADES");
	public final static Suit HEARTS = new Suit ("HEARTS");
	public final static Suit DIAMOND = new Suit ("DIAMOND");
	public final static Suit CLUBS = new Suit ("CLUBS");
	
	private Suit (String n) {
		
		this.name=n;
		
	}

	@Override
	public String toString() {
		return "Suit [name=" + name + "]";
	}
	
	
	
	
}
