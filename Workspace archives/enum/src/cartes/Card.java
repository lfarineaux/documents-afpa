package cartes;

public class Card  {

	private final  Suit suit;
	private final Rank rank;
	
	public Card (Suit s, Rank r) {
		this.suit=s;
		this.rank=r;
		
	}

	@Override
	public String toString() {
		return "Card [suit=" + suit + ", rank=" + rank + "]";
	}

	
	
}
