package affichage_selectif2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Affichage {


	public static void main (String [] args) {
		int [] tab = {1, 4, -2, 9, -3, 5, -3, 12, 7, 6, -11,0 } ;
		System.out.println("-- Les positifs : ");
		affichage (tab, ee -> ee > 0) ;
		System.out.println();
		System.out.println("-- Les n�gatifs :");
		affichage (tab, ee -> ee < 0) ;
		System.out.println();
		System.out.println("-- Les pairs :");
		affichage (tab, ee -> ee %2==0) ;
		System.out.println();
		System.out.println();
		
		System.out.println("-- Les positifs : ");
		Arrays.stream(tab).filter(x -> x>0).forEach(x -> System.out.print(x + "*"));
		System.out.println();
		System.out.println("-- Les n�gatifs :");
		Arrays.stream(tab).filter(x -> x < 0).forEach(x -> System.out.print(x + "*"));
		System.out.println();
		System.out.println("-- Les pairs :");
		Arrays.stream(tab).filter(x -> x% 2 == 0).forEach(x -> System.out.print(x + "*"));
		
	}


	private static void affichage(int[] tab, Iaffichage e) {
	
		for (int i=0;i<tab.length;i++) {
			if (e.affiche(tab[i])) {
				System.out.print(+tab[i]+"*");
			}
		}
	}
	
	
}







