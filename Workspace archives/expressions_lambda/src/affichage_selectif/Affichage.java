package affichage_selectif;

import java.util.function.Predicate;

public class Affichage {


	public static void main (String [] args) {
		int [] tab = {1, 4, -2, 9, -3, 5, -3 } ;
		System.out.println ("-- Les positifs de tab : ") ;
		affichage_selectif (tab, ee -> ee > 0) ;
		System.out.println();
		affichage_selectif2 (tab, ee -> ee > 0) ;
	}


	private static void affichage_selectif(int[] tab, Iaffichage e) {
		for (int i=0;i<tab.length;i++) {
			if (e.affiche(tab[i])) {
				System.out.print(tab[i]+" ");
			}
		}
	}

	private static void affichage_selectif2(int[] tab, Predicate<Integer> e) {
		for (int i=0;i<tab.length;i++) {
			if (e.test(tab[i])) {
				System.out.print(tab[i]+" ");
			}
		}
	}
}







