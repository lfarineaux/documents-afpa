package p4.t1;

class Multiplication implements Calcul {
	@Override
	public int calc(int a, int b) {
		return a*b;
	}
}
