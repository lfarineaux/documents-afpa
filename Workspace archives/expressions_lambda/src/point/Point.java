package point;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Point {

	private int x, y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void affiche() {
		System.out.print("[" + x + ", " + y + "]");
	}

	public void affiche2() {
		System.out.print("(abs = " + x + ", ord = " + y + ")");
	}


	public static void main(String[] args) {

		Point pta = new Point(2, 5);
		Point ptb = new Point(2, 3);
		Point ptc = new Point(6, -3);
		Point ptd = new Point(-3, 4);

		List<Point> lp = new ArrayList<>();

		lp.add(pta);
		lp.add(ptb);
		lp.add(ptc);
		lp.add(ptd);

		traiteListe(lp, Ipoint::abspos, (a, b) -> {
			int resX = Integer.compare(a.x, b.x);
			if (resX == 0) {
				return Integer.compare(a.y, b.y);
			}
			return resX;
		},
				Point::affiche);

		System.out.println();


		traiteListe(lp, 
				(a)->true, (a, b) -> {
					//			int sa = a.x + a.y;
					//			int sb = b.x + b.y;

					return Integer.compare(a.x + a.y, b.x + b.y);
				},
				(a)->a.affiche2());

		//				Point::affiche2);

	}

	public static void traiteListe(List<Point> lp, Predicate<Point> pred, Comparator<Point> comp, Consumer<Point> cons) {
		//		List<Point> lptmp = new ArrayList<Point>();
		//		for(Point p : lp) {
		//			if(pred.test(p)) {
		//				lptmp.add(p);
		//			}
		//		}
		//		lptmp.sort(comp);
		//		lptmp.forEach(cons);


		lp.stream().filter(pred).sorted(comp).forEach(cons);

	}


}
