package tableau_occurences;

import java.util.Arrays;
import java.util.Random;
public class RechercheOccurence<T> implements Runnable {
	private static final int TAILLE_MIN = 100;
	private final T[] donneesTab;
	private final int indexDebut;
	private final int indexFin;
	private final T valeurRecherchee;
	private static int nbOccurences;
	public RechercheOccurence(T[] td,T v, int indD, int indF) {
		this.donneesTab = td;
		this.indexDebut = indD;
		this.indexFin = indF;
		this.valeurRecherchee = v;
	}
	@Override
	public void run() {
		rechercheOccurence();
		System.out.println(Thread.currentThread().getName()+" termin�");
	}
	public void rechercheOccurence() {
		for(int i=this.indexDebut; i<this.indexFin; i++) {
			if(this.valeurRecherchee.equals(this.donneesTab[i])) {
				synchronized (donneesTab) {
					nbOccurences++;
				}
			}
		}
	}

	public static void main(String[] args) throws Exception {
		final int tailleDonnees = 101;
		final String valeurRecherchee = "3";
		String[] donnees = new String[tailleDonnees];
		Random r = new Random();
		for(int i=0; i<tailleDonnees; i++) {
			donnees[i] = Integer.toString(r.nextInt(20));
		}

		if(tailleDonnees > TAILLE_MIN) {
			RechercheOccurence<String> chercheur1 = new RechercheOccurence<String>(donnees, valeurRecherchee, 0, tailleDonnees/2);
			Thread t1 = new Thread(chercheur1);
			RechercheOccurence<String> chercheur2 = new RechercheOccurence<String>(donnees, valeurRecherchee, tailleDonnees/2, tailleDonnees);
			Thread t2 = new Thread(chercheur2);

			t1.start();
			t2.start();

			t1.join();
			t2.join();
		} else {
			RechercheOccurence<String> chercheur = new RechercheOccurence<String>(donnees, valeurRecherchee, 0, tailleDonnees);
			chercheur.rechercheOccurence();
		}

		System.out.println("la valeur "+valeurRecherchee+" est presente "+RechercheOccurence.nbOccurences+" fois dans le tableau :");
		System.out.println(Arrays.toString(donnees));
	}
}



