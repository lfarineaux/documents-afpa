package perroquet_thread;

import java.util.Date;

public class Exec {

	public static void main(String[] args) throws Exception {
		
		System.out.println(Thread.currentThread().getName());
		
		Perroquet p1 = new Perroquet("jacko");
		Perroquet p2 = new Perroquet("jacki");
		
		Thread t1 =new Thread (p1);
		Thread t2 =new Thread (p2);
		
		System.out.println(t1.getState());
		System.out.println(t2.getState());
		
		t1.start();
		t2.start();
		t1.join();
		
		System.out.println(t1.getState());
		System.out.println(t2.getState());

		System.out.println(Thread.currentThread().getName());
		System.out.println(Thread.currentThread().getId());
		System.out.println(Thread.currentThread().getClass());
		
		
	}
}
