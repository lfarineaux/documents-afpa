package compteur;

import java.util.Random;

public class Compteur implements Runnable {

	public final static String JETON="";
	public static int cpt = 0;
	private String nom;
	private int n;

	public Compteur(String nm, int n) {
		this.nom = nm;
		this.n = n;
	}

	Random r = new Random();

	public void run() {
		try {
			for (int i = 1; i <= n; i++) {
				System.out.println(nom + " : " + i);
				int x = r.nextInt(500);
				Thread.sleep(x);
			}
			incrementerCompteur();
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}

	}

	private void incrementerCompteur() {
		synchronized (JETON) {
			System.out.println("*** " + nom + " a fini de compter jusqu'� " + n + " en position " + cpt);
			for (int i = 0; i < 100; i++) {
				System.out.print("");
			}
			cpt++;
		}
	}

}
