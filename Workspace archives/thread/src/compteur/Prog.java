package compteur;


public class Prog {

	public static void main(String[] args) throws Exception {

		Compteur c1 = new Compteur("Toto",10);
		Thread t1 = new Thread(c1);
		Compteur c2 = new Compteur("Titi",27);
		Thread t2 = new Thread(c2);
		Compteur c3 = new Compteur("Tutu",13);
		Thread t3 = new Thread(c3);

		t1.start();
		t2.start();
		t3.start();

	}
}
