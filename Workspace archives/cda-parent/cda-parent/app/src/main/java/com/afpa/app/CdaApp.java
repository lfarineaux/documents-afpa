package com.afpa.app;

import org.apache.commons.lang3.StringUtils;

import com.afpa.tools.CdaTools;
import com.afpa.tools.ParametreNonValideException;

public class CdaApp {
	
	public String doubleFactAndLeftPad(int n) throws ParametreNonValideException{
		if(n <0 || n>= 19) {
			throw new ParametreNonValideException();
		}else {
			CdaTools calcFact = new CdaTools();
			return StringUtils.leftPad(""+calcFact.doubleFactoriel(n), 19,'0');
		}

	}

}
