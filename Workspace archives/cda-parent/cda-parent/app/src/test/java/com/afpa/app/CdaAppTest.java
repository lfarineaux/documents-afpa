package com.afpa.app;

import com.afpa.tools.ParametreNonValideException;

import junit.framework.TestCase;

public class CdaAppTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testDoubleFactAndLeftPadPositif() throws NumberFormatException, ParametreNonValideException {
		CdaApp app = new CdaApp();
		
		assertTrue(Integer.parseInt(app.doubleFactAndLeftPad(4)) == 48);
		
//		try {
//			
//			String test =app.doubleFactAndLeftPad(4);
//			System.out.println(test);
//	
//		} catch (ParametreNonValideException e) {
//			
//			System.out.println("exception");
//		}
	}
	
	public void testDoubleFactAndLeftPadNegatif() {
		CdaApp app = new CdaApp();
		
		try {
			
			String test =app.doubleFactAndLeftPad(-4);
			System.out.println(test);
	
		} catch (ParametreNonValideException e) {
			
			System.out.println("exception");
		}
	}
	
	public void testDoubleFactAndLeftPadSupp() {
		CdaApp app = new CdaApp();
		
		try {
			
			String test =app.doubleFactAndLeftPad(20);
			System.out.println(test);

		} catch (ParametreNonValideException e) {
			
			System.out.println("exception");
		}
	}

}
