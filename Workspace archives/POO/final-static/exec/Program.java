package exec;

import modele.Stagiaire;

public class Program {
	public static void main(String[] args) {

		Stagiaire stg = new Stagiaire();
		stg.setNom("Maxime");
		stg.nouveauSalaire(1500);
		System.out.println(stg);

		stg.nouveauSalaire(2000);
		System.out.println(stg);

		System.out.println(stg);

		Stagiaire stg2 = new Stagiaire();
		stg2.setNom("Laurent");
		System.out.println(stg2);

		System.out.println("duree de stage : "+stg2.DUREE_STAGE);
	}
}
