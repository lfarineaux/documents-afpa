package modele;

public class Stagiaire {
	
	public final int DUREE_STAGE = 6;
	
	private String nom;
	private int salaire;
	
	public String getNom() {
		return this.nom;
	}
	public int getSalaire() {
		return this.salaire;
	}
	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}
	
	
	public void nouveauSalaire(int salaire) {
		this.salaire=salaire;
	}

	public String toString() {
		return "Stagiaire [nom=" + nom + ", salaire=" + salaire + "]";
	}

	public final void setNom(String nom) {
		this.nom = nom;
	}

}
