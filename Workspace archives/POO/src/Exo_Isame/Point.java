package Exo_Isame;

public class Point {

	public char nom;
	public double abscisse;

	//	public Point () {
	//							VIDE
	//	}
	//	
	//public Point () {
	//	this.nom = c;
	//	this.abscisse = d; 
	//		}
	//	

	public Point (char c, double d) { 
		this.nom = c;
		this.abscisse = d; 
	}
	// SI PRIVATE VARIABLES
	//	public char getnom (char nom) {
	//		return this.nom;
	//	}
	//	public double getabscisse (double abscisse) {
	//		return this.abscisse;
	//	}

	public void affiche () { // m�thode qui appelle 1 objet sans STATIC
							// m�thode de classe avec STATIC
		System.out.println("Nom du point : "+this.nom);
		System.out.println("Abscisse du point : "+this.abscisse);
		System.out.println();
	}	

	public double translate (double move) {

		System.out.println("Valeur de translation : "+move);
		System.out.println();
		this.abscisse += move; 
		return this.abscisse;
	}

}
