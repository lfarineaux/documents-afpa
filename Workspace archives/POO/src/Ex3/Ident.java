package Ex3;

public class Ident {

	private static int cpt=0;
	private int num;
	private String nom;

	public Ident (String n) {
		this.nom = n;
	}

	public int getIdent () {
		cpt++;
		this.num=cpt;
		return cpt;
	}

	public int getIdentMax () {
		this.num=cpt;
		return cpt;
	}

	public void afficher() {
			System.out.println ("Identit� de l'objet "+this.nom+" : "+this.num) ;	
			System.out.println ();
	}

	public void affichermax() {
		System.out.println ();
		System.out.println ("Identit� du dernier objet "+this.nom+" : "+this.num) ;	
	}

}


