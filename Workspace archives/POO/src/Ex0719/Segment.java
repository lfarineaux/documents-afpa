package Ex0719;

public class Segment {

	private Point ptA;
	private Point ptB;

	public Segment(Point a, Point b) {
		this.ptA = a;
		this.ptB = b;
	}

	public void setptA (Point pA) {
		this.ptA=pA;
	}

	public void setptB (Point pB) {
		this.ptB=pB;
	}

	public Point getptA () {
		return this.ptA;
	}

	public Point getptB () {
		return this.ptB;
	}

	public String toString() {
		return "["+this.ptA.getnom()+this.ptB.getnom()+"] : "+this.ptA+this.ptB;
		// return +this.ptA+this.ptB; // INCORRECT
		// return ""+this.ptA+this.ptB; // CORRECT
	}

	public void afficher () { 
		System.out.println("**Coordonnées du segment "+"["+this.ptA.getnom()+this.ptB.getnom()+"]");
		this.ptA.afficher();
		this.ptB.afficher();
		System.out.println();
	}

	public double taille () {
		double di=this.ptA.distance(ptB);
		return di;
	}

	public void translate (int ta, int to) {
		this.ptA.translation(ta,to);
		this.ptB.translation(ta,to);
	}

	public Segment symetrique () {
		Point c=this.ptA.symetrique();
		Point d=this.ptB.symetrique();
		Segment seg=new Segment(c,d);
		return seg;		
	}


	public boolean equals (Segment segeg) {

		boolean seg=(((this.ptA.equals(segeg.ptA))&&(this.ptB.equals(segeg.ptB)))
				||(this.ptA.equals(segeg.ptB))&&(this.ptB.equals(segeg.ptA)));
		return seg;
	}

}

