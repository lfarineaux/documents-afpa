package Ex0719;

public class Triangle {

	private Segment sx;
	private Segment sy;
	private Segment sz;

	public Triangle (Segment x, Segment y, Segment z) {
		this.sx = x;
		this.sy = y;
		this.sz = z;
	}

	public void setsx (Segment sX) {
		this.sx=sX;
	}

	public void setsy (Segment sY) {
		this.sy=sY;
	}

	public void setsz (Segment sZ) {
		this.sz=sZ;
	}

	public Segment getsx () {
		return this.sx;
	}

	public Segment getsy () {
		return this.sy;
	}

	public Segment getsz () {
		return this.sz;
	}

	public String toString() {
		return "{ "+this.sx.getptA()+this.sx.getptB()+this.sy.getptA()+this.sy.getptB()+this.sz.getptA()+this.sz.getptB()+"}"+this.sx+this.sy+this.sz;
	}

	public void afficher () { 
		System.out.println("***Coordonnées du triangle {"+this.sx.getptA()+this.sx.getptB()+this.sy.getptA()+this.sy.getptB()+this.sz.getptA()+this.sz.getptB()+"}");
		System.out.println();
	}

	public boolean isocele () {

		boolean iso;

		double dX=sx.taille();
		double dY=sy.taille();
		double dZ=sz.taille();

		iso=((dX==dZ)||(dX==dY)||(dZ==dY));

		return iso;
	}

	public boolean rectangle () {

		boolean rect;

		double dX=sx.taille();
		double dY=sy.taille();
		double dZ=sz.taille();

		double dL=0;
		double dP=0;

		if (dX>dY && dX>dZ) { 
			dL=(Math.pow(dX,2));
			dP=(Math.pow(dY,2)+Math.pow(dZ,2)); 
		}
		if (dY>dZ && dY>dX) {
			dL=(Math.pow(dY,2));
			dP=(Math.pow(dX,2)+Math.pow(dZ,2));
		}
		if (dZ>dX && dZ>dY) {
			dL=(Math.pow(dZ,2));
			dP=(Math.pow(dX,2)+Math.pow(dY,2));
		}

		rect = (dL==dP);

		return rect;
	}

	public double surface () {

		double surf=0;
		double dX=sx.taille();
		double dY=sy.taille();
		double dZ=sz.taille();

		double dp=((dX+dY+dZ)/2);

		surf = Math.sqrt((dp*(dp-dX)*(dp-dY)*(dp-dZ)));

		return surf;

	}

	public boolean equals (Triangle treg) {

		boolean teg=((this.sx.equals(treg.sx))&&(this.sy.equals(treg.sy))&&(this.sz.equals(treg.sz)))
				||((this.sx.equals(treg.sy))&&(this.sy.equals(treg.sz))&&(this.sz.equals(treg.sx)))
				||((this.sx.equals(treg.sz))&&(this.sy.equals(treg.sx))&&(this.sz.equals(treg.sy)));

		return teg;
	}

}
