package Ex0719;

import java.util.Scanner;

public class Programme {
	public static void main(String[] args) {
		//		Programme.testPoint();
		Programme.testSegment();
		Programme.testTriangle();
	}
	public static void testPoint() {
		Scanner sc = new Scanner(System.in);

		// Cr�ation du point
		System.out.print("** POINT **");
		System.out.println();
		System.out.println();
		System.out.print("* CREATION D'UN POINT *");
		System.out.println();
		System.out.print("Saisir le nom du point (une lettre) : ");
		String st = sc.next();
		char name = st.charAt(0);
		System.out.println();

		System.out.println("Saisir les coordonn�es du point "+name);
		System.out.print("Abscisse : ");
		double ab = sc.nextDouble();

		System.out.print("Ordonn�e : ");
		double or = sc.nextDouble();
		System.out.println();

		Point ptA = new Point (name,ab,or);

		ptA.afficher();

		// Translation du point
		System.out.print("* TRANSLATION DU POINT");
		System.out.println();
		System.out.println("Saisir les valeurs de translation du point "+ptA.getnom()+" ("+ptA.getabscisse()+" ,"+ptA.getordonnee()+")");
		System.out.print("En abscisse : ");
		int ta = sc.nextInt();

		System.out.print("En ordonn�e : ");
		int to = sc.nextInt();
		System.out.println();

		ptA.translation(ta,to);

		System.out.print("Apr�s translation :");
		System.out.println();
		ptA.afficher();

		// Distance entre 2 points
		System.out.print("* DISTANCE ENTRE LE POINT "+ptA.getnom()+" ET UN POINT D");
		System.out.println();
		System.out.println("Saisir les coordonn�es du point D : ");

		System.out.print("Abscisse D : ");
		double abd = sc.nextDouble();

		System.out.print("Ordonn�e D : ");
		double ord=sc.nextDouble();
		System.out.println();

		Point ptD = new Point ('D',abd,ord);

		double di=ptD.distance(ptA);
		System.out.println("Distance entre les 2 points "+ptA.getnom()+" et "+ptD.getnom()+" : "+di);
		System.out.println();

		// Sym�trie du point
		Point ptS=ptA.symetrique();
		System.out.println("* SYMETRIE DU POINT ");
		System.out.println("Cr�ation d'un point S sym�trique au point "+ptA.getnom());
		ptS.afficher();
		System.out.println();

		// Egalit� avec un autre point
		System.out.println("* EGALITE ENTRE 2 POINTS *");
		System.out.println();
		System.out.println("Saisir les coordonn�es du point E � tester : ");
		System.out.print("Abscisse E : ");
		double abe = sc.nextDouble();

		System.out.print("Ordonn�e E : ");
		double ore= sc.nextDouble();
		System.out.println();

		Point ptE = new Point ('E',abe,ore);

		boolean eg=ptE.equals(ptA);

		System.out.println("* L'�galit� entre le point "+ptA.getnom()+" ("+ptA.getabscisse()+" ,"+ptA.getordonnee()+")"+" et le point "+ptE.getnom()+" ("+ptE.getabscisse()+" ,"+ptE.getordonnee()+")"+" est : "+eg);
		System.out.println();

		sc.close();
	}

	public static void testSegment() {
		Scanner scan = new Scanner(System.in);

		// Cr�ation des points du segment
		System.out.print("** SEGMENT **");
		System.out.println();Point a = new Point('a', 0, 0);
		Point b = new Point('b', 4, 0);
		Segment sA = new Segment(a,b);

		// Taille du segment
		System.out.print("* TAILLE DU SEGMENT");
		System.out.println();
		double di=sA.taille();
		System.out.println("Taille du segment ["+sA.getptA()+sA.getptB()+"] : "+di);
		System.out.println();

		// Translation du segment
		int abs = 2;
		int ord = 2;

		System.out.print("* TRANSLATION DU SEGMENT");
		System.out.println();
		System.out.println("Valeurs de translation du point ");
		System.out.println("En abscisse : "+abs+", en ordonn�e : "+ord);
		sA.translate(abs,ord);
		System.out.println();
		System.out.print("Apr�s translation");
		System.out.println();

		sA.afficher();

		// Sym�trie du segment
		Segment sS=sA.symetrique();

		System.out.print("* SYMETRIE DU SEGMENT");
		System.out.println();
		System.out.println("Sym�trie du segment : ");
		sS.afficher();

		// Egalit� des segments
		System.out.print("* EGALITE DES SEGMENTS");
		System.out.println();
		Point e = new Point('E', 2, 2);
		Point f = new Point('F', 6, 2);
		Segment sE = new Segment(e,f);

		System.out.println("Egalit� entre le segment ["+sA.getptA()+sA.getptB()+"] et un segment "+sE.getptA()+sE.getptB());

		sE.afficher();

		boolean seg=sE.equals(sA);
		System.out.println("L'�galit� entre les segments est : "+seg);

		scan.close();
	}

	public static void testTriangle() {

		System.out.print("** TRIANGLE **");
		System.out.println();

		// Cr�ation des segments du triangle

		// 1 Triangle isoc�le
		Point a = new Point('a', 0, 0);
		Point b = new Point('b', 4, 0);
		Segment sA = new Segment(a,b);

		Point c = new Point('c', 4, 0);
		Point d = new Point('d', 2, 2);
		Segment sC = new Segment(c,d);

		Point e = new Point('e', 2, 2);
		Point f = new Point('f', 0, 0);
		Segment sE = new Segment(e,f);

		//		// 1 Triangle rectangle
		//		Point a = new Point('a', 0, 0);
		//		Point b = new Point('b', 5, 0);
		//		Segment sA = new Segment(a,b);
		//
		//		Point c = new Point('c', 5, 0);
		//		Point d = new Point('d', 0, 3);
		//		Segment sC = new Segment(c,d);
		//
		//		Point e = new Point('e', 0, 3);
		//		Point f = new Point('f', 0, 0);
		//		Segment sE = new Segment(e,f);

		Triangle Ta = new Triangle(sA, sC, sE);

		Ta.afficher();

		// Triangle isoc�le
		double dX=sA.taille();
		double dY=sC.taille();
		double dZ=sE.taille();

		System.out.print("* TAILLE DU TRIANGLE");
		System.out.println();
		System.out.println("Taille des segments "+Ta.getsx()+Ta.getsy()+Ta.getsz()+" : ");
		System.out.println("*Taille du segment "+Ta.getsx()+" : "+dX);
		System.out.println("Taille du segment "+sC+" : "+dY);
		System.out.println("Taille du segment "+sE+" : "+dZ);
		System.out.println();

		boolean isoc;
		isoc=Ta.isocele();
		System.out.print("* TRIANGLE ISOCELE");
		System.out.println();
		System.out.println("Le triangle est isoc�le : "+isoc);
		System.out.println();

		// Triangle rectangle
		boolean rect;
		rect=Ta.rectangle();
		System.out.print("* TRIANGLE RECTANGLE");
		System.out.println();
		System.out.println("Le triangle est rectangle : "+rect);
		System.out.println();

		// Surface Triangle
		double surf=Ta.surface();
		System.out.print("* SURFACE DU RECTANGLE");
		System.out.println();
		System.out.println("La surface du triangle est : "+surf);
		System.out.println();

		// Egalit� des triangles
		// Cr�tion 2�me Triangle (=isoc�le avecx les segments invers�s)

		System.out.print("* EGALITE DE 2 TRIANGLES ");
		System.out.println();
		Point k = new Point('k', 2, 2);
		Point l = new Point('l', 0, 0);
		Segment sK = new Segment(k,l);

		Point m = new Point('m', 0, 0);
		Point n = new Point('n', 4, 0);
		Segment sM = new Segment(m,n);

		Point o = new Point('o', 4, 0);
		Point p = new Point('p', 2, 2);
		Segment sO = new Segment(o,p);

		Triangle Tk = new Triangle(sK, sM, sO);

		Tk.afficher();

		boolean teg;
		teg=Tk.equals(Ta);
		System.out.println("Egalit� entre le triangle");
		System.out.println("["+Ta.getsx()+Ta.getsy()+Ta.getsz()+"] et un triangle "+Tk.getsx()+Tk.getsy()+Tk.getsz());
		System.out.println();
		System.out.println("L'�galit� des triangles est : "+teg);
		System.out.println();

	}

}
