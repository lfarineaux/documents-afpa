package Ex0719;

public class Point {

	private char nom;
	private double abscisse;
	private double ordonnee;

	public Point () {
		this.nom=' ';
		this.abscisse=0;
		this.ordonnee=0;
	}

	public Point (char n, double a, double o) { 
		this.nom = n;
		this.abscisse = a;
		this.ordonnee = o;
	}

	public void setnom (char ch) {
		this.nom=ch;
	}

	public void setabcisse (double ab) {
		this.abscisse=ab;
	}

	public void setordonnee (double or) {
		this.ordonnee=or;
	}

	public char getnom () {
		return this.nom;
	}

	public double getabscisse () {
		return this.abscisse;
	}

	public double getordonnee () {
		return this.ordonnee;
	}

	public String toString() {
		return this.nom+" ("+this.abscisse+","+this.ordonnee+") ";
	}

	public void afficher () { 
		System.out.println("*Coordonn�es du point "+this.nom);
		System.out.println("Abscisse : "+this.abscisse);
		System.out.println("Ordonn�e : "+this.ordonnee);
		System.out.println();
	}	

	public void translation (int a, int o) {
		this.abscisse+=a;
		this.ordonnee+=o;
	}

	public double distance (Point ptdi) {
		double di = Math.sqrt(Math.pow((this.abscisse-ptdi.abscisse),2)
				+(Math.pow((this.ordonnee-ptdi.ordonnee),2)));
		return di;
	}

	
	public Point symetrique () {
		char n=' ';
		double as=-this.abscisse;
		double os=-this.ordonnee;

		Point pts=new Point(n,as,os);
		return pts;
	}

	public boolean equals (Point pteg) {
		boolean eg=((pteg.abscisse==this.abscisse)&&(pteg.ordonnee==this.ordonnee));
		return eg;
	}


}
