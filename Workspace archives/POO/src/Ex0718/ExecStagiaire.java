package Ex0718;


public class ExecStagiaire {

	public static void main(String[] args) throws Exception {
		
		Stagiaire[] tab1 = Tools.create();
		
		Tools.input(tab1);
		
		System.out.print("Tableau avant tri : ");
		
		Tools.display(tab1);
		
		Tools.triAscendant(tab1);

		System.out.print("Tableau apr�s tri : ");
	
		Tools.display(tab1);
	}

}
