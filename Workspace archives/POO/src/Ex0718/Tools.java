package Ex0718;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class Tools {

	public static Stagiaire[] create () {
		System.out.println();
		Scanner t = new Scanner(System.in);

		System.out.print("Saisir la taille du tableau de stagiaire : ");
		System.out.println();

		int taille = t.nextInt();

		Stagiaire tab1 []= new Stagiaire [taille];

		return tab1;
		
		
	}

	public static void input (Stagiaire[] tab1) throws Exception {
		Scanner p = new Scanner(System.in);

		SimpleDateFormat  sdf = new SimpleDateFormat ("yyyy/MM/dd");
		for(int i = 0; i<tab1.length; i++) {


			System.out.print("Saisir le jour de naissance du stagiaire (yyyy/MM/dd) : ");
			System.out.println();

			String birth = p.next();
			Date d = sdf.parse(birth);

			System.out.print("Saisir le nom du stagiaire : ");
			System.out.println();

			String name = p.next();

			Stagiaire student = new Stagiaire (d,name);

			tab1[i] = student;
		}
		p.close();
	}

	public static void display (Stagiaire[] tab1 ) {

		for (int i=0;i<tab1.length;i++) {

			System.out.println(tab1[i]);
			System.out.println();

		}
	}

	public static void triAscendant (Stagiaire tab[]) {
		System.out.println();
		Stagiaire swap;
		int permut = 1;

		while (permut == 1) {
			permut = 0;

			for (int j=0;j<tab.length-1;j++) {
				if (tab[j].getjour().after(tab [j+1].getjour()))
				{
					swap = tab [j];
					tab [j] = tab [j+1];
					tab [j+1] = swap;
					permut = 1;
				}
			}
		}

		
	}

}
