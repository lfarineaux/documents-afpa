package Ex0718;

import java.util.Date;

public class Stagiaire {

	private Date jour = new Date();

	private String nom = "";

	public Stagiaire (Date day, String name) {
		this.nom = name;
		this.jour = day;
	}

	public void setjour (Date j) {
		this.jour=j;
	}

	public void setnom (String n) {
		this.nom=n;
	}

	public Date getjour () {
		return this.jour;
	}

	public String getnom () {
		return this.nom;
	}

	public String toString() {
		return "Nom : "+this.nom+", Date de naisance : "+this.jour;
	}

}
