package Stage;
import java.util.Scanner;
public class Tools {

	public static Stagiaire[] initTabStag (int n) {
		Stagiaire [] tabStagiaire = new Stagiaire [n];
		return tabStagiaire;
	}

	public static void tableauDeStagiaire(Stagiaire[] tabStagiaire) {
		Scanner sc = new Scanner(System.in);
		for(int i =0; i<tabStagiaire.length; i++) {
			System.out.println("saisie informations du stagiaire "+ (i+1));
			tabStagiaire[i] = new Stagiaire();
			System.out.print("  rentre le jour : ");
			tabStagiaire[i].jour = sc.nextInt();

			sc.nextLine();

			System.out.print("  rentre le nom : ");
			tabStagiaire[i].nom = sc.nextLine();
		}
		sc.close();
	}


	public static void afficherTableauStagiaire(Stagiaire[] tabStagiaire) {
		for (int i=0; i<tabStagiaire.length;i++) {
			System.out.println(tabStagiaire[i]);

		}

	}
	
	public static void triAscendant(Stagiaire[] tabStagiaire) {
		int tampon = 0;
		boolean c;

		do {
			// hypoth�se : le tableau est tri�
			c = false;
			for (int i = 0; i < tabStagiaire.length - 1; i++) {
				// Teste si 2 �l�ments successifs sont dans le bon ordre ou non
				if (tabStagiaire[i].jour > tabStagiaire[i + 1].jour) {
					// s'ils ne le sont pas, on �change leurs positions
					tampon = tabStagiaire[i].jour;
					tabStagiaire[i].jour= tabStagiaire[i + 1].jour;
					tabStagiaire[i + 1].jour = tampon;
					c = true;
				}

			}
		} while (c);

	}

}