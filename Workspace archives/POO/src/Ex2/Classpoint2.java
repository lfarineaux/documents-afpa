package Ex2;

public class Classpoint2 {
	
	// moule de l'objet
	
		// attributs
		private double x ; // abscisse
		private double y ; // ordonnee
		
		// constructeur car pas de void ni int
		public Classpoint2 (int abs, int ord) {
			x = abs ;
			y = ord ;
		}
		// methode car void
		public void deplace (int dx, int dy) {
			x += dx ; y += dy ;
		}
// methode afficher
		
		public double abscisse() {
			return this.x;
		}
		
		public double ordonnee() {
			return this.y;
		}
}
