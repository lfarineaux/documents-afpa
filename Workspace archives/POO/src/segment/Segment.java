package segment;

public class Segment {

	private int a;
	private int b;

	public Segment (int y, int z) {
		this.a=Math.min(y, z);
		this.b=Math.max(y, z);
	}

	public String toString() {
		return "["+this.a+", "+this.b+"]";
	}

	public int longueur () {
		return Math.abs(this.a-this.b);
	}

	public boolean appartient (int pt) {

		boolean ap=false;

		if (pt>this.a && pt<this.b) {
			ap=true;
		}

		return ap;
	}

}
