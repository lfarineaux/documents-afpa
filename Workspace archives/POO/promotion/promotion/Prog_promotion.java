package promotion;

import java.util.Scanner;

/** @author Laurent Fx
 *  @version 2.0
 */

public class Prog_promotion {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int menu = 0;
		boolean fin = true;
		Scolarite school = new Scolarite();
		/** @param
		 *  menu : num�ro saisi dans le menu 
		 *  fin : arr�t du programme
		 */ 

		while (fin) {
			System.out.println();
			System.out.println("      **************");
			System.out.println("      *    MENU    *");
			System.out.println("      **************");
			System.out.println();
			System.out.println(" 1. Ajout d'une promotion");
			System.out.println(" 2. Retrait d'une promotion");
			System.out.println(" 3. Lister les promotions");
			System.out.println(" 4. Ajout d'un �tudiant");
			System.out.println(" 5. Retrait d'un �tudiant");
			System.out.println(" 6. Retrouver un �tudiant par son nom");
			System.out.println(" 7. Retrouver un �tudiant par son id");
			System.out.println(" 8. Retrouver la promotion d'un �tudiant");
			System.out.println(" 9. Lister les �tudiants d'une promotion");
			System.out.println(" 10. Ajouter des notes � un �tudiant");
			System.out.println(" 11. Afficher la moyenne des notes d'un �tudiant");		
			System.out.println(" 12. Afficher la moyenne des notes d'une promotion");		
			System.out.println(" 13. Arr�ter le programme");
			System.out.println();
			System.out.print("   ->  ");
			menu = sc.nextInt();
			sc.nextLine();
			System.out.println();

			String promo,nom,prenom = "";
			Integer not;
			int id,moy = 0;
			boolean testpromo,testnom,testid;
			/** @param
			 *  promo : nom de la promo 
			 *  nom : nom de l'�tudiant
			 *  prenom : pr�nom de l'�tudiant
			 *  id : N� Id de l'�tudiant
			 *  not : notes de l'�tudiant
			 *  moy : moyenne des notes
			 *  testpromo : test si promo existante
			 *  testnom : test si �tudiant existant
			 *  testid : test si id existant
			 */ 

			switch (menu) {
			case 1:
				System.out.println(" 1. Ajout d'une promotion");
				System.out.println("Saisir la promotion � ajouter : ");
				System.out.print("   ->  ");
				promo = sc.next();
				school.ajout_promo(promo);
				break;
			case 2:
				System.out.println(" 2. Retrait d'une promotion");
				System.out.println("Saisir la promotion � retirer : ");
				school.affichepro();
				System.out.print("   ->  ");
				promo = sc.next();
				testpromo = school.testnompromo(promo);
				if (testpromo) {
					school.retrait_promo(promo);
				}
				break;
			case 3:
				System.out.println(" 3. Lister les promotions");
				school.affichepro();
				System.out.println();
				break;
			case 4: 
				System.out.println(" 4. Ajout d'un �tudiant");
				System.out.println("Saisir l'�tudiant � ajouter : ");
				System.out.println("Choisir une promotion existante : ");
				school.affichepro();
				System.out.print("   ->  ");
				promo = sc.next();
				testpromo = school.testnompromo(promo);
				if (testpromo) {
					System.out.println("Saisir : ");
					System.out.print("Nom : ");
					nom = sc.next();
					System.out.print("Pr�nom : ");
					prenom = sc.next();
					Promotion pro = school.getPromoParNom(promo);
					pro.ajoutetu(nom,prenom);
				}
				break;
			case 5:
				System.out.println(" 5. Retrait d'un �tudiant");
				System.out.println("Saisir l'�tudiant � retirer : ");
				System.out.println("Choisir une promotion existante : ");
				school.affichepro();
				System.out.print("   ->  ");
				promo = sc.next();
				testpromo = school.testnompromo(promo);
				if (testpromo) {
					System.out.print("Saisir : ");
					System.out.print("Nom : ");
					nom = sc.next();
					testnom = school.testnometu(nom);
					if (testnom) {
						System.out.print("Pr�nom : ");
						prenom = sc.next();
						Promotion pro = school.getPromoParNom(promo);
						pro.retraitetu(nom,prenom);
					}
				}
				break;
			case 6:
				System.out.println(" 6. Retrouver un �tudiant par son nom");
				System.out.println("Saisir le nom de l'�tudiant : ");
				System.out.print("   ->  ");
				nom= sc.next();
				testnom = school.testnometu(nom);
				if (testnom) {
					school.getEtuParNom(nom);
				}
				break;
			case 7:	
				System.out.println(" 7. Retrouver un �tudiant par son id");
				System.out.print("Saisir l'id de l'�tudiant : ");
				System.out.print("   ->  ");
				id = sc.nextInt();
				testid = school.testidetu(id);
				if (testid) {
					school.getEtuParId(id);
				}
				break;
			case 8:	
				System.out.println(" 8. Retrouver la promotion d'un �tudiant");
				System.out.println("Saisir le nom de l'�tudiant : ");
				System.out.print("   ->  ");
				nom = sc.next();
				testnom = school.testnometu(nom);
				if (testnom) {
					school.getEtuParPromo(nom);
				}
				break;
			case 9:	
				System.out.println(" 9. Lister les �tudiants d'une promotion");
				System.out.println("Choisir une promotion existante : ");
				school.affichepro();
				System.out.print("   ->  ");
				promo = sc.next();
				testpromo = school.testnompromo(promo);
				if (testpromo) {
					school.getPromoParEtu(promo);
				}
				break;
			case 10:	
				System.out.println(" 10. Ajouter des notes � un �tudiant");
				System.out.println("Saisir le nom de l'�tudiant : ");
				nom = sc.next();
				testnom = school.testnometu(nom);
				if (testnom) {
					System.out.println("Saisir une note : ");
					System.out.print("   ->  ");
					not = sc.nextInt();
					school.ajoutnotes(nom,not);
					System.out.println();
				}
				break;
			case 11:	
				System.out.println(" 11. Afficher la moyenne des notes d'un �tudiant");
				System.out.println("Saisir le nom de l'�tudiant : ");
				System.out.print("   ->  ");
				nom = sc.next();
				testnom = school.testnometu(nom);
				if (testnom) {
					moy = school.moyEtu(nom);
					if (moy!=0) {
						System.out.println("Moyenne de l'�tudiant : "+moy);
					}
				}
				break;	
			case 12:	
				System.out.println(" 12. Afficher la moyenne des notes d'une promotion");
				System.out.println("Choisir une promotion existante : ");
				System.out.println();
				school.affichepro();
				System.out.print("   ->  ");
				promo= sc.next();
				testpromo = school.testnompromo(promo);
				if (testpromo) {
					moy = school.moyPromo(promo);
					if (moy!=0) {
						System.out.println("Moyenne de la promo : "+moy);
					}
				}
				break;	
			case 13:	
				System.out.println("Arr�t du programme");
				fin=false;
				break;	
			default:
				System.out.println("Num�ro incorrect");
				break;
			}
		}
		sc.close();
	}
}