package promotion;

import java.util.HashSet;
import java.util.Iterator;

/** @author Laurent Fx
 *  @version 2.0
 */

public class Scolarite {

	private HashSet<Promotion> prolist ;

	public Scolarite() {
		this.prolist =new HashSet<Promotion>();
	}

	/** @param
	 *  prolist : liste de promotions
	 */ 
	
	public void affichepro () {
		Iterator <Promotion>val = prolist.iterator(); 
		if (!val.hasNext()) {
			System.out.println("Liste de promotion vide");
		}
		while (val.hasNext()) { 
			System.out.println(val.next()); 
		} 
	}

	public boolean testnometu (String nometudiant) {
		boolean etudiant=false;
		for(Promotion pr : this.prolist) {
			HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant et : etudiantlistTmp) {
				if (et.getNomEt().equals(nometudiant)) {
					etudiant=true;
					break;
				}
			}
		}
		if (!etudiant) {
			System.out.println("ERREUR : Le nom "+nometudiant+" n'existe pas.");
		}
		return etudiant;
	}

	public boolean testnompromo (String nompromo) {
		boolean promo=false;
		for (Promotion pr : this.prolist ) {
			if (pr.getNomPro().equals(nompromo)) {
				promo=true;
				break;
			} 
		}
		if (!promo) {
			System.out.println("ERREUR : La promotion "+nompromo+" n'existe pas.");
		}
		return promo;
	}

	public boolean testidetu (int i) {
		boolean id=false;
		for(Promotion pr : this.prolist) {
			HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant et : etudiantlistTmp) {
				if (et.getId()==i) {
					id=true;
					break;
				}
			}
		}
		if (!id) {
			System.out.println("ERREUR : L'Id "+i+" n'existe pas.");
		}
		return id;
	}

	public void ajout_promo (String nompromo) {
		Promotion p = new Promotion (nompromo);
		boolean newpromo = true;
		for (Promotion pr:prolist) {
			if (pr.getNomPro().equals(nompromo)) {
				System.out.println("ERREUR : La promotion "+nompromo+" existe d�j�.");
				newpromo=false;
				break;
			}
		}
		if (newpromo) {
			prolist.add(p);
			System.out.println("Ajout de La promotion "+nompromo+".");
		}
	}

	public void retrait_promo (String nompromo) {
		Promotion p = new Promotion (nompromo);
		prolist.remove(p);
		System.out.println("Suppression de la promo "+nompromo+".");
	}

	//	public void ajout_etu (String nompromo, String nometudiant, String prenometudiant) {
	//		for (Promotion pr:this.prolist) {
	//			if (pr.getNomPro().equals(nompromo)) {
	//				HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
	//				for (Etudiant etu : etudiantlistTmp) {
	//					if ((etu.getNomEt().equals(nometudiant))&&(etu.getPrenomEt().equals(prenometudiant))) {
	//						System.out.println("ERREUR : Le nom "+nometudiant+" existe d�j�.");
	//						break;
	//					}
	//					else {
	//						Etudiant e = new Etudiant (nometudiant,prenometudiant);
	//						pr.getEtudiantlist().add(e);
	//					}
	//				}
	//			}
	//		}
	//	}
	//
	//	
	//	public void retrait_etu (String nompromo, String nometudiant, String prenometudiant) {
	//	
	//		for (Promotion pr:this.prolist) {
	//			if (pr.getNomPro().equals(nompromo)) {
	//				HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
	//				for (Etudiant etu : etudiantlistTmp) {
	//					if ((etu.getNomEt().equals(nometudiant))&&(etu.getPrenomEt().equals(prenometudiant))) {
	//						Etudiant e = new Etudiant (nometudiant,prenometudiant);
	//						pr.getEtudiantlist().remove(e);
	//						break;
	//					}
	//				}
	//			}
	//		}
	//	}


	public Promotion getPromoParNom (String nompromo) {
		Promotion p=null;
		for (Promotion pr : this.prolist ) {
			if (pr.getNomPro().equals(nompromo)) {
				p=pr;
				break;
			} 
		}
		return p;
	}

	public void getEtuParNom (String nometudiant) {
		for(Promotion pr : this.prolist) {
			HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant etu : etudiantlistTmp) {
				if (etu.getNomEt().equals(nometudiant)) {
					System.out.println(etu);
					break;
				}
			}
		}
	}

	public void getEtuParId (int id) {
		for(Promotion pr : this.prolist) {
			HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant etu : etudiantlistTmp) {
				if (etu.getId()==(id)) {
					System.out.println(etu);
					break;
				}
			}
		}
	}

	public void getEtuParPromo (String nometudiant) {
		for(Promotion pr : this.prolist) {
			HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant etu : etudiantlistTmp) {
				if (etu.getNomEt().equals(nometudiant)) {
					System.out.println(etu);
					System.out.println(pr);
					break;
				}
			}
		}
	}

	public void getPromoParEtu (String nompromo) {
		for(Promotion pr : this.prolist) {
			if (pr.getNomPro().equals(nompromo)) {
				pr.afficheetu();	
				break;
			}
		}
	}

	public void ajoutnotes (String nometudiant, Integer not) {
		for(Promotion pr : this.prolist) {
			HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant etu : etudiantlistTmp) {
				if (etu.getNomEt().equals(nometudiant)) {
					etu.getNotes().add(not);
					break;
				}
			}
		}
	}

	public int moyEtu (String nometudiant) {
		int cpt=0;
		int moy=0;
		for(Promotion pr : this.prolist) {
			HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
			for (Etudiant etu : etudiantlistTmp) {
				if (etu.getNomEt().equals(nometudiant)) {
					for (int notes : etu.getNotes()) {
						moy+=notes;
						cpt++;
					}
				}
			}
		}
		if (moy==0) {
			System.out.println("Aucune note enregistr�e pour l'�tudiant "+nometudiant+".");
			cpt=1;
		}
		return (moy/cpt);
	}

	public int moyPromo (String nompromo) {
		int cpt=0;
		int moy=0;
		for(Promotion pr : this.prolist) {
			HashSet<Etudiant> etudiantlistTmp = pr.getEtudiantlist();
			if (pr.getNomPro().equals(nompromo)) {
				for (Etudiant etu : etudiantlistTmp) {
					for (int notes : etu.getNotes()) {
						moy+=notes;
						cpt++;
					}
				}
			}
		}
		if (moy==0) {
			System.out.println("Aucune note enregistr�e pour la promotion "+nompromo+".");
			cpt=1;
		}
		return (moy/cpt);
	}
}



