package promotion;

import java.util.HashSet;
import java.util.Iterator;

/** @author Laurent Fx
 *  @version 2.0
 */

public class Promotion {

	private final String nom;
	private HashSet<Etudiant> etudiantlist;

	public Promotion (String p) {
		this.nom=p;
		this.etudiantlist=new HashSet<Etudiant>();
	}

	/** @param
	 *  nom : nom de la promo
	 *  etudiantlist : liste d'�tudiants
	 */ 

	public String getNomPro() {
		return this.nom;
	}

	public void afficheetu () {
		Iterator <Etudiant>val = etudiantlist.iterator(); 
		if (!val.hasNext()) {
			System.out.println("Liste d'�tudiants vide");
		}
		while (val.hasNext()) { 
			System.out.println(val.next()); 
		} 
	}

	public void ajoutetu (String nometudiant, String prenometudiant) {
		boolean newetu = true;
		Etudiant e = new Etudiant (nometudiant,prenometudiant);
		for (Etudiant et:etudiantlist) {
			if (et.equals(e)) {
				System.out.println("L'�tudiant "+nometudiant+" "+prenometudiant+" existe d�j�.");
				newetu=false;
			}
		}
		if (newetu) {
			etudiantlist.add(e);
			System.out.println("Ajout de l'�tudiant "+nometudiant+" "+prenometudiant+".");
		}
	}

	public void retraitetu (String nometudiant, String prenometudiant) {
		Etudiant e = new Etudiant (nometudiant,prenometudiant);
		etudiantlist.remove(e);
		System.out.println("Suppression de l'�tudiant "+nometudiant+" "+prenometudiant+".");
	}

	public String toString() {
		return "Promotion ["+ nom +"]";
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Promotion other = (Promotion) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	public HashSet<Etudiant> getEtudiantlist() {
		return this.etudiantlist;
	}
}
