package promotion;

import java.util.HashSet;

/** @author Laurent Fx
 *  @version 2.0
 */

public class Etudiant {

	private String nom;
	private String prenom;
	private static int cpt=0;
	private int id;
	private HashSet<Integer> notes;

	/** @param
	 *  nom : nom de l'�tudiant
	 *  prenom : pr�nom de l'�tudiant
	 *  id : N� Id de l'�tudiant
	 *  notes : notes de l'�tudiant
	 */ 

	public Etudiant (String nm, String p) {
		this.nom = nm;
		this.prenom = p;
		cpt++;
		this.id=cpt;

		this.notes=new HashSet<Integer>();
	}

	public String getNomEt() {
		return this.nom;
	}

	public String getPrenomEt() {
		return this.prenom;
	}

	public int getId() {
		return this.id;
	}

	public String toString() {
		return "Etudiant [Nom=" + nom + ", Pr�nom=" + prenom + ", id=" + id + "]";
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Etudiant other = (Etudiant) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equalsIgnoreCase(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equalsIgnoreCase(other.prenom))
			return false;
		return true;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public static int getCpt() {
		return cpt;
	}

	public HashSet<Integer> getNotes() {
		return this.notes;
	}
}
