package mockito.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import mockito.main.BloodType;
import mockito.main.Etudiant;

class EtudiantTest {
	Etudiant e;

//	@BeforeAll
//	static void setUpBeforeClass() throws Exception {
//	}
//
//	@AfterAll
//	static void tearDownAfterClass() throws Exception {
//	}

	@BeforeEach
	void setUp() throws Exception {
		System.err.println("Etape 1");
		e=new Etudiant();
		e.setName("Laurent");
		System.err.println("J'ai nomm�");
		e.aE=mock(BloodType.class);
	}

//	@AfterEach
//	void tearDown() throws Exception {
//	}
//
//	@Test
//	void testGetName() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	void testSetName() {
//		fail("Not yet implemented");
//	}

	@Test
	void testInfoOnStudent() {
		System.err.println("Test when");
		when(e.aE.getGroupeSanguin(e)).thenReturn("A+");
		System.err.println("Test when 2");
		assertTrue(e.infoOnStudent(e).equals("Laurent A+"));
	}

}
