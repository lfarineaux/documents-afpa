package liste;

public class Stagiaire {

	private final String prenom;
	private final boolean tete;

	public Stagiaire (String p, boolean t) {
		this.prenom = p;
		this.tete = t;
	}

	public String getPrenom() {
		return prenom;
	}

	public boolean isTete() {
		return tete;
	}

	public String toString() {
		return "Prenom :"+this.prenom +" , tete : "+this.tete;
	}
}

