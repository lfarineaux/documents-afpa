package liste;

import java.util.ArrayList;

public class Ilot {

	private ArrayList<Stagiaire> stagiairesList;
	private final int numero;
	
	public Ilot(int n) {
		this.numero = n;
		this.stagiairesList = new ArrayList<Stagiaire>();
	}

	public ArrayList<Stagiaire> getStagiairesList() {
		return stagiairesList;
	}

	public void ajoute (Stagiaire a) {
		this.stagiairesList.add(a);
	}
	
	public void setStagiairesList(ArrayList<Stagiaire> stagiairesList) {
		this.stagiairesList = stagiairesList;
	}

	public int getNumero() {
		return numero;
	}

	public String toString() {
		return "Ilot= " + stagiairesList + ", numero= " + numero;
	}
	
}
