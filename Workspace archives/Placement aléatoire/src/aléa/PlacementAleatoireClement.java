package al�a;

import java.util.Random;

public class PlacementAleatoireClement {

	public static void melanger(String[] tete) {
		String stage = "";
		int alea = 0;
		Random num = new Random();
		for(int i=0; i<tete.length;i++) {
			alea = num.nextInt(tete.length);
			stage = tete[i];
			tete[i] = tete[alea];
			tete[alea] = stage;
		}
	}

	public static void main(String[] args) {
		String[] tete = {"REDOUANE","MAXIME","ANTHONY","JP","LAURENT"};
		String[] stagiaire = {"Linda","Richard", "Amaia", "Clement", "Ketsia", "Yassine", "Soulaiman", "Badrane", "Mohamed", "Mostapha"};

		melanger(tete);

		String[][] ilots = new String[5][];
		for(int i = 0; i<5; i++) {
			ilots[i] = new String[3]; 
			ilots[i][0] = tete[i];
		}

		melanger(stagiaire);

		int tailleIlot = 3;
		int m = 0;
		for(int i = 0; i<5; i++) {
			for(int k = 1; k<tailleIlot; k++){
				ilots[i][k] = stagiaire[m++];
			}
		}

		for(int i=0; i<5; i++) {
			melanger(ilots[i]);
			System.out.println("ilot "+(i+1)+ "\n\t"+ilots[i][0] + "\n\t" + ilots[i][1] + "\n\t" + ilots[i][2] + "\n\t");
		}

	}
	
	
	
	
}