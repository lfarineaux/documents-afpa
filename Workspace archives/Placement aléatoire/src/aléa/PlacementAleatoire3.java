package al�a;

import java.util.Arrays;
import java.util.Random;  

public class PlacementAleatoire3 {

	public static void main(String[] args){

		int place1 = 5;
		int place2 = 10;

		String [] tab_t�tes= {"Anthony","R�douane","Maxime","Laurent","Jean-Philippe"};

		String [] tab_stagiaires= {"Richard","Linda","Cl�ment","Ama�a",
				"Yassine","Bertille","Soulaiman","Badrane","Mustapha","Mohamed"};

		String tab_il�ts [] = new String [place1];
		String tab_places [] = new String [place2];
		
		Arrays.fill(tab_il�ts, "");
		Arrays.fill(tab_places, "");

		Random r= new Random();

		for(int i=0;i<place1;i++)
		{
			int x=0;

			do
			{
				x=r.nextInt(place1);
			}
			while(tab_il�ts[x]!="" || x==i);
			tab_il�ts[x]=tab_t�tes[i];
		}

		for(int i=0;i<place2;i++)
		{
			int x=0;

			do
			{
				x=r.nextInt(place2);
			}
			while(tab_places[x]!="" || x==i);
			tab_places[x]=tab_stagiaires[i];
		}

		for(int i=0;i<5;i++) {

			System.out.println("Il�t "+(i+1)+" : ");
			System.out.println("  Place 1 : " + tab_il�ts[i]); 
			for(int j=0;j<2;j++)
			{
				System.out.println("  Place "+(j+2)+" : " +tab_places[(i*2)+j]);
			}
		}
	}
}



