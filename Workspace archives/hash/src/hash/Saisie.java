package hash;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Scanner;

public class Saisie {

	public static void main(String[] args) {

		
		Scanner ph = new Scanner(System.in);
		System.out.print("Saisir une phrase : ");
		String str = ph.nextLine();

		HashMap<Character, Integer> phrase = new HashMap<>();

		int maxcpt = 0;

		for (char ch : str.toCharArray()) {
			if (phrase.containsKey(ch)) { // containsValue
				System.out.println("ch "+ch);
				
				int cpt = phrase.get(ch); // int = value en Integer
				cpt ++;	
				System.out.println("cpt "+cpt);
				if (maxcpt<cpt) {
					maxcpt=cpt;
				}
				phrase.put(ch, cpt);
			} else {
				phrase.put(ch, 1);
			}
		}
		System.out.println("maxcpt : "+maxcpt);
		Iterator<Character> iterat = phrase.keySet().iterator();
//		Iterator<Integer> iterat = phrase.values().iterator();
		while(iterat.hasNext()){ // hasNext boole�n : y at'il un suivant ?
			char key = iterat.next(); // retourne la valeur courante
			if(phrase.get(key)==maxcpt){
				System.out.println("Le caract�re le plus occurent est '" + key + "' et est pr�sent " +  maxcpt+" fois.");
			}
		}
		ph.close();
	}
}
