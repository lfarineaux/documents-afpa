package transform;

public class Voiture {

	private String marque;
	private String model;
	private int annee;
	
	public Voiture(String ma, String mo, int an) {
		this.marque = ma;
		this.model = mo;
		this.annee = an;
	}

	public String toString() {
		return "Voiture [Marque= "+marque+"/Mod�le= "+model+"/Ann�e="+annee +"]";
	}

	public String getMarque() {
		return marque;
	}

	public String getModel() {
		return model;
	}

	public int getAnnee() {
		return annee;
	}

//	 public int hashCode() {
//	        return this.annee;
//	    } 
	
	public int comparer(Voiture v) {
		if(this.annee < v.getAnnee()) {
			return -1;
		} else if(this.annee > v.getAnnee()) {
			return 1;
		} else {
			int compareMarque = this.marque.compareTo(v.getMarque());
			if(compareMarque != 0) {
				return compareMarque;
			} else {
				int compareModele = this.model.compareTo(v.getModel());
				return compareModele;
			}
		}
		
	}
	
}
