package transform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;



public class Transform {

	public static ArrayList<String> transformeA2AL(String[] tab) {
		ArrayList<String> al = new ArrayList<String>();

		for (String s : tab) {
			al.add(s);
		}
		return al;

	}

	public static String[]	transformeAL2A (ArrayList<String> al) {
		String str[] = new String[al.size()]; 

		for (int j = 0; j < al.size(); j++) { 
			str[j] = al.get(j); 
		}
		return str;
	}

	public static String[] transformeHM2A (HashMap<Voiture,String> hm) {
		ArrayList<Voiture> alvoiture = new ArrayList<Voiture>();

		Iterator <Voiture> iterat = hm.keySet().iterator();
		while(iterat.hasNext()){ 
			Voiture key = iterat.next(); 
			alvoiture.add(key);
		}

		trivoiture(alvoiture);

		String[] tab =new String [hm.size()];
		for (int i = 0; i < alvoiture.size(); i++) {
			Voiture voiture = alvoiture.get(i);
			String uneVal = hm.get(voiture);
			tab[i] = uneVal;
		}

		return tab;
	}

	public static void trivoiture (ArrayList<Voiture> alvoiture) {
		int tail=alvoiture.size();
		int permut=0;
		while (permut ==0) {
			permut=1;

			for (int k=0; k<tail-1;k++) {

				Voiture v1 =alvoiture.get(k);
				Voiture v2 =alvoiture.get(k+1);

				int resCompare = v1.comparer(v2);

				if (resCompare>0) {

					alvoiture.set(k,v2);
					alvoiture.set(k+1,v1);

					permut=0;
				}
			}
		}
	}


	public static HashMap<Integer, String> transformeA2HM (String[] tab) {

		HashMap<Integer, String> table1 = new HashMap<Integer, String>();
		Arrays.sort(tab);

		for (int j = 0; j < tab.length; j++) {
			table1.put(new Integer(j), tab[j]);
		}

		return table1;

	}

	public static ArrayList<String> transformeHM2AL  (HashMap<Voiture,String> hm) { 
		String[] var1 = transformeHM2A(hm);
		ArrayList<String> var2 = transformeA2AL(var1);
		return var2;
	}

}
