package transform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Programme {

	public static void main(String[] args) {

		// 1-Conversion tableau de String vers arraylist de String

		System.out.println("* Conversion tableau de String vers arraylist de String");
		String[] tab1 = {"titre 1, titre 2, titre 3"};

		ArrayList<String> al1 = new ArrayList<String>();

		al1=Transform.transformeA2AL (tab1);

		System.out.println("Tableau tab1 : "+Arrays.toString(tab1));
		System.out.println("Array-List al1 :"+al1);
		System.out.println();

		// 2-Conversion arraylist de String vers tableau de String 

		System.out.println("* Conversion arraylist de String vers tableau de String");
		ArrayList<String> al2 = new ArrayList<String>(); 

		al2.add("titre 4"); 
		al2.add("titre 5"); 
		al2.add("titre 6"); 

		String[] tab2 = Transform.transformeAL2A(al2); 

		System.out.println("Array-List al2 : "+al2);
		System.out.println("Tableau tab2 : "+Arrays.toString(tab2));
		System.out.println();

		// 3-Conversion hashmap Voiture,String vers un tableau de String

		System.out.println("* Conversion hashmap Voiture,String vers un tableau de String");
		Voiture V1 = new Voiture ("Volkswagen","Polo",2002);
		Voiture V2 = new Voiture ("Fiat","Punto",2008);
		Voiture V3 = new Voiture ("Seat","Ibiza",2016);
		Voiture V4 = new Voiture ("Ford","Focus",1999);

		HashMap<Voiture, String> h3 = new HashMap<>();
		h3.put(V1,"2002-VW-Polo");
		h3.put(V2,"2008-Fiat-Punto");
		h3.put(V3,"2016-Seat-Ibiza");
		h3.put(V4,"1999-Ford-Focus");
	
		System.out.println("HashMap h3 : "+h3);

		String [] tab3 = Transform.transformeHM2A(h3);

		System.out.println("tab3 : "+Arrays.toString(tab3));
		System.out.println();

		// 4-Conversion tableau de String vers une hashmap Integer/String
	
		String tab4[] = {"Ketsia", "Linda", "Laurent", "Amaia", "Isame", "Anthony", "Jean-Philippe"};
		System.out.println("* Conversion tableau de String vers une hashmap Integer/String");
		HashMap<Integer, String> h4 = new HashMap<>();
		
		h4 = Transform.transformeA2HM(tab4);
		System.out.println("tab4 : "+Arrays.toString(tab4));
		System.out.println("HashMap h4 : "+h4);
		System.out.println();


		// 5-Conversion hashmap Voiture/String vers une arraylist de String
	
		HashMap<Voiture, String> h5 = new HashMap<>();
		h5 = h3;
		
		System.out.println("* Conversion hashmap Voiture/String vers une arraylist de String");
		
		System.out.println("HashMap h5 : "+h5);
		
		ArrayList<String> al5 = new ArrayList<String>();

		al5 = Transform.transformeHM2AL(h5);
		
		System.out.println("Array-List al5 : "+al5);
		
	}

}
