package menu;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		while(!Menu.menu()) {} // Le programme fonctionne tant qu'on ne saisit pas 5
	}

	public static boolean menu () { // La m�thode retourne un boolean

		Scanner sc = new Scanner(System.in);
		System.out.print("* MENU *");
		System.out.println();
		System.out.println("1 - ajouter un entier");
		System.out.println("2 - ajouter une cha�ne de caract�res");
		System.out.println("3 - afficher les entiers saisis");
		System.out.println("4 - afficher les chaines de caract�res saisies");
		System.out.println("5 - arr�ter le programme");
		System.out.println();
		System.out.print("-> ");
		int m = sc.nextInt();

		if (m==1) {
			System.out.print("1 - valeur de l'entier : ");
			int val_int = sc.nextInt();
			Entier.ajout_int(val_int);
			System.out.println();

		} else if (m==2) {
			System.out.print("2 - valeur de la cha�ne : ");
			String val_str = sc.next();
			Chaine.ajout_str(val_str);
			System.out.println();

		} else if (m==3) {
			System.out.print("3 - affichage des entiers : ");
			Entier.afficher_int();
			System.out.println();

		} else if (m==4) {
			System.out.print("4 - affichage des cha�nes : ");
			Chaine.afficher_str();
			System.out.println();

		} else if (m==5) {
			System.out.print("Arr�t du programme");	
			return true; // Sortie de la boucle while du main

		} else if (m>5) {
			System.out.print("=> Saisir un nombre entre 1 et 5");
			System.out.println();
			System.out.println();
		}
		return false;
	}
}


