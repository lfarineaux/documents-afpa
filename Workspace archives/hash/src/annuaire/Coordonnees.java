package annuaire;

public class Coordonnees {

	private  String tel;
	private String adr;

	public Coordonnees(String t, String a) {
		this.tel = t;
		this.adr = a;
	}

	public  String getTel() {
		return tel;
	}

//	public void setTel(String tel) {
//		this.tel = tel;
//	}

	public  String getAdr() {
		return adr;
	}

//	public void setAdr(String adr) {
//		this.adr = adr;
//	}

	public String toString() {
		return " [T�l : " + tel + ", adresse : " + adr + "]";
	}

}
