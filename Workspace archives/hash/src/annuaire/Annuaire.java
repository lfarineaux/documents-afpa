package annuaire;

import java.util.HashMap;
import java.util.Iterator;

public class Annuaire {

	static HashMap<String, Coordonnees> table = new HashMap<>();

	public static void ajout (String n, Coordonnees c)  {
		table.put(n,c);
	}

	public static void affichCoord (String n) {

		System.out.println(n+" "+table.get(n));
		System.out.println();
	}

	public static void modifCoord (String n, Coordonnees c) {
		System.out.println("Modifier Coordonn�es de : "+n);
		table.replace(n,c);
	} 

	public static void suppression (String n) {
		System.out.println("Supprimer : "+n);
		table.remove(n);
	}

	public static void listerNoms () {
		System.out.println("Liste des noms");

		Iterator<String> iterat = table.keySet().iterator();
		while(iterat.hasNext()) { 
			String str = iterat.next();
			System.out.println(str);
		}
		System.out.println();
	}
	public static void listerTel () {
		System.out.println("Liste des N� T�l�phones");
	
		Iterator<String> iterat = table.keySet().iterator();
		while(iterat.hasNext()) { 
			String str = iterat.next();
			System.out.println(str+" "+table.get(str).getTel());
		}
		System.out.println();
	}

	public static void listerAdr () {
		System.out.println("Liste des N� adresses");		
		
		Iterator<String> iterat = table.keySet().iterator();
		while(iterat.hasNext()) { 
			String str = iterat.next();
			System.out.println(str+" "+table.get(str).getAdr());
		}
		System.out.println();
	}

	public static void affichAnnuaire () {
		System.out.println("Affichage de l'annuaire :");		
		
		Iterator<String> iterat = table.keySet().iterator();
		while(iterat.hasNext()) { 
			String str = iterat.next();
			System.out.println(str+" "+table.get(str));
		}
		
		System.out.println();
	}

}