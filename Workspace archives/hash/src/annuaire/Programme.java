package annuaire;

public class Programme {

	public static void main(String[] args) {

		Coordonnees a = new Coordonnees ("03.27","rue");
		Coordonnees b = new Coordonnees ("02.10","boulevard");
		Coordonnees c = new Coordonnees ("01.08","avenue");

		Annuaire.ajout("Laurent", a);
		Annuaire.ajout("Linda", b);
		Annuaire.ajout("Anthony", c);

		Annuaire.affichCoord("Laurent");
		Annuaire.affichCoord("Linda"); 
		Annuaire.affichCoord("Anthony");

		Coordonnees d = new Coordonnees ("06.31","boulevard");

		Annuaire.modifCoord ("Linda",d);
		Annuaire.affichCoord("Linda");

		Annuaire.listerNoms ();

		Annuaire.suppression("Anthony");

		Annuaire.listerNoms ();

		Annuaire.listerTel ();

		Annuaire.listerAdr ();
 
		Annuaire.affichAnnuaire ();

	}

}
