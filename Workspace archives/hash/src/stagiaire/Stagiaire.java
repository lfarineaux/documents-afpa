package stagiaire;

public class Stagiaire {

	private final int id;
    private final int jourNaissance;
    private final String nom;
    public Stagiaire(int i,int j,String n) {
        this.nom=n;
        this.id=i;
        this.jourNaissance = j;
    }
    public int hashCode() {
        return this.jourNaissance;
    } 
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Stagiaire other = (Stagiaire) obj;
        if (id != other.id)
            return false;
        if (jourNaissance != other.jourNaissance)
            return false;
        return true;
    }
    public String toString() {
        return "Stagiaire [id=" + id + ", jourNaissance=" + jourNaissance + ", nom=" + nom + "]";
    }

}
