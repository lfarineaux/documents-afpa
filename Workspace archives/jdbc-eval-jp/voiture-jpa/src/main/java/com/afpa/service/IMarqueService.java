package com.afpa.service;

import com.afpa.dto.reponse.ReponseDto;

public interface IMarqueService {
	public ReponseDto creerMarque(String marque);
	
	public ReponseDto chercherMarqueParCode(int code);

	ReponseDto chercherMarqueParLabel(String marque);

	public ReponseDto recupererToutesLesMarques();
}
