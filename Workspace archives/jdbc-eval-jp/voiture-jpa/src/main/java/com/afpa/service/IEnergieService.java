package com.afpa.service;

import com.afpa.dto.reponse.ReponseDto;

public interface IEnergieService {

	ReponseDto creerEnergie(String energie);

	ReponseDto chercherEnergieParCode(int code);

	ReponseDto chercherEnergieParLabel(String energie);

	ReponseDto recupererToutesLesEnergies();

}
