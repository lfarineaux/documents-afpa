package fr.catteau.examen;

import lombok.Getter;
import lombok.ToString;

@ToString
public class Triangle {
	@Getter
	private String nom;
	@Getter
	private Point p1;
	@Getter
	private Point p2;
	@Getter
	private Point p3;

	/**
	 * 
	 * @param nom
	 * @param p1
	 * @param p2
	 * @param p3
	 */
	public Triangle(String nom, Point p1, Point p2, Point p3) {
		super();
		this.nom = nom;
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}

	public double calculSurface() {
		return this.getP1().distanceEntreDeuxPoints(getP2())*this.getP2().distanceEntreDeuxPoints(getP3());
	}
}
