package fr.catteau.examen;

import java.io.InputStream;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Program {
	public static void main(String[] args) throws InvalideParametreException {
		exec(System.in);
	}

	public static void exec(InputStream in) throws InvalideParametreException {
		String choix = "";
		boolean prog = true;
		String nom;
		double abscisse;
		double ordonnee;
		int choixlist;
		int choixlist2;
		int choixlist3;
		Scanner sc = new Scanner(in);
		while (prog) {
			System.out.println("1 - Créer un point.");
			System.out.println("2 - Créer un cercle.");
			System.out.println("3 - Créer un triangle.");
			System.out.println("4 - Lister les points.");
			System.out.println("5 - Lister les cercles.");
			System.out.println("6 - Lister les triangles.");
			System.out.println("7 - Quitter");
			choix = sc.nextLine();
			switch (choix) {
			case "1":
				try {
					System.out.println("Insérez le nom du point (1 lettre et 1 chiffre)");
					nom = sc.nextLine();
					while (nom.length() < 0 || nom.length() > 2 || !(Character.isAlphabetic(nom.charAt(0)))) {
						System.out.println("Erreur : nom incorrect.\nInsérez le nom du point (1 lettre et 1 chiffre)");
						nom = sc.nextLine();
					}
					System.out.println("Insérez l'abscisse du point");
					abscisse = sc.nextDouble();
					sc.nextLine();
					System.out.println("Insérez l'ordonnee du point");
					ordonnee = sc.nextDouble();
					sc.nextLine();

					SauvegardePoint.ajoutPoint(nom, abscisse, ordonnee);
				} catch (InputMismatchException e) {
				}
				break;
			case "2":
				try {
					if (SauvegardePoint.getListPoint().isEmpty()) {
						System.out.println("Il n'y as pas asser de points pour créer un cercle");
						break;
					}
					Cercle cercle;
					System.out.println("Insérez le nom du cercle");
					nom = sc.next();
					System.out.println("Choisissez le point (1,2,3,etc)");
					SauvegardePoint.listerPoint();
					choixlist = sc.nextInt();
					if (choixlist != 0) {
						choixlist = choixlist - 1;
					}

					SauvegardeCercle.ajoutCercle(nom, SauvegardePoint.getListPoint().get(choixlist));
				} catch (InputMismatchException e) {
				}
				break;
			case "3":
				try {
					if (SauvegardePoint.getListPoint().size() < 2) {
						System.out.println("Il n'y as pas asser de points pour créer un triangle");
						break;
					}
					System.out.println("Insérez le nom du triangle");
					nom = sc.next();
					System.out.println("Choisissez le 1er point (1,2,3,etc)");
					SauvegardePoint.listerPoint();
					choixlist = sc.nextInt();
					if (choixlist != 0) {
						choixlist = choixlist - 1;
					}
					System.out.println("Choisissez le 2e point (1,2,3,etc)");
					SauvegardePoint.listerPoint();
					choixlist2 = sc.nextInt();
					if (choixlist2 != 0) {
						choixlist2 = choixlist2 - 1;
					}
					while (choixlist2 == choixlist) {
						System.out.println("Erreur : Point identique au 1er.\nChoisissez le 2e point (1,2,3,etc)");
						SauvegardePoint.listerPoint();
						choixlist2 = sc.nextInt();
					}
					System.out.println("Choisissez le 3e point (1,2,3,etc)");
					SauvegardePoint.listerPoint();
					choixlist3 = sc.nextInt();
					if (choixlist3 != 0) {
						choixlist3 = choixlist3 - 1;
					}
					while (choixlist3 == choixlist || choixlist3 == choixlist2) {
						System.out
								.println("Erreur : Point identique au 1er/2e.\nChoisissez le 1er/2e point (1,2,3,etc)");
						SauvegardePoint.listerPoint();
						choixlist3 = sc.nextInt();
					}

					SauvegardeTriangle.ajoutTriangle(nom, SauvegardePoint.getListPoint().get(choixlist),
							SauvegardePoint.getListPoint().get(choixlist2),
							SauvegardePoint.getListPoint().get(choixlist3));
				} catch (InputMismatchException e) {
				}
				break;
			case "4":
				if (SauvegardePoint.getListPoint().isEmpty()) {
					System.out.println("La liste de point est vide.");
				} else {
					SauvegardePoint.listerPoint();
				}
				break;
			case "5":
				if (SauvegardeCercle.getListCercle().isEmpty()) {
					System.out.println("La liste de Cercle est vide.");
				} else {
					SauvegardeCercle.listerCercle();
				}
				break;
			case "6":
				if (SauvegardeTriangle.getListTriangle().isEmpty()) {
					System.out.println("La liste de Triangle est vide.");
				} else {
					SauvegardeTriangle.listTriangle();
				}
				break;
			case "7":
				prog = false;
				System.out.println("Au revoir.");
				break;
			default:
				System.out.println("Saisie invalide");
				break;
			}
		}
	}
}
