package fr.catteau.examen;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;

public abstract class SauvegardePoint {
	private static Logger monLogger = LoggerFactory.getLogger(SauvegardePoint.class);
	@Getter
	private static List<Point> listPoint = new ArrayList<Point>();

	/**
	 * 
	 * @param Un point
	 * @throws InvalideParametreException
	 */
	public static void ajoutPoint(String n,double abscisse, double ordonnee) throws InvalideParametreException {
		Point p = new Point(n, abscisse, ordonnee);
		monLogger.info("Point créer.");
		boolean nom = false;
		boolean position = false;

		for (Point point : listPoint) {
			if (point.getAbscisse() == p.getAbscisse() 
			&& point.getOrdonnee() == p.getOrdonnee()
			&& point.getNom() == p.getNom()) {
				position = true;
				nom = true;
				break;
			} else if (point.getNom() == p.getNom()) {
				nom = true;
				break;
			} else if (point.getAbscisse() == p.getAbscisse() 
					&& point.getOrdonnee() == p.getOrdonnee()) {
				position = true;
				break;
			}
		}

		if (nom && position) {
			System.out.println("Erreur : Impossible le nom, l'abscisse et l'ordonnée sont déjà utilisée.");
			throw new InvalideParametreException();
		} else if (nom) {
			System.out.println("Erreur : Impossible le nom du point existe déjà.");
			throw new InvalideParametreException();
		} else if (position) {
			System.out.println("Erreur : Impossible l'abscisse et l'ordonnee du point sont déjà utilisée");
			throw new InvalideParametreException();
		} else {
			listPoint.add(p);
			monLogger.debug("Point ajouter à la list.");
			System.out.println("Le point est désormais crée");
		}
	}
	
	public static void listerPoint() {
		for (Point point : listPoint) {
			System.out.println(point);
		}
	}
}
