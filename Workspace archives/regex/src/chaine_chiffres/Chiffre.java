package chaine_chiffres;

import java.util.Scanner;
import java.util.regex.*; 

public class Chiffre {

	private static Pattern motif;
	private static Matcher text;

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Saisir une cha�ne de caract�res : ");
		String chaine = sc.next();
		sc.nextLine();

		// 1�re m�thode (recherche par caract�re)
		motif = Pattern.compile("^\\d*$");
		text = motif.matcher(chaine);
		while (text.find()) {
			System.out.print("M�thode 1 : ");
			System.out.println(chaine);
		}  

		//2�me m�thode (recherche de cha�nes)
		if (Pattern.matches("\\d*", chaine)) {
			System.out.print("M�thode 2 : ");
			System.out.println(chaine);
		}

		sc.close();
	}
}











