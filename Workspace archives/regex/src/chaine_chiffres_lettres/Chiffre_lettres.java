package chaine_chiffres_lettres;

import java.util.Scanner;
import java.util.regex.*; 

public class Chiffre_lettres {

	private static Pattern motif;
	private static Matcher text;

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Saisir une cha�ne de caract�res : ");
		String chaine = sc.next();
		sc.nextLine();

		// 1�re m�thode (recherche une sous-cha�ne)
		motif = Pattern.compile("^[a-zA-Z]+[0-9]+[a-zA-Z]+$");
		text = motif.matcher(chaine);
		while (text.find()) {
			System.out.print("M�thode 1 : ");
			System.out.println(chaine);
		}  

		//2�me m�thode (recherche toute la cha�ne)
		if (Pattern.matches("\\D+\\d+\\D+", chaine)) {
			System.out.print("M�thode 2 : ");
			System.out.println(chaine);
		}

		sc.close();
	}
}











