package entiers_naturels;

public class EntNat {

	private int ent ;

	public EntNat (int a) throws ErrConst {
		if (a<0) {
			ErrConst e = new ErrConst (a, "Le nombre doit �tre un entier positif ou nul.");
			throw e;
		}
		this.ent=a;
	}

	public int getN() {
		return this.ent;
	}

//	@Override
//	public String toString() {
//		return "EntNat [ent=" + ent + "]";
//	}


}
