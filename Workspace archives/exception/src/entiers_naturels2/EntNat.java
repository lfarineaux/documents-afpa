package entiers_naturels2;


public class EntNat {

	private int ent;

	public EntNat (int a) throws ErrConst {
		if (a<0) {
			ErrConst e = new ErrConst (a);
			throw e;
		}
		this.ent=a;
	}

	public int getN() {
		return ent;
	}

	public static EntNat somme (EntNat a, EntNat b) throws ErrSom, ErrConst{
		int c=a.ent;
		int d=b.ent;
		int s=c+d;

		if (s>Integer.MAX_VALUE || s<0) {
			ErrSom som = new ErrSom (s);
			throw som;
		}
		EntNat res = new EntNat(s);
		return res;
	}

	public static EntNat difference (EntNat a, EntNat b) throws ErrDiff, ErrConst {
		int c=a.ent;
		int d=b.ent;
		int s=c-d;

		if (s<0) {
			ErrDiff dif = new ErrDiff (s);
			throw dif;
		}
		EntNat res = new EntNat(s);
		return res;
	}

	public static EntNat produit (EntNat a, EntNat b) throws ErrProd, ErrConst {
		int c=a.ent;
		int d=b.ent;
		int s=c*d;

		if (s>Integer.MAX_VALUE || s<0 ) {
			ErrProd prod = new ErrProd (s);
			throw prod;
		}
		EntNat res = new EntNat(s);
		return res;
	}
}

