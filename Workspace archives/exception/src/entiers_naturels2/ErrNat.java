package entiers_naturels2;

public class ErrNat extends Exception {

	private int en;
	
	public ErrNat (String message) {
		super(message);
	}
	public int getN() {
		return en;
	}
	
}
