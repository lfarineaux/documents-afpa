package entiers_naturels2;

public class ErrConst extends ErrNat {

	private int en;
	
	public ErrConst (int a) {
	super ("-> Le nombre doit �tre un entier positif ou nul.");
	this.en=a;
	}

	public int getN() {
		return en;
	}
	
}
