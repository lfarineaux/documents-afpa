package entiers_naturels2;


public class Program {

	public static void main(String[] args) throws ErrConst {
//		int a=Integer.MAX_VALUE;
//		int a=-1;
		int a=(Integer.MAX_VALUE/2)-2;
		int b=(Integer.MAX_VALUE/2)-2;
		EntNat ena=new EntNat(a);
		EntNat enb=new EntNat(b);

		// Exemple 1
		System.out.println("EXEMPLE 1");
		System.out.println("---------");
		try {
			ena = new EntNat(a);
			enb = new EntNat(b);
			System.out.println("Nombre a : "+ena.getN()+" - Nombre b : "+enb.getN());
			System.out.println();
			EntNat s=EntNat.somme(ena,enb);
			System.out.println("Somme = "+s.getN());
			System.out.println();
			EntNat d=EntNat.difference(ena,enb);
			System.out.println("Difference = "+d.getN());
			System.out.println();
			EntNat p=EntNat.produit(ena, enb);
			System.out.println("Produit = "+p.getN());
			System.out.println();
		}
		catch (ErrNat er) {
			System.out.println(er.getMessage());
			System.out.println();
		}

		// Exemple 2
		System.out.println("EXEMPLE 2");
		System.out.println("---------");
		try {
			ena = new EntNat(a);
			enb = new EntNat(b);
			System.out.println("Nombre a : "+ena.getN()+" - Nombre b : "+enb.getN());
			System.out.println();
			EntNat s=EntNat.somme(ena,enb);
			System.out.println("Somme = "+s.getN());
			System.out.println();
			EntNat d=EntNat.difference(ena,enb);
			System.out.println("Difference = "+d.getN());
			System.out.println();
			EntNat p=EntNat.produit(ena, enb);
			System.out.println("Produit = "+p.getN());
			System.out.println();
		}
		catch (ErrConst | ErrSom | ErrDiff | ErrProd er) {
			System.out.println("-> Erreur avec la valeur de l'entier : "+er.getN());
			System.out.println(er.getMessage());
			System.out.println("-> Classe "+er.getStackTrace()[0].getClassName()+" | Ligne "+er.getStackTrace()[0].getLineNumber());
			System.out.println();
			}
	}
}

