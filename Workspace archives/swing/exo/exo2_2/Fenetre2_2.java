package exo2_2;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class Fenetre2_2 extends JFrame{
	public Fenetre2_2(int posX, int posY, int hX, int hY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, hX, hY);
		this.setTitle(titre);
		this.setVisible(true);
	}

	public static void main(String[] args)
	{
		JFrame fenetre1= new JFrame();
		JFrame fenetre2= new JFrame();
		JFrame fenetre3= new JFrame();

		fenetre1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre1.setBounds(20,20,500,600);
		fenetre1.setTitle("Fen�tre Une");
		fenetre1.setVisible(true);
		fenetre1.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("appui sur la fen�tre 1 en x= "+e.getX()+" , y = "+e.getY());
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("rel�chement sur la fen�tre 1 en x= "+e.getX()+" , y = "+e.getY());
			}
		}); 
		
		fenetre2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre2.setBounds(600,20,500,600);
		fenetre2.setTitle("Fen�tre Deux");
		fenetre2.setVisible(true);
		fenetre2.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("appui sur la fen�tre 2 en x= "+e.getX()+" , y = "+e.getY());
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("rel�chement sur la fen�tre 2 en x= "+e.getX()+" , y = "+e.getY());
			}
		}); 
		
		fenetre3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre3.setBounds(1200,20,500,600);
		fenetre3.setTitle("Fen�tre Trois");
		fenetre3.setVisible(true);
		fenetre3.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("appui sur la fen�tre 3 en x= "+e.getX()+" , y = "+e.getY());
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("rel�chement sur la fen�tre 3 en x= "+e.getX()+" , y = "+e.getY());
			}
		}); 
	}
}