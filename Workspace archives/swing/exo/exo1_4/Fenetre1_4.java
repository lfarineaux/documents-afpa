package exo1_4;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class Fenetre1_4 extends JFrame{
	static int x, y;
	public Fenetre1_4(int posX, int posY, int hX, int hY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, hX, hY);
		this.setTitle(titre);
		this.setVisible(true);
	}

	public static void main(String[] args)
	{
		JFrame fenetre1 = new JFrame();
		JFrame fenetre2 = new JFrame();

		fenetre1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre1.setBounds(20,20,300,600);
		fenetre1.setTitle("Fen�tre");
		fenetre1.setVisible(true);
		fenetre1.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				x = e.getX();
				y = e.getY();
				System.out.println("appui en x= "+x+" , y = "+y);
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				x = e.getX();
				y = e.getY();
				System.out.println("rel�chement en x= "+x+" , y = "+y);
			}
		}); 
		
	}
}