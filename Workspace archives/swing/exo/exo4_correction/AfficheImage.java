package exo4_correction;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Random;

import javax.swing.JPanel;

public class AfficheImage extends JPanel {
    Image eau;
    AfficheImage(String s) {
        eau = getToolkit().getImage(s);
    }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        // redimensionne de mani�re aleatoire les coordonnees de l'image
        Random r = new Random(); 
        g.drawImage(eau, r.nextInt(150), r.nextInt(100), 50, 50, this);
    }
}