package exo3;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Fenetre3 extends JFrame{

	public Fenetre3(int posX, int posY, int hX, int hY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, hX, hY);
		this.setTitle(titre);
		this.setVisible(true);

	}
	
	public static void main(String[] args) throws IOException
	{
		JFrame fenetre = new JFrame();
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setBounds(30,30,800,800);
		fenetre.setTitle("Fen�tre image");
		
		BufferedImage photo = ImageIO.read(new File("monde.jpeg"));
		ImageIcon icone = new ImageIcon(photo);
        JLabel image = new JLabel(icone);
        fenetre.add(image);
      
        fenetre.setVisible(true);
        
	}
}