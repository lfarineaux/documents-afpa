package exo4;

import java.awt.Color;

import javax.swing.JFrame;

public class Program {
	
	public static void main(String[] args) throws Exception {
		JFrame fenetre = new JFrame();
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setBounds(30,30,500,500);
		ImagePanel imagePanel = new ImagePanel();
		imagePanel.setBackground(Color.RED);
		fenetre.add(imagePanel);
		fenetre.setVisible(true);
		
		int i=0;
		while(i<5)	{
			imagePanel.repaint();
			Thread.sleep(1000);
			i++;
		}
	}
}
