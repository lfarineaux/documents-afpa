package exo5_3;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {
	private BufferedImage image;

	public ImagePanel() throws IOException {
		image = ImageIO.read(new File("monde.jpeg"));
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawImage(image, Ecouteur5_3.x, Ecouteur5_3.y,100,100, null);
		
	}
}
