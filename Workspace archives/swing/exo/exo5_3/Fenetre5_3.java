package exo5_3;

import java.awt.Color;

import javax.swing.JFrame;

public class Fenetre5_3 extends JFrame {
	
	public static void main(String[] args) throws Exception {
		
		Fenetre5_3 fenetre = new Fenetre5_3();
		Ecouteur5_3 ecouteur = new Ecouteur5_3();
		
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setBounds(30,30,500,500);
	
		ImagePanel imagePanel = new ImagePanel();
		imagePanel.setBackground(Color.lightGray);
		fenetre.add(imagePanel);
		
		fenetre.setVisible(true);

		fenetre.addKeyListener(ecouteur);
		
		while(Ecouteur5_3.x>0 && Ecouteur5_3.x<(fenetre.getWidth()) && Ecouteur5_3.y>0 && Ecouteur5_3.y<fenetre.getHeight()) {
			imagePanel.repaint();
			
//			Thread.sleep(1000);
		}
		
		System.exit(0);
		
	}

}
