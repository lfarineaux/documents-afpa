package exo1_2;

import javax.swing.JFrame;

public class Fenetre1_2 extends JFrame{

	public Fenetre1_2(int posX, int posY, int hX, int hY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, hX, hY);
		this.setTitle(titre);
		this.setVisible(true);

	}
	
	
	public static void main(String[] args)
	{
		Ecouteur1_2 ecouteur = new Ecouteur1_2();
		JFrame fenetre = new JFrame();

		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setBounds(80,80,600,600);
		fenetre.setTitle("Fen�tre principale");
		fenetre.setVisible(true);
		fenetre.addMouseListener(ecouteur);

	}
}