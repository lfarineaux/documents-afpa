package exo2_1;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

public class Fenetre2_1 extends JFrame implements MouseListener {
	int num;
	//private static int cpt;


	public Fenetre2_1(int posX, int posY, int hX, int hY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, hX, hY);
		this.setTitle(titre);
		this.setVisible(true);
	}

	public void setNum(int num) {
		this.num = num;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("appui dans la fen�tre "+this.num+" en x= "+e.getX()+" , y = "+e.getY());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("rel�chement dans la fen�tre "+this.num+" en x= "+e.getX()+" , y = "+e.getY());
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	public static void main(String[] args){
		Fenetre2_1 fenetre_ecouteur1 = new Fenetre2_1(20,20,300,600,"Fen�tre 1");
		fenetre_ecouteur1.setNum(1);
		Fenetre2_1 fenetre_ecouteur2 = new Fenetre2_1(350,20,300,600,"Fen�tre 2");
		fenetre_ecouteur2.setNum(2);
		Fenetre2_1 fenetre_ecouteur3 = new Fenetre2_1(670,20,300,600,"Fen�tre 3");
		fenetre_ecouteur3.setNum(3);

		fenetre_ecouteur1.addMouseListener(fenetre_ecouteur1);
		fenetre_ecouteur2.addMouseListener(fenetre_ecouteur2);
		fenetre_ecouteur3.addMouseListener(fenetre_ecouteur3);
	}

}