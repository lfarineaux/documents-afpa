package exo3Bis;

import java.awt.Color;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JFrame;

public class Fenetre3Bis extends JFrame {
	
	public static void main(String[] args) throws Exception {
		Fenetre3Bis fenetre = new Fenetre3Bis();
		fenetre.setSize(300, 300);
		fenetre.setBackground(Color.GREEN);
		
//		fenetre.setLayout(new GridLayout(2,2));
		
		ImagePanel imagePanel = new ImagePanel();
		imagePanel.setBackground(Color.RED);
		
//		ImagePanel imagePanel2 = new ImagePanel();
//		imagePanel2.setBackground(Color.blue);
//
//		ImagePanel imagePanel3 = new ImagePanel();
//		imagePanel3.setBackground(Color.CYAN);

		fenetre.add(imagePanel);
//		fenetre.add(imagePanel2);
//		fenetre.add(imagePanel3);
		
		fenetre.setVisible(true);
	}

}
