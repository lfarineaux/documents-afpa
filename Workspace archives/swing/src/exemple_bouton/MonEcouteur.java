package exemple_bouton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
public class MonEcouteur implements ActionListener
{
	private JButton r�f1;
	private JButton r�f2;
	public MonEcouteur(JButton b1,JButton b2)
	{
		this.r�f1 = b1;
		this.r�f2 = b2;
	}
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == this.r�f1)
			System.out.println("Clic sur bouton 1");
		else if (e.getSource() == this.r�f2)
			System.out.println("Clic sur bouton 2");
	}
}