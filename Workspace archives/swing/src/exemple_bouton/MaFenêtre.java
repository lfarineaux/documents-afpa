package exemple_bouton;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MaFen�tre extends JFrame
{
	private JButton bouton1;
	private JButton bouton2;
	public MaFen�tre()
	{ 
		this.bouton1 = new JButton();
		this.bouton1.setText("BOUTON 1");
		this.bouton1.setBounds(10,10,200,30);
		this.bouton2 = new JButton();
		this.bouton2.setText("BOUTON 2");
		this.bouton2.setBounds(10,50,200,30);
		MonEcouteur �couteur = new MonEcouteur(this.bouton1, this.bouton2);
		this.bouton1.addActionListener(�couteur);
		this.bouton2.addActionListener(�couteur);
		getContentPane().setLayout(null);
		
		getContentPane().add(this.bouton1);
		getContentPane().add(this.bouton2);
	}
}