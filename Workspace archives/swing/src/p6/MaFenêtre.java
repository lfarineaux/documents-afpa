package p6;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;


public class MaFen�tre extends JFrame {
	private JButton bDessine;
	private JButton bEfface;
	private MonPanneau panneau;

	public MaFen�tre() {
		
		this.setSize(300,300);
		this.setLocation(200, 200);
		this.bDessine = new JButton("Dessine");
		this.bDessine.setActionCommand("Dessine");
		this.bEfface = new JButton("Efface");
		this.bEfface.setActionCommand("Efface");
		this.panneau = new MonPanneau();
		//this.panneau.repaint();
		MonEcouteur �couteur = new MonEcouteur(panneau);
		this.bDessine.addActionListener(�couteur);
		this.bEfface.addActionListener(�couteur);
		getContentPane().add(this.bDessine, BorderLayout.NORTH);
		getContentPane().add(this.bEfface, BorderLayout.SOUTH);
		getContentPane().add(this.panneau, BorderLayout.CENTER);
		this.setVisible(true);
	}
}