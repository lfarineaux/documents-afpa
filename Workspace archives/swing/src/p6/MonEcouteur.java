package p6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MonEcouteur implements ActionListener {
	private MonPanneau lePanneau;

	public MonEcouteur(MonPanneau r�f) {
		this.lePanneau = r�f;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "Dessine")
			this.lePanneau.dessin = true;
		else if (e.getActionCommand() == "Efface")
			this.lePanneau.dessin = false;
		this.lePanneau.repaint();
	}
}
