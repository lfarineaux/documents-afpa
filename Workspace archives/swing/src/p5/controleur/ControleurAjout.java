package p5.controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import p5.modele.Compte;

public class ControleurAjout implements ActionListener {

	private JLabel soldeLabel;

	public ControleurAjout(JLabel s){
		this.soldeLabel = s;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Compte.solde++;
		this.soldeLabel.setText(Integer.toString(Compte.solde));
	}

}
