package exemple_layout;

import javax.swing.*;
import java.awt.*;
public class MaFen�tre extends JFrame
{
	private JButton bPlus;
	private JButton bMoins;
	private JButton bRaZ;
	private JLabel �tiq;
	private JPanel pBoutons;
	public MaFen�tre()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(10,20,400,300);
		setTitle("Fen�tre principale");
		getContentPane().setLayout(new BorderLayout(10,10));
		this.bPlus = new JButton("Plus");
		this.bMoins = new JButton("Moins");
		this.bRaZ = new JButton("Reset");
		this.pBoutons = new JPanel(new GridLayout(3,1,10,10));
		this.pBoutons.add(this.bPlus);
		this.pBoutons.add(this.bMoins);
		this.pBoutons.add(this.bRaZ);
		this.getContentPane().add(this.pBoutons,BorderLayout.WEST);
		this.�tiq = new JLabel("Texte",SwingConstants.CENTER);
		getContentPane().add(this.�tiq,BorderLayout.CENTER);
	}
}