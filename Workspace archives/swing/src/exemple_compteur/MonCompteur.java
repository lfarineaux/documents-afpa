package exemple_compteur;

public class MonCompteur {
	private int valMax = 10;
	private int valInit;
	private int valCou;
	private ObservateurDeCompteur obs ;
	public MonCompteur(int init)
	{
	this.valInit = init;
	this.valCou = init;
	}
	public void inc() {
	this.valCou++;
	if (this.valCou > this.valMax) this.valCou = 0;
	obs.nouvelleValeur(this.valCou);
	}
	public void dec() {
	if (this.valCou > 0) this.valCou--;
	obs.nouvelleValeur(this.valCou);
	}
	public void raz() {
	this.valCou = this.valInit;
	obs.nouvelleValeur(this.valCou);
	}
	public int getValCou() { return this.valCou; }
	public void enregistrerANotifier(ObservateurDeCompteur r�f) {
	this.obs = r�f;
	}
	}