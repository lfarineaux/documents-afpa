package exemple_compteur;

public interface ObservateurDeCompteur {
	public void nouvelleValeur(int val);
}
