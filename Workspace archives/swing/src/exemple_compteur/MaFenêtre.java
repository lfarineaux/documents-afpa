package exemple_compteur;

import javax.swing.*;
import java.awt.*;
public class MaFen�tre extends JFrame implements ObservateurDeCompteur
{
	private JButton bPlus;
	private JButton bMoins;
	private JButton bRaZ;
	private JLabel �tiq;
	private JPanel pBoutons;
	
	public MaFen�tre(int val, MonEcouteur r�f)
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(10,20,400,300);
		setTitle("Fen�tre principale");
		getContentPane().setLayout(new BorderLayout(10,10));
		this.bPlus = new JButton("Plus");
		bPlus.setActionCommand("Plus");
	
		this.bMoins = new JButton("Moins");
		bMoins.setActionCommand("Moins");
		this.bRaZ = new JButton("Reset");
		bRaZ.setActionCommand("Reset");
		this.pBoutons = new JPanel(new GridLayout(3,1,10,10));
		this.pBoutons.add(this.bPlus);
		this.pBoutons.add(this.bMoins);
		this.pBoutons.add(this.bRaZ);
		this.getContentPane().add(this.pBoutons,BorderLayout.WEST);
		
		�tiq = new JLabel(Integer.toString(val),SwingConstants.CENTER);
		bPlus.addActionListener(r�f);
		bMoins.addActionListener(r�f);
		bRaZ.addActionListener(r�f);
		
		getContentPane().add(pBoutons,BorderLayout.WEST);
		getContentPane().add(�tiq,BorderLayout.CENTER);
	}
	public void nouvelleValeur(int val)
	{
		�tiq.setText(Integer.toString(val));
	}
}
