package cda.syntaxe;

import java.util.Scanner;

public class Inferieurs_do_while
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);

		System.out.println("saisir un nombre entier positif : ");
		int nombre = sc.nextInt();

		int i=0;
		
		do
		{
			System.out.println(i);
			i++;
		}
		while (i<=nombre-1);

		sc.close();
	}
}
