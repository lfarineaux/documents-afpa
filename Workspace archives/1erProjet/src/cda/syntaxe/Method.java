package cda.syntaxe;

import java.util.Scanner;

public class Method {

	public static void main(String[] args) {

		//		MAX
		Scanner nn = new Scanner(System.in);

		System.out.println("Max retourne le max de 3 entiers");
		System.out.println("Saisir le premier nombre entier :");
		int max1 = nn.nextInt();

		System.out.println("Saisir le deuxi�me nombre entier :");
		int max2 = nn.nextInt();

		System.out.println("Saisir le trois�me nombre entier :");
		int max3 = nn.nextInt();

		int maxi = Method.max(max1,max2,max3);

		System.out.println("Le max est : "+maxi);
		System.out.println();

		//		SOMME
		System.out.println("Somme retourne la somme de 2 r�els");
		System.out.println("Saisir le premier nombre r�el :");
		float som1 = nn.nextFloat();

		System.out.println("Saisir le deuxi�me nombre r�el :");
		float som2 = nn.nextFloat();

		float tot = Method.som(som1,som2);

		System.out.println("Le somme est : "+tot);
		System.out.println();


		//		CONJONCTION
		System.out.println("Conjonction retourne la conjonction de 2 bool�ens");
		System.out.println("Saisir le premier bool�en :");
		boolean b1 = nn.nextBoolean();

		System.out.println("Saisir le deuxi�me bool�en :");
		boolean b2 = nn.nextBoolean();

		boolean bo = Method.con(b1,b2);

		System.out.println("La conjonction est : "+bo);
		System.out.println();


		//		MOITIE
		System.out.println("Moiti� retourne la moiti� d'un entier");
		System.out.println("Saisir le nombre entier :");
		int mo = nn.nextInt();


		int demi = Method.moitie(mo);

		System.out.println("La moyenne est : "+demi);
		System.out.println();


		//		SIGNE
		System.out.println("Signe retourne le signe d'un entier");
		System.out.println("Saisir un nombre entier : ");
		int s = nn.nextInt();

		char sig = Method.signe(s);

		System.out.println("Le signe est : "+sig);
		System.out.println();

		nn.close();

	}

	public static int max(int a, int b, int c)
	{
		int res = 0;		
		if (a>b && a>c) {
			res = a;
		}
		else
			if (b>c && b>a) {
				res = b;
			}
			else
				res = c ;

		return res;
	}    

	public static float som(float d, float e)
	{
		return d+e;
	}    

	public static boolean con(boolean b1, boolean b2)
	{
		if (b1&&b2) {
			return true;
		} else {
			return false;
		}
	}   

	public static int moitie(int m)
	{
		return m/2;
	}   

	public static char signe(int sig)
	{
		if (sig>=0) {
			return '+';
		}
		else {
			return '-';
		}

	}

}
