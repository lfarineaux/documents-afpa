
package cda.syntaxe;

import java.util.Scanner;

public class Signe {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("saisir un nombre entier : ");
		int monEntier = sc.nextInt();

		{
			if (monEntier >= 0) {

				System.out.println("+");
			} else {
				System.out.println("-");
			}

			sc.close();
		}
	}
}