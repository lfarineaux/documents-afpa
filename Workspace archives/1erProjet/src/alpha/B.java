package alpha;

public class B extends A {

	protected String b;

	public B() {
		
		System.out.println("B");
		
		
	}

	@Override
	public String toString() {
		return "B [b=" + b + "]";
	}

	public String getB() {
		return b;
	}

	public void setB(String b) {
		this.b = b;
	}
	
	
	
}
