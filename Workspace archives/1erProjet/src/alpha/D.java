package alpha;

public class D extends C {

	protected String d;

	public D() {
		
		System.out.println("D");
		
		
	}

	@Override
	public String toString() {
		return "D [d=" + d + "]";
	}

	public String getD() {
		return d;
	}

	public void setD(String d) {
		this.d = d;
	}
	
	
	
}
