package alpha;
	
	
public class A {
		
	protected String a;

	
	public A() {
		 System.out.println("=======================\n" + "A");
	}

	
	@Override
	public String toString() {
		return "A [A=" + a + "]";
	}

	public String getA() {
		return a;
	}

	public void setA(String a) {
		a = a;
	}
	
	
}
