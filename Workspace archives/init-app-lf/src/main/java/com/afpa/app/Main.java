package com.afpa.app;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.afpa.api.ITools;


public class Main {

	public static void main(String[] args) {
		
		String conf = "config.xml";
		// create a context and find beans
		try (AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(conf);) {
			ITools tools = ctx.getBean("tools", ITools.class);
			use(tools);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public static void use(ITools tools) {
		String str = "bonjour";
		System.out.println(tools.action(str));
		
	}
	
	
}
