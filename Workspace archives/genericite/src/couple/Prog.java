package couple;

public class Prog {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Couple<String> c1 = new Couple<>("Abc","Cde");
		c1.affiche();
		
		CoupleNomme<String> c2 = new CoupleNomme<>("bgt", "nhy", "Martin");
		c2.affiche();
		
		PointNomme c3 = new PointNomme(11, 22, "Dubois");
		c3.affiche();
		
		PointNomme2 c4 = new PointNomme2(11, 22, "Dubois");
		c4.affiche();
		
		
	}

}
