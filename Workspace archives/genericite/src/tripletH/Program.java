package tripletH;

import java.util.Date;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TripletH <String,Integer,Double> t1 = new TripletH <> ("ab",1,100.0);		
		t1.affiche();

		TripletH <Character,Boolean,Date> t2 = new TripletH<> ('l',true,new Date(999999999999l));
		t2.affiche();

		TripletH <Long,Byte,Float> t3 = new TripletH<> (5000L, Byte.MAX_VALUE,2.5F);
		t3.affiche();
		
		TripletH <Boolean,Boolean,Date> t4 = new TripletH<> (false,true,new Date(999999999999l));
		t4.affiche();
		
		
	}

}
