package point;

public class NomSpecial implements Comparable<NomSpecial> {
	
	private final String nom;

	public NomSpecial(String nom) {
		this.nom = nom;
	}

	@Override
	public int compareTo(NomSpecial o) {
		if(this.nom.startsWith("L")) {
			return 1;
		} else if(o.nom.startsWith("L")) {
			return -1;
		}
		return this.nom.compareTo(o.nom);
	}

	@Override
	public String toString() {
		return "NomSpecial [nom=" + nom + "]";
	}
	
	

}
