package point;

import java.lang.Comparable;

public class Point implements Comparable <Point>{
	private  int x, y ;
	
	public Point (int x, int y) {
		this.x = x ; 
		this.y = y ;
	}
	
	public void affiche(){
		System.out.println ("coordonnees : " + x + " " + y ) ;
	}

	public  double distance () {
		return Math.sqrt(this.x*this.x+this.y*this.y);
	}

	@Override
	public  int compareTo(Point t) {
		if (t.distance() < this.distance()) {
			return 1; 
		} else if (t.distance() > this.distance()) {
			return -1;
		} else {
			return 0;
		}
	}

	
	public static <T extends Comparable<T>> T afficheLePlusGrand(T[] tab) {

		T save = tab[0];

		for (T t : tab) {
			if ((t.compareTo(save)) > 0) {
				save = t;
			}
		}
		return save;

	}

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + "]";
	}
	
	
}

