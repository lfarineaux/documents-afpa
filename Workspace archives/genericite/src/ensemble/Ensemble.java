package ensemble;

import java.util.ArrayList;
import java.util.List;

public class Ensemble <T> {


	final List <T> list;

	public Ensemble() {
		this.list = new ArrayList<>() ;
	}

	public boolean ajouter (T t) {

		if (!this.list.contains(t)) {
			this.list.add(t);
			return true;
		}
		else {
			return false;
		}
	}

	public  boolean enlever (T t) {
		if (this.list.contains(t)) {
			this.list.remove(t);
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Ensemble - list= " + list + " - + cardinal = "+list.size();
	}

}
