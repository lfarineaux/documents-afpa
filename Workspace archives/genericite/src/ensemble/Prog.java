package ensemble;

import java.util.Scanner;

public class Prog {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int menu;
		boolean fin = true;
		String choix = null;

		Ensemble<?> ens = new Ensemble<>();

		boolean creationDejaFaite = false;

		while (fin) {
			System.out.println();
			System.out.println("      **************");
			System.out.println("      *    MENU    *");
			System.out.println("      **************");
			System.out.println();
			System.out.println(" 0. Arr�ter le programme");
			if(!creationDejaFaite) {
				System.out.println(" 1. Cr�er un Ensemble");
			} else {
				System.out.println(" 2. Ajouter un �l�ment");
				System.out.println(" 3. Enlever un �l�ment");
				System.out.println(" 4. Afficher");	
			}
			menu = sc.nextInt();
			System.out.println();

			switch (menu) {
			case 0:	
				System.out.println("0. Arr�t du programme");
				fin=false;
				break;	
			case 1:
				System.out.println(" 1. Cr�er un Ensemble");
				System.out.println(" Quel type voulez cr�er : (E)ntiers / (R)�els / (S)tring :");
				choix = sc.next();
				creationDejaFaite = true;
				break;
			case 2:
				System.out.println(" 2. Ajouter un �l�ment");
				System.out.println(" Saisir l'�l�ment � ajouter : ");
				if (choix.equalsIgnoreCase("E")) {
					System.out.println(((Ensemble<Integer>)ens).ajouter(sc.nextInt()));

				} else if (choix.equalsIgnoreCase("R")) {
					System.out.println(((Ensemble<Double>)ens).ajouter(sc.nextDouble()));

				} else if (choix.equalsIgnoreCase("S")) {
					System.out.println(((Ensemble<String>)ens).ajouter(sc.next()));
				}	
				sc.nextLine();
				break;
			case 3:	
				System.out.println(" 3. Enlever un �l�ment");
				System.out.println(" Saisir l'�l�ment � enlever : ");
				if (choix.equalsIgnoreCase("E")) {
					System.out.println(((Ensemble<Integer>)ens).enlever(sc.nextInt()));

				} else if (choix.equalsIgnoreCase("R")) {
					System.out.println(((Ensemble<Double>)ens).enlever(sc.nextDouble()));

				} else if (choix.equalsIgnoreCase("S")) {
					System.out.println(((Ensemble<String>)ens).enlever(sc.next()));
				}	
				sc.nextLine();
				break;
			case 4:	
				System.out.println(" 4. Afficher");	
				System.out.println(ens);
				break;
			}
		}
		sc.close();
	}
}