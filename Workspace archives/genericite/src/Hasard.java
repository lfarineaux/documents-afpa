import java.util.Random;

public class Hasard {

	public static <T> T alea (T [] a) {
		Random r= new Random();
		int leRandom = r.nextInt(a.length);
		return a[leRandom];
	}

	
	public static <T> T alea2 (T a, T b) {
		Random r= new Random();
		int leRandom = r.nextInt(2);
		
		if (leRandom==1) {
		return a; 
		}
		else return b;
	}
	
}
