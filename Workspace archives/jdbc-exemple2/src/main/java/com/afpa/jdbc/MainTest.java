package com.afpa.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class MainTest {

	private static Statement statement;
	private static Connection connection;
	private static boolean authentifie = false;

	private static void initConnection() throws Exception {
		//		String url = "jdbc:mysql://localhost:3306/jdbc-test?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		//		String user = "user1";
		//		String password = "pwd1";

		//		String url = "jdbc:postgresql://10.115.58.129:5432/jdbs-test";
		String url = "jdbc:postgresql://localhost:5432/jdbc-test";
		String user = "postgres";
		String password = "sql";

		connection = DriverManager.getConnection(url, user, password);
		statement = connection.createStatement();
	}

	private static void ajouterUnePersonne(Scanner sc) throws Exception {
		if(!authentifie) {
			System.out.println("veuillez d'abord vous authentifier !!! ");
			System.out.println("veuillez vous loguer.");
			return;
		}
		System.out.println("ajout personne : ");
		System.out.print(" saisir nom >");
		String nom = sc.nextLine();
		System.out.print(" saisir prenom >");
		String prenom = sc.nextLine();

		String request = "INSERT INTO Personne (nom,prenom)	VALUES ('"+nom+"','"+prenom+"');";
		int nbr = statement.executeUpdate(request);
		if (0 != nbr)
			System.out.println("insertion r�eussie");
	}

	private static void afficherPersonnes() throws Exception {
		if(!authentifie) {
			System.out.println("veuillez d'abord vous authentifier !!! ");
			System.out.println("veuillez vous loguer.");
			return;
		}
		ResultSet result = statement.executeQuery("SELECT * FROM Personne;");
		while (result.next()) {
			// on indique chaque fois le nom de la colonne et le type
			int idPersonne = result.getInt("num");

			String nom = result.getString("nom");

			String prenom = result.getString("prenom");

			// pareil pour tous les autres attributs
			System.out.println(idPersonne + " " + nom + " " + prenom);
		}
		System.out.println();
	}

	public static void main(String[] args) throws Exception {
		try {
			initConnection();

			Scanner sc = new Scanner(System.in);

			int choix = 0;
			boolean continuer = true;

			while (continuer) {
				System.out.println();
				System.out.println("0- arr�ter le programme");
				System.out.println("1- ajouter une personne");
				System.out.println("2- lister les personnes");
				System.out.println("3- login");
				System.out.print("> ");

				choix = sc.nextInt();
				sc.nextLine();

				switch (choix) {
				case 0:
					System.out.println("au revoir !");
					continuer = false;
					break;
				case 1:
					ajouterUnePersonne(sc);
					break;
				case 2:
					afficherPersonnes();
					break;
				case 3:
					login(sc);
					break;
				default:
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	private static void login(Scanner sc) throws Exception {
		System.out.println("entrez votre nom : ");
		System.out.print(">");
		String nom = sc.nextLine();
		ResultSet result = statement.executeQuery("select * from personne where nom = '"+nom+"';");
		if(result.next()) {
			authentifie = true;
			System.out.println("auth reussie !");
		} else {
			System.out.println("mauvais login mot de passe !!");
		}
	}

}
