package com.afpa.id.easyMock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;


@RunWith(EasyMockRunner.class)


public class MathAppTest {
	private MathApplication mathApp;
	private CalculatorService calcServ;
	@BeforeEach
	public void setUp() throws Exception {
		System.out.println("coucou");
		mathApp = new MathApplication();
		//création du Mock
		calcServ =  EasyMock.createMock(CalculatorService.class);
		mathApp.setCalcServ(calcServ);
	}
	@AfterEach
	public void tearDown() throws Exception {
	}
	@Test
	public void testAddAndSubstract() {
		//Comportement a avoir
		EasyMock.expect(calcServ.add(20.0, 10.0)).andReturn(30.0);
		EasyMock.expect(calcServ.substract(20.0, 10.0)).andReturn(10.0);
		//Activer le mock
		EasyMock.replay(calcServ);
		//test
		assertEquals(mathApp.substract(20.0, 10.0), 10.0);
		assertEquals(mathApp.add(20.0, 10.0), 30.0);
		//verifier l'appel de calcServ (ou pas)
		EasyMock.verify(calcServ);
	}
	@Test
	public void testMultiply() {
		EasyMock.expect(calcServ.multiply(2.0, 10.0)).andReturn(20.0);
		EasyMock.replay(calcServ);
		assertEquals(mathApp.multiply(2.0, 10.0), 20.0);
		EasyMock.verify(calcServ);
	}
	@Test
	public void testDivide() {
		EasyMock.expect(calcServ.divide(20.0, 10.0)).andReturn(2.0);
		EasyMock.replay(calcServ);
		assertEquals(mathApp.divide(20.0, 10.0), 2.0);
		EasyMock.verify(calcServ);
	}
}