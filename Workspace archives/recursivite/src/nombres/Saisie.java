package nombres;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Saisie {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		//		int n;
		//		do {
		//			System.out.print("Saisir un nombre positif : ");
		//			n = sc.nextInt();
		//		}
		//		while (n < 0 ) ;
		//
		//		System.out.println("-> Somme des n carr�s : "+carre (n));
		//
		//		System.out.println();
		//		System.out.print("Saisir la taille du tableau d'entier : ");
		//		int t = sc.nextInt();
		//
		//		int [] tab_int =input_tab_int (t);
		//		System.out.println("Tableau d'entier : "+Arrays.toString(tab_int));
		//		System.out.println("-> Somme du tableau d'entier : "+somme_tab_int(tab_int,0));

		//		System.out.println();
		//		System.out.print("Saisir le String d'entiers : ");
		//		String s = sc.next();
		//
		//		int val_str =conv_string (s,s.length());
		//		System.out.println("-> Valeur num�rique du String : "+val_str);

		//		System.out.println();
		//		System.out.print("Saisir le String � tester en palindrome : ");
		//		String p = sc.next();
		//		System.out.println("String � tester en palindrome : "+p);
		//
		//		boolean pal = palindrome (p,0);
		//		System.out.println(pal);

		int tableauEntier[] = {12,21,32,43,64,85,96,17,80};
		System.out.println("Avant tri : "+Arrays.toString(tableauEntier));

		tri (tableauEntier,(tableauEntier.length)-1);
		System.out.println("Apr�s tri : "+Arrays.toString(tableauEntier));
	}

	public static double carre (int n) {
		if (n==0) return n;
		return Math.pow(n,2)+ carre(n-1);
	}

	public static int [] input_tab_int (int tail) {

		int [] tab = new int [tail];

		for (int a=0;a<tab.length;a++) {
			System.out.println("Saisir la valeur "+(a+1)+" du tableau : ");
			int v = sc.nextInt();
			tab[a] = v;
		}
		return tab;

	}

	public static int somme_tab_int (int [] tab, int i) {

		if(i>=tab.length) {
			return 0;
		}else {
			return tab[i]+somme_tab_int(tab,i+1);
		}
	}

	public static int conv_string (String str, int i) {
		if(i==0) {
			return 0;
		}else {
			return (str.charAt(i-1) -48) + conv_string (str,i-1)*10 ;

		}

	}

	//	public static boolean palindrome (String c) {
	//		int taille = c.length();
	//		System.out.println(taille);
	//		if ((taille == 0) || (taille == 1))
	//			return true;
	//		else
	//			if (c.charAt(0)==c.charAt(taille-1))   
	//
	//				return palindrome(c.substring(1,taille-1)); 
	//			else return false;
	//
	//	}

	private static boolean palindrome(String str, int i) {
		if (i == str.length()) {
			return true;
		}
		if (str.charAt(i)!=(str.charAt(str.length() - 1 - i))) {
			return false;
		}
		return palindrome(str, i+1);
	}

//	public static void tri(int[] a, int i) 	{
//		if(a[i] < a[i-1]) {
//			int b = a[i-1];
//			a[i-1] = a[i];
//			a[i] = b;
//			i--;
//			tri(a,i);
//		}
//		else
//		{
//			i--;
//			tri(a,i);
//		}
//	}

	public static void tri( int [] tableau, int tail )
	{
	    int temp=0;
	    int Pmax=0;
	    int max = tableau[0];
	    if ( tail > 0 )
	    {
	        for ( int i = 0; i <= tail; i++ )  {
	            if ( max < tableau[i] )      {
	                max = tableau[i];
	                Pmax = i;
	            }
	        }
	        temp = tableau[Pmax];
	        tableau[Pmax] = tableau[tail];
	        tableau[tail] = temp;
	        tri( tableau, tail - 1 );
	    }
	}
	
	
}
