package nombres;

import java.util.Scanner;

public class Somme {

	public static int add (int n) {

		if (n==0) return n;

		return n + add (n-1);
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Saisir un entier N : ");
		int n = sc.nextInt();
		
		System.out.println(add (n));
	}
}
