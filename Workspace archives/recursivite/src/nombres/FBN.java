package nombres;

import java.util.Scanner;

public class FBN {

	
	
	public static int fibo (int n) {
		
		if (n==0 || n==1) return n;
		return fibo (n-1) + fibo (n-2);
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Saisir un entier N : ");
		int n = sc.nextInt();
		
		System.out.println("El�ment N�"+n+" de la suite de Fibonacci : "+fibo (n));
		
	}
	
}
