import java.sql.*;

public class Exemple2 {

	public static void main(String[] args) throws Exception {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        String url = "jdbc:postgresql://localhost:5432/postgres?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
        String user = "postgres";
        String password = "cda";
        Connection connexion = null;
        try {
            connexion = DriverManager.getConnection(url, user, password);
            // cr�eation de la requ�ete (statement)
            Statement statement = connexion.createStatement();
            // Ex�ecution de la requ�ete
            ResultSet result = statement.executeQuery("SELECT * FROM emp where prenom = 'JEAN';");
            
            int nbPersonne = 0;
            while (result.next()) {
                nbPersonne++;
                // on indique chaque fois le nom de la colonne et le type
                int idPersonne = result.getInt("noemp");
                String nom = result.getString("nom");
                String prenom = result.getString("prenom");
                // pareil pour tous les autres attributs
                System.out.println(idPersonne + " " + nom + " " + prenom);
            }
            
            System.out.println("le nombre de personne : "+nbPersonne);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connexion != null)
                try {
                    connexion.close();
                } catch (SQLException ignore) {
                    ignore.printStackTrace();
                }
        }
    }
}
    
//     public  static  void main (String[] args) {
//           try {
//              // chargement de la classe par son nom
////             Class c = Class.forName("com.postgresql.jdbc.Driver") ;
////             Driver pilote = (Driver)c.newInstance() ;
//              // enregistrement du pilote aupr�s du DriverManager
////             DriverManager.registerDriver(pilote);
//              // Protocole de connexion
//             String protocole =  "jdbc:postgresql:" ;
//              // Adresse IP de l�h�te de la base et port
//             String ip =  "localhost" ;  // d�pend du contexte
//             String port =  "5432" ;  // port MySQL par d�faut
//              // Nom de la base ;
//             String nomBase =  "postgres" ;  // d�pend du contexte
//              // Cha�ne de connexion
//             String conString = protocole +  "//" + ip +  ":" + port +  "/" + nomBase ;
//              // Identifiants de connexion et mot de passe
//             String nomConnexion =  "postgres" ;  // d�pend du contexte
//             String motDePasse =  "cda" ;  // d�pend du contexte
//              // Connexion
//             Connection con = DriverManager.getConnection(
//                conString, nomConnexion, motDePasse) ;
//
//              // Envoi d�un requ�te g�n�rique
//             String sql =  "select * from emp" ;
//             Statement smt = (Statement) con.createStatement() ;
//             ResultSet rs = ((java.sql.Statement) smt).executeQuery(sql) ;
//              while (rs.next()) {
//                System.out.println(rs.getString("sal")) ;
//                System.out.println(rs.getString("prenom")) ;
//                System.out.println(rs.getString("nom")) ;
//             }
//          }  catch (Exception e) {
//              // gestion des exceptions
//          }
//       }
//    }
	

