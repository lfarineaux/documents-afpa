import java.sql.*;

public class Exemple1 {

	public static void main(String[] args) throws Exception {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        String url = "jdbc:postgresql://localhost:5432/postgres?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
        String user = "postgres";
        String password = "cda";
        Connection connexion = null;
        try {
            connexion = DriverManager.getConnection(url, user, password);
            // cr�eation de la requ�ete (statement)
            Statement statement = connexion.createStatement();
            // Ex�ecution de la requ�ete
            ResultSet result = statement.executeQuery("SELECT * FROM emp where prenom = 'JEAN';");
            
            int nbPersonne = 0;
            while (result.next()) {
                nbPersonne++;
                // on indique chaque fois le nom de la colonne et le type
                int idPersonne = result.getInt("noemp");
                String nom = result.getString("nom");
                String prenom = result.getString("prenom");
                // pareil pour tous les autres attributs
                System.out.println(idPersonne + " " + nom + " " + prenom);
            }
            
            System.out.println("le nombre de personne : "+nbPersonne);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connexion != null)
                try {
                    connexion.close();
                } catch (SQLException ignore) {
                    ignore.printStackTrace();
                }
        }
    }

}
