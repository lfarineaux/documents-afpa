import java.beans.Statement;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Exemple {
	public  static  void main (String[] args) {
	       try {
//	           chargement de la classe par son nom
//	         Class c = Class.forName("com.postgresql.jdbc.Driver") ;
//	         Driver pilote = (Driver)c.newInstance() ;
//	           enregistrement du pilote aupr�s du DriverManager
//	         DriverManager.registerDriver(pilote);
//	           Protocole de connexion
	         String protocole =  "jdbc:postgresql:" ;
	          // Adresse IP de l�h�te de la base et port
	         String ip =  "localhost" ;  // d�pend du contexte
	         String port =  "5432" ;  // port MySQL par d�faut
	          // Nom de la base ;
	         String nomBase =  "postgres" ;  // d�pend du contexte
	          // Cha�ne de connexion
	         String conString = protocole +  "//" + ip +  ":" + port +  "/" + nomBase ;
	          // Identifiants de connexion et mot de passe
	         String nomConnexion =  "postgres" ;  // d�pend du contexte
	         String motDePasse =  "cda" ;  // d�pend du contexte
	         // Connexion
	         Connection con = DriverManager.getConnection(
	            conString, nomConnexion, motDePasse) ;
	          // Envoi d�un requ�te g�n�rique
	         String sql =  "select * from emp" ;
	         PreparedStatement selec = con.prepareStatement(sql);
//	         Statement smt = (Statement) con.createStatement() ;
//	         System.out.println(selec.executeQuery());
	         ResultSet rs =  selec.executeQuery() ;
	          while (rs.next()) {
	            System.out.println(rs.getString("sal")) ;
	            System.out.println(rs.getString("prenom")) ;
	            System.out.println(rs.getString("nom")) ;
	         }
	      }  catch (Exception e) {
	          // gestion des exceptions
	      }
	   }
	}

