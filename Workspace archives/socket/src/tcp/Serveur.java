package tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur {

	public static void main(String[] args) throws IOException {

		ServerSocket s = new ServerSocket (8080);

		Socket c = s.accept();

		OutputStream os = c.getOutputStream ();

		os.write (2);

		InputStream is = c.getInputStream ();

		int y = is.read ();

		System.out.println("y=" +y);

		os.write (y+5);
		
		c.close();

		s.close();
	}

}
