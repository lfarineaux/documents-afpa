package tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class Client {

	public static void main(String[] args) throws IOException {
	
	Socket c = new Socket ();
	
	SocketAddress sa= new InetSocketAddress("localhost",8080);
	
	c.connect (sa);
	
	InputStream is = c.getInputStream ();
	
	int x = is.read ();
	
	System.out.println("x=" +x);
	
	OutputStream os = c.getOutputStream ();
	
	os.write (x+1);
	
	int y= is.read ();
	
	System.out.println("y=" +y);
	
	c.close();
	
	}
	
}
