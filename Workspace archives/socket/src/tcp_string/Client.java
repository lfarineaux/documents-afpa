package tcp_string;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Scanner;

public class Client {

	public static void main(String[] args) throws IOException {
		System.out.println("Client");
		
		System.out.print("Saisir une cha�ne : ");
		Scanner sc = new Scanner(System.in);
		Socket c = new Socket ();

		SocketAddress sa= new InetSocketAddress("localhost",8080);

		c.connect (sa);

		OutputStream os = c.getOutputStream ();

		os.write (sc.nextLine().getBytes());

		InputStream is2 = c.getInputStream ();

		System.out.println(is2);

//		BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
//		String str = in.readLine();
//		System.out.println(str);

		c.close();
		sc.close();
	}

}
