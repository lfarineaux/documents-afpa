package tcp_string;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur {

	public static void main(String[] args) throws IOException {
		do 	{
			System.out.println("Serveur");
			ServerSocket s = new ServerSocket (8080);
			Socket soc = s.accept();
			System.out.println("Connexion OK");

			InputStream is = soc.getInputStream ();

			BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
			String str = in.readLine();
			System.out.println(str);
			String str2="";

			char[] chars = str.toCharArray();
			for (int i = 0; i < chars.length; i++) {
				char c = chars[i];
				str2 += Character.isUpperCase(c) ? Character.toLowerCase(c) : Character.toUpperCase(c);
			}
//			str2+="\n";
			System.out.println(str2);

			OutputStream os = soc.getOutputStream ();

			os.write (str2.getBytes());
			soc.close();
			s.close();
			System.out.println("Fin de connexion");
		}
		while (true) ;
	}
}
