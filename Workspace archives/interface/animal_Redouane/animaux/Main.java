package animaux;

import capacite.Marcheur;
import capacite.Nageur;

public class Main {

	public static void main(String[] args) {


		Animal[] tab = new Animal[7];
		
		tab[0] = new Chat();
		tab[1] = new Chien();
		tab[2] = new Sardine();
		tab[3] = new Chat();
		tab[4] = new Chien();
		tab[5] = new Sardine();
		tab[6] = new Chat();
		
		
		
		for(Animal e : tab) {
			
			System.out.println("--------------------");
			
			e.repire();
			
			
			if(e instanceof Marcheur) {
				((Marcheur)e).marche();
			}
			
			if(e instanceof Nageur) {
				((Nageur)e).nage();
			}
			
			e.exec(); //m�thode perso
			
		}


	}


}