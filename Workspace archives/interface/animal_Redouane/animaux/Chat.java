package animaux;

import capacite.Criant;
import capacite.Marcheur;

public class Chat extends Animal implements Criant, Marcheur {

	public void exec() {
		System.out.println("je veux des calin");
	}

	@Override
	public void marche() {
		System.out.println("je marche seul, je suis un solitaire");
		
	}

	@Override
	public void crier() {
		System.out.println("je miaule");
		
	};
}
