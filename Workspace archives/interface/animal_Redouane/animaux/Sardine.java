package animaux;

import capacite.Nageur;

public class Sardine extends Animal implements Nageur{
	
	public void exec() {
		System.out.println("je me cache des predateur");
	}

	@Override
	public void nage() {
		System.out.println("l'eau est mon domaine");
	}

}
