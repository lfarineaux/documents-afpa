package org.eclipse.interfaces;

import java.util.ArrayList;

public class Program {

	public static void main(String[] args) {

		Chiens d1 = new Chiens ();
		Chiens d2 = new Chiens ();
		Chats c1 = new Chats ();
		Chats c2 = new Chats ();
		Chats c3 = new Chats ();
		Sardines s1 = new Sardines ();
		Sardines s2 = new Sardines ();

		ArrayList <Animal> animaux = new ArrayList <> () ;

		animaux.add(d1);
		animaux.add(d2);
		animaux.add(c1);
		animaux.add(c2);
		animaux.add(c3);
		animaux.add(s1);
		animaux.add(s2);

		for (Animal an : animaux) {
			an.respire ();
			if (an instanceof Marcheur) {
				((Marcheur)an).marcher();
			}
			if (an instanceof Nageur) {
				((Nageur)an).nager();
			}
			if (an instanceof Chiens) {
				((Chiens)an).ramenerBalle ();
			}
			if (an instanceof Chats) {
				((Chats)an).grimperArbres();
			}
			if (an instanceof Sardines) {
				((Sardines)an).pondreOeufs () ;
			}
			System.out.println();
		}
	}
}
