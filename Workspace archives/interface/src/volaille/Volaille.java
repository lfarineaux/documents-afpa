package volaille;

import java.util.ArrayList;
import java.util.Scanner;

public abstract class Volaille extends Volatile implements Abattage {


	public Volaille () {
	}

	public abstract void modifierPoidsVolaille ()	;	

	public abstract void modifierPoidsAbattage ();	
	
	public abstract  void modifierPrixDuJour ()	;


	public final void calculPrixVente () {
		for (Volatile v : listVolatiles) {
			if (v instanceof Canard) {
				Canard c= (Canard)v;
				c.prixVente=Canard.prixDuJour*c.poidsCanard;
			}
		}	
		for (Volatile v : listVolatiles) {
			if (v instanceof Poulet) {
				Poulet p= (Poulet)v;
				p.prixVente=Poulet.prixDuJour*p.poidsPoulet;
			}
		}	
	}

	public final void voirPrixTotalVolaillesAbattables () {
		if (this  instanceof Canard) {
			double prix = 0;
			for (Volatile v : listVolatiles) {
				if (v instanceof Canard) {
					Canard c= (Canard)v;
					if (c.poidsCanard>=Canard.poidsAbattage) {
						prix+=c.prixVente;
						System.out.println("Id canard vendable : "+c.idCanard);
					}
				}
			}	
			System.out.println("Total prix vente des canards : "+prix+" (�)");
		}
		if (this  instanceof Poulet) {
			double prix = 0;
			for (Volatile v : listVolatiles) {
				if (v instanceof Poulet) {
					Poulet p= (Poulet)v;
					if (p.poidsPoulet>=Poulet.poidsAbattage) {
						prix+=p.prixVente;
						System.out.println("Poulet vendable : "+p.idPoulet);
					}
				}
			}
			System.out.println("Total prix vente des poulets : "+prix+" (�)");
		}
	}


}