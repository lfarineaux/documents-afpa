package volaille;

import java.util.Scanner;

public final class Canard extends Volaille {

	protected final static int NBMAXCANARDS = 4;
	protected static int nbCanards;
	protected int idCanard;
	double poidsCanard;
	protected static double prixDuJour;
	protected static double poidsAbattage;
	double prixVente;

	public Canard () {
	}

	public Canard (int i) {
		idCanard=i;
	}

	public Canard (int i,double p) {
		idCanard=i;
		nbCanards++;
		nbVolatiles++;
		poidsCanard=p;
	}

	public  void ajouterVolatile ()	{
		Scanner sc = new Scanner(System.in);
		if (nbVolatiles<NBMAXVOLATILES) {
			if (nbCanards<NBMAXCANARDS) {
				System.out.println("Entrez le N� de collier du nouveau canard : ");
				int id = sc.nextInt();
				System.out.println("Entrez le poids du nouveau canard (0,0 kgs) : ");
				Double poids = sc.nextDouble();
				Canard c = new Canard(id,poids);
				listVolatiles.add(c);
				c.calculPrixVenteCanard ();
				System.out.println("Nouveau canard ajout�. Il y a "+nbCanards+" canard(s).");
			}
			else {
				System.out.println("Ajout impossible, la capacit� MAX ("+NBMAXCANARDS+") est d�pass�e.");
			}
		}
		else {
			System.out.println("Ajout impossible, la capacit� MAX de volatiles ("+NBMAXVOLATILES+") est d�pass�e.");
		}
	}

	public void modifierPoidsVolaille () {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez l'id du canard : ");
		int num = sc.nextInt();
		boolean test=false;
		for (Volatile v : listVolatiles) {
			if (v instanceof Canard) {
				Canard c= (Canard)v;
				if 	((c.idCanard)==num) {
					System.out.println("Poids du canard "+c.idCanard+" : "+c.poidsCanard+" kg.");
					System.out.println("Voulez-vous modifier le poids du canard "+c.idCanard+" (oui/non) ?");
					String modif = sc.next();
					modif=modif.toLowerCase();
					switch (modif) {
					case "oui" :	
						System.out.println("Saisir le nouveau poids (0,0 kg) : ");
						double poids = sc.nextDouble();
						c.poidsCanard=poids;
						System.out.println("Nouveau poids pour le canard "+c.idCanard+" : "+c.poidsCanard+" kg.");
						c.calculPrixVenteCanard ();
						test = true;
						break;	
					case "non":
						test = true;
						break;
					default :
						System.out.println("Mauvaise saisie");
						test = true;
						break;
					}
				}	
			}
		}
		if (!test) {
			System.out.println("l'Id n'existe pas");
		}
	}

	public final void retirerVolatile () {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez le N� de collier du canard � vendre : ");
		int id = sc.nextInt();
		boolean test=false;
		for (Volatile v : listVolatiles) {
			if(v instanceof Canard) {
				Canard p = (Canard) v;
				if (p.idCanard==id) {
					p.listVolatiles.remove(p);	
					nbCanards--;
					nbVolatiles--;
					System.out.println("La quantit� de canards a �t� mise � jour. Il y a "+nbCanards+" canards.");
					test = true;
					break;
				}
			}
		}
		if (!test) {
			System.out.println("l'Id n'existe pas");
		}
	}


	public void calculPrixVenteCanard () {
		this.prixVente = this.poidsCanard*prixDuJour;
	}

	@Override
	public String toString() {
		return "| CANARD | Id : " + idCanard + " | poids (kg) : " + poidsCanard +  " | poids d'abattage (kg) : " + poidsAbattage+ " | prix du jour (�/kg) : "
				+ prixDuJour +" | prix de vente (�) : "
				+ prixVente+" |";
	}

	public void modifierPoidsAbattage () {
		Scanner sc = new Scanner(System.in);

		System.out.println("Poids d'abattage du canard (kg) : "+Canard.poidsAbattage);
		System.out.println("Voulez-vous modifier le poids d'abattage du canard (kg) : (oui/non) ?");
		String modifc = sc.next();
		modifc=modifc.toLowerCase();
		switch (modifc) {
		case "oui" :	
			System.out.println("Saisir le nouveau poids d'abattage du canard : ");
			double poids = sc.nextDouble();
			Canard.poidsAbattage=poids;
			System.out.println("Nouveau poids d'abattage du canard (0,0 kgs) : "+Canard.poidsAbattage);
			break;
		case "non":
			break;
		default :
			System.out.println("Mauvaise saisie");
			break;
		}
	}

	public final void modifierPrixDuJour ()	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Prix du jour du canard  : "+Canard.prixDuJour+" (�/kg)");
		System.out.println("Voulez-vous modifier prix du jour du canard (kg) : (oui/non) ?");
		String modifc = sc.next();
		modifc=modifc.toLowerCase();
		switch (modifc) {
		case "oui" :	
			System.out.println("Saisir le nouveau prix du jour du canard (0,0 �/kg) : ");
			double prix = sc.nextDouble();
			Canard.prixDuJour=prix;
			System.out.println("Nouveau prix du jour du canard  : "+Canard.prixDuJour+" (�/kg)");
			calculPrixVente ();
		case "non":
			break;
		default :
			System.out.println("Mauvaise saisie");
			break;
		}
	}
}
