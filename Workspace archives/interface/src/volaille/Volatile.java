package volaille;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public abstract class Volatile{

	protected String tv;
	protected static int nbVolatiles;
	protected final static int NBMAXVOLATILES= 7;

	public static List<Volatile> listVolatiles = new ArrayList<>();

	public Volatile () {
	}

	public  abstract  void ajouterVolatile ()	;	

	public  abstract  void retirerVolatile ()	;	

	public static void afficheVolatile () {
		for (Volatile v : listVolatiles) {
			if (v instanceof Canard) {
				System.out.println(v);
			}
		}
		System.out.println();
		for (Volatile v : listVolatiles) {
			if (v instanceof Poulet) {
				System.out.println(v);
			}
		}
		System.out.println();
		for (Volatile v : listVolatiles) {
			if (v instanceof Paon) {
				System.out.println(v);
			}
		}
		System.out.println();
	}

	public static void voirVollailleParType ()	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez le type de volaille � afficher (Canard/Poulet/Paon) : ");
		String type = sc.next();
		type=type.toLowerCase();
		switch (type) {
		case "canard" :
			for (Volatile v : listVolatiles) {
				if (v instanceof Canard) {
					Canard c= (Canard)v;
					System.out.println(c);
				}
			}
			break;
		case "poulet" :
			for (Volatile v : listVolatiles) {
				if (v instanceof Poulet) {
					Poulet p= (Poulet)v;
					System.out.println(p);
				}
			}
			break;
		case "paon" :
			for (Volatile v : listVolatiles) {
				if (v instanceof Paon) {
					Paon p= (Paon)v;
					System.out.println(p);
				}
			}
			break;
		default :
			System.out.println("Mauvaise saisie");
			break;
		}
	}

	public static List<Volatile> getListVolatiles() {
		return listVolatiles;
	}

	@Override
	public String toString() {
		return "Volatile - " ;
	}
}
