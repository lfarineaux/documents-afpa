package volaille_compareTo;

import java.util.Scanner;


public final class Paon extends Volatile {
	protected static int nbPaons;
	protected final static int NBMAXPAONS = 2;
	protected int idPaon;

	public Paon () {
	}


	public Paon (int i) {
		idPaon=i;
		nbVolatiles++;
		nbPaons++;
	}

	public void ajouterVolatile ()	{
		Scanner sc = new Scanner(System.in);
		if (nbPaons<NBMAXPAONS) {
			System.out.println("Entrez le N� de collier du nouveau paon : ");
			int id = sc.nextInt();
			Paon p = new Paon(id);
			listVolatiles.add(p);
			System.out.println("Nouveau paon ajout�. Il y a "+nbPaons+" paon(s).");
		}
		else {
			System.out.println("Ajout impossible, la capacit� MAX de paons ("+NBMAXPAONS+") est d�pass�e.");
		}
	}

	public final void retirerVolatile () {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez le N� de collier du paon � rendre : ");
		int id = sc.nextInt();
		boolean test = false;
		for (Volatile v : listVolatiles) {
			if(v instanceof Paon) {
				Paon p = (Paon) v;
				if (p.idPaon==id) {
					p.listVolatiles.remove(p);	
					nbPaons--;
					nbVolatiles--;
					System.out.println("La quantit� de paons a �t� mise � jour. Il y a "+nbPaons+" paons.");
					break;
				}
			}
		}
		if (!test) {
			System.out.println("l'Id n'existe pas");
		}
	}

	@Override
	public String toString() {
		return "| PAON   | Id : " + idPaon+"  |";
	}
//	@Override
	
	
//	public int compareTo(Volatile o) {
//		if(o instanceof Canard) {
//			return -1;
//		} else if(o instanceof Poulet){
//			return 2;
//		} else {
//			return Integer.valueOf(this.idPaon).compareTo(((Paon) o).idPaon);
//		}
//	}

}
