package volaille_compareTo;

import java.util.Scanner;

public class Program {

	public static void main(String[] args) {

		objetsTest();

		Scanner sc = new Scanner(System.in);
		String menu = "";
		String type;
		boolean fin = true;
		/** @param
		 *  menu : num�ro saisi dans le menu 
		 *  fin : arr�t du programme
		 */ 

		while (fin) {
			System.out.println();
			System.out.println("      ***************");
			System.out.println("      *     MENU    *");
			System.out.println("      ***************");
			System.out.println();
			System.out.println(" 0. Arr�ter le programme");
			System.out.println(" 1. Ajouter un volatile");
			System.out.println(" 2. Retirer/vendre un volatile");
			System.out.println(" 3. Voir/Modifier le poids d'une volaille (kg)");
			System.out.println(" 4. Voir/Modifier le poids d'abattage (kg)");
			System.out.println(" 5. Voir/Modifier le prix du jour d'une volaille (�/kg)");
			System.out.println(" 6. Afficher le prix total des volailles abattables (�)");
			System.out.println(" 7. Afficher les volatiles par type (Canard/Poulet/Paon)");
			System.out.println(" 8. Afficher tout le parc de volatiles");

			System.out.println();
			System.out.print("   ->  ");
			menu = sc.next();
			int menuint = Integer.parseInt(menu);
			System.out.println();

			switch (menuint) {
			case 0:	
				System.out.println("Arr�t du programme");
				fin=false;
				break;	
			case 1:
				System.out.println(" 1. Ajouter un volatile");
				System.out.println("Entrez le type de volaille (Canard/Poulet/Paon) : ");
				type = sc.next();
				type=type.toLowerCase();
				switch (type) {
				case "canard" :
					Canard ca= new Canard();
					ca.ajouterVolatile();
					break;
				case "poulet" :
					Poulet po = new Poulet();
					po.ajouterVolatile();
					break;
				case "paon" :
					Paon pa = new Paon();
					pa.ajouterVolatile();
					break;
				default :
					System.out.println("Mauvaise saisie");
					break;
				}
				break;
			case 2:
				System.out.println(" 2. Retirer/vendre un volatile");
				System.out.println("Entrez le type de volaille (Canard/Poulet/Paon) : ");
				type = sc.next();
				type=type.toLowerCase();
				switch (type) {
				case "canard" :
					Canard ca= new Canard();
					ca.retirerVolatile();
					break;
				case "poulet" :
					Poulet po = new Poulet();
					po.retirerVolatile();
					break;
				case "paon" :
					Paon pa = new Paon();
					pa.retirerVolatile();
					break;
				default :
					System.out.println("Mauvaise saisie");
					break;
				}
				break;
			case 3:
				System.out.println(" 3. Voir/Modifier le poids d'une volaille (kg)");
				System.out.println("Entrez le type de volaille (Canard/Poulet) : ");
				type = sc.next();
				type=type.toLowerCase();
				switch (type) {
				case "canard" :
					Canard ca= new Canard();
					ca.modifierPoidsVolaille();
					break;
				case "poulet" :
					Poulet po = new Poulet();
					po.modifierPoidsVolaille();
					break;
				default :
					System.out.println("Mauvaise saisie");
					break;
				}
				break;
			case 4: 
				System.out.println(" 4. Voir/Modifier le poids d'abattage (kg)");
				System.out.println("Entrez le type de volaille (Canard/Poulet) : ");
				type = sc.next();
				type=type.toLowerCase();
				switch (type) {
				case "canard" :
					Canard ca= new Canard();
					ca.modifierPoidsAbattageAux();
					break;
				case "poulet" :
					Poulet po = new Poulet();
					po.modifierPoidsAbattageAux();
					break;
				default :
					System.out.println("Mauvaise saisie");
					break;
				}
				break;
			case 5:
				System.out.println(" 5. Voir/Modifier le prix du jour d'une volaille (�/kg)");
				System.out.println("Entrez le type de volaille (Canard/Poulet) : ");
				type = sc.next();
				type=type.toLowerCase();
				switch (type) {
				case "canard" :
					Canard ca= new Canard();
					ca.modifierPrixDuJour();
					break;
				case "poulet" :
					Poulet po = new Poulet();
					po.modifierPrixDuJour();
					break;
				default :
					System.out.println("Mauvaise saisie");
					break;
				}
				break;
			case 6:
				System.out.println(" 6. Afficher le prix total des volailles abattables (�)");
				System.out.println("Entrez le type de volaille (Canard/Poulet) : ");
				type = sc.next();
				type=type.toLowerCase();
				switch (type) {
				case "canard" :
					Canard ca= new Canard();
					ca.voirPrixTotalVolaillesAbattables();
					break;
				case "poulet" :
					Poulet po = new Poulet();
					po.voirPrixTotalVolaillesAbattables();
					break;
				default :
					System.out.println("Mauvaise saisie");
					break;
				}
				break;
			case 7:	
				System.out.println(" 7. Afficher les volatiles par type (Canard/Poulet/Paon)"); 
				Volatile.voirVollailleParType ();
				break;
			case 8:	
				System.out.println(" 8. Afficher tout le parc de volatiles");
				Volatile.afficheVolatile();
				break;
			default:
				System.out.println("Mauvaise saisie");
				break;
			}
		}
		sc.close();
	}

	public static void objetsTest () {
		Canard ca1 = new Canard(11, 0.9);
		Canard ca2 = new Canard(12, 1.1);
		Poulet po1 = new Poulet(21, 1.2);
		Poulet po2 = new Poulet(22, 1.4);
		Paon pa1 = new Paon(6);

		Volatile.listVolatiles.add(ca1);
		Volatile.listVolatiles.add(ca2);
		Volatile.listVolatiles.add(po1);
		Volatile.listVolatiles.add(po2);
		Volatile.listVolatiles.add(pa1);
	}
}
