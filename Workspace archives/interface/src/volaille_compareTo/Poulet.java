package volaille_compareTo;

import java.util.Scanner;

public final class Poulet extends Volaille {

	protected final static int NBMAXPOULETS = 5;
	protected static int nbPoulets;
	protected int idPoulet;
	double poidsPoulet;
	protected static double prixDuJour;
	protected static double poidsAbattage;
	double prixVente;

	public Poulet () {
	}

	public Poulet (int i) {
		idPoulet=i;
	}

	public Poulet (int i,double p) {
		idPoulet=i;
		nbPoulets++;
		nbVolatiles++;
		poidsPoulet=p;
	}

	public  void ajouterVolatile ()	{
		Scanner sc = new Scanner(System.in);
		if (nbVolatiles<NBMAXVOLATILES) {
			if (nbPoulets<NBMAXPOULETS) {
				System.out.println("Entrez le N� de collier du nouveau poulet : ");
				int id = sc.nextInt();
				System.out.println("Entrez le poids du nouveau poulet (0,0 kgs) : ");
				Double poids = sc.nextDouble();
				Poulet p = new Poulet(id,poids);
				listVolatiles.add(p);
				p.calculPrixVentePoulet ();
				System.out.println("Nouveau poulet ajout�. Il y a "+nbPoulets+" poulet(s).");
			}
			else {
				System.out.println("Ajout impossible, la capacit� MAX ("+NBMAXPOULETS+") est d�pass�e.");
			}
		}
		else {
			System.out.println("Ajout impossible, la capacit� MAX de volatiles ("+NBMAXVOLATILES+") est d�pass�e.");
		}
	}

	public void modifierPoidsVolaille () {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez l'id du poulet : ");
		int num = sc.nextInt();
		boolean test=false;
		for (Volatile v : listVolatiles) {
			if (v instanceof Poulet) {
				Poulet c= (Poulet) v;
				if 	((c.idPoulet)==num) {
					System.out.println("Poids du poulet "+c.idPoulet+" : "+c.poidsPoulet+" kg.");
					System.out.println("Voulez-vous modifier le poids du poulet "+c.idPoulet+" (oui/non) ?");
					String modif = sc.next();
					modif=modif.toLowerCase();
					switch (modif) {
					case "oui" :	
						System.out.println("Saisir le nouveau poids (0,0 kg) : ");
						double poids = sc.nextDouble();
						c.poidsPoulet=poids;
						System.out.println("Nouveau poids pour le poulet "+c.idPoulet+" : "+c.poidsPoulet+" kg.");
						c.calculPrixVentePoulet ();
						test = true;
						break;	
					case "non":
						test = true;
						break;
					default :
						System.out.println("Mauvaise saisie");
						test = true;
						break;
					}
				}	
			}
		}
		if (!test) {
			System.out.println("l'Id n'existe pas");
		}
	}


	public final void retirerVolatile () {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez le N� de collier du poulet � vendre : ");
		int id = sc.nextInt();
		boolean test=false;
		for (Volatile v : listVolatiles) {
			if(v instanceof Poulet) {
				Poulet p = (Poulet) v;
				if (p.idPoulet==id) {
					p.listVolatiles.remove(p);	
					nbPoulets--;
					nbVolatiles--;
					System.out.println("La quantit� de poulets a �t� mise � jour. Il y a "+nbPoulets+" poulets.");
					test = true;
					break;
				}
			}
		}
		if (!test) {
			System.out.println("l'Id n'existe pas");
		}
	}

	public void calculPrixVentePoulet () {
		this.prixVente = this.poidsPoulet*prixDuJour;
	}

	@Override
	public String toString() {
		return "| POULET | Id : " + idPoulet + " | poids (kg) : " + poidsPoulet +  " | poids d'abattage (kg) : " + poidsAbattage+ " | prix du jour (�/kg) : "
				+ prixDuJour +" | prix de vente (�) : "
				+ prixVente+" |";
	}
	public void modifierPoidsAbattageAux () {
		Scanner sc = new Scanner(System.in);
		System.out.println("Poids d'abattage du poulet (kg) : "+Poulet.poidsAbattage);
		System.out.println("Voulez-vous modifier le poids d'abattage du poulet (kg) : (oui/non) ?");
		String modifp = sc.next();
		modifp=modifp.toLowerCase();
		switch (modifp) {
		case "oui" :	
			System.out.println("Saisir le nouveau poids d'abattage du poulet : ");
			double poids = sc.nextDouble();
			Poulet.poidsAbattage=poids;
			System.out.println("Nouveau poids d'abattage du poulet (0,0 kgs) : "+Poulet.poidsAbattage);
			break;
		case "non":
			break;
		default :
			System.out.println("Mauvaise saisie");
			break;
		}
	}

	public final void modifierPrixDuJour ()	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Prix du jour du poulet  : "+Poulet.prixDuJour+" (�/kg)");
		System.out.println("Voulez-vous modifier prix du jour du poulet (kg) : (oui/non) ?");
		String modifc = sc.next();
		modifc=modifc.toLowerCase();
		switch (modifc) {
		case "oui" :	
			System.out.println("Saisir le nouveau prix du jour du poulet (0,0 �/kg) : ");
			double prix = sc.nextDouble();
			Poulet.prixDuJour=prix;
			System.out.println("Nouveau prix du jour du poulet  : "+Poulet.prixDuJour+" (�/kg)");
			calculPrixVente ();
		case "non":
			break;
		default :
			System.out.println("Mauvaise saisie");
			break;
		}
	}

	//	@Override
	//	public int compareTo(Volatile o) {
	//		if(o instanceof Poulet) {
	//			return Integer.valueOf(this.idPoulet).compareTo(((Poulet) o).idPoulet);
	//		} else {
	//			return -1;
	//		}
	//	}

//	@Override
//	public int compareTo(Volatile o) {
//		if(o instanceof Paon) {
//			return -1;
//		} else 
////			(o instanceof Poulet) {

//				return Integer.valueOf(this.idPoulet).compareTo(((Poulet) o).idPoulet);
//			}
//		}
	}
	

