package metodes;

import erreurs.*;

public class Method {

	/**
	 * @param String
	 * @return String Methode utiliser pour rentrer dans les cases du Swtich, elle
	 *         s�pare le string d'entr�e des potentiels param�tres l'accompagnant
	 */
	public static String stringcase(String s) throws ErrMenu {
		s = s.toLowerCase();
		s = s.trim();
		String[] tab = s.split(" ");
		if (tab.length!=1 && ((tab[0].equalsIgnoreCase("help")) || (tab[0].equalsIgnoreCase("pwd")) ||
				(tab[0].equalsIgnoreCase("exit")) || (tab[0].equalsIgnoreCase("quit")) || (tab[0].equalsIgnoreCase("history"))
				|| (tab[0].equalsIgnoreCase("dir")) || (tab[0].equalsIgnoreCase("dirng")))) {
			throw new ErrMenu();
		}
		return tab[0];
	}

	/**
	 * 
	 * @param String
	 * @return tableau de int
	 * @throws ErrParam M�thode pour retourner le /les param�tre(s) d'une commande
	 *                  utilisateur
	 */
	public static int[] userParam(String s) throws ErrParam {

		s = s.toLowerCase();
		s = s.trim();
		String[] tab = s.split(" ");
		int[] retour = new int[tab.length - 1];

		if (tab.length > 1) {

			for (int i = 1; i < tab.length; i++) {
				for (int j = 0; j < tab[i].toCharArray().length; j++) {
					if (!Character.isDigit(tab[i].toCharArray()[j])) {
						throw new ErrParam();
					}
				}
			}
			for (int i = 1; i < tab.length; i++) {
				if (i > 0) {
					retour[i - 1] = Integer.parseInt(tab[i]);
				}
			}
		}

		return retour;
	}

	/**
	 * 
	 * @param String
	 * @return String
	 * @throws ErrCd M�thode pour retourner le param�tre (unique ou s�par� par des
	 *               espaces) apr�s la commande cd
	 */
	public static String stringcase1(String s) throws ErrCd {
		s = s.toLowerCase();
		s = s.trim();
		String[] tab = s.split(" ");

		if (tab.length > 2) {
			String res = "";
			for (int i = 1; i < tab.length; i++) {
				res += tab[i] + " ";
			}
			res = res.trim();
			String[] tab2 = { tab[0], res };
			tab = tab2.clone();
		}

		if (tab.length < 2) {
			throw new ErrCd();
		}
		return tab[1];
	}
	
	public static String[] stringcasemultiple(String s) throws ErrMult{
		
		s = s.trim();
		String[] tab = s.split(" ");
		String[] retour = new String[2];

		if (tab.length > 2) {
			retour[0] = tab[1];
			retour[1] = tab[2];
		}else {
			throw new ErrMult();
		}

		return retour;
	}
	
	/**
	 * 
	 * @param String
	 * @return tableau de String
	 * 
	 *         m�thode pour cr�er un tableau de String s�par�s par un espace
	 */
	public static String[] stringcasex(String s) {
		s = s.toLowerCase();
		s = s.trim();
		String[] tab = s.split(" ");
		return tab;

	}

	/**
	 * 
	 * @param String
	 * @return String
	 * @throws ErrFind
	 * 
	 *                 M�thode pour retourner le 1er param apr�s la commande find
	 */
	public static String find1 (String [] str) throws ErrFind{
		String res =null;
		if (str.length<2) {
			throw new ErrFind();
		} else {
			res=str[1];
		}
		return res;
	}

	/**
	 * 
	 * @param tableau de String
	 * @return String
	 * 
	 *         Methode pour retourner le crit�re de recherche apr�s l'instruction
	 *         dans la commande find
	 */
	public static String rechStarts(String[] tab) {
		String start = "";
		for (int i = 1; i < tab.length; i++) {
			if (tab[i].equalsIgnoreCase("-starts")) {
				start = tab[i + 1];
				break;
			}
		}
		return start;
	}

	/**
	 * 
	 * @param tableau de String
	 * @returnString
	 * 
	 *               methode pour retourner le crit�re de recherche apr�s
	 *               l'instruction ends de la commande find
	 */
	public static String rechEnds(String[] tab) {
		String end = "";
		for (int i = 1; i < tab.length; i++) {
			if (tab[i].equalsIgnoreCase("-ends")) {
				end = tab[i + 1];
				break;
			}
		}
		return end;
	}
	
	public static String getVars1 (String [] str){
		String res =null;
		if (str.length>1) {
			res=str[1];
		} 
		return res;
	}
	
}
