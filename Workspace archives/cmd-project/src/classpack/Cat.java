package classpack;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import erreurs.ErrCat;



public final class Cat {

	private final static String name = "CAT";


	public static void affichageCat(String repUser) throws ErrCat {
		
		try {
			String str = RepEnCours.getRepEnCours()+"\\"+ repUser;
			Path src = Paths.get(str);
			List<String> lignes = Files.readAllLines(src);

			for (String s : lignes) {
				System.out.println(s);
			}
		} catch (IOException e) {
			throw new ErrCat(e);
		}
		
	}


	@Override
	public String toString() {
		return "Copie un fichier dans un autre";
	}

	public static String getName() {
		return name;
	}



}
