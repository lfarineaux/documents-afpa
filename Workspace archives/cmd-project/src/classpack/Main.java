package classpack;

import java.io.IOException;
import java.util.Scanner;

import erreurs.ErrCd;
import erreurs.ErrFind;
import erreurs.ErrMenu;
import erreurs.ErrMult;
import erreurs.ErrParam;
import metodes.Method;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String repUser = "";
		boolean enMarche = true;

		while (enMarche) {

			System.out.print("> ");
			repUser = sc.nextLine();
			repUser = repUser.toLowerCase();
			String rep = "";
			try {
			rep=Method.stringcase(repUser);
			}
			catch (ErrMenu e) {
			}
			switch (rep) {

			case "help":

				Help.affichageHelp();
				Help.helping();
				break;

			case "pwd":
				Pwd.affichagePwd();
				break;

			case "exit":
				enMarche = Exit.superfalse();
				break;

			case "quit":
				enMarche = Quit.superfalse();
				break;

			case "history":
				History.affichHistory();
				break;
			case "histclear":
				History.suprimerHistory();
				break;

			case "isprime":

				try {
					Isprime.isprime(Method.userParam(repUser)[0]);
				} catch (ErrParam e) {
					System.out.println(e);
				}
				History.addHistorique(repUser);
				break;

			case "river":

				try {
					River.river(Method.userParam(repUser)[0], Method.userParam(repUser)[1]);
				} catch (ErrParam e) {
					System.out.println(e);
				}
				History.addHistorique(repUser);
				break;

			case "dir":
				Dir.affichageDir();
				History.addHistorique(repUser);
				break;

			case "dirng":
				Dirng.affichageDirNg();
				History.addHistorique(repUser);
				break;

			case "cd":
				try {
					Cd.affichageCd((Method.stringcase1(repUser)));
				} catch (ErrCd e) {
					System.out.println(e);
				}
				History.addHistorique(repUser);
				break;

			case "copy":
				try {
					Copy.affichageCopy(Method.stringcasemultiple(repUser));
				} catch (IOException | ErrMult e ) {
					System.out.println(e);
				}
				History.addHistorique(repUser);
				break;

			case "cat":
				try {
					Cat.affichageCat(Method.stringcase1(repUser));
				} catch (IOException | ErrCd e) {
					System.out.println(e);
				}
				History.addHistorique(repUser);
				break;
				
			case "crf":
				try {
					Crf.createFichier(Method.stringcase1(repUser));
				} catch (IOException | ErrCd e) {
					e.printStackTrace();
				}
				History.addHistorique(repUser);
				break;
			case "crd":
				try {
					Crd.creerRepertoire(Method.stringcase1(repUser));
				} catch ( ErrCd e) {
					e.printStackTrace();
				}
				History.addHistorique(repUser);
				break;
				
			case "find":
                try {
                Find.infoFind(repUser);
//                Find.affichecpt();
                } catch (ErrFind e) {
                        System.out.println(e);
                    }
                History.addHistorique(repUser);
                break;

			case "getvars" :
				try {
				Getvars.infoGetVars(repUser);
				} catch (ErrMenu e) {
					System.out.println(e);
				}
				History.addHistorique(repUser);
				
				break;
				
                
			default:
				System.out.println("Commande introuvable");
				break;
			}
		}
		sc.close();
	}
	
}
