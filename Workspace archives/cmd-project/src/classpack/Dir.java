package classpack;

import java.io.File;

public class Dir {

	private static String name = "DIR";

	public static void affichageDir() {

		File repertoire = new File(RepEnCours.getRepEnCours());
		System.out.println(repertoire);

		File liste[] = repertoire.listFiles();
		if (liste != null) {
			System.out.println("<DIR> .");
			System.out.println("<DIR> ..");
			for (File fi : repertoire.listFiles()) {
				if (fi.isFile()) {
					System.out.println("      "+fi.getName());
				}
				if (fi.isDirectory()) {
					System.out.println("<DIR> "+fi.getName());
				}
			}
		}
	}

	public String toString() {

		return "Affiche le contenu du répertoire C:";
	}
	public static String getName() {
		return name;
	}



}
