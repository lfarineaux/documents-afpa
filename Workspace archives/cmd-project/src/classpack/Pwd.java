package classpack;

import java.io.File;

public final class Pwd {
	
	private final static String name = "PWD";
	public static void affichagePwd() {
	
		File repertoire = new File(RepEnCours.getRepEnCours());
		System.out.println(repertoire.getAbsolutePath());
	}

	@Override
	public String toString() {
		return "affiche le dossier courant";
	}

	public static String getName() {
		return name;
	}

	
	
}
