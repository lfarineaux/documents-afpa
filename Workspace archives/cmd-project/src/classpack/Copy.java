package classpack;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public final class Copy {

	private final static String name = "COPY";


	public static void affichageCopy(String[] tab) throws IOException {
		String fichierSrc = tab[0];
		String nouveauNom = tab[1];
		Path src = Paths.get(RepEnCours.getRepEnCours()+"\\"+ fichierSrc);
		Path dest = Paths.get(RepEnCours.getRepEnCours()+"\\"+nouveauNom);
		Files.copy(src, dest);

	}


	@Override
	public String toString() {
		return "Copie un fichier dans un autre";
	}

	public static String getName() {
		return name;
	}



}
