package classpack;

public class River {
	
	private static String name = "RIVER";
	public static void river (int r1, int r2)  {

		while (r1!=r2) {

			if (r1>r2) {
				int swap = r2;
				r2=r1;
				r1=swap;
			}

			int s1=0;
			String str1 = String.valueOf(r1);
			char [] ch1 = str1.toCharArray();

			for (int i=0;i<str1.length();i++) {
				int a=Character.getNumericValue(ch1[i]);
				s1 = s1+a;
			}
			r1+=s1;
		}
		System.out.println(r1);
	}
	
	@Override
	public String toString() {
		
		return  "Affiche la premi�re intersection des rivi�res obtenues pour ces param�tres";
	}
	public static String getName() {
		return name;
	}

}
