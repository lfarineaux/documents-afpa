package classpack;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class History {
	
	private static final String nom = "HISTORY";
	
	private static SimpleDateFormat sdf;
	private static List<String> listeHistorique = new ArrayList<>();
	
	
	public static void addHistorique(String s) {
		
		sdf = new SimpleDateFormat("HH:mm:ss' 'dd:MM:yyyy");
		
		s = s + " " + sdf.format(new Date());
		
		if (listeHistorique.size()>9) {
			
			listeHistorique.remove(0);
		}
		
		listeHistorique.add(s);
	}

	
	public static void suprimerHistory() {
		listeHistorique.clear();
	}
	
	public static void affichHistory() {
		
		for(String str : listeHistorique) {
			System.out.println(str);
		}
		
	}
	
	@Override
	public String toString() {
		return "Affiche l'historique des 10 derni�res commandes (Commande et date)." + "\n" ;
	}

	public static List<String> getListeHistorique() {
		return listeHistorique;
	}

	public static String getName() {
		return nom;
	}
}

	
	
	
	
	

