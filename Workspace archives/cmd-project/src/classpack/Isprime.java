package classpack;

public final class Isprime {

	private static String name = "ISPRIME";

	public static void isprime (int n) {
		
		boolean isPremier = true; 
		if (n <2 ) {
			isPremier = false;
		} 
		if (n != 0 && n != 1) {
			for (int i = 2; i <= n/2; i++) {
				if (n != i && n % i == 0) {
					isPremier = false; 
					break;
				}
			}
		}
		if (isPremier) {
			System.out.println("yes");
		}
		else {
			System.out.println("no");	
		}
	}


	@Override
	public String toString() {

		return  "Affiche yes si ce param�tre est un nombre premier no sinon";
	}
	public static String getName() {
		return name;
	}

}
