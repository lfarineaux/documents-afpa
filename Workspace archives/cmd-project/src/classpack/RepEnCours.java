package classpack;

public final class RepEnCours {
	// M�thode pour sauvegarder la valeur du r�pertoire saisi par l'utilisateur pour la commande cd

	private static String repEnCours=Getvars.initialisatinRepEnCours();

	@Override
	public String toString() {
		return repEnCours;
	}


	public static String getRepEnCours() {
		return repEnCours;
	}

	public static void setRepEnCours(String rep) {
		RepEnCours.repEnCours = rep;
	}
	

}
