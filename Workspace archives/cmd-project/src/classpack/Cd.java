package classpack;

import java.io.File;


public class Cd {

	private static String name = "CD";


	public static void affichageCd(String s) {

		File repertoire = new File(RepEnCours.getRepEnCours());

		if (s.equals("..")) {
			RepEnCours.setRepEnCours(repertoire.getParent());
		}
		else  {
			// recherche du r�pertoire saisi apr�s la commande cd
			File liste[] = repertoire.listFiles();
			boolean test=true;
			if (liste != null) {
				for (File fi : liste) {
					if ((fi.isDirectory())&& fi.getName().equalsIgnoreCase(s)) {
						RepEnCours.setRepEnCours(fi.getAbsolutePath());
						test=false;
						break;
					}
				}

				if (test) {
					System.out.println("Le chemin d�acc�s sp�cifi� est introuvable");
				}
			}
		}
	}


	@Override
	public String toString() {
		return "Permet de se d�placer dans un r�pertoire";
	}

	public static String getName() {
		return name;
	}



}
