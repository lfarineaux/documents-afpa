package classpack;

import java.io.File;

import erreurs.ErrFind;
import metodes.Method;

public class Find {
          
	private static String name = "Find";

	public static void infoFind(String str) throws ErrFind {

		String[] tab = Method.stringcasex(str);
		String param = Method.find1(tab);
		String res = null;
		String rechstart = null;
		String rechend = null;
		boolean start = false;
		boolean end = false;

		if (param.equalsIgnoreCase("-starts")) {
			start = true;
			rechstart = Method.rechStarts(tab);
		} else if (param.equalsIgnoreCase("-ends")) {
			end = true;
			rechend = Method.rechEnds(tab);
		} else {
			res = param;
		}

		File rep = new File(RepEnCours.getRepEnCours());

		int a = affichageFind(rep, start, rechstart, end, rechend, res);
		System.out.println(a + " fichiers trouvés");

	}

	public static int affichageFind(File rep, boolean sta, String rechsta, boolean end, String rechend, String find) {
		int cpt = 0;
		File list[] = rep.listFiles();

		for (File fi : list) {
			if (fi.isDirectory()) {
				cpt = cpt + affichageFind(fi, sta, rechsta, end, rechend, find);
			} else if (fi.isFile()) {
				if (sta && !end) {
					if (fi.getName().startsWith(rechsta)) {
						System.out.println(fi.getAbsoluteFile());
						cpt++;
					}
				}

				if (end && !sta) {
					if (fi.getName().endsWith(rechend)) {
						System.out.println(fi.getAbsoluteFile());
						cpt++;
					}
				}

				if (sta && end) {
					if (fi.getName().startsWith(rechsta) && fi.getName().endsWith(rechend)) {
						System.out.println(fi.getAbsoluteFile());
						cpt++;
					}
				}
				if (find != null) {
					if (fi.getName().equalsIgnoreCase(find)) {
						System.out.println(fi.getAbsoluteFile());
						cpt++;
					}
				}
			}
		}
		return cpt;

	}

	// public static void affichecpt () {
	// System.out.println(+cpt+" fichiers trouvé(s)");
	// }

	@Override
	public String toString() {
		return "Trouve un fichier";
	}

	public static String getName() {
		return name;
	}

}
