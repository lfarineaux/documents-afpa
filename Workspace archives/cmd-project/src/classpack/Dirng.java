package classpack;

import java.io.File;

public final class Dirng {

	private static String name = "DIRNG";

	public static void affichageDirNg() {

		File repertoire = new File(RepEnCours.getRepEnCours());

		File liste[] = repertoire.listFiles();	
		
		int f=0;
		int r=0;
		if (liste != null) {
			System.out.println("<DIR> .");
			System.out.println("<DIR> ..");
			for (File fi : repertoire.listFiles()) {
				if (fi.isFile()) {
					System.out.println("      "+fi.getName());
					f++;
				}
				if (fi.isDirectory()) {
					System.out.println("<DIR> "+fi.getName());
					r++;
				}
			}
		}
		System.out.println(+f+" fichiers");
		System.out.println(+r+" répertoires");
	}

	public String toString() {

		return "Affiche le contenu du répertoire en cours";
	}
	public static String getName() {
		return name;
	}



}
