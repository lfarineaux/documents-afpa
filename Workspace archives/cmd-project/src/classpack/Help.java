package classpack;

import java.util.HashMap;
import java.util.Map;

public final class Help {
	
	@SuppressWarnings("unused")
	private static String name = "HELP";
	private static Map<String, String> helping = new HashMap<>();

	public static void helping() {
		River river = new River();
		Isprime isprime = new Isprime ();
		Pwd pwd = new Pwd();
		Exit exit = new Exit();
		Quit quit = new Quit();
		History history = new History();
		Histclear histclear = new Histclear();
		Dir dir = new Dir();
		Cat cat = new Cat ();
		Cd cd = new Cd ();
		Copy copy = new Copy ();
		Crd crd = new Crd();
		Crf crf = new Crf();
		Dirng dirng = new Dirng();
		@SuppressWarnings("unused")
		Find find = new Find ();
		
		helping.put(River.getName(), river.toString());
		helping.put(Isprime.getName(), isprime.toString());
		helping.put(Pwd.getName(), pwd.toString());
		helping.put(Exit.getName(), exit.toString());
		helping.put(Quit.getName(), quit.toString());
		helping.put(History.getName(), history.toString());
		helping.put(Histclear.getName(), histclear.toString());
		helping.put(Dir.getName(), dir.toString());
		helping.put(Cat.getName(), cat.toString());
		helping.put(Cd.getName(), cd.toString());
		helping.put(Copy.getName(), copy.toString());
		helping.put(Crd.getName(), crd.toString());
		helping.put(Dirng.getName(), dirng.toString());
		helping.put(Crf.getName(), crf.toString());
		
		

		for (Map.Entry<String, String> entry : helping.entrySet()) {
			System.out.println(entry.getKey()+"    "+entry.getValue());
		}

	}

	public static void affichageHelp() {
		System.out.println("Bonjour sur la console cdi");
	}

	@Override
	public String toString() {
		return "afficher une description pour chaque commande";
	}

}
