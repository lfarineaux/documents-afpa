package classpack;

import java.io.File;
import java.util.Map;
import java.util.Set;

import erreurs.ErrMenu;
import metodes.Method;

public class Getvars {

	public static void infoGetVars (String str)  throws ErrMenu {
		String [] tab = Method.stringcasex(str);
		String param = Method.getVars1(tab);

		if (param==null) {
			Getvars.getEnv();
			Getvars.getProp();
		}
		else if (param.equalsIgnoreCase("-env")) {
			Getvars.getEnv();
		} else if (param.equalsIgnoreCase("-prop")) {
			Getvars.getProp();
		} else {
			throw new ErrMenu ();
		}
	}

	public static void getEnv () {

		Map <String,String> env =System.getenv(); 
		Set <String> key = env.keySet();
		for (String k : key) {
			System.out.println(k+" : "+env.get(k));

		}
	}

	public static void getProp () {

		Map <Object,Object> env =System.getProperties(); 
		Set <Object> key = env.keySet();
		for (Object k : key) {
			System.out.println(k+" : "+env.get(k));
			
		}
	}
	
	public static String initialisatinRepEnCours() {
		String property = System.getProperty("cdi.default.folder");
		
		if (property!=null) {
			
			File f = new File(property);
			if(! f.exists()) {
				System.out.println("erreur dossier n'existe pas pour la de la propriete cdi.default.folder = "+property);
			} else if(f.isFile()) {
				System.out.println("erreur le chemin designe un fichier pour la de la propriete cdi.default.folder = "+property);
			} else {
				return property;
			}
			
		}
		return System.getProperty("user.dir");
	}
}


