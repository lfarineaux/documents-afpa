package erreurs;

import java.io.IOException;

public class ErrCopy extends ErrNatIO {
	public ErrCopy(Throwable t) {
		super(t);
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "Erreur copie fichier";
	}
}