package erreurs;

public class ErrCat extends ErrNatIO {
    
    public ErrCat (Throwable t) {
        super(t);
    }
  
    private static final long serialVersionUID = 1L;
    @Override
    public String toString() {
        return "Erreur contenu fichier";
    }
}