package afpa.console.command;

import java.io.File;
import java.io.IOException;

import afpa.console.command.interfaces.ICommand;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandCrd extends AbstractCommandeAvecParam implements ICommand , IHistoriqueCommand {


	public static final String CMD = "crd";
	private static final String DESC = "cree un dossier dans le repertoire en cours";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandCrd(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {

		this.parameter = this.parameter.trim();
		File repertoire = new File(CommandeFactory.CURRENT_FILE.getAbsolutePath() + "\\" + this.parameter);
		repertoire.mkdirs();

	}
}
