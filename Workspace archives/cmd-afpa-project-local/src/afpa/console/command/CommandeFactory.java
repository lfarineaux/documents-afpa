package afpa.console.command;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import afpa.console.command.interfaces.ICommand;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.hist.EntreeCmdHist;

public class CommandeFactory {

	private static final List<EntreeCmdHist> COMMANDES_HIST_LIST = new ArrayList<>();

	private static final Map<String, String> COMMANDES_LIST_DESC = new HashMap<>();

	public static File CURRENT_FILE = CommandeGetvars.initialisationRepEnCours();

	static {
		CommandCat.chargerStaticPortion();
		CommandeCd.chargerStaticPortion();
		CommandeCopy.chargerStaticPortion();
		CommandCrd.chargerStaticPortion();
		CommandCrf.chargerStaticPortion();
		CommandeDir.chargerStaticPortion();
		CommandeDirng.chargerStaticPortion();
		CommandeExit.chargerStaticPortion();
		CommandeFin.chargerStaticPortion();
		CommandeFind.chargerStaticPortion();
		CommandeGetvars.chargerStaticPortion();
		CommandeHelp.chargerStaticPortion();
		CommandeHist.chargerStaticPortion();
		CommandeHistclear.chargerStaticPortion();
		CommandeIsprime.chargerStaticPortion();
		CommandePwd.chargerStaticPortion();
		CommandeQuit.chargerStaticPortion();
		CommandeRIver.chargerStaticPortion();
		CommandeNow.chargerStaticPortion();
		CommandeCount.chargerStaticPortion();
		CommandeFline.chargerStaticPortion();
	
	}

	public static void addCommandeDesc(String cmd, String desc) {
		COMMANDES_LIST_DESC.put(cmd, desc);
	}

	public static ICommand create(String cmd) {
		ICommand theCommand = null;

		if (cmd.length() == 0) {
			theCommand = new CommandeVide();

		} else if (CommandePwd.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandePwd();

		} else if (CommandeHist.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeHist(COMMANDES_HIST_LIST);

		} else if (CommandeExit.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeExit();

		} else if (CommandeDir.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeDir();

		} else if (cmd.toLowerCase().startsWith(CommandeCd.CMD + " ")) {
			theCommand = new CommandeCd(cmd);

		} else if (cmd.toLowerCase().startsWith(CommandeCopy.CMD + " ")) {
			theCommand = new CommandeCopy(cmd);
			
		} else if (cmd.toLowerCase().startsWith(CommandeFline.CMD)) {
			theCommand = new CommandeFline(cmd);

		} else if (cmd.toLowerCase().startsWith(CommandeNow.CMD)) {
			theCommand = new CommandeNow(cmd);
			
		} else if (cmd.toLowerCase().startsWith(CommandeCount.CMD)) {
			theCommand = new CommandeCount(cmd);

		} else if (CommandeFin.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeFin();

		} else if (CommandeHistclear.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeHistclear(COMMANDES_HIST_LIST);

		} else if (CommandeDirng.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeDirng();

		} else if (CommandeHelp.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeHelp();

		} else if (CommandeQuit.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeQuit();

		} else if (cmd.toLowerCase().startsWith(CommandCat.CMD + " ")) {
			theCommand = new CommandCat(cmd);
		} else if (cmd.toLowerCase().startsWith(CommandCrf.CMD + " ")) {
			theCommand = new CommandCrf(cmd);
		} else if (cmd.toLowerCase().startsWith(CommandCrd.CMD + " ")) {
			theCommand = new CommandCrd(cmd);
		} else if (cmd.toLowerCase().startsWith(CommandeFind.CMD + " ")) {
			theCommand = new CommandeFind(cmd);
		} else if (cmd.toLowerCase().startsWith(CommandeIsprime.CMD + " ")) {
			theCommand = new CommandeIsprime(cmd);
		} else if (cmd.toLowerCase().startsWith(CommandeRIver.CMD + " ")) {
			theCommand = new CommandeRIver(cmd);
		} else {
			theCommand = new CommandeIntrouvable();
		}

		if (theCommand != null && theCommand instanceof IHistoriqueCommand) {
			COMMANDES_HIST_LIST.add(new EntreeCmdHist(cmd));
			if (COMMANDES_HIST_LIST.size() == 11) {
				COMMANDES_HIST_LIST.remove(0);
			}
		}

		return theCommand;
	}

	public static Map<String, String> getCommandesList() {
		return new HashMap<>(COMMANDES_LIST_DESC);
	}

}
