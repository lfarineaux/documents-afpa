package afpa.console.command;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.tools.Methodes;

class CommandeRIver extends AbstractCommandeAvecParam implements IHistoriqueCommand{

	public static final String CMD = "river";
	private static final String DESC = "Affiche la premi�re intersection des rivi�res obtenues pour ces param�tres";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeRIver(String c) {
		super(c, CMD);
	}

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {

		try {
		
		String[] tab = Methodes.splitstring(this.parameter);
		int r1 = Integer.parseInt(tab[0]);
		int r2 = Integer.parseInt(tab[1]);
		
		
		
		while (r1!=r2) {

			if (r1>r2) {
				int swap = r2;
				r2=r1;
				r1=swap;
			}

			int s1=0;
			String str1 = String.valueOf(r1);
			char [] ch1 = str1.toCharArray();

			for (int i=0;i<str1.length();i++) {
				int a=Character.getNumericValue(ch1[i]);
				s1 = s1+a;
			}
			r1+=s1;
		}
		
		System.out.println(r1);
	}
		catch(NumberFormatException | IndexOutOfBoundsException e) {
			System.err.println("Attendu : un nombre");
		}
		
}
}
