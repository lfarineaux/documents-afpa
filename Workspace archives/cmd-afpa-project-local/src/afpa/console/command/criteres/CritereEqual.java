package afpa.console.command.criteres;

class CritereEqual extends Critere {

	public CritereEqual(String s) {
		super(s);
	}

	@Override
	public boolean test(String s) {
		  return s.equalsIgnoreCase(param);
	}

}
