package afpa.console.command;

import java.io.File;
import java.util.List;

import afpa.console.command.criteres.Critere;
import afpa.console.command.criteres.CritereFactory;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

class CommandeFind extends AbstractCommandeAvecParam implements IHistoriqueCommand {

	public static final String CMD = "find";
	private static final String DESC = "Quitte l'interprétateur de commandes.";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeFind(String c) {
		super(c, CMD);
	}

	private void find(File file, List<Critere> criteres) {
        boolean res = false;
        for (final File fIn : file.listFiles()) {
            res = true;
            if (fIn.isFile()) {
                for (final Critere c : criteres) {
                    res &= c.test(fIn.getName());
                    if(! res) {
                        break;
                    }
                }
                if(res) {
                    System.out.println(fIn.getAbsolutePath());
                }
            } else {
                find(fIn,criteres);
            }
        }
    }

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		final List<Critere> criteres = CritereFactory.create(this.parameter);
		this.find(CommandeFactory.CURRENT_FILE, criteres);
	}

}
