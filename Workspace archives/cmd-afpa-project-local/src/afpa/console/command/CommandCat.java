package afpa.console.command;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import afpa.console.command.interfaces.ICommand;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.tools.Methodes;

public class CommandCat extends AbstractCommandeAvecParam implements ICommand , IHistoriqueCommand  {


	public static final String CMD = "cat";
	private static final String DESC = "lire un fichier";
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	
	public static void chargerStaticPortion() {
	}
	
	public CommandCat(String c) {
		super(c, CMD);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		
		this.parameter = this.parameter.trim();
		String fichierSrc =this.parameter;
		
		try {
			String str = CommandeFactory.CURRENT_FILE+"\\"+ fichierSrc;
			Path src = Paths.get(str);
			List<String> lignes = Files.readAllLines(src);

			for (String s : lignes) {
				System.out.println(s);
			}
		} catch (IOException e) {
			throw new CommandException("Lecture impossible.");
		}		
		
//		String str = "" + CommandeFactory.CURRENT_FILE;
//			File directory = new File(str);
//			File[] content = directory.listFiles();
//			for (File file : content) {
//				if (file.isFile()) {
//					if (file.getName().equals(this.parameter)) {
//						BufferedReader br = null;
//						try {
//							br = new BufferedReader(new FileReader(str + "\\" + this.parameter));
//							String line;
//							while ((line = br.readLine()) != null) {
//								System.out.println(line);
//							}
//						} catch (IOException e) {
//							e.printStackTrace();
//						} finally {
//							try {
//								br.close();
//							} catch (IOException e) {
//								e.printStackTrace();
//							}
//						}
//						break;
//					}
//				}
//			}
		}
	}


