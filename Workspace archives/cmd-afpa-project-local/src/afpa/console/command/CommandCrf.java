package afpa.console.command;

import java.io.File;
import java.io.IOException;

import afpa.console.command.interfaces.ICommand;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandCrf extends AbstractCommandeAvecParam implements ICommand , IHistoriqueCommand {

	
	public static final String CMD = "crf";
	private static final String DESC = "cree un fichier";
	
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	
	public static void chargerStaticPortion() {
	}
	
	public CommandCrf(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		
		this.parameter = this.parameter.trim();
		File file = new File(CommandeFactory.CURRENT_FILE + "\\" + this.parameter);
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
