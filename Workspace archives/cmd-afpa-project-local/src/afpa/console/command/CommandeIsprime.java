package afpa.console.command;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.tools.Methodes;

class CommandeIsprime extends AbstractCommandeAvecParam implements IHistoriqueCommand{

	public static final String CMD = "isprime";
	private static final String DESC = "Retourne yes si le nombre est premier.";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeIsprime(String c) {
		super(c, CMD);
	}

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
	
		
		String[] tab = Methodes.splitstring(this.parameter);
		int n = Integer.parseInt(tab[0]);
		
		boolean isPremier = true; 
		if (n <2 ) {
			isPremier = false;
		} 
		if (n != 0 && n != 1) {
			for (int i = 2; i <= n/2; i++) {
				if (n != i && n % i == 0) {
					isPremier = false; 
					break;
				}
			}
		}
		if (isPremier) {
			System.out.println("yes");
		}
		else {
			System.out.println("no");	
		}
		
	}

}
