package afpa.console.command;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.tools.Methodes;

class CommandeCopy extends AbstractCommandeAvecParam implements IHistoriqueCommand {

	public static final String CMD = "copy";
	private static final String DESC = "Copie un fichier dans un autre.";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeCopy(String c) {
		super(c, CMD);
	}

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		boolean fin = true;
		String[] tab = Methodes.splitstring(this.parameter);
		if(tab.length < 2) {

			fin = false;
			System.out.println("destination inconnue");
		}

		if(fin) {
			String fichierSrc = tab[0];
			String nouveauNom = tab[1];
			Path src = Paths.get(CommandeFactory.CURRENT_FILE+"\\"+ fichierSrc);
			Path dest = Paths.get(CommandeFactory.CURRENT_FILE+"\\"+nouveauNom);
			try {

				Files.copy(src, dest);
			} catch (IOException e) {

				throw new CommandException("Copie impossible.");
			} 

		}
	}
}
