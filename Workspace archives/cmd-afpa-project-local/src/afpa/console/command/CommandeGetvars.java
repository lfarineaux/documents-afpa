package afpa.console.command;

import java.io.File;
import java.util.Map;
import java.util.Set;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.OptionsIncorrectesException;
import afpa.console.tools.Methodes;

class CommandeGetvars extends AbstractCommandeAvecParam implements IHistoriqueCommand {

	public static final String CMD = "getvars";
	private static final String DESC = "Affiche variable d'environnement.";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeGetvars(String c) {
		super(c, CMD);
	}

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws OptionsIncorrectesException  {

		String[] tab = Methodes.splitstring(this.parameter); 
		String param =null;
		if (tab.length>1) {
			param=tab[0];
		} 

		if (param==null) {
			CommandeGetvars.getEnv();
			CommandeGetvars.getProp();
		}
		else if (param.equalsIgnoreCase("-env")) {
			CommandeGetvars.getEnv();
		} else if (param.equalsIgnoreCase("-prop")) {
			CommandeGetvars.getProp();
		} else {
			throw new OptionsIncorrectesException(param);
		}
	}

	public static void getEnv () {

		Map <String,String> env =System.getenv(); 
		Set <String> key = env.keySet();
		for (String k : key) {
			System.out.println(k+" : "+env.get(k));
		}
	}

	public static void getProp () {

		Map <Object,Object> env =System.getProperties(); 
		Set <Object> key = env.keySet();
		for (Object k : key) {
			System.out.println(k+" : "+env.get(k));
		}
	}


	public static File initialisationRepEnCours() {
		String property = System.getProperty("cdi.default.folder");

		if (property!=null) {

			File f = new File(property);
			if(! f.exists()) {
				System.out.println("erreur dossier n'existe pas pour la de la propriete cdi.default.folder = "+property);
			} else if(f.isFile()) {
				System.out.println("erreur le chemin designe un fichier pour la de la propriete cdi.default.folder = "+property);
			} else {
				return new File(property);
			}
		}
		return new File(System.getProperty("user.dir"));
	}

}
