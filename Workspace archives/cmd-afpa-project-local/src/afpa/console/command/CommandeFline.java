package afpa.console.command;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import afpa.console.command.interfaces.ICommand;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.tools.Methodes;

public class CommandeFline extends AbstractCommandeAvecParam implements ICommand , IHistoriqueCommand  {

	public static final String CMD = "fline";
	private static final String DESC = "Traitement sur un fichier ligne par ligne.";
	public static final String N = "-n";
	public static final String D = "-d";
	public static final String F = "-f";
	public static final String S = "-s";
	static int cpt=0;

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeFline(String c) {
		super(c, CMD);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {

		String[] tab = Methodes.splitstring(this.parameter);
		if (this.parameter.length()==0) {
			System.out.println("Erreur de syntaxe");
		}
		else {
			String fichier =tab[0];


			try {
				String str = CommandeFactory.CURRENT_FILE+"\\"+ fichier;
				Path src = Paths.get(str);
				List<String> lignes = Files.readAllLines(src);

				boolean n=false;
				boolean s=false;
				int deb=0;
				int fin=lignes.size();
				String sub=null;

				for (int i = 1; i < tab.length; i++) {
					if (tab[i].equalsIgnoreCase(N)) {
						n=true;
					}
					if (tab[i].equalsIgnoreCase(D) && i != tab.length-1) {
						String val = tab[i + 1];
						if(Methodes.isNumeric(val)) {
							deb  = (Integer.parseInt(val))-1;
						} else {
							throw new CommandException("valeur num�rique attendue");
						}
					}
					else if (tab[i].equalsIgnoreCase(F) && i != tab.length-1) {
						String val = tab[i + 1];
						if(Methodes.isNumeric(val)) {
							fin = (Integer.parseInt(tab[i + 1]));
						} else {
							throw new CommandException("valeur num�rique attendue");
						}
					}
					else if (tab[i].equalsIgnoreCase(S) && i != tab.length-1) {
						
							s=true;
							sub = tab[i + 1];
						} 
					}
				
				if (n) {
					System.out.println(lignes.size()+ " lignes");
				} else if (!s) {
					for (int j=deb;j<fin;j++) {
						System.out.println(lignes.get(j));
					}
				}
				else if (s) {
					for (int j=deb;j<fin;j++) {
						if (lignes.get(j).contains(sub)) {
							System.out.println(lignes.get(j));
						}
					}
				}
			} 
			catch (IOException e) {
				//				e.printStackTrace();
				throw new CommandException("Lecture impossible.");
			}		
		}
	}
}
