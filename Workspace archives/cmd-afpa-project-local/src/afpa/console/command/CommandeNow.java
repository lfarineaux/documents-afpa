package afpa.console.command;

import java.text.SimpleDateFormat;
import java.util.Date;
import afpa.console.command.interfaces.ICommand;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeNow extends AbstractCommandeAvecParam implements ICommand, IHistoriqueCommand{

	public static final String CMD = "now";
	private static final String DESC = "Affiche la date";
	public static final String TEMPS = "-t";
	public static final String DATE = "-d";
	public static final String TEMPSDATE = "-td";
	public static final String DATETEMPS = "-dt";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeNow(String c) {
		super(c, CMD);
	}

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {

		StringBuilder calendrier = new StringBuilder();
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat temps = new SimpleDateFormat("HH:mm:ss");
		String erreur = "unknown option " + this.parameter.trim();
		String heure = temps.format(new Date());
		String jours = date.format(new Date());

		this.parameter = this.parameter.replace(" ", "").substring(0);

		if (this.parameter.equals(TEMPS + DATE) || this.parameter.equals(DATE + TEMPS)) {
			calendrier.append(heure + " " + jours);
		} else {
			if (this.parameter.equals(TEMPS)) {
				calendrier.append(heure + " ");
			} else if (this.parameter.equals(DATE)) {
				calendrier.append(jours);
			} else if (TEMPSDATE.equals(this.parameter) || DATETEMPS.equals(this.parameter)) {
				calendrier.append(heure + " " + jours);
			} else if (this.parameter.isEmpty()) {
				calendrier.append(heure + " " + jours);
			}
		}
		if (calendrier.toString().isEmpty()) {
			
			throw new CommandException(erreur);
		} else {
			System.out.println(calendrier);
		}
	}

}