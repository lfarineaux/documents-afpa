package afpa.console.command;

import java.io.File;

import afpa.console.command.interfaces.ICommand;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

class CommandeCount extends AbstractCommandeAvecParam implements ICommand, IHistoriqueCommand {

	public static final String CMD = "count";
	private static final String DESC = "Calcule le nombre de fichiers et dossiers dans le repertoire en cours.";
	public static final String FILE = "-f";
	public static final String DIRECTORY = "-d";
	public static final String FILEDIRECTORY = "-fd";
	public static final String DIRECTORYFILE = "-df";
	String erreur = "unknown option " + this.parameter.trim();

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeCount(String c) {
		super(c, CMD);
	}

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		this.parameter = this.parameter.replace(" ", "").substring(0);

		final File[] filesList = CommandeFactory.CURRENT_FILE.listFiles();
		int cmptF = 0;
		int cmptD = 0;

		for (final File childFile : filesList) {
			if (childFile.isDirectory()) {
				cmptD++;
			} else {
				cmptF++;
			}
		}

		if (this.parameter.equals(FILE+DIRECTORY) || this.parameter.equals(DIRECTORY+FILE)) {
			System.out.println(cmptF + " fichiers");
			System.out.println(cmptD + " dossiers");
		}else {
			if (this.parameter.equals(FILE)) {
				System.out.println(cmptF + " fichiers");
			}else if (this.parameter.equals(DIRECTORY)){
				System.out.println(cmptD + " dossiers");
			} else if (FILEDIRECTORY.equals(this.parameter)|| DIRECTORYFILE.equals(this.parameter)) {
				System.out.println(cmptF + " fichiers");
				System.out.println(cmptD + " dossiers");
			} else if (this.parameter.isEmpty()) {
				System.out.println(cmptF + " fichiers");
				System.out.println(cmptD + " dossiers");
			}	

			else {
				
				throw new CommandException(erreur);

			} 

		}
	}
}