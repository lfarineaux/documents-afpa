package afpa.console.exception;

public class OptionsIncorrectesException extends CommandException {

	private static final long serialVersionUID = 1L;

	public OptionsIncorrectesException(String msg) {
		super(msg);
	}

}
