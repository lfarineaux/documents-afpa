package afpa.console.exception;

public class ParametresIncorrectesException extends CommandException {

	private static final long serialVersionUID = 1L;

	public ParametresIncorrectesException(String msg) {
		super(msg);
	}
}
