package com.afpa.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class Personne{
	private int num;
	@NonNull
	private String nom;
	@NonNull
	private String prenom;	
}
