package org.eclipse.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */
//@WebServlet({ "/TestServlet", "/mapage" })
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	response.getWriter().append("Served at: ").append(request.getContextPath());
	//	response.getWriter().print("Hello World");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
	//	out.println("* Hello World");
	
//		out.println("<!DOCTYPE html>");
//		out.println("<html>");
//		out.println("<head>");
//		out.println("<meta charset=\"utf-8\" />");
//		out.println("<title>Projet JEE</title>");
//		out.println("</head>");
//		out.println("<body>");
//		out.println("* Hello World *");
//		out.println("</body>");
//		out.println("</html>");
		
//		String nom = request.getParameter("nom");
//		String prenom = request.getParameter("prenom");
//		PrintWriter out = response.getWriter();
//		out.print("Hello " + nom + " " + prenom);
		
		int nbre = Integer.parseInt(request.getParameter("param1"));
		String signe = request.getParameter("signe");
		int nbre2 = Integer.parseInt(request.getParameter("param2"));
		if(signe.equals("+")) {
			out.println("Resultat "+nbre+" "+signe+" "+nbre2+" = "+(nbre+nbre2));
		}else if(signe.equals("-")) {
			out.println("Resultat "+nbre+" "+signe+" "+nbre2+" = "+(nbre-nbre2));
		}else if(signe.equals("*")) {
			out.println("Resultat "+nbre+" "+signe+" "+nbre2+" = "+(nbre*nbre2));
		}else if(signe.equals("/")) {
			if(nbre2 != 0){
				out.println("Resultat "+nbre+" "+signe+" "+nbre2+" = "+(nbre/nbre2));
			}else {
				out.println("Attention division par 0");
			}
			
		}else {
			out.println("Erreur");
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	doGet(request, response);
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		
		int nbre = Integer.parseInt(request.getParameter("param1"));
		String signe = request.getParameter("signe");
		int nbre2 = Integer.parseInt(request.getParameter("param2"));
		if(signe.equals("+")) {
			out.println("Resultat "+nbre+" "+signe+" "+nbre2+" = "+(nbre+nbre2));
		}else if(signe.equals("-")) {
			out.println("Resultat "+nbre+" "+signe+" "+nbre2+" = "+(nbre-nbre2));
		}else if(signe.equals("*")) {
			out.println("Resultat "+nbre+" "+signe+" "+nbre2+" = "+(nbre*nbre2));
		}else if(signe.equals("/")) {
			if(nbre2 != 0){
				out.println("Resultat "+nbre+" "+signe+" "+nbre2+" = "+(nbre/nbre2));
			}else {
				out.println("Attention division par 0");
			}
			
		}else {
			out.println("Erreur");
		}
		
	}

}
