package cda.http.server;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.file.Files;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
public class ServeurAuxiliaireBad implements Runnable {
	private final Socket client;
	public ServeurAuxiliaireBad(Socket c) {
		this.client = c;
	}
	@Override
	public void run() {
		try {
			InputStream clientIn = client.getInputStream();
			OutputStream clientOut = client.getOutputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));
			String uneLigne = br.readLine();
			System.out.println(uneLigne);
			String[] requeteTab = uneLigne.split(" ");
			File f = new File("C:\\outils\\cda-www\\"+requeteTab[1].substring(1));
			if ("get".equalsIgnoreCase(requeteTab[0])) {
				if( !(new File("C:\\outils\\cda-www\\"+requeteTab[1].substring(1))).exists() || !f.getCanonicalPath().contains("outils") ) {
					clientOut.write(("HTTP/1.1 " + CdaStatus.ERROR.getCode() + " " + CdaStatus.ERROR.getDescription() + "\n").getBytes());
					clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
					clientOut.write("\n".getBytes());
					clientOut.write((CdaStatus.ERROR.getCode()+ " "+CdaStatus.ERROR.getDescription()).getBytes() );
				}else if ((new File("C:\\outils\\cda-www\\"+requeteTab[1].substring(1))).exists() && f.getCanonicalPath().contains("outils")) {
				
					if(requeteTab[1].substring(1).contains(".jpeg")) {
						clientOut.write(("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n").getBytes());
						clientOut.write("content-type:image/jpeg; charset=UTF-8\n".getBytes());
						clientOut.write("\n".getBytes());
						System.out.println(requeteTab[1].substring(1));
						clientOut.write(Files.readAllBytes(f.toPath()));
					
					}else if (requeteTab[1].substring(1).contains(".html")) {
						clientOut.write(("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n").getBytes());
						clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
						clientOut.write("\n".getBytes());
						System.out.println(requeteTab[1].substring(1));
						try {
							File fichier = new File("C:\\outils\\cda-www\\"+requeteTab[1].substring(1));
							BufferedReader monBuffer = new BufferedReader(new InputStreamReader(new FileInputStream(fichier)));
							String st;
							while ((st = monBuffer.readLine()) != null) {
								clientOut.write(st.getBytes());
							}
						}catch(Exception e) {
						}
					}
				}else {
					clientOut.write(("HTTP/1.1 " + CdaStatus.ERRORS.getCode() + " " + CdaStatus.ERRORS.getDescription() + "\n").getBytes());
					clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
					clientOut.write("\n".getBytes());
					clientOut.write((CdaStatus.ERRORS.getCode()+ " "+CdaStatus.ERRORS.getDescription()).getBytes() );
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				this.client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

