package cda.http.server;

public enum CdaStatus {
	
	OK(200,"CDA OK"), BAD (400, "Bad request"), NOTFOUND (404,"Not found") ,ERROR (100,"error"), ERRORS (101,"errors");
	
	private final int code;

	private final String description;
	
	private CdaStatus(int c, String d) {
		this.code = c;
		this.description = d;
	}
	
	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
}
