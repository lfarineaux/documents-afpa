package cda.http.server;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;

public class ServeurAuxiliaire implements Runnable {
	private final Socket client;

	public ServeurAuxiliaire(Socket c) {
		this.client = c;
	}

	@Override
	public void run() {
		try {
			InputStream clientIn = client.getInputStream();
			OutputStream clientOut = client.getOutputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));

			String uneLigne = br.readLine();
			if(uneLigne == null) {
				this.client.close();
				return;
			}
			System.out.println("Lecture : "+uneLigne);
			String[] requeteTab = uneLigne.split(" ");

			String fichier =null;
			fichier= requeteTab[1];
			System.out.println("Nom fichier : " +fichier);
//			System.out.println("Substring "+requeteTab[1].substring(1));

			//			String[] requeteTab2 = fichier.split(".");
			//			String extension =null;
			//						extension= requeteTab2[0];
			//			System.out.println("Extension : " +extension);

			String nom=null;
			String rep="c:/outils/cda-www";
			nom=rep+fichier;

			File fich = new File (nom)	;			

			
			 if (fichier.endsWith("jpg") || fichier.endsWith("jpeg")) {
				try {
					System.out.println("Code 200-image");
					clientOut.write(
							("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n").getBytes());
					clientOut.write("content-type:image/jpeg; charset=UTF-8\n".getBytes());
					clientOut.write("\n".getBytes());
					clientOut.write(Files.readAllBytes(fich.toPath()));

				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}	


			}
			else if (fichier.equals("/") || !fich.exists() || !fich.isFile() || fichier.endsWith("c:/")) {
				try {
					System.out.println("Code 400");
					clientOut.write(("HTTP/1.1 " + CdaStatus.BAD.getCode() + " " + CdaStatus.BAD.getDescription() 
					+ "\n").getBytes());
					clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
					clientOut.write("\n".getBytes());
					clientOut.write(("Ereur : " + CdaStatus.BAD.getCode()+ " "+CdaStatus.BAD.getDescription()).getBytes());

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				//			} else if (!fich.getCanonicalPath().contains("cda-www")) {
				//				System.out.println("Directory traversal attack");
				//				System.out.println("Code 404");
				//				clientOut.write(("HTTP/1.1 " + CdaStatus.NOTFOUND.getCode() + " " + CdaStatus.NOTFOUND.getDescription() 
				//				+ "\n").getBytes());
				//				clientOut.write("\n".getBytes());
				//				clientOut.write(("Ereur : " + CdaStatus.NOTFOUND.getCode()+ " "+CdaStatus.NOTFOUND.getDescription()).getBytes());
			}
			else if (fich.isFile() && fich.getCanonicalPath().contains("/outils/cda-www")) {
					System.out.println("Code 200");
					clientOut.write(
							("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n").getBytes());
					clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
					clientOut.write("\n".getBytes());
					String line;
					BufferedReader brf = new BufferedReader(new InputStreamReader(new FileInputStream(fich)));
					while ((line = brf.readLine()) != null) {
						System.out.println(line);
						clientOut.write(line.getBytes());
					}

				} 
//				try {
//					System.out.println("Code 404");
//					clientOut.write(("HTTP/1.1 " + CdaStatus.NOTFOUND.getCode() + " " + CdaStatus.NOTFOUND.getDescription() 
//					+ "\n").getBytes());
//					clientOut.write(("Ereur : " + CdaStatus.NOTFOUND.getCode()+ " "+CdaStatus.NOTFOUND.getDescription()).getBytes());
//
//
//				} catch (FileNotFoundException e) {
//					e.printStackTrace();
//				}

//			}

			//					clientOut.write(
			//							("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n").getBytes());
			//					clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
			//					clientOut.write("\n".getBytes());
			//					clientOut.write("<html><body style='background-color:red'><b>coucou</b></body></html>".getBytes());
			//
			//					}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				this.client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
