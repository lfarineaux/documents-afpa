package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.afpa.main.BinString;
import com.afpa.main.ChaineVideException;
import com.afpa.main.ImplBinString;
import com.afpa.main.ParametreNegatifException;

public class ImplBinStringTest {

	BinString limplementationATester;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		limplementationATester = new ImplBinString();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test_convert() {
		try {
			assertEquals("11000011", limplementationATester.convert("ab"));
		} catch (ChaineVideException e) {
			fail("Faux");
		}
	}
	
	
	@Test
	void test_convert_param_chaine_vide() {
		String str="";
		try {
			limplementationATester.convert(str);
			fail("Faux pour une cha�ne vide");
		}
		catch (Exception e) {
			assertTrue(e instanceof ChaineVideException);
		}
	}

	@Test
	void test_convert_param_chaine_null() {
		String str=null;
		try {
			limplementationATester.convert(str);
			
			fail("Faux pour une cha�ne nulle");
		}
		catch (Exception e) {
			assertTrue(e instanceof ChaineVideException);
		}
	}
 
	@Test
	void test_sum() {
		try {
			assertEquals(195, limplementationATester.sum("ab"));
		} catch (Exception e) {
			fail("normalement pas d exception pour le cas simple ab "+e.getMessage());
		}
	}

	@Test
	void test_sum_param_chaine_vide() {
		String str="";
		try {
			limplementationATester.sum(str);
			fail("Faux pour une cha�ne vide");
		}
		catch (Exception e) {
			assertTrue(e instanceof ChaineVideException);
		}
	}

	@Test
	void test_sum_param_chaine_null() {
		String str=null;
		try {
			limplementationATester.sum(str);
			fail("Faux pour une cha�ne nulle");
		}
		catch (Exception e) {
			assertTrue(e instanceof ChaineVideException);
		}
	}

	@Test
	void testBinarise()  {
		try {
			assertEquals("11000011", limplementationATester.binarise(195));
		} catch (Exception e) {
			fail("Faux");
		}
	}

	@Test
	void test_Binarise_param_negatif() {
		int a=-1;
		try {
			limplementationATester.binarise(a);
			fail("Faux pour un param�tre n�gatif");
		}
		catch (Exception e) {
			assertTrue(e instanceof ParametreNegatifException);
		}
	}
}
