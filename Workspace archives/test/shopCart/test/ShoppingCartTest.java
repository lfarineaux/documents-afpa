package test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import main.Product;
import main.ProductNotFoundException;
import main.ShoppingCart;

@TestMethodOrder(OrderAnnotation.class)
class ShoppingCartTest {
	static ShoppingCart shop;
	static Product prod;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		shop = new ShoppingCart();
		prod = new Product ("pomme",2.2);		
	}

	@Order(1)
	@Test
	void testGetItemCount() {
		assertTrue(shop.getItemCount()==0);
		shop.addItem(prod);
		assertTrue(shop.getItemCount()==1);
	}

	@Order(2)
	@Test
	void testGetBalance() {
		assertTrue(shop.getBalance()==2.2);
	}

	@Order(3)
	@Test
	void testGetBalance2() {
		shop.addItem(new Product("banane", 1.1));
		assertTrue(Double.compare(shop.getBalance(),3.3)==0);
	}

	@Order(4)
	@Test
	void testRemoveItem() throws Exception {
		assertTrue(shop.getItemCount()==2);
		shop.removeItem(prod);
		assertTrue(shop.getItemCount()==1);
	}
	
	@Order(5)
	@Test
	void testRemoveItem2() throws Exception {
		assertTrue(shop.getItemCount() == 1);
		Throwable e = new Throwable();
		try {
			shop.removeItem(prod);
			fail("Erreur code");
		} catch (Exception ex) {
			e = ex;
		}
		assertTrue(e instanceof ProductNotFoundException);
	}

	@Order(6)
	@Test
	void testEmpty() {
		shop.addItem(new Product("banane", 1.0));
		assertTrue(shop.getItemCount()==2);
		shop.empty();
		assertTrue(shop.getItemCount()==0);
	}

}
