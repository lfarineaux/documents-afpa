package tab_personne;

import java.util.Arrays;

public class Programme {

	public static void main(String[] args) {

		Enseignant isame = new Enseignant (1,"Elmesbahi","Isame",3600);
		Enseignant said = new Enseignant (2,"Said","Mohamed",3500);
		Etudiant badrane = new Etudiant (11, "Houmadi","Badrane",10);
		Etudiant ketsia = new Etudiant (12, "Bertille","Ketsia",8);

		Personne [] pers = new Personne [4];

		pers [0]=isame;
		pers [1]=said;
		pers [2]=badrane;
		pers [3]=ketsia;

		for (Personne p : pers) {
			System.out.print(p.getNom()+" ");
			if (p instanceof Etudiant) {
				System.out.println("Niveau "+((Etudiant) p).getNiveau());
			}
			else if (p instanceof Enseignant) {
				System.out.println("Salaire "+((Enseignant) p).getSalaire());
			}
		}
	}
}
