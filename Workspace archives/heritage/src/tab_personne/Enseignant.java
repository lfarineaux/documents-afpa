package tab_personne;

public class Enseignant extends Personne {

	
	protected long salaire;
	
	public Enseignant (int num, String nom,String prenom, long s) {
		super(num,nom,prenom);
		this.salaire = s;
		
	}

	@Override
	public String toString() {
		return super.toString() +" Enseignant [salaire=" + salaire + "]";
	}

	public long getSalaire() {
		return this.salaire;
	}
	
	
}
