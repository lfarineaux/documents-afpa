package tab_personne;

public class Etudiant extends Personne {

	
	protected int niveau;
	
	public Etudiant (int num, String nom, String prenom, int n) {
		super(num,nom,prenom);
		this.niveau = n;
		
	}

	@Override
	public String toString() {
		return super.toString()+" Etudiant [niveau=" + niveau + "]";
	}

	public int getNiveau() {
		return this.niveau;
	}
	
	
}

