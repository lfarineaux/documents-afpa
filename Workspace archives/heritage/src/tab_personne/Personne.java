package tab_personne;

public class Personne {

	protected int num;
	protected String nom;
	protected String prenom;
	
	public  Personne (int num, String n, String p) {
		this.num=num;
		this.nom=n;
		this.prenom=p;
	}

	@Override
	public String toString() {
		return "Personne [num=" + num + ", nom=" + nom + ", prenom=" + prenom + "]";
	}

	public int getNum() {
		return this.num;
	}

	public String getNom() {
		return this.nom;
	}

	public String getPrenom() {
		return this.prenom;
	}


	
	
	
}
