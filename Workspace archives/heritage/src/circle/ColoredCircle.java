package circle;

public class ColoredCircle extends Circle {

	protected String color ;	
	
	public ColoredCircle () {
		super(0,0,0) ;
		color = null;
		
	}
	
	public ColoredCircle (double x, double y, double r,String c) {
		super(x,y,r) ;
		color = c;
	}

	@Override
	public String toString() {
		return super.toString() + "ColoredCircle [color=" + color + "]";
	}
	
	
}
