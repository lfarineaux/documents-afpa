package circle;

public class Programme {

	public static void main(String[] args) {
		Circle c1,c2,c3 ;
		c1 = new Circle(1,1,3) ;
		c2 = new Circle() ;

		System.out.println(c1.toString() + "\n" + c2.toString()) ;

		// m�thode d'instance
		boolean test1 = c1.isBigger(c2);
		System.out.println("Test 1 = "+test1);

		// m�thode de classe
		boolean test2 = Circle.isBigger(c2, c1);
		System.out.println("Test 2 = "+test2);

		Cylinder cy1 = new Cylinder (2,2,5,4);
		
		System.out.println("Cyl "+cy1);
		
		// m�thode d'instance
		double vol=cy1.volume();
		System.out.println("Volume = "+vol);
		
		// m�thode de classe
		double vol2=Cylinder.volume(cy1);
		
		ColoredCircle co1=new ColoredCircle(3,3,2,"red");
		System.out.println("ColoredCircle = "+co1);		
		
	}

}