package circle;

public class Circle extends Shape {
	
	final static double PI = 3.141592564 ;
	protected double radius ;
	protected double surface;
	
	public Circle() {
		super(0,0) ;
		radius = 0 ;
		surface = 0;
	}
	
	public Circle(double x, double y, double r) {
		super(x,y) ;
		radius = r ;
		surface = PI*r*r;
	}
	public String toString() {
		return super.toString() + " [ (Cercle ) Rayon : " + radius +" Surface " + surface + " ]";
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return this.radius;
	}

	public double getSurface() {
		return this.surface;
	}
	
//	m�thode d'instance	
public  boolean isBigger(Circle c) {
		
		boolean surf=this.surface >c.getSurface();
		
		return surf;
	}
//m�thode de classe	
public static boolean isBigger(Circle a,Circle b) {
	
	return a.isBigger(b);
	
//	boolean surf=a.surface>b.surface;
//			
//	return surf;
}

}



