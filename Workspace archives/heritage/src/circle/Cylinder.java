package circle;

public class Cylinder extends Circle {

	protected double haut ;


	public Cylinder () {
		super(0,0,0) ;
		haut = 0;
	}

	public Cylinder (double x, double y, double r,double h) {
		super(x,y,r) ;
		haut = h;
	}

	public double getHaut() {
		return this.haut;
	}

	@Override
	public String toString() {
		return super.toString() + " (Cylinder) [haut=" + haut +"]";
	}

	//	m�thode d'instance
	public  double volume() {
		double v=this.surface*this.haut;	

		return v;
	}	

	// m�thode de classe	
	public static double volume(Cylinder c) {
//		double v=c.getSurface()*c.haut;	

		return c.volume();
	}	

}
