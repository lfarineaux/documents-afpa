package entreprise;

import java.util.ArrayList;
import java.util.Scanner;

public class Employes {

	/** @param
	 *  nom : nom de l'employ�
	 *  matricule : matricule de l'employ�
	 *  indiceSalarial : Indice salarial de l'employ�
	 *  baseSalariale : base salariale de l'employ�  
	 *  salaire : salaire de l'employ�
	 */ 
	
	protected String nom; 
	protected int matricule;
	protected int indiceSalarial; 
	protected static int baseSalariale = 1000;
	protected int salaire;
	protected static ArrayList<Employes> listEmp = new ArrayList<>();

	public Employes() {
	}

	public Employes(String nom, int matricule, int indiceSalarial) {
		this.nom = nom;
		this.matricule = matricule;
		this.indiceSalarial =indiceSalarial;
		this.salaire = baseSalariale * indiceSalarial;

	}


	public static void setBaseSalariale(int b) {
		baseSalariale = b;
	}

	public int getSalaire() {
		return this.salaire;
	}

	public static int getBaseSalariale() {
		return baseSalariale;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}


	public  ArrayList<Employes> getListEmp() {
		return listEmp;
	}

	public static void afficheLesEmployes () {
		for (Employes e : listEmp) {
			System.out.println(e);
		}
	}

	public static void ajouterunEmploye () {

		Scanner sc = new Scanner(System.in);


		String str;
		int mat=0;
		int indice=0;
		System.out.println("Saisir le nom :");
		str=sc.next();
		sc.nextLine();
		System.out.println("Saisir le matricule : ");
		mat=sc.nextInt();
		sc.nextLine();
		System.out.println("Saisir l'indice salarial : ");
		indice=sc.nextInt();
		sc.nextLine();

		Employes e1 = new Employes(str,mat,indice) ;
		listEmp.add(e1);
		System.out.println("Employ� ajout� : "+str);
	}

	public static void afficheSalaireEmploye () {
		Scanner sc = new Scanner(System.in);
		String str;
		System.out.println("Saisir le nom :");
		str=sc.next();
		sc.nextLine();
		for (Employes e : listEmp) {
			if (e.getNom().equals(str)) {
				System.out.println("Salaire de l'employ� "+str+" = "+e.getSalaire());
			}
			else {
				System.out.println("L'employ� n'existe pas");
			}
		}
	}

	public static void afficheSalairesEmployes () {
		int salaires = 0;
		for (Employes e : listEmp) {
			salaires+=e.getSalaire();
		}
		System.out.println("Salaires des employ�s = "+salaires);
	}


	public static void modifIndiceSalarial () {
		Scanner sc = new Scanner(System.in);

		String str;
		int indice=0;
		System.out.println("Saisir le nom :");
		str=sc.next();
		sc.nextLine();
		System.out.println("Saisir le nouvel indice salarial :");
		indice=sc.nextInt();
		sc.nextLine();
		for (Employes e : listEmp) {
			if (e.getNom().equals(str)) {
				e.setIndiceSalarial(indice);
				e.recalculerSalaire();
				break;
			}
			else {
				System.out.println("L'employ� n'existe pas");
			}
		}
	}

	public static void modifBaseSalariale () {
		Scanner sc = new Scanner(System.in);

		int base;
		System.out.println("Saisir la nouvelle base salariale : ");
		base=sc.nextInt();
		sc.nextLine();
		setBaseSalariale(base);
		for (Employes e : listEmp) {
			e.recalculerSalaire();
		}
	}

	private void recalculerSalaire() {
		this.salaire=baseSalariale * this.indiceSalarial;		
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getMatricule() {
		return matricule;
	}
	public void setMatricule(int matricule) {
		this.matricule = matricule;
	}
	public int getIndiceSalarial() {
		return indiceSalarial;
	}
	public void setIndiceSalarial(int indice) {
		this.indiceSalarial = indice;
	}
	public String toString() {
		return "Nom de l'employ� : " +this.nom +", Matricule : " +this.matricule +", Indice salarial : " +this.indiceSalarial+" Salaire "+this.salaire;
	}


}


