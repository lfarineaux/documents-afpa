package entreprise;

import java.util.Scanner;

public class Commerciaux extends Employes {
	
	/** @param
	 *  nbventes : nombre de ventes du commercial
	 * interes : intéressement du commercial
	 */ 

	protected int nbventes;
	protected int interes;

	public Commerciaux () {
		super(null,0,0) ;
		nbventes = 0;
		interes = 0;
	}

	public Commerciaux (String n, int mat,int indice, int nbv, int inter) {
		super (n,mat,indice);
		this.nbventes = nbv;
		this.interes = inter;
		this.salaire=(baseSalariale * this.indiceSalarial)+(this.nbventes * this.interes);	
	}

	public static void afficheLesCommerciaux () {
		for (Employes c : listEmp) {
			if(c instanceof Commerciaux) {
				System.out.println(c);
			}
		}
	}

	public static void addCommerciaux () {
		Scanner sc = new Scanner(System.in);
		String str;
		int mat;
		int ind;
		int nbv;
		int inter;
		System.out.println("Entrez le nom du commercial : ");
		str = sc.nextLine();
		System.out.println("Entrez le matricule du commercial : ");
		mat = sc.nextInt();
		sc.nextLine();
		System.out.println("Entrez l'indice salarial du commercial : ");
		ind = sc.nextInt();
		sc.nextLine();
		System.out.println("Entrez le nombre de ventes du commercial : ");
		nbv = sc.nextInt();
		sc.nextLine();
		System.out.println("Entrez la valeur d'intéressement du commercial : ");
		inter = sc.nextInt();
		sc.nextLine();
		Commerciaux c1 = new Commerciaux (str,mat,ind,nbv,inter);
		System.out.println("Commercial "+str+" ajouté");
		listEmp.add(c1);
	}

	public static void modifNbventeCom () {
		Scanner sc = new Scanner(System.in);
		String str;
		int nbv=0;
		System.out.println("Saisir le nom :");
		str=sc.next();
		sc.nextLine();
		System.out.println("Saisir le nouveau nombre de ventes :");
		nbv=sc.nextInt();
		sc.nextLine();
		for (Employes em : listEmp) {
			if(em instanceof Commerciaux) {
				Commerciaux co = (Commerciaux) em;
				if (co.getNom().equals(str)) {
					co.setNbventes(nbv);
					break;
				}
				else {
					System.out.println("Le commercial "+str+" n'existe pas");
				}
			}
		}
	}
	
	public static void modifSalaireCom () {
		Scanner sc = new Scanner(System.in);
		String str;
		int sal=0;
		System.out.println("Saisir le nom :");
		str=sc.next();
		sc.nextLine();
		System.out.println("Saisir le nouveau salaire");
		sal=sc.nextInt();
		sc.nextLine();
		for (Employes em : listEmp) {
			if(em instanceof Commerciaux) {
				Commerciaux co = (Commerciaux) em;
				if (co.getNom().equals(str)) {
					co.setSalaire(sal);
					break;
				}
				else {
					System.out.println("Le commercial "+str+" n'existe pas");
				}
			}
		}
	}

	public static void modifInteresCom () {
		Scanner sc = new Scanner(System.in);

		String str;
		int inter=0;
		System.out.println("Saisir le nom :");
		str=sc.next();
		sc.nextLine();
		System.out.println("Saisir le nouvel intéressement");
		inter=sc.nextInt();
		sc.nextLine();

		for (Employes em : listEmp) {
			if(em instanceof Commerciaux) {
				Commerciaux co = (Commerciaux) em;
				if (co.getNom().equals(str)) {
					co.setInteres(inter);
					break;
				}
				else {
					System.out.println("Le commercial "+str+" n'existe pas");
				}
			}
		}	
	}


	@Override
	public String toString() {
		return " Commercial - "+super.toString() +"Nombre de ventes = " + nbventes + ", Intéressement = " + interes;
	}

	public void setNbventes(int nbventes) {
		this.nbventes = nbventes;
	}

	public void setInteres(int interes) {
		this.interes = interes;
	}

}
