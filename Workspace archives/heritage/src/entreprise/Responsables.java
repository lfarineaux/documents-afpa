package entreprise;

import java.util.ArrayList;
import java.util.Scanner;

public class Responsables extends Employes {
	/** @param
	 *  listSub : liste des subordonnés
	 */ 
	protected ArrayList <Employes> listSub;


	public Responsables() {
		super(null,0,0) ;
	}

	public Responsables(String nom, int mat, int ind) {
		super(nom, mat, ind) ;
		listSub = new ArrayList<>();
	}

	@Override
	public String toString() {
		return " Responsable - "+super.toString();
	}

	public static void ajouterUnReponsable() {
		Scanner sc = new Scanner(System.in);

		String str;
		int mat;
		int ind;
		System.out.println("Entrez le nom du responsable : ");
		str = sc.nextLine();
		System.out.println("Entrez le matricule du responsable : ");
		mat = sc.nextInt();
		sc.nextLine();
		System.out.println("Entrez l'indice salarial du responsable : ");
		ind = sc.nextInt();
		sc.nextLine();
		Responsables r1 = new Responsables (str,mat,ind);
		System.out.println("Responsable ajouté : "+str);
		listEmp.add(r1);
	}

	public static void ajouterunEmployeAunResp () {
		Scanner sc = new Scanner(System.in);
		String resp;
		String sub;
		System.out.println("Saisir le nom du responsable :");
		resp=sc.next();
		sc.nextLine();

		for (Employes em : listEmp) {
			if(em instanceof Responsables) {
				Responsables re = (Responsables) em;
				if (re.getNom().equals(resp)) {
					System.out.println("Saisir le nom du subordonné :");
					sub=sc.next();
					for (Employes emsub : listEmp) {
						if (emsub.getNom().equals(sub)) {
							re.listSub.add(emsub);
							System.out.println("Subordonné ajouté : "+sub);
							break;	
						}
					}
				}
				else {
					System.out.println("Le responsable "+resp+" n'existe pas");
				}
			}
		}
	}

	public static void supprimerUnEmployeAUnResponsable() {
		Scanner sc = new Scanner(System.in);
		String resp;
		String sub;
		System.out.println("Saisir le nom du responsable :");
		resp=sc.next();
		sc.nextLine();

		for (Employes em : listEmp) {
			if(em instanceof Responsables) {
				Responsables re = (Responsables) em;
				if (re.getNom().equals(resp)) {
					System.out.println("Saisir le nom du subordonné :");
					sub=sc.next();
					for (Employes emsub : listEmp) {
						if (emsub.getNom().equals(sub)) {
							re.listSub.remove(emsub);
							System.out.println("Subordonné retiré : "+sub);
							break;
						}
					}
				}
				else {
					System.out.println("Le responsable "+resp+" n'existe pas");
				}
			}
		}
	}

	public static void afficherEquipeResp () {
		Scanner sc = new Scanner(System.in);

		String resp;
		System.out.println("Saisir le nom du responsable :");
		resp=sc.next();
		sc.nextLine();

		for (Employes em : listEmp) {
			if(em instanceof Responsables) {
				Responsables re = (Responsables) em;

				if (re.getNom().equals(resp)) {
					System.out.print("Liste subordonnés :");
					System.out.println(re.listSub);
					break;
				}
				else {
					System.out.println("Le responsable "+resp+" n'existe pas");
				}
			}
		}
	}
	public static void affichageResponsables() {

		for (Employes em : listEmp) {
			if(em instanceof Responsables) {
				System.out.println(em);
			}
		}
	}
}