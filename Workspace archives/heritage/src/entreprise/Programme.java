package entreprise;

import java.util.Scanner;

public class Programme {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int menu = 0;
		boolean fin = true;

		/** @param
		 *  menu : num�ro saisi dans le menu 
		 *  fin : arr�t du programme
		 */ 

		while (fin) {
			System.out.println();
			System.out.println("      **************");
			System.out.println("      *    MENU    *");
			System.out.println("      **************");
			System.out.println();
			System.out.println(" 0. Arr�ter le programme");
			System.out.println(" 1. cr�er un employ� simple");
			System.out.println(" 2. cr�er un responsable");
			System.out.println(" 21. afficher la liste des responsables");
			System.out.println(" 3. cr�er un commercial");
			System.out.println(" 4. afficher le salaire d'un employ�");
			System.out.println(" 5. afficher le total des salaires de tous les employ�s");
			System.out.println(" 6. ajouter un employ� � l'�quipe d'un responsable");
			System.out.println(" 7. supprimer un employ� d'une �quipe d'un responsable");
			System.out.println(" 8. afficher l'�quipe d'un responsable");
			System.out.println(" 9. modifier l'indice salarial d'un employ�s");
			System.out.println(" 10. modifier la valeur fixe des salaires des employ�s");		
			System.out.println(" 11. modifier le nombre de vente d'un commercial");		
			System.out.println(" 12. modifier la valeur du salaire fixe d'un commercial");
			System.out.println(" 13. modifier la valeur d�int�ressement des commerciaux");
			System.out.println();
			System.out.print("   ->  ");
			menu = sc.nextInt();
			sc.nextLine();
			System.out.println();


			switch (menu) {
			case 0:	
				System.out.println("Arr�t du programme");
				fin=false;
				break;	
			case 1:
				System.out.println(" 1. cr�er un employ� simple");
				Employes.ajouterunEmploye();
				Employes.afficheLesEmployes();
				break;

			case 2:	
				System.out.println(" 2. cr�er un responsable");
				Responsables.ajouterUnReponsable();
				break;	

			case 21:
				System.out.println(" 21. afficher la liste des responsables");
				Responsables.affichageResponsables();
				break;

			case 3:	
				System.out.println(" 3. cr�er un commercial");
				Commerciaux.addCommerciaux();
				Commerciaux.afficheLesCommerciaux();
				break;		

			case 4:	
				System.out.println(" 4. afficher le salaire d'un employ�");
				Employes.afficheSalaireEmploye();
				break;		

			case 5:	
				System.out.println(" 5. afficher le total des salaires de tous les employ�s");
				Employes.afficheSalairesEmployes();
				break;	

			case 6:
				System.out.println(" 6. ajouter un employ� � l'�quipe d'un responsable");
				Responsables.ajouterunEmployeAunResp();
				break;	

			case 7:
				System.out.println(" 7. supprimer un employ� d'une �quipe d'un responsable");
				Responsables.supprimerUnEmployeAUnResponsable();
				break;
				
			case 8:
				System.out.println(" 8. afficher l'�quipe d'un responsable");
				Responsables.afficherEquipeResp();
				break;	

			case 9:
				System.out.println(" 9. modifier l'indice salarial d'un employ�s");
				Employes.modifIndiceSalarial();
				Employes.afficheLesEmployes();
				break;	

			case 10:
				System.out.println(" 10. modifier la valeur fixe des salaires des employ�s");		
				Employes.modifBaseSalariale();
				Employes.afficheLesEmployes();	
				break;		

			case 11:	
				System.out.println(" 11. modifier le nombre de vente d'un commercial");		
				Commerciaux.modifNbventeCom();
				Commerciaux.afficheLesCommerciaux();
				break;		

			case 12:	
				System.out.println(" 12. modifier la valeur du salaire fixe d'un commercial");
				Commerciaux.modifSalaireCom();
				Commerciaux.afficheLesCommerciaux();
				break;		

			case 13:	
				System.out.println(" 13. modifier la valeur d�int�ressement des commerciaux");
				Commerciaux.modifInteresCom();
				Commerciaux.afficheLesCommerciaux();
				break;	
		
			default:
				System.out.println("Num�ro incorrect");
				break;
			}
		}

		sc.close();
	}
}
