package com.afpa.javadoc;

/**
 * @author Thinkpad T440
 *
 */
public class NegativeAgeException extends Exception {

	public NegativeAgeException(String string) {
		super(string);
	}

}
